% simple script for storing the values of 
% the parallel connection length with the same 
% time basis of LP probes for each of the shots 
% analyzed
% 52474
% 52476
% 52478
% 52479
% 52481
% 52482
% 52484
% 52489
% 52492
% 52493
% 52517
% 52518
% 52519
% aaaa

clear
close all
clc

shotList = [52474 52476 52478 52479 52481 52482 52484 52489 52492 52493 52517 52518 52519];
for shot = 1:length(shotList)
    disp(['Working on shot ' num2str(shotList(shot))])
    % get the time basis
    %load(['../data/lp_interelm_' num2str(shotList(shot))]);
    a=mdsopen(shotList(shot));
    time_tmp = mdsvalue('\results::langmuir:time');
    %lParUp = zeros(length(time), 41);
    %lParDiv = zeros(length(time), 41);
    %drUs = 0.03*(logspace(0, 2, 41)-10^0)/(10^2-10^0);
    lParUp = [];
    lParDiv = [];
    drUs = [];
    fxLFS = [];
    time = [];
    for t = 1:length(time_tmp)
        try
	    disp(['Time t=' num2str(time_tmp(t))])
            h =sol_geometry(shotList(shot), time_tmp(t), 'dR_us', 0.03);
            % we need to ensure they have the same x basis
	    if not(t==1)
	        size_min = min([size(lParUp,2) size(h.cl_lfs,2)]);
                lParUp = [lParUp(:,1:size_min); h.cl_lfs(1:size_min)];
                lParDiv = [lParDiv(:,1:size_min); h.cl_div_lfs(:,1:size_min)];
                drUs = [drUs(:,1:size_min); h.dr_us(:,1:size_min)];
                fxLFS = [fxLFS(:,1:size_min); h.fx_lfs(:,1:size_min)];
            else
	        size_min = size(h.cl_lfs,2);
                lParUp = [lParUp; h.cl_lfs];
                lParDiv = [lParDiv; h.cl_div_lfs];
                drUs = [drUs; h.dr_us];
                fxLFS = [fxLFS; h.fx_lfs];
	    end
            time=[time; time_tmp(t)];
            close all
        catch
            warning('Sol Geometry does not work for this time')
        end
    end
    save(['../data/connectionlength' num2str(shotList(shot)) 'mat'], ...
         'drUs', 'time', 'lParDiv', 'lParUp', 'fxLFS')
    disp(['Saved in ' pwd '/../data/connectionlength' num2str(shotList(shot)) 'mat.mat'])
end
        

    
