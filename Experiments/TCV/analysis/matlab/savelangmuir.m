shotList = [61482];
for Sh = 1:length(shotList)
        disp(['Working on shot ' num2str(shotList(Sh))]);
        load(['/home/oliveira/Matlab/10_tools/' ...
              '01_plot_profiles/processed_fits_ELM_removal_21/LP_ELM_analysis_4params_' ...
              num2str(shotList(Sh)) '_0.01s_b0.2_e0.9.mat']);
        LP=LP_fileToMDS(LP);
        dens = LP.FourParamsFit.dens;
        te = LP.FourParamsFit.Te;
        P_perp = LP.FourParamsFit.Pperp;
        Jsat2 = LP.Jsat2;
        area = LP.area;
        time = LP.time;
        time2 = LP.time2;
        probes=LP.probes;
        pos = LP.pos;
        dsep_mid=LP.dsep_mid;
        dsep_mid2=LP.dsep_mid2;
        rho_psi = LP.rho_psi;
        rho_psi2 = LP.rho_psi2;
        ang = LP.ang;
        ang2 = LP.ang2;
        save(['../data/lp_interelm_' num2str(shotList(Sh))], 'ang', 'ang2' ,'time', 'time2', 'probes', 'pos', 'dsep_mid', 'dsep_mid2', 'rho_psi', 'rho_psi2', 'dens', 'te', 'P_perp', 'Jsat2', 'area');
end
