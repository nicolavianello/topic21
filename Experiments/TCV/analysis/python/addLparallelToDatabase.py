import pandas as pd
import MDSplus as mds
import numpy as np
DataTCV = pd.read_csv('../../data/BlobDatabaseQuality.csv')
Lpar = np.zeros(DataTCV.shape[0])
for i, (shot, rho) in enumerate(
        zip(DataTCV['Shots'].values,
            DataTCV['Rho'].values)):
    Tree = mds.Tree('tcv_topic21', int(shot))
    time = Tree.getNode(r'\LPDIVX').getDimensionAt(0).data()
    _idx = np.argmin(np.abs(time-1))
    l = Tree.getNode(r'\LPDIVX').data()[_idx, :]
    r = Tree.getNode(r'\LPRHO').data()[_idx, :]
    Lpar[i] = l[np.argmin(np.abs(r-rho))]
DataTCV['Lpar'] = Lpar
DataTCV.to_csv('../../data/BlobDatabaseQualityLpar.csv')
