import writeTarget
from os import listdir
from os.path import isfile, join
import warnings
import numpy as np
mypath = '/home/vianello/NoTivoli/work/topic21/Experiments/TCV/data/tree'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
shots = []
for f in onlyfiles:
    try:
        shots.append(int(f[12:17]))
    except:
        pass

shots = np.unique(np.asarray(shots)).astype('int')
# only H-Mode
shots = shots[np.where(shots >= 58640)[0]]
for shot in shots:
    # first of all verify that the data are written
    # in the pulse file
    T = writeTarget.Target( shot, write=True )
    warnings.warn('Topic21 tree amended for shot %5i' % shot)

