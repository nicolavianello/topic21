import topic21Mds
import writeTarget
from os import listdir
from os.path import isfile, join
import warnings
import numpy as np
mypath = '/home/vianello/NoTivoli/work/topic21/Experiments/TCV/data/tree'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
shots = []
for f in onlyfiles:
    try:
        shots.append(int(f[12:17]))
    except:
        pass

shots = [68265, 68266, 68268, 68272, 64494, 65595, 65596, 64944, 64925, 64947, 64950]
# only H-Mode
for shot in shots:
    # first of all read the target profiles and write
    # to the tree
    try:
#        T = writeTarget.Target(shot, write=True)
        # the use topic21Mds 
        Out = topic21Mds.Tree(shot)
        Out.toMds()
        warnings.warn('Written pulse file for shot %5i' % shot)
    except:
        warnings.warn('Shot file for shot {} not written'.format(shot))
