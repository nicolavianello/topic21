import warnings
import numpy as np
import matplotlib as mpl
from matplotlib.ticker import AutoMinorLocator, MaxNLocator
from scipy.interpolate import UnivariateSpline, interp1d
from scipy import signal
from scipy.io import loadmat
import pandas as pd
from boloradiation import Radiation

# from tcv.diag.bolo import Bolo
import eqtools
import MDSplus as mds
import xarray as xray
from tcv import langmuir, baratrons, gas, tcvFilaments, tcvProfiles, tcvgeom

# from tcv.diag.axuv import AXUV
import smooth
import seaborn as sns
import bottleneck as bn
import elm_detection

from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel, WhiteKernel

mpl.rcParams["font.family"] = "sans-serif"
mpl.rc("font", size=22)
# mpl.rc('font', **{'family': 'sans-serif', 'sans-serif': ['Tahoma']})
mpl.rc("lines", linewidth=2)


def print_menu():
    print(30 * "-", "MENU", 30 * "-")
    print("1. General plot 64157, 64158, 64159, 64160")
    print("2. General plot 64167, 64168, 64169, 64170")
    print("3. Compare divertor total flux vs density")
    print("4. Compare divertor total flux vs divertor pressure")
    print("5. Compare profiles density/pressure evolution")
    print("6. Compare profiles extreme values")
    print("7. Compare profiles V1/V2 high density")
    print("8. General parameters V1/V2 shots ")
    print("9. Compare Equilibria LSN and USN")
    print("10. Compare Equilibria Low-High-Angle")
    print("11. General plot comparison 64950, 68266")
    print("12. General plot shot 68268")
    print("13. Target Upstream baffled/unbaffled together")
    print("14. Compare baffled/unbaffled")
    print("15. Compare N2 seeding")
    print("16. Compare equilibria baffled/unbaffled")
    print("17. Compare upstream profiles 68265, 68266, 68267")
    print("18. Compare upstream/target fuelling/seedings")
    print("99: End")
    print(67 * "-")


loop = True

while loop:
    print_menu()
    selection = int(input("Enter your choice [1-99] "))
    if selection == 1:
        shotList = (64157, 64158, 64159, 64160)
        colorList = ("k", "#F08C05", "#FF0C71", "#1D1AEB", "#17FFC4")
        # create the plot
        fig, ax = mpl.pylab.subplots(figsize=(14, 14), nrows=3, ncols=2, sharex="col")
        fig.subplots_adjust(wspace=0.25, top=0.98, hspace=0.08, left=0.1, right=0.95)
        for shot, col in zip(shotList, colorList):
            Tree = mds.Tree("tcv_shot", shot)
            iP = mds.Data.compile(r"tcv_ip()").evaluate()
            enAVG = Tree.getNode(r"\results::fir:n_average")
            # load the vloop
            Vloop = Tree.getNode(r"\magnetics::vloop")
            # now load the bolometry radiation
            Bolo = Radiation(shot)
            try:
                POhm = Tree.getNode(r"\results::conf:ptot_ohm")
                _ = POhm.data()
                tagPohm = False
            except:
                POhm = np.abs(iP.data() * Vloop.data()[0, :])
                tagPohm = True
            # load the fueling
            Gas = gas.Gas(shot, gases="D2", valves=1)
            # load the H-alpha calibrated from the vertical line
            HalphaV = mds.Data.compile(r"pd_calibrated(1)").evaluate()
            # now check if data are available for the LPs so that
            # we can also plot the integrated flux
            try:
                Target = langmuir.LP(shot, Type="floor")
            except:
                pass
            Pressure = baratrons.pressure(shot)
            ax[0, 0].plot(
                iP.getDimensionAt().data(),
                -iP.data() / 1e6,
                color=col,
                label="# %5i" % shot,
            )
            ax[0, 0].set_ylabel(r"I$_p$ [MA]")
            ax[0, 0].set_xlim([0, 1.8])
            ax[0, 0].set_ylim([0, 0.4])
            ax[0, 0].axes.get_xaxis().set_visible(False)
            ax[0, 0].legend(loc="best", numpoints=1, frameon=False)

            if tagPohm:
                ax[1, 0].plot(
                    iP.getDimensionAt().data(),
                    POhm / 1e6,
                    "-",
                    color=col,
                    label="Ohmic",
                )
            else:
                ax[1, 0].plot(
                    POhm.getDimensionAt().data(),
                    POhm.data() / 1e6,
                    "-",
                    color=col,
                    label="Ohmic",
                )
            try:
                ax[1, 0].plot(
                    Bolo.time,
                    (Bolo.LfsSol() + Bolo.LfsLeg()) / 1e3,
                    "--",
                    color=col,
                    label=r"LFS SOL + Leg",
                )
            except:
                pass
            ax[1, 0].set_ylabel(r"[MW]")
            ax[1, 0].legend(loc="best", numpoints=1, frameon=False)
            ax[1, 0].set_xlim([0, 1.8])
            ax[1, 0].axes.get_xaxis().set_visible(False)
            try:
                ax[2, 0].plot(
                    Pressure.divertor.t,
                    Pressure.divertor.rolling(t=20, center=True).mean(),
                    color=col,
                )
                ax[2, 0].set_ylabel(r"Divertor pressure [Pa]")
                ax[2, 0].set_xlim([0, 1.8])
                ax[2, 0].set_ylim([0, 0.2])
                ax[2, 0].set_xlabel("t[s]")
            except:
                pass
            ax[0, 1].plot(enAVG.getDimensionAt().data(), enAVG.data() / 1e19)
            ax[0, 1].set_ylabel(r"$\langle n_e \rangle [10^{19}$m$^{-3}$]")
            ax[0, 1].axes.get_xaxis().set_visible(False)

            ax[1, 1].plot(Gas.flow.time, Gas.flow, color=col)
            ax[1, 1].set_ylabel(r"Gas Injection")
            ax[1, 1].axes.get_xaxis().set_visible(False)

            try:
                ax[2, 1].plot(Target.t2, Target.TotalSpIonFlux() / 1e23, color=col)
                ax[2, 1].text(
                    0.1,
                    0.85,
                    r"Total Ion Flux [10$^{23}$s$^{-1}$]",
                    transform=ax[2, 1].transAxes,
                )
                ax[2, 1].set_xlim([0, 1.8])
            #                ax[2, 1].set_ylim([0, 4.5])
            except:
                pass
            ax[2, 1].set_xlabel(r"t[s]")
        mpl.pylab.savefig("../pdfbox/ShotCW38_2019_Valves3.pdf", bbox_to_inches="tight")
    elif selection == 2:
        shotList = (64167, 64168, 64169, 64170)
        colorList = ("k", "#F08C05", "#FF0C71", "#1D1AEB", "#17FFC4")
        # create the plot
        fig, ax = mpl.pylab.subplots(figsize=(14, 14), nrows=3, ncols=2, sharex="col")
        fig.subplots_adjust(hspace=0.05, top=0.98, wspace=0.25, left=0.1, right=0.95)
        for shot, col in zip(shotList, colorList):
            Tree = mds.Tree("tcv_shot", shot)
            iP = mds.Data.compile(r"tcv_ip()").evaluate()
            enAVG = Tree.getNode(r"\results::fir:n_average")
            # load the vloop
            Vloop = Tree.getNode(r"\magnetics::vloop")
            # now load the bolometry radiation
            Bolo = Radiation(shot)
            try:
                POhm = Tree.getNode(r"\results::conf:ptot_ohm")
                _ = POhm.data()
                tagPohm = False
            except:
                POhm = np.abs(iP.data() * Vloop.data()[0, :])
                tagPohm = True
            # load the fueling
            Gas = gas.Gas(shot, gases="D2", valves=2)
            # load the H-alpha calibrated from the vertical line
            HalphaV = mds.Data.compile(r"pd_calibrated(1)").evaluate()
            # now check if data are available for the LPs so that
            # we can also plot the integrated flux
            try:
                Target = langmuir.LP(shot, Type="floor")
            except:
                pass
            Pressure = baratrons.pressure(shot)
            ax[0, 0].plot(
                iP.getDimensionAt().data(),
                -iP.data() / 1e6,
                color=col,
                label="# %5i" % shot,
            )
            ax[0, 0].set_ylabel(r"I$_p$ [MA]")
            ax[0, 0].set_xlim([0, 1.8])
            ax[0, 0].set_ylim([0, 0.4])
            ax[0, 0].axes.get_xaxis().set_visible(False)
            ax[0, 0].legend(loc="best", numpoints=1, frameon=False)

            if tagPohm:
                ax[1, 0].plot(
                    iP.getDimensionAt().data(),
                    POhm / 1e6,
                    "-",
                    color=col,
                    label="Ohmic",
                )
            else:
                ax[1, 0].plot(
                    POhm.getDimensionAt().data(),
                    POhm.data() / 1e6,
                    "-",
                    color=col,
                    label="Ohmic",
                )
            try:
                ax[1, 0].plot(
                    Bolo.time,
                    (Bolo.LfsSol() + Bolo.LfsLeg()) / 1e3,
                    "--",
                    color=col,
                    label=r"LFS SOL + Leg",
                )
            except:
                pass
            ax[1, 0].set_ylabel(r"[MW]")
            ax[1, 0].legend(loc="best", numpoints=1, frameon=False)
            ax[1, 0].set_xlim([0, 1.8])
            ax[1, 0].axes.get_xaxis().set_visible(False)
            try:
                ax[2, 0].plot(
                    Pressure.divertor.t,
                    Pressure.divertor.rolling(t=20, center=True).mean(),
                    color=col,
                )
                ax[2, 0].set_ylabel(r"Divertor pressure [Pa]")
                ax[2, 0].set_xlim([0, 1.8])
                ax[2, 0].set_ylim([0, 0.2])
                ax[2, 0].set_xlabel("t[s]")
            except:
                pass
            ax[0, 1].plot(enAVG.getDimensionAt().data(), enAVG.data() / 1e19)
            ax[0, 1].set_ylabel(r"$\langle n_e \rangle [10^{19}$m$^{-3}$]")
            ax[0, 1].axes.get_xaxis().set_visible(False)

            ax[1, 1].plot(Gas.flow.time, Gas.flow, color=col)
            ax[1, 1].set_ylabel(r"Gas Injection")
            ax[1, 1].axes.get_xaxis().set_visible(False)

            try:
                ax[2, 1].plot(Target.t2, Target.TotalSpIonFlux() / 1e23, color=col)
                ax[2, 1].text(
                    0.1,
                    0.85,
                    r"Total Ion Flux [10$^{23}$s$^{-1}$]",
                    transform=ax[2, 1].transAxes,
                )
                ax[2, 1].set_xlim([0, 1.8])
            #                ax[2, 1].set_ylim([0, 4.5])
            except:
                pass
            ax[2, 1].set_xlabel(r"t[s]")
        mpl.pylab.savefig("../pdfbox/ShotCW38_2019_Valves1.pdf", bbox_to_inches="tight")
    elif selection == 3:
        Df = pd.read_csv("../data/PlungeTimesC38_2019.csv")
        shotV3 = (64157, 64158, 64159)
        shotV1 = (64167, 64168, 64169)
        shotA = (shotV3, shotV1)
        title = ("Divertor", "Main chamber")
        colorList = ("#FF0C71", "#1D1AEB", "#F08C05")
        fig, ax = mpl.pylab.subplots(figsize=(10, 9), nrows=2, ncols=1, sharex=True)
        fig.subplots_adjust(hspace=0.05, top=0.98, left=0.16, right=0.95)

        for i, (shL, tl) in enumerate(zip(shotA, title)):
            A = Df[Df["shots"].isin(shL)]
            pnPlunges = np.append(A.en1.values, A.en2.values)
            for _is, (shot, col) in enumerate(zip(shL, colorList)):
                Lp = langmuir.LP(shot, Type="floor")
                Tree = mds.Tree("tcv_shot", shot)
                enAVG = Tree.getNode(r"\results::fir:n_average")
                _idx = np.where(enAVG.getDimensionAt().data() >= Lp.t2.min())
                en = enAVG.data()[_idx]
                _idx2 = np.where(
                    (Lp.t2 >= enAVG.getDimensionAt().data()[_idx].min())
                    & (Lp.t2 <= enAVG.getDimensionAt().data()[_idx].max())
                )
                flux = Lp.TotalSpIonFlux()[_idx2]
                S = interp1d(
                    enAVG.getDimensionAt().data()[_idx], en / 1e19, kind="linear"
                )
                _x = S(Lp.t2[_idx2])
                _x = smooth.smooth(_x, window_len=10)
                _y = flux / 1e23

                ax[i].plot(
                    _x[np.argsort(_x)],
                    _y[np.argsort(_x)],
                    ".",
                    color=col,
                    label=r"#{}".format(shot),
                )
                ax[i].text(0.1, 0.87, tl, transform=ax[i].transAxes)
                ax[i].text(
                    0.05,
                    0.7 - _is * 0.15,
                    r"#{}".format(shot),
                    color=col,
                    transform=ax[i].transAxes,
                )
            for p in pnPlunges:
                ax[i].axvline(p, ls="--", lw=2, color="gray")

        ax[0].tick_params(labelbottom=False)
        ax[0].set_ylabel(r"$\int$ Ion Flux [10$^{23}$s$^{-1}$]")
        ax[1].set_ylabel(r"$\int$ Ion Flux [10$^{23}$s$^{-1}$]")
        ax[1].set_xlabel(r"$\langle n_e \rangle [10^{19}$m$^{-3}$]")
        ax[1].set_xlim([0, 13])
        fig.savefig(
            "../pdfbox/TotalIonFluxVsDensity_ShotCW38_2019.pdf", bbox_to_inches="tight"
        )
    elif selection == 4:
        Df = pd.read_csv("../data/PlungeTimesC38_2019.csv")

        shotV3 = (64157, 64158, 64159)
        shotV1 = (64167, 64168, 64169)
        shotA = (shotV3, shotV1)
        title = ("Divertor", "Main chamber")
        colorList = ("#FF0C71", "#1D1AEB", "#F08C05")
        fig, ax = mpl.pylab.subplots(figsize=(10, 9), nrows=2, ncols=1, sharex=True)
        fig.subplots_adjust(hspace=0.05, top=0.98, left=0.16, right=0.95)

        for i, (shL, tl) in enumerate(zip(shotA, title)):
            A = Df[Df["shots"].isin(shL)]
            pnPlunges = np.append(A.pn45_1.values, A.pn45_2.values)
            for _is, (shot, col) in enumerate(zip(shL, colorList)):
                Lp = langmuir.LP(shot, Type="floor")
                Tree = mds.Tree("tcv_shot", shot)
                enTime = (
                    Tree.getNode(r"\results::fir:n_average").getDimensionAt().data()
                )
                tmin, tmax = Lp.t2.min(), enTime.max()
                _idx2 = np.where((Lp.t2 >= tmin) & (Lp.t2 <= tmax))
                flux = Lp.TotalSpIonFlux()[_idx2]
                Pressure = baratrons.pressure(shot)
                P = Pressure.p45.where(
                    (Pressure.p45.t >= tmin) & (Pressure.p45.t <= tmax), drop=True
                )

                S = interp1d(
                    P.t.values, P.values, kind="linear", fill_value="extrapolate"
                )
                _x = S(Lp.t2[_idx2])
                _y = flux / 1e23

                ax[i].plot(
                    _x[np.argsort(_x)],
                    _y[np.argsort(_x)],
                    ".",
                    color=col,
                    label=r"#{}".format(shot),
                )
                ax[i].text(0.1, 0.87, tl, transform=ax[i].transAxes)
                ax[i].text(
                    0.87,
                    0.87 - _is * 0.15,
                    r"#{}".format(shot),
                    color=col,
                    transform=ax[i].transAxes,
                )
            for p in pnPlunges:
                ax[i].axvline(p, ls="--", lw=2, color="gray")
        ax[0].tick_params(labelbottom=False)
        ax[0].set_ylabel(r"$\int$ Ion Flux [10$^{23}$s$^{-1}$]")
        ax[1].set_ylabel(r"$\int$ Ion Flux [10$^{23}$s$^{-1}$]")
        ax[1].set_xlabel(r"P$_{45}$ [Pa]")
        ax[1].set_xlim([0, 0.13])
        fig.savefig(
            "../pdfbox/TotalIonFluxVsP45Pressure_ShotCW38_2019.pdf",
            bbox_to_inches="tight",
        )
    elif selection == 5:
        shotV3 = (64157, 64158, 64159)
        shotV1 = (64167, 64168, 64169)
        shotA = (shotV3, shotV1)
        title = ("Divertor", "Main chamber")
        # read the information on the timing and profiles
        Df = pd.read_csv("../data/PlungeTimesC38_2019.csv")
        flatui = ["#9b59b6", "#3498db", "#95a5a6", "#e74c3c", "#34495e", "#2ecc71"]
        colorProfiles = sns.color_palette(flatui).as_hex()
        fig, ax = mpl.pylab.subplots(figsize=(16, 10), nrows=2, ncols=2, sharey="row")
        fig.subplots_adjust(hspace=0.25, top=0.95, left=0.14, right=0.95, wspace=0.05)

        fig2, ax2 = mpl.pylab.subplots(figsize=(16, 10), nrows=2, ncols=2, sharey="row")
        fig2.subplots_adjust(hspace=0.25, top=0.95, left=0.14, right=0.95, wspace=0.05)

        for i, (shL, tl) in enumerate(zip(shotA, title)):
            for _is, shot in enumerate(shL):
                Lp = langmuir.LP(shot, Type="floor")
                Tree = mds.Tree("tcv_shot", shot)
                enAVG = Tree.getNode(r"\results::fir:n_average")
                _idx = np.where(enAVG.getDimensionAt().data() >= Lp.t2.min())
                en = enAVG.data()[_idx]
                _idx2 = np.where(
                    (Lp.t2 >= enAVG.getDimensionAt().data()[_idx].min())
                    & (Lp.t2 <= enAVG.getDimensionAt().data()[_idx].max())
                )
                flux = Lp.TotalSpIonFlux()[_idx2]
                S = interp1d(
                    enAVG.getDimensionAt().data()[_idx], en / 1e19, kind="linear"
                )
                _x = S(Lp.t2[_idx2])
                _x = smooth.smooth(_x, window_len=10)
                _y = flux / 1e23

                ax[0, i].plot(
                    _x[np.argsort(_x)],
                    _y[np.argsort(_x)],
                    ".",
                    color="gray",
                    label=r"#{}".format(shot),
                )
                ax[0, i].set_title(tl)
                # now the part with the pressure
                enTime = (
                    Tree.getNode(r"\results::fir:n_average").getDimensionAt().data()
                )
                tmin, tmax = Lp.t2.min(), enTime.max()
                _idx2 = np.where((Lp.t2 >= tmin) & (Lp.t2 <= tmax))
                flux = Lp.TotalSpIonFlux()[_idx2]
                Pressure = baratrons.pressure(shot)
                P = Pressure.p45.where(
                    (Pressure.p45.t >= tmin) & (Pressure.p45.t <= tmax), drop=True
                )

                S = interp1d(
                    P.t.values, P.values, kind="linear", fill_value="extrapolate"
                )
                _x = S(Lp.t2[_idx2])
                _y = flux / 1e23

                ax2[0, i].plot(
                    _x[np.argsort(_x)],
                    _y[np.argsort(_x)],
                    ".",
                    color="gray",
                    label=r"#{}".format(shot),
                )
                ax2[0, i].set_title(tl)

            # now collect all the plots which are in the group
            A = Df[Df["shots"].isin(shL)]
            # build an array
            _ss = np.append(A.shots.values, A.shots.values)
            _ttmn = np.append(A.tmin1.values, A.tmin2.values)
            _ttmx = np.append(A.tmax1.values, A.tmax2.values)
            _en = np.append(A.en1.values, A.en2.values)
            _pn = np.append(A.pn45_1.values, A.pn45_2.values)
            for shot, tmin, tmax, eN, pn, col in zip(
                _ss, _ttmn, _ttmx, _en, _pn, colorProfiles
            ):
                P = tcvProfiles.tcvProfiles(shot)
                ## first plunge
                ax[0, i].axvline(eN, ls="--", color=col, lw=2)
                ax2[0, i].axvline(pn, ls="--", color=col, lw=2)
                EnP = P.profileNe(t_min=tmin - 0.05, t_max=tmax, abscissa="sqrtpsinorm")
                rhoN = np.linspace(0.8, 1.08, 100)
                if np.abs(rhoN - 1).min() != 0:
                    rhoN = np.append(rhoN, 1)
                    rhoN = rhoN[np.argsort(rhoN)]
                try:

                    yN, yE, dY, dYE = P.gpr_robustfit(
                        rhoN, kernel="RationalQuadraticFGPR1D"
                    )
                    _norm = yN[np.argmin(np.abs(rhoN - 1))]
                    ax[1, i].plot(rhoN, yN / _norm, "--", color=col, lw=4)
                    ax[1, i].fill_between(
                        rhoN, (yN - yE) / _norm, (yN + yE) / _norm, color=col, alpha=0.3
                    )
                    ax2[1, i].plot(rhoN, yN / _norm, "--", color=col, lw=4)
                    ax2[1, i].fill_between(
                        rhoN, (yN - yE) / _norm, (yN + yE) / _norm, color=col, alpha=0.3
                    )
                except:
                    _x = EnP.X.ravel()
                    _y = EnP.y[np.argsort(_x)]
                    _x = _x[np.argsort(_x)]
                    S = UnivariateSpline(_x, _y)
                    _norm = S(1)
                    print("GPR fit not worked for shot {}".format(shot))
                # ax[1, i].errorbar(EnP.X.ravel(),
                #                   EnP.y/_norm, xerr=EnP.err_X.ravel(),
                #                   yerr=EnP.err_y/_norm, fmt='o', color=col,
                #                   alpha=0.6, ms=10)
                # ax2[1, i].errorbar(EnP.X.ravel(),
                #                   EnP.y/_norm, xerr=EnP.err_X.ravel(),
                #                   yerr=EnP.err_y/_norm, fmt='o', color=col,
                #                    alpha=0.6, ms=10)
                ax[1, i].plot(EnP.X.ravel(), EnP.y / _norm, ".", color=col, alpha=0.6)
                ax2[1, i].plot(EnP.X.ravel(), EnP.y / _norm, ".", color=col, alpha=0.6)

        ax[0, 0].set_xlabel(r"$\langle$n$_e\rangle [10^{19}$m$^{-3}]$")
        ax[0, 1].set_xlabel(r"$\langle$n$_e\rangle [10^{19}$m$^{-3}]$")
        ax[0, 1].tick_params(labelleft=False)
        ax[0, 0].set_ylabel(r"$\int$ Ion Flux [10$^{23}$s$^{-1}$]")
        ax[1, 0].set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        ax[1, 1].tick_params(labelleft=False)
        ax[1, 0].set_xlabel(r"$\langle n_e \rangle [10^{19}$m$^{-3}$]")
        ax[1, 1].set_xlabel(r"$\langle n_e \rangle [10^{19}$m$^{-3}$]")
        ax[1, 0].set_ylim([0.01, 3])
        ax[1, 0].set_yscale("log")
        ax[1, 1].set_yscale("log")
        ax[1, 1].set_ylim([0.1, 4])
        ax[1, 0].set_xlim([0.95, 1.1])
        ax[1, 1].set_xlim([0.95, 1.1])
        fig.savefig(
            "../pdfbox/TotalIonFluxVsDensityUpstreamProfiles_ShotCW38_2019.pdf",
            bbox_to_inches="tight",
        )

        ax2[0, 0].set_xlabel(r"P$_{45}$ [Pa]")
        ax2[0, 1].set_xlabel(r"P$_{45}$ [Pa]")
        ax2[0, 1].tick_params(labelleft=False)
        ax2[0, 0].set_ylabel(r"$\int$ Ion Flux [10$^{23}$s$^{-1}$]")
        ax2[1, 0].set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        ax2[1, 1].tick_params(labelleft=False)
        ax2[1, 0].set_xlabel(r"$\rho$")
        ax2[1, 1].set_xlabel(r"$\rho$")
        ax2[1, 0].set_ylim([0.1, 3])
        ax2[1, 0].set_yscale("log")
        ax2[1, 1].set_yscale("log")
        ax2[1, 1].set_ylim([0.1, 4])
        ax2[1, 0].set_xlim([0.95, 1.1])
        ax2[1, 1].set_xlim([0.95, 1.1])
        fig2.savefig(
            "../pdfbox/TotalIonFluxVsPressureUpstreamProfiles_ShotCW38_2019.pdf",
            bbox_to_inches="tight",
        )
    elif selection == 6:
        shotV3 = (64157, 64158, 64159)
        shotV1 = (64167, 64168, 64169)
        shotA = (shotV3, shotV1)
        title = ("Divertor", "Main chamber")
        # read the information on the timing and profiles
        Df = pd.read_csv("../data/PlungeTimesC38_2019.csv")
        flatui = ["#9b59b6", "#34495e"]
        colorProfiles = sns.color_palette(flatui).as_hex()
        fig, ax = mpl.pylab.subplots(figsize=(16, 10), nrows=2, ncols=2, sharey="row")
        fig.subplots_adjust(hspace=0.25, top=0.95, left=0.14, right=0.95, wspace=0.05)

        fig2, ax2 = mpl.pylab.subplots(figsize=(16, 10), nrows=2, ncols=2, sharey="row")
        fig2.subplots_adjust(hspace=0.25, top=0.95, left=0.14, right=0.95, wspace=0.05)

        for i, (shL, tl) in enumerate(zip(shotA, title)):
            for _is, shot in enumerate(shL):
                Lp = langmuir.LP(shot, Type="floor")
                Tree = mds.Tree("tcv_shot", shot)
                enAVG = Tree.getNode(r"\results::fir:n_average")
                _idx = np.where(enAVG.getDimensionAt().data() >= Lp.t2.min())
                en = enAVG.data()[_idx]
                _idx2 = np.where(
                    (Lp.t2 >= enAVG.getDimensionAt().data()[_idx].min())
                    & (Lp.t2 <= enAVG.getDimensionAt().data()[_idx].max())
                )
                flux = Lp.TotalSpIonFlux()[_idx2]
                S = interp1d(
                    enAVG.getDimensionAt().data()[_idx], en / 1e19, kind="linear"
                )
                _x = S(Lp.t2[_idx2])
                _x = smooth.smooth(_x, window_len=10)
                _y = flux / 1e23

                ax[0, i].plot(
                    _x[np.argsort(_x)],
                    _y[np.argsort(_x)],
                    ".",
                    color="gray",
                    label=r"#{}".format(shot),
                )
                ax[0, i].set_title(tl)
                # now the part with the pressure
                enTime = (
                    Tree.getNode(r"\results::fir:n_average").getDimensionAt().data()
                )
                tmin, tmax = Lp.t2.min(), enTime.max()
                _idx2 = np.where((Lp.t2 >= tmin) & (Lp.t2 <= tmax))
                flux = Lp.TotalSpIonFlux()[_idx2]
                Pressure = baratrons.pressure(shot)
                P = Pressure.p45.where(
                    (Pressure.p45.t >= tmin) & (Pressure.p45.t <= tmax), drop=True
                )

                S = interp1d(
                    P.t.values, P.values, kind="linear", fill_value="extrapolate"
                )
                _x = S(Lp.t2[_idx2])
                _y = flux / 1e23

                ax2[0, i].plot(
                    _x[np.argsort(_x)],
                    _y[np.argsort(_x)],
                    ".",
                    color="gray",
                    label=r"#{}".format(shot),
                )
                ax2[0, i].set_title(tl)

            A = Df[Df["shots"].isin(shL)]
            # determine the minimum and maximum in density
            _ss = np.append(
                A[A["en1"] == np.min(A["en1"])].shots.values,
                A[A["en2"] == np.max(A["en2"])].shots.values,
            )
            _ttmn = np.append(
                A[A["en1"] == np.min(A["en1"])].tmin1.values,
                A[A["en2"] == np.max(A["en2"])].tmin2.values,
            )
            _ttmx = np.append(
                A[A["en1"] == np.min(A["en1"])].tmax1.values,
                A[A["en2"] == np.max(A["en2"])].tmax2.values,
            )
            _en = np.append(
                A[A["en1"] == np.min(A["en1"])].en1.values,
                A[A["en2"] == np.max(A["en2"])].en2.values,
            )

            for shot, tmin, tmax, eN, col in zip(_ss, _ttmn, _ttmx, _en, colorProfiles):
                P = tcvProfiles.tcvProfiles(shot)
                ## first plunge
                ax[0, i].axvline(eN, ls="--", color=col, lw=2)
                EnP = P.profileNe(t_min=tmin - 0.05, t_max=tmax, abscissa="sqrtpsinorm")
                rhoN = np.linspace(0.8, 1.08, 100)
                if np.abs(rhoN - 1).min() != 0:
                    rhoN = np.append(rhoN, 1)
                    rhoN = rhoN[np.argsort(rhoN)]
                try:

                    yN, yE, dY, dYE = P.gpr_robustfit(
                        rhoN, kernel="RationalQuadraticFGPR1D"
                    )
                    _norm = yN[np.argmin(np.abs(rhoN - 1))]
                    ax[1, i].plot(rhoN, yN / _norm, "--", color=col, lw=4)
                    ax[1, i].fill_between(
                        rhoN, (yN - yE) / _norm, (yN + yE) / _norm, color=col, alpha=0.3
                    )
                except:
                    _x = EnP.X.ravel()
                    _y = EnP.y[np.argsort(_x)]
                    _x = _x[np.argsort(_x)]
                    S = UnivariateSpline(_x, _y)
                    _norm = S(1)
                    print("GPR fit not worked for shot {}".format(shot))
                ax[1, i].plot(EnP.X.ravel(), EnP.y / _norm, ".", color=col, alpha=0.6)

            # determine the minimum and maximum in density
            _ss = np.append(
                A[A["pn45_1"] == np.min(A["pn45_1"])].shots.values,
                A[A["pn45_2"] == np.max(A["pn45_2"])].shots.values,
            )
            _ttmn = np.append(
                A[A["pn45_1"] == np.min(A["pn45_1"])].tmin1.values,
                A[A["pn45_2"] == np.max(A["pn45_2"])].tmin2.values,
            )
            _ttmx = np.append(
                A[A["pn45_1"] == np.min(A["pn45_1"])].tmax1.values,
                A[A["pn45_2"] == np.max(A["pn45_2"])].tmax2.values,
            )
            _en = np.append(
                A[A["pn45_1"] == np.min(A["pn45_1"])].pn45_1.values,
                A[A["pn45_2"] == np.max(A["pn45_2"])].pn45_2.values,
            )
            for shot, tmin, tmax, pn, col in zip(_ss, _ttmn, _ttmx, _en, colorProfiles):
                P = tcvProfiles.tcvProfiles(shot)
                ## first plunge
                ax2[0, i].axvline(pn, ls="--", color=col, lw=2)
                EnP = P.profileNe(t_min=tmin - 0.05, t_max=tmax, abscissa="sqrtpsinorm")
                rhoN = np.linspace(0.8, 1.08, 100)
                if np.abs(rhoN - 1).min() != 0:
                    rhoN = np.append(rhoN, 1)
                    rhoN = rhoN[np.argsort(rhoN)]
                try:

                    yN, yE, dY, dYE = P.gpr_robustfit(
                        rhoN, kernel="RationalQuadraticFGPR1D"
                    )
                    _norm = yN[np.argmin(np.abs(rhoN - 1))]
                    ax2[1, i].plot(rhoN, yN / _norm, "--", color=col, lw=4)
                    ax2[1, i].fill_between(
                        rhoN, (yN - yE) / _norm, (yN + yE) / _norm, color=col, alpha=0.3
                    )
                except:
                    _x = EnP.X.ravel()
                    _y = EnP.y[np.argsort(_x)]
                    _x = _x[np.argsort(_x)]
                    S = UnivariateSpline(_x, _y)
                    _norm = S(1)
                    print("GPR fit not worked for shot {}".format(shot))
                ax2[1, i].plot(EnP.X.ravel(), EnP.y / _norm, ".", color=col, alpha=0.6)

        ax[0, 0].set_xlabel(r"$\langle$n$_e\rangle [10^{19}$m$^{-3}]$")
        ax[0, 1].set_xlabel(r"$\langle$n$_e\rangle [10^{19}$m$^{-3}]$")
        ax[0, 1].tick_params(labelleft=False)
        ax[0, 0].set_ylabel(r"$\int$ Ion Flux [10$^{23}$s$^{-1}$]")
        ax[1, 0].set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        ax[1, 1].tick_params(labelleft=False)
        ax[1, 0].set_xlabel(r"$\rho$")
        ax[1, 1].set_xlabel(r"$\rho$")
        ax[1, 0].set_ylim([0.01, 3])
        ax[1, 0].set_yscale("log")
        ax[1, 1].set_yscale("log")
        ax[1, 1].set_ylim([0.1, 4])
        ax[1, 0].set_xlim([0.95, 1.1])
        ax[1, 1].set_xlim([0.95, 1.1])
        fig.savefig(
            "../pdfbox/TotalIonFluxVsDensityUpstream2Profiles_ShotCW38_2019.pdf",
            bbox_to_inches="tight",
        )

        ax2[0, 0].set_xlabel(r"P$_{45}$ [Pa]")
        ax2[0, 1].set_xlabel(r"P$_{45}$ [Pa]")
        ax2[0, 1].tick_params(labelleft=False)
        ax2[0, 0].set_ylabel(r"$\int$ Ion Flux [10$^{23}$s$^{-1}$]")
        ax2[1, 0].set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        ax2[1, 1].tick_params(labelleft=False)
        ax2[1, 0].set_xlabel(r"$\rho$")
        ax2[1, 1].set_xlabel(r"$\rho$")
        ax2[1, 0].set_ylim([0.1, 3])
        ax2[1, 0].set_yscale("log")
        ax2[1, 1].set_yscale("log")
        ax2[1, 1].set_ylim([0.1, 4])
        ax2[1, 0].set_xlim([0.95, 1.1])
        ax2[1, 1].set_xlim([0.95, 1.1])
        fig2.savefig(
            "../pdfbox/TotalIonFluxVsPressureUpstream2Profiles_ShotCW38_2019.pdf",
            bbox_to_inches="tight",
        )
    elif selection == 7:
        shotList = (64159, 64169, 64432, 64533)
        colorCode = ("#FF8100", "#479b55", "#b53841", "#6d5bff")
        tRangeList = ([1.13, 1.29], [1.19, 1.35], [1.12, 1.28], [0.93, 1.02])
        fig, ax = mpl.pylab.subplots(figsize=(10, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15)
        for _idx, (shot, _col, _tr) in enumerate(zip(shotList, colorCode, tRangeList)):
            P = tcvProfiles.tcvProfiles(shot)
            if shot == 64159:
                rho = 1.03
            else:
                rho = 1.02
            En = P.profileNe(
                t_min=_tr[0],
                t_max=_tr[1],
                abscissa="sqrtpsinorm",
                remove_divertor=True,
                rho_threshold=rho,
            )
            rhoN = np.linspace(0.9, 1.11, 100)
            yF, yE, _, _ = P.gpr_robustfit(rhoN)
            _norm = yF[np.argmin(np.abs(rhoN - 1))]
            ax.errorbar(
                En.X.ravel(),
                En.y / _norm,
                xerr=En.err_X.ravel(),
                yerr=En.err_y / _norm,
                fmt="o",
                color=_col,
                alpha=0.4,
                markersize=9,
            )
            ax.plot(rhoN, yF / _norm, "-", color=_col, lw=3)
            ax.fill_between(
                rhoN, (yF - yE) / _norm, (yF + yE) / _norm, color=_col, alpha=0.1
            )
            ax.text(
                0.05,
                0.7 - 0.1 * _idx,
                r"#{}".format(shot),
                color=_col,
                transform=ax.transAxes,
            )
        ax.set_xlim([0.9, 1.15])
        ax.set_ylim([0.05, 3])
        ax.set_xlabel(r"$\rho_p$")
        ax.set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/ComparingUpstreamShotT16_LMode_V1V2.pdf", bbox_to_inches="tight"
        )
        fig.savefig(
            "../pngbox/ComparingUpstreamShotT16_LMode_V1V2.png",
            bbox_to_inches="tight",
            dpi=300,
        )
    elif selection == 8:
        shotList = (64159, 64169, 64432, 64533)
        colorCode = ("#FF8100", "#479b55", "#b53841", "#6d5bff")
        tRangeList = ([1.13, 1.29], [1.19, 1.35], [1.12, 1.28], [0.93, 1.02])
        fig, ax = mpl.pylab.subplots(figsize=(10, 10), nrows=3, ncols=2, sharex="col")
        fig.subplots_adjust(hspace=0.1, wspace=0.35, bottom=0.1, right=0.98, top=0.98)
        ax[2, 0].set_xlabel(r"t [s]")
        ax[2, 1].set_xlabel(r"t [s]")
        for shot, col in zip(shotList, colorCode):
            Tree = mds.Tree("tcv_shot", shot)
            Data = Tree.getNode(r"\results::fir:n_average")
            ax[0, 0].plot(
                Data.getDimensionAt().data(),
                Data.data() / 1e19,
                color=col,
                label=r"#{}".format(shot),
            )
            ax[0, 0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
            ax[0, 0].tick_params(labelbottom=False)

            P = baratrons.pressure(shot)
            ax[1, 0].plot(
                P.divertor.t, bn.move_median(P.divertor.values, 10) * 1e3, color=col
            )
            ax[1, 0].set_ylabel("P$_{div}$ [mPa]")
            ax[1, 0].tick_params(labelbottom=False)
            LP = langmuir.LP(shot)
            ax[2, 0].plot(
                bn.move_mean(LP.t2, 10),
                bn.move_median(LP.TotalSpIonFlux() / 1e23, 10),
                color=col,
            )
            ax[2, 0].set_ylabel(r"Ion flux [10$^{23}$ s$^{-1}$]")
            ax[2, 0].set_xlabel(r"t [s]")

            Gas = gas.Gas(shot, gases=("D2", "D2"), valves=(1, 2))
            ax[0, 1].plot(
                Gas.time, (Gas.flow.sel(Valves=1).values) / 1e21, color=col,
            )
            ax[0, 1].set_ylabel(r"D$_2$[molecules/s]")
            ax[0, 1].text(0.7, 0.87, "V1", transform=ax[0, 1].transAxes)

            ax[1, 1].plot(
                Gas.time, (Gas.flow.sel(Valves=2).values) / 1e21, color=col,
            )
            ax[1, 1].set_ylabel(r"D$_2$[molecules/s]")
            ax[1, 1].text(0.7, 0.87, "V2", transform=ax[1, 1].transAxes)

            iP = mds.Data.compile(r"tcv_ip()").evaluate()
            ax[2, 1].plot(
                iP.getDimensionAt().data(),
                -iP.data() / 1e6,
                color=col,
                label="# %5i" % shot,
            )
            ax[2, 1].set_ylabel(r"I$_p$ [MA]")

        for _ax in ax.flatten():
            _ax.autoscale(enable=True, axis="y")
            _ax.set_xlim([0, 1.8])
        ax[0, 0].legend(loc="best", numpoints=1, frameon=False)

        for tr, col in zip(tRangeList, colorCode):
            t0 = np.average(tr)
            for _ax in ax.flatten():
                _ax.axvline(t0, linestyle="--", linewidth=2, color=col)
        fig.savefig(
            "../pdfbox/TCVGeneralTime{}_{}_{}_{}.pdf".format(
                shotList[0], shotList[1], shotList[2], shotList[3]
            ),
            bbox_to_inches="tight",
        )
        fig.savefig(
            "../pngbox/TCVGeneralTime{}_{}_{}_{}.png".format(
                shotList[0], shotList[1], shotList[2], shotList[3]
            ),
            bbox_to_inches="tight",
            dpi=300,
        )
    elif selection == 9:
        shotList = (64159, 64530)
        fig, ax = mpl.pylab.subplots(
            figsize=(10, 5), nrows=1, ncols=2, sharex="col", sharey="col"
        )
        fig.subplots_adjust(bottom=0.17, top=0.98)
        colorList = ("#FF8100", "#479b55")
        for shot, col, _ax in zip(shotList, colorList, ax):
            tcvgeom.tcvview(
                shot,
                1,
                vessel=True,
                baffles="all",
                core_levels=10,
                sol_levels=4,
                col=col,
                ax=_ax,
            )
            _ax.set_aspect("equal")
            _ax.set_xlabel(r"R [m]")
            _ax.set_ylabel(r"Z [m]")
            _ax.autoscale(enable=True, axis="all")
            _ax.set_xticks([0.6, 0.8, 1])
        fig.savefig("../pdfbox/EquilibraUSN-LSN.pdf")
        fig.savefig("../pngbox/EquilibriaUSN-LSN.png", dpi=300)
    elif selection == 10:
        shotList = (63127, 63161)
        fig, ax = mpl.pylab.subplots(
            figsize=(10, 5), nrows=1, ncols=2, sharex="col", sharey="col"
        )
        fig.subplots_adjust(bottom=0.17, top=0.98)
        colorList = ("#FF8100", "#479b55")
        for shot, col, _ax in zip(shotList, colorList, ax):
            tcvgeom.tcvview(
                shot,
                1,
                vessel=True,
                baffles="all",
                core_levels=10,
                sol_levels=4,
                col=col,
                ax=_ax,
            )
            _ax.set_aspect("equal")
            _ax.set_xlabel(r"R [m]")
            _ax.set_ylabel(r"Z [m]")
            _ax.autoscale(enable=True, axis="all")
            _ax.set_xticks([0.6, 0.8, 1])
        fig.savefig("../pdfbox/EquilibraLow-High-Angle.pdf")
        fig.savefig("../pngbox/EquilibriaLow-High-Angle.png", dpi=300)
    elif selection == 11:
        shotList = (64950, 68266)
        colorList = ("#011C40", "#FF0C71")
        # create the plot
        fig, ax = mpl.pylab.subplots(figsize=(7, 14), nrows=6, sharex="col")
        fig.subplots_adjust(wspace=0.25, top=0.98, hspace=0.08, left=0.15, right=0.95)
        for shot, col in zip(shotList, colorList):
            Tree = mds.Tree("tcv_shot", shot)
            enAVG = Tree.getNode(r"\results::fir:n_average")
            ax[0].plot(
                enAVG.getDimensionAt().data(),
                enAVG.data() / 1e19,
                label=r"#{}".format(shot),
                color=col,
            )
            ax[0].set_ylabel(r"$\langle n_e \rangle [10^{19}$m$^{-3}$]")
            ax[0].tick_params(labelbottom=False)
            Gas = gas.Gas(shot, gases="D2", valves=1)
            ax[1].plot(
                Gas.flow.sel(Valves=1).time.values,
                Gas.flow.sel(Valves=1).values / 1e21,
                color=col,
            )
            ax[1].text(
                0.05, 0.85, r"Flow rate [molecules/s]", transform=ax[1].transAxes
            )
            ax[1].tick_params(labelbottom=False)
            # load the H-alpha calibrated from the vertical line
            HalphaV = mds.Data.compile(r"pd_calibrated(1)").evaluate()
            ax[2].plot(
                HalphaV.getDimensionAt().data(), HalphaV.data(), color=col, alpha=0.6
            )
            ax[2].tick_params(labelbottom=False)
            ax[2].set_ylabel(r"D$_{\alpha}$")
            # now check if data are available for the LPs so that
            # we can also plot the integrated flux
            try:
                Target = langmuir.LP(shot, Type="floor")
                ax[3].plot(
                    bn.move_mean(Target.t2, 30),
                    bn.move_mean(Target.TotalSpIonFlux() / 1e22, 50),
                    color=col,
                )
                ax[3].tick_params(labelbottom=False)
                ax[3].set_ylabel(r"$\int$J$_{sat}$ [ion/s]")
            except:
                pass
            Pressure = baratrons.pressure(shot)
            ax[4].plot(
                Pressure.divertor.t,
                Pressure.divertor.rolling(t=20, center=True).mean(),
                color=col,
            )
            ax[4].text(0.05, 0.85, r"Divertor pressure [Pa]", transform=ax[4].transAxes)
            ax[4].set_xlim([0, 2.0])
            ax[4].set_ylim([0, 0.2])
            ax[4].tick_params(labelbottom=False)
            ax[5].plot(
                Pressure.midplane.t,
                Pressure.midplane.rolling(t=20, center=True).mean(),
                color=col,
            )
            ax[5].text(0.05, 0.85, r"Midplane pressure [Pa]", transform=ax[5].transAxes)
            ax[5].set_xlim([0, 2.0])
            ax[5].set_ylim([0, 0.05])
            ax[5].set_xlabel(r"t[s]")
            ax[0].legend(loc="best", numpoints=1, frameon=False)
        mpl.pylab.savefig(
            "../pdfbox/CompareGeneralShot_{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inches="tight",
        )
    elif selection == 12:
        shot = 68268
        col = "#011C40"
        # create the plot
        fig, ax = mpl.pylab.subplots(figsize=(7, 14), nrows=6, sharex="col")
        fig.subplots_adjust(wspace=0.25, top=0.98, hspace=0.08, left=0.15, right=0.95)
        Tree = mds.Tree("tcv_shot", shot)
        enAVG = Tree.getNode(r"\results::fir:n_average")
        ax[0].plot(
            enAVG.getDimensionAt().data(),
            enAVG.data() / 1e19,
            label=r"#{}".format(shot),
            color=col,
        )
        ax[0].set_ylabel(r"$\langle n_e \rangle [10^{19}$m$^{-3}$]")
        ax[0].tick_params(labelbottom=False)
        Gas = gas.Gas(shot, gases=("D2", "N2"), valves=(1, 3))
        ax[1].plot(
            Gas.flow.sel(Valves=1).time.values,
            Gas.flow.sel(Valves=1).values / 1e21,
            color=col,
        )
        ax[1].plot(
            Gas.flow.sel(Valves=3).time.values,
            Gas.flow.sel(Valves=3).values / 1e21,
            color="#FF0C71",
        )
        ax[1].text(
            0.45, 0.85, r"D$_2$ [molecules/s]", transform=ax[1].transAxes, color=col
        )
        ax[1].text(
            0.45,
            0.7,
            r"N$_2$ [molecules/s]",
            transform=ax[1].transAxes,
            color="#FF0C71",
        )
        ax[1].tick_params(labelbottom=False)
        # load the H-alpha calibrated from the vertical line
        HalphaV = mds.Data.compile(r"pd_calibrated(1)").evaluate()
        ax[2].plot(
            HalphaV.getDimensionAt().data(), HalphaV.data(), color=col, alpha=0.6
        )
        ax[2].tick_params(labelbottom=False)
        ax[2].set_ylabel(r"D$_{\alpha}$")
        # now check if data are available for the LPs so that
        # we can also plot the integrated flux
        try:
            Target = langmuir.LP(shot, Type="floor")
            ax[3].plot(
                bn.move_mean(Target.t2, 30),
                bn.move_mean(Target.TotalSpIonFlux() / 1e22, 50),
                color=col,
            )
            ax[3].tick_params(labelbottom=False)
            ax[3].set_ylabel(r"$\int$J$_{sat}$ [ion/s]")
        except:
            pass
        Pressure = baratrons.pressure(shot)
        ax[4].plot(
            Pressure.divertor.t,
            Pressure.divertor.rolling(t=20, center=True).mean(),
            color=col,
        )
        ax[4].text(0.05, 0.85, r"Divertor pressure [Pa]", transform=ax[4].transAxes)
        ax[4].set_xlim([0, 2.0])
        ax[4].set_ylim([0, 0.2])
        ax[4].tick_params(labelbottom=False)
        ax[5].plot(
            Pressure.midplane.t,
            Pressure.midplane.rolling(t=20, center=True).mean(),
            color=col,
        )
        ax[5].text(0.05, 0.85, r"Midplane pressure [Pa]", transform=ax[5].transAxes)
        ax[5].set_xlim([0, 2.0])
        ax[5].set_ylim([0, 0.05])
        ax[5].set_xlabel(r"t[s]")
        ax[0].legend(loc="best", numpoints=1, frameon=False)
        mpl.pylab.savefig(
            "../pdfbox/GeneralPlotShot_{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 13:
        shotBaffled = (64494, 64947)
        tBaffled = ([0.88, 0.96], [1.5, 1.75])
        shotUnBaffled = (68265, 68266)
        tUnbaffled = ([0.93, 0.96], [1.3, 1.5])
        colorList = ("#011C40", "#FF0C71")
        figProfile = mpl.pylab.figure(figsize=(14, 9), tight_layout=True)
        gs_top = mpl.pylab.GridSpec(
            2, 4, wspace=0.37, top=0.95, left=0.1, right=0.96, bottom=0.12,
        )
        # Bottom axes with the same x and y axis
        upAxes = figProfile.add_subplot(gs_top[0, :-2])
        upAxes.yaxis.set_major_locator(
            mpl.ticker.LogLocator(
                base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
            )
        )
        upAxes.set_xlabel(r"$\rho$")
        upAxes.set_ylim([3e-2, 4])
        upAxes.set_xlim([0.9, 1.12])
        upAxes.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        upAxes.set_yscale("log")
        # The shared in time axes
        axNe = figProfile.add_subplot(gs_top[1, 0])
        axTe = figProfile.add_subplot(gs_top[1, 1])
        # Hide shared x-tick labels
        for ax in (axNe, axTe):
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())
            ax.set_xlim([0.92, 1.2])
            ax.set_xlabel(r"$\rho$")
        axNe.set_ylabel(r"n$_e [10^{19}$m$^{-3}$]")
        axTe.set_ylabel(r"T$_e$ [eV]")
        for _idxshot, (shot, col, tr) in enumerate(
            zip(shotBaffled, colorList, tBaffled)
        ):
            Tree = mds.Tree("tcv_shot", shot)
            Data = Tree.getNode(r"\base::pd:pd_001")
            En = Tree.getNode(r"\results::fir:n_average")
            tEn = En.getDimensionAt().data()
            En = En.data()
            Pr = baratrons.pressure(shot).divertor
            tPr = Pr.t.values
            Pr = Pr.values
            _idx = np.where((tEn >= tr[0]) & (tEn <= tr[1]))[0]
            enLabel = (
                r"$\langle$n$_e\rangle$ = {:.2f}".format((En[_idx].mean() / 1e19))
                + r"$\times 10^{19}$ m$^{-3}$"
            )
            _idx = np.where((tPr >= tr[0]) & (tPr <= tr[1]))[0]
            PrLabel = "\nP = {:.1f} mPa".format((Pr[_idx].mean() * 1e3))
            label = enLabel + PrLabel
            if _idxshot == 0:
                upAxes.text(
                    0.05, 0.4, label, color=col, fontsize=20, transform=upAxes.transAxes
                )
            else:
                upAxes.text(
                    0.57, 0.8, label, color=col, fontsize=20, transform=upAxes.transAxes
                )

            P = tcvProfiles(shot, interelm=True)
            En = P.profileNe(
                t_min=tr[0],
                t_max=tr[1],
                abscissa="sqrtpsinorm",
                interelm=True,
                rho=0.82,
                width=0.05,
            )
            _ = En.remove_points(
                np.logical_and(En.X.ravel() <= 0.9, En.X.ravel() >= 1.12)
            )
            rhoN = np.linspace(En.X.ravel().min(), 1.12, 120)
            yFit, yFitE, _, _ = P.gpr_robustfit(
                rhoN, density=True, temperature=False, regularization_parameter=6
            )
            _norm = yFit[np.argmin(np.abs(rhoN - 1))]
            upAxes.errorbar(
                En.X.ravel(),
                En.y / _norm,
                yerr=En.err_y / _norm,
                xerr=En.err_X.ravel(),
                fmt="o",
                ms=10,
                color=col,
                alpha=0.5,
            )
            upAxes.plot(rhoN, yFit / _norm, "-", color=col, lw=2)
            upAxes.fill_between(
                rhoN,
                (yFit - yFitE) / _norm,
                (yFit + yFitE) / _norm,
                color=col,
                alpha=0.3,
            )
            Target = langmuir.LP(shot)
            out = Target.UpStreamProfile(trange=tr)
            axNe.plot(out["rho"], out["en"] / 1e19, "o", color=col)
            axTe.plot(out["rho"], out["te"], "o", color=col)
        axNe.set_ylim([0.0, 4])
        axTe.set_ylim([0, 20])
        upAxes.set_title(r"Baffled")
        # -----------
        # now repeat for the unbaffled case
        upAxes2 = figProfile.add_subplot(gs_top[0, 2:])
        upAxes2.yaxis.set_major_locator(
            mpl.ticker.LogLocator(
                base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
            )
        )
        upAxes2.set_xlabel(r"$\rho$")
        upAxes2.set_ylim([3e-2, 4])
        upAxes2.set_xlim([0.9, 1.12])
        upAxes2.tick_params(labelleft=False)
        upAxes2.set_yscale("log")
        # The shared in time axes
        axNe2 = figProfile.add_subplot(gs_top[1, 2])
        axTe2 = figProfile.add_subplot(gs_top[1, 3])
        # Hide shared x-tick labels
        for ax in (axNe2, axTe2):
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())
            ax.set_xlim([0.92, 1.2])
            ax.set_xlabel(r"$\rho$")
        axNe2.set_ylabel(r"n$_e [10^{19}$m$^{-3}$]")
        axTe2.set_ylabel(r"T$_e$ [eV]")
        for _idxshot, (shot, col, tr) in enumerate(
            zip(shotUnBaffled, colorList, tUnbaffled)
        ):
            Tree = mds.Tree("tcv_shot", shot)
            Data = Tree.getNode(r"\base::pd:pd_001")
            En = Tree.getNode(r"\results::fir:n_average")
            tEn = En.getDimensionAt().data()
            En = En.data()
            Pr = baratrons.pressure(shot).divertor
            tPr = Pr.t.values
            Pr = Pr.values
            _idx = np.where((tEn >= tr[0]) & (tEn <= tr[1]))[0]
            enLabel = (
                r"$\langle$n$_e\rangle$ = {:.2f}".format((En[_idx].mean() / 1e19))
                + r"$\times 10^{19}$ m$^{-3}$"
            )
            _idx = np.where((tPr >= tr[0]) & (tPr <= tr[1]))[0]
            PrLabel = "\nP = {:.1f} mPa".format((Pr[_idx].mean() * 1e3))
            label = enLabel + PrLabel
            if _idxshot == 0:
                upAxes2.text(
                    0.05,
                    0.4,
                    label,
                    color=col,
                    fontsize=20,
                    transform=upAxes2.transAxes,
                )
            else:
                upAxes2.text(
                    0.57,
                    0.8,
                    label,
                    color=col,
                    fontsize=20,
                    transform=upAxes2.transAxes,
                )

            P = tcvProfiles(shot, interelm=True)
            try:
                En = P.profileNe(
                    t_min=tr[0],
                    t_max=tr[1],
                    abscissa="sqrtpsinorm",
                    interelm=True,
                    rho=0.82,
                    width=0.05,
                )
            except:
                En = P.profileNe(t_min=tr[0], t_max=tr[1], abscissa="sqrtpsinorm")

            _ = En.remove_points(
                np.logical_and(En.X.ravel() <= 0.9, En.X.ravel() >= 1.12)
            )
            rhoN = np.linspace(En.X.ravel().min(), 1.12, 120)
            yFit, yFitE, _, _ = P.gpr_robustfit(
                rhoN, density=True, temperature=False, regularization_parameter=6
            )
            _norm = yFit[np.argmin(np.abs(rhoN - 1))]
            upAxes2.errorbar(
                En.X.ravel(),
                En.y / _norm,
                yerr=En.err_y / _norm,
                xerr=En.err_X.ravel(),
                fmt="o",
                ms=10,
                color=col,
                alpha=0.5,
            )
            upAxes2.plot(rhoN, yFit / _norm, "-", color=col, lw=2)
            upAxes2.fill_between(
                rhoN,
                (yFit - yFitE) / _norm,
                (yFit + yFitE) / _norm,
                color=col,
                alpha=0.3,
            )
            Target = langmuir.LP(shot)
            out = Target.UpStreamProfile(trange=tr)
            axNe2.plot(out["rho"], out["en"] / 1e19, "o", color=col)
            axTe2.plot(out["rho"], out["te"], "o", color=col)
        axNe2.set_ylim([0.0, 4])
        axTe2.set_ylim([0, 20])
        upAxes2.set_title(r"Unbaffled")
        figProfile.savefig(
            "../pdfbox/TCVGeneralProfileShotsBaffledUnbaffled.pdf",
            bbox_to_inches="tight",
        )
    elif selection == 14:
        shotlist = (64947, 68266)
        colorList = ("#253B59", "#FF0C71")
        trlist = ([1.5, 1.75], [1.3, 1.5])
        figProfile = mpl.pylab.figure(figsize=(9, 9), tight_layout=True)
        gs_top = mpl.pylab.GridSpec(
            2, 2, wspace=0.35, top=0.98, left=0.16, right=0.96, bottom=0.12,
        )
        # Bottom axes with the same x and y axis
        upAxes = figProfile.add_subplot(gs_top[0, :])
        upAxes.yaxis.set_major_locator(
            mpl.ticker.LogLocator(
                base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
            )
        )
        upAxes.set_xlabel(r"$\rho$")
        upAxes.set_ylim([3e-2, 4])
        upAxes.set_xlim([0.9, 1.12])
        upAxes.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        upAxes.set_yscale("log")
        # The shared in time axes
        axNe = figProfile.add_subplot(gs_top[1, 0])
        axTe = figProfile.add_subplot(gs_top[1, 1])
        # Hide shared x-tick labels
        for ax in (axNe, axTe):
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())
            ax.set_xlim([0.92, 1.2])
            ax.set_xlabel(r"$\rho$")
        axNe.set_ylabel(r"n$_e [10^{19}$m$^{-3}$]")
        axTe.set_ylabel(r"T$_e$ [eV]")
        for _idxshot, (shot, col, tr) in enumerate(zip(shotlist, colorList, trlist)):
            Tree = mds.Tree("tcv_shot", shot)
            Data = Tree.getNode(r"\base::pd:pd_001")
            En = Tree.getNode(r"\results::fir:n_average")
            tEn = En.getDimensionAt().data()
            En = En.data()
            Pr = baratrons.pressure(shot).divertor
            tPr = Pr.t.values
            Pr = Pr.values
            _idx = np.where((tEn >= tr[0]) & (tEn <= tr[1]))[0]
            enLabel = (
                r"$\langle$n$_e\rangle$ = {:.2f}".format((En[_idx].mean() / 1e19))
                + r"$\times 10^{19}$ m$^{-3}$"
            )
            _idx = np.where((tPr >= tr[0]) & (tPr <= tr[1]))[0]
            PrLabel = "\nP = {:.1f} mPa".format((Pr[_idx].mean() * 1e3))
            label = enLabel + PrLabel
            if _idxshot == 0:
                upAxes.text(
                    0.05, 0.4, label, color=col, fontsize=20, transform=upAxes.transAxes
                )
                upAxes.text(0.05, 0.2, "Baffled", color=col, transform=upAxes.transAxes)
            else:
                upAxes.text(
                    0.57, 0.8, label, color=col, fontsize=20, transform=upAxes.transAxes
                )
                upAxes.text(
                    0.05, 0.1, "Unbaffled", color=col, transform=upAxes.transAxes
                )

            P = tcvProfiles(shot, interelm=True)
            En = P.profileNe(
                t_min=tr[0],
                t_max=tr[1],
                abscissa="sqrtpsinorm",
                interelm=True,
                rho=0.82,
                width=0.05,
            )
            _ = En.remove_points(
                np.logical_and(En.X.ravel() <= 0.9, En.X.ravel() >= 1.12)
            )
            rhoN = np.linspace(En.X.ravel().min(), 1.12, 120)
            yFit, yFitE, _, _ = P.gpr_robustfit(
                rhoN, density=True, temperature=False, regularization_parameter=6
            )
            _norm = yFit[np.argmin(np.abs(rhoN - 1))]
            upAxes.errorbar(
                En.X.ravel(),
                En.y / _norm,
                yerr=En.err_y / _norm,
                xerr=En.err_X.ravel(),
                fmt="o",
                ms=10,
                color=col,
                alpha=0.5,
            )
            upAxes.plot(rhoN, yFit / _norm, "-", color=col, lw=2)
            upAxes.fill_between(
                rhoN,
                (yFit - yFitE) / _norm,
                (yFit + yFitE) / _norm,
                color=col,
                alpha=0.3,
            )
            Target = langmuir.LP(shot)
            out = Target.UpStreamProfile(trange=tr)
            axNe.plot(out["rho"], out["en"] / 1e19, "o", color=col)
            axTe.plot(out["rho"], out["te"], "o", color=col)
        axNe.set_ylim([0.0, 4])
        axTe.set_ylim([0, 20])
        figProfile.savefig(
            "../pdfbox/ProfileShots{}_{}.pdf".format(shotlist[0], shotlist[1]),
            bbox_to_inches="tight",
        )
    elif selection == 15:
        shotlist = (68268, 64494)
        trlist = ([1.5, 1.85], [0.88, 0.96])
        # shotlist = (68268, 68268)
        # trlist = ([1.1, 1.3], [1.6, 1.7])
        colorList = ("#253B59", "#FF0C71")
        figProfile = mpl.pylab.figure(figsize=(9, 9), tight_layout=True)
        gs_top = mpl.pylab.GridSpec(
            2, 2, wspace=0.35, top=0.98, left=0.16, right=0.96, bottom=0.12,
        )
        # Bottom axes with the same x and y axis
        upAxes = figProfile.add_subplot(gs_top[0, :])
        upAxes.yaxis.set_major_locator(
            mpl.ticker.LogLocator(
                base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
            )
        )
        upAxes.set_xlabel(r"$\rho$")
        upAxes.set_ylim([3e-2, 4])
        upAxes.set_xlim([0.9, 1.12])
        upAxes.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        upAxes.set_yscale("log")
        # The shared in time axes
        axNe = figProfile.add_subplot(gs_top[1, 0])
        axTe = figProfile.add_subplot(gs_top[1, 1])
        # Hide shared x-tick labels
        for ax in (axNe, axTe):
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())
            ax.set_xlim([0.92, 1.2])
            ax.set_xlabel(r"$\rho$")
        axNe.set_ylabel(r"n$_e [10^{19}$m$^{-3}$]")
        axTe.set_ylabel(r"T$_e$ [eV]")
        for _idxshot, (shot, col, tr) in enumerate(zip(shotlist, colorList, trlist)):
            Tree = mds.Tree("tcv_shot", shot)
            Data = Tree.getNode(r"\base::pd:pd_001")
            En = Tree.getNode(r"\results::fir:n_average")
            tEn = En.getDimensionAt().data()
            En = En.data()
            Pr = baratrons.pressure(shot).divertor
            tPr = Pr.t.values
            Pr = Pr.values
            _idx = np.where((tEn >= tr[0]) & (tEn <= tr[1]))[0]
            enLabel = (
                r"$\langle$n$_e\rangle$ = {:.2f}".format((En[_idx].mean() / 1e19))
                + r"$\times 10^{19}$ m$^{-3}$"
            )
            _idx = np.where((tPr >= tr[0]) & (tPr <= tr[1]))[0]
            PrLabel = "\nP = {:.1f} mPa".format((Pr[_idx].mean() * 1e3))
            label = enLabel + PrLabel
            if _idxshot == 0:
                upAxes.text(
                    0.05, 0.4, label, color=col, fontsize=20, transform=upAxes.transAxes
                )
                upAxes.text(
                    0.05,
                    0.3,
                    r"N$_2$",
                    color=col,
                    fontsize=20,
                    transform=upAxes.transAxes,
                )
            else:
                upAxes.text(
                    0.57, 0.8, label, color=col, fontsize=20, transform=upAxes.transAxes
                )

            P = tcvProfiles(shot, interelm=True)
            En = P.profileNe(
                t_min=tr[0],
                t_max=tr[1],
                abscissa="sqrtpsinorm",
                interelm=True,
                rho=0.82,
                width=0.05,
            )
            _ = En.remove_points(
                np.logical_and(En.X.ravel() <= 0.9, En.X.ravel() >= 1.12)
            )
            rhoN = np.linspace(En.X.ravel().min(), 1.12, 120)
            yFit, yFitE, _, _ = P.gpr_robustfit(
                rhoN, density=True, temperature=False, regularization_parameter=6
            )
            _norm = yFit[np.argmin(np.abs(rhoN - 1))]
            upAxes.errorbar(
                En.X.ravel(),
                En.y / _norm,
                yerr=En.err_y / _norm,
                xerr=En.err_X.ravel(),
                fmt="o",
                ms=10,
                color=col,
                alpha=0.5,
            )
            upAxes.plot(rhoN, yFit / _norm, "-", color=col, lw=2)
            upAxes.fill_between(
                rhoN,
                (yFit - yFitE) / _norm,
                (yFit + yFitE) / _norm,
                color=col,
                alpha=0.3,
            )
            Target = langmuir.LP(shot)
            out = Target.UpStreamProfile(trange=tr)
            axNe.plot(out["rho"], out["en"] / 1e19, "o", color=col)
            axTe.plot(out["rho"], out["te"], "o", color=col)
        axNe.set_ylim([0.0, 4])
        axTe.set_ylim([0, 20])
        figProfile.savefig(
            "../pdfbox/ProfileShots{}_{}.pdf".format(shotlist[0], shotlist[1]),
            bbox_to_inches="tight",
        )
    elif selection == 16:
        shotList = (68266, 64950)
        colorList = ("#FF0C71", "#011C40")
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=2)
        fig.subplots_adjust(bottom=0.15)
        for shot, col, _ax in zip(shotList, colorList, ax):
            if shot == 68266:
                tcvgeom.tcvview(shot, 1.2, ax=_ax, col=col)
            else:
                tcvgeom.tcvview(shot, 1.2, ax=_ax, baffles="all", col=col)
            _ax.autoscale(enable=True, axis="both", tight=True)
            _ax.xaxis.set_major_locator(MaxNLocator(2))
        fig.savefig(
            "../pdfbox/TCVEquilibra_{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inchest="tight",
        )
    elif selection == 17:
        shotList = (68265, 68266, 68267)
        tList = (0.95, 1.4, 1.7)
        colorList = ("#0CA7DB", "#F56C05", "#EB0C6C")
        fig, ax = mpl.pylab.subplots(figsize=(7, 10), nrows=2, ncols=1)
        fig.subplots_adjust(hspace=0.25, bottom=0.13, left=0.2)
        for shot, t0, col in zip(shotList, tList, colorList):
            if shot != 68267:
                tr = [t0-0.1, t0+0.1]
            else:
                tr = [t0-0.2, t0]
            Tree = mds.Tree("tcv_shot", shot)
            En = Tree.getNode(r"\results::fir:n_average")
            tEn = En.getDimensionAt().data()
            En = En.data()
            Pr = baratrons.pressure(shot).divertor
            tPr = Pr.t.values
            Pr = Pr.values
            _idx = np.where((tEn >= t0 - 0.1) & (tEn <= t0 + 0.1))[0]
            enLabel = (
                r"$\langle$n$_e\rangle$ = {:.2f}".format((En[_idx].mean() / 1e19))
                + r"$\times 10^{19}$ m$^{-3}$"
            )
            _idx = np.where((tPr >= tr[0]) & (tPr <= tr[1]))[0]
            PrLabel = "\nP = {:.1f} mPa".format((Pr[_idx].mean() * 1e3))
            label = enLabel + PrLabel
            P = tcvProfiles(shot, interelm=True)
            En = P.profileNe(
                t_min=tr[0],
                t_max=tr[1],
                abscissa="sqrtpsinorm",
                interelm=True,
                rho=0.82,
                width=0.05,
            )
            _ = En.remove_points(
                np.logical_and(En.X.ravel() <= 0.9, En.X.ravel() >= 1.12)
            )
            rhoN = np.linspace(En.X.ravel().min(), 1.12, 120)
            yFit, yFitE, _, _ = P.gpr_robustfit(
                rhoN, density=True, temperature=False, regularization_parameter=6
            )
            _norm = yFit[np.argmin(np.abs(rhoN - 1))]
            ax[0].errorbar(
                En.X.ravel(),
                En.y / _norm,
                yerr=En.err_y / _norm,
                xerr=En.err_X.ravel(),
                fmt="o",
                ms=10,
                color=col,
                alpha=0.5,
            )
            ax[0].plot(rhoN, yFit / _norm, "-", color=col, lw=2, label=label)
            ax[0].fill_between(
                rhoN,
                (yFit - yFitE) / _norm,
                (yFit + yFitE) / _norm,
                color=col,
                alpha=0.3,
            )
            try:
                Target = langmuir.LP(shot)
                ax[1].plot(
                    bn.move_mean(Target.t2, 30),
                    bn.move_mean(Target.TotalSpIonFlux() / 1e22, 50),
                    color=col,
                )
            except:
                pass
            ax[1].axvline(t0, linestyle="--", color=col, lw=2)
        ax[0].legend(loc=0, numpoints=1, frameon=False, fontsize=12)
        ax[0].set_xlabel(r"$\rho$")
        ax[0].set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax[1].set_xlabel(r"t[s]")
        ax[1].set_ylabel(r"$\int$j$_s$ [10$^{23}$ ion/s]")
        ax[0].set_yscale('log')
        ax[0].set_ylim([1e-2, 5])
        ax[0].set_xlim([0.92, 1.11])
        fig.savefig(
            "../pdfbox/UpstreamProfiles_{}_{}_{}.pdf".format(
                shotList[0], shotList[1], shotList[2]
            ),
            bbox_to_inches="tight",
        )
    elif selection == 18:
        shotlist = (64494, 64947)
        colorList = ("#253B59", "#D92534")
        trlist = ([0.88, 0.96], [1.5, 1.75])

    elif selection == 99:
        loop = False
    else:
        input("Unknown Option Selected!")
