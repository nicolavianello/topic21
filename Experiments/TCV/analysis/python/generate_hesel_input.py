## script to generate the input scripts for running
## hesel simulations. We need to provide in ASCII format
## the following quantities
## 1. Density profiles [-4, 2] in r-rsep
## 2. Te profiles [-4, 2] in r-rsep
## 3. Ion temperature profiles in r-rsep
## 4. Lparallel in r-rsep upstream-midplane
## 5. Input power
import tcvProfiles
import eqtools
import matplotlib as mpl
import MDSplus as mds
import numpy as np


shotList = (64465, 64432)
timeList = (1.45, 1.45)
for shot, tL in zip(shotList, timeList):
    # load the profiles
    P = tcvProfiles.tcvProfiles(shot)
    Eq = eqtools.TCVLIUQEMATTree(shot)
    if shot <= 64000:
        if shot == 57497:
            # i dati della temperature ionica devono
            # essere caricati per tutto l'impulso all'inizio
            Tree = mds.Tree('tcv_shot', shot)
            Ti = Tree.getNode(r'\results::cxrs:ti').data()
            tTi = Tree.getNode(
                r'\results::cxrs:ti').getDimensionAt(1).data()
            rhoTi = Tree.getNode(
                r'\results::cxrs:ti').getDimensionAt(0).data()
            TiErr = Tree.getNode(r'\results::cxrs:ti:err').data()
            Tree.quit
        for t in tL:
            # Directory
            directory = '../data/hesel_input/{}/'.format(shot)
            # create the plot (will be a column plot with ne, te and ti)
            if shot != 57497:
                fig, ax = mpl.pylab.subplots(
                    figsize=(7, 9),
                    ncols=1, nrows=2, sharex=True)
            else:
                fig, ax = mpl.pylab.subplots(
                    figsize=(7, 7),
                    ncols=1, nrows=3, sharex=True)

            Rmid = Eq.getRmidOutSpline()(t)
            # ---------------
            # density profile
            En = P.profileNe(
                t_min = t-0.05,
                t_max = t+0.05,
                abscissa='Rmid')
            if shot == 57437 and t == 0.63:
                _ = En.remove_points(
                    (En.X[:, 0] >= 1.0944) & (En.X[:, 0] <= 1.1))
            # do the fit and create the plot
            RFake = np.linspace(
                En.X.ravel().min(),
                En.X.ravel().max(), 100)
            yN, yE, _,_ = P.gpr_robustfit(RFake)
            ax[0].errorbar(En.X.ravel()-Rmid,
                           En.y, yerr=En.err_y, fmt='o',
                           color='#014873')
            ax[0].plot(RFake-Rmid, yN, 'k--', lw=2)
            ax[0].fill_between(RFake-Rmid, yN-yE, yN+yE,
                               color='#67696E', alpha=0.5)
            # raw data
            File = directory + 'density_t%2.1f.txt' % t
            np.savetxt(File,
                       np.c_[En.X.ravel()-Rmid, En.y, En.err_y],
                       header='R-Rs, Density [10^20 m^-3], Err')
            # fitted data
            File = directory + 'densityFitted_t%2.1f.txt' % t
            np.savetxt(File, zip(RFake-Rmid, yN, yE),
                       header='R-Rs, Density [10^20 m^-3], Err')
            # --------------
            # temperature profile
            Te = P.profileTe(t_min = t-0.05,
                             t_max = t+0.05,
                             abscissa='Rmid')
            RFake = np.linspace(
                Te.X.ravel().min(),
                Te.X.ravel().max(), 100)
            yN, yE, gp = P.gpr_robustfit(
                RFake, density=False, temperature=True)
            ax[1].errorbar(Te.X.ravel()-Rmid,
                           Te.y, yerr=Te.err_y, fmt='o',
                           color='#014873')
            ax[1].plot(RFake-Rmid, yN, 'k--', lw=2)
            ax[1].fill_between(RFake-Rmid, yN-yE, yN+yE,
                               color='#67696E', alpha=0.5)

            # save the data
            File = directory + 'Electrontemperature_t%2.1f.txt' % t
            np.savetxt(File, zip(Te.X.ravel()-Rmid, Te.y, Te.err_y),
                       header='R-Rs, Te [eV], Err')
            # fitted data
            File = directory + 'ElectrontemperatureFitted_t%2.1f.txt' % t
            np.savetxt(File, zip(RFake-Rmid, yN, yE),
                       header='R-Rs, Te [eV], Err')

            # load also the parallel connection length
            Tree = mds.Tree('tcv_topic21', shot)
            Lp = Tree.getNode(r'\LPDIVU').data()
            Rrsep = Tree.getNode(r'\LPDIVU').getDimensionAt(1).data()
            LpTime = Tree.getNode(r'\LPDIVU').getDimensionAt(0).data()
            idx = np.where((LpTime >= t-0.2) &
                           (LpTime <= t+0.2))[0]
            LLp = np.nanmean(Lp[idx, :], axis=0)
            File = directory + 'ParallelConnection_t%2.1f.txt' % t
            np.savetxt(File, zip(Rrsep, LLp), header='R-Rsep Lparallel')
            if shot == 57497:
                # -------
                # now the ion temperature
                idx = np.where((tTi >= t-0.1) &
                               (tTi <= t+0.1))[0]
                Y = np.nanmean(Ti[idx, :], axis=0)
                Xrho = np.nanmean(rhoTi[idx, :], axis=0)
                E = np.nanmean(TiErr[idx, :], axis=0)
                X = Eq.psinorm2rmid(np.power(Xrho, 2), t) - Rmid
                ax[2].errorbar(X, Y, yerr=E, fmt='o',
                               color='#014873')
                File = directory + 'Iontemperature_t%2.1f.txt' % t
                np.savetxt(File, zip(X, Y, E),
                           header='R-Rs, Ti [eV], Err')
                ax[1].axes.get_xaxis().set_visible(False)
                ax[2].set_xlabel(r'R-R$_{lcfs}$ [m]')
                ax[2].set_ylabel(r'T$_i$ [eV]')
                ax[2].set_ylim([0, 700])
            else:
                ax[1].set_xlabel(r'R-R$_{lcfs}$ [m]')

            ax[0].axes.get_xaxis().set_visible(False)
            ax[0].set_ylabel(r'n$_e [10^{20}$m$^{-3}]$')
            ax[1].set_ylabel(r'nT$_e$ [eV]')
            ax[0].set_ylim([0, 1.])
            ax[1].set_ylim([0, 1200])
            ax[0].set_title(r'# %5i' % shot +' t = %2.1f' % t)
            fig.savefig(
                '../data/hesel_input/{}/Profiles_t{}.pdf'.format(shot, t),
                bbox_to_inches='tight')
    else:
        directory = '../data/hesel_input/{}/'.format(shot)
        if shot == 64465:
            div=False
        else:
            div=True

        tmin, tmax = tL-0.02,tL+0.02
        Rmid = Eq.getRmidOutSpline()(tL)
        fig, ax = mpl.pylab.subplots(figsize=(6,7), nrows=2, ncols=1, sharex=True)
        P = tcvProfiles.tcvProfiles(shot)
        En = P.profileNe(t_min=tmin, t_max=tmax, abscissa='Rmid', remove_divertor=div, rho_threshold=1.03)

        RFake = np.linspace(
            En.X.ravel().min(),
            En.X.ravel().max(), 100)
        yN, yE, _, _ = P.gpr_robustfit(RFake)
        ax[0].errorbar(En.X.ravel() - Rmid,
                       En.y, yerr=En.err_y, fmt='o',
                       color='#014873')
        ax[0].plot(RFake - Rmid, yN, 'k--', lw=2)
        ax[0].fill_between(RFake - Rmid, yN - yE, yN + yE,
                           color='#67696E', alpha=0.5)
        # raw data
        File = directory + 'density_t%2.1f.txt' % tL
        np.savetxt(File,
                   np.c_[En.X.ravel() - Rmid, En.y, En.err_y],
                   header='R-Rs, Density [10^20 m^-3], Err')
        # fitted data
        File = directory + 'densityFitted_t%2.1f.txt' % tL
        np.savetxt(File, np.c_[RFake - Rmid, yN, yE],
                   header='R-Rs, Density [10^20 m^-3], Err')
        # --------------
        # temperature profile
        P = tcvProfiles.tcvProfiles(shot)
        Te = P.profileTe(t_min=tmin,
                         t_max=tmax,
                         abscissa='Rmid',
                         remove_divertor=div, rho_threshold=1.03)
        RFake = np.linspace(
            Te.X.ravel().min(),
            Te.X.ravel().max(), 100)
        yN, yE, _, _ = P.gpr_robustfit(
            RFake, density=False, temperature=True)
        ax[1].errorbar(Te.X.ravel() - Rmid,
                       Te.y, yerr=Te.err_y, fmt='o',
                       color='#014873')
        ax[1].plot(RFake - Rmid, yN, 'k--', lw=2)
        ax[1].fill_between(RFake - Rmid, yN - yE, yN + yE,
                           color='#67696E', alpha=0.5)

        # save the data
        File = directory + 'Electrontemperature_t%2.1f.txt' % tL
        np.savetxt(File, np.c_[Te.X.ravel() - Rmid, Te.y, Te.err_y],
                   header='R-Rs, Te [eV], Err')
        # fitted data
        File = directory + 'ElectrontemperatureFitted_t%2.1f.txt' % tL
        np.savetxt(File, np.c_[RFake - Rmid, yN, yE],
                   header='R-Rs, Te [eV], Err')
        ax[0].tick_params(labelbottom=False)
        ax[0].set_ylabel(r'n$_e [10^{20}$m$^{-3}$]')
        ax[1].set_xlim([-0.2, 0.03])
        ax[1].set_ylabel(r'T$_e$ [eV]')
        ax[1].set_ylim([0, 800])
        ax[0].set_ylim([0, 2])
        ax[0].set_title(r'#{} @ {:.2f}'.format(shot,tL))
        ax[1].set_xlabel(r'R-R$_s$ [m]')
        fig.savefig(
            '../data/hesel_input/{}/Profiles_t{}.pdf'.format(shot, tL),
            bbox_to_inches='tight')
