import sys
sys.path.append('/home/vianello/NoTivoli/work/topic21/Codes/python/tcv/')
import topic21Mds
import numpy as np
import warnings
shotList = (53575, 61806, 61808, 61860, 61964)
for shot in shotList:
    Out=topic21Mds.Tree(shot)
    Out.toMds()
    warnings.warn('Pulse file for shot {} written'.format(shot))
