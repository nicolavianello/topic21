This README file is for the TCV shots taken in W38-2019 for the T21. Data stored in the folders 64157-64160, 64167-64173, 64209

Datafile are made for C-HESEL. 

Naming of the datafiles: shot.Time.alpha.TCV_SI.dat

The 4 coulums in the datafiles are organised as:
r-rsep [m]  Electron density [10^19 m^-3] Electron temperature [eV] Ion temperature [eV]

Ion temperature is not known and are set to Ti = alpha*Te, where alpha is given in the file name.

Data are extracted using the matlab script tcv_ts_profiles_v2.m found in the Matlab folder. 

Transform of the rho_polodal coordinate to R on outboard midplane on magnetic axis
R  = tcv_coordinate_conversion(shot,1.0,tefit.dim{2},0,'p',[]);