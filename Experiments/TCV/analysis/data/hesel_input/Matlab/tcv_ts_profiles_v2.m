function	tcv_ts_profiles(shot,times)

% addpath('/home/behn/public/ts');

%--- get the data ---
mdsopen(shot)

nefit=tdi('\results::thomson.profiles.auto:ne'); % first-dim:time second-dim:radial
tefit=tdi('\results::thomson.profiles.auto:te'); % first-dim:time second-dim:radial
%tifit=tdi('\results::cxrs:ti');                  % first-dim:radial second-dim:time

q95=tdi('\results::q_95');     % only time signal
qedge=tdi('\results::q_edge'); % only time signal

vers=mdsdata('\results::thomson.profiles.auto:te:version_num');

mdsclose

%
%--- check out the AUTOFIT version ----------------------------
%
fprintf(' AUTOFIT version is : %3.2f\n', vers)
if (vers>=2.99)
    it=1;		% new, correct ordering
    ir=2;
else
    it=2;		% old ordering
    ir=1;
end


% [shot_data]=show_autofits(shot);
rho  = nefit.dim{ir};
rho  = tefit.dim{ir};
time = nefit.dim{it};
time = tefit.dim{it};

m= size(nefit.data)';
me = m(1);

% Transform the rho_polodal coordinate to R and on outboard midplane on
% magnetic axis
R  = tcv_coordinate_conversion(shot,1.0,tefit.dim{2},0,'p',[]);


% Find plot time
ss = {'   ','   ','   ','   ','   ','   ','    ','   '};
for i=1:size(times,2)
    [val,idx(i)] = min(abs(time'-times(i)));
    ss{i} = num2str(time(idx(i)),'%3.2f');
end

% write the data to ascii file for input to HESEL
alpha = 1; % Ion pressure not known so we use electron temperature multiplied with alpha
lcfs  = R.R(end); % LCFS is end of R-data 
for i=1:size(times,2)
    ff= fopen([num2str(shot) '.' num2str(times(i)) '.' num2str(alpha) '.TCV_SI.dat'],'w');
    for j=1:size(R.R,1)
        Ne = nefit.data(idx(i),j)/1.e19;
        Te = tefit.data(idx(i),j);
        Ti = alpha*Te;
        fprintf(ff,'%g %g %g %g\n',R.R(j)-lcfs,Ne,Ne.*Te,Ne*Ti);
    end
    for j=1:40
        xx = j*0.0025; % 2.5 mm resolution i SOL
        Ne = nefit.data(idx(i),end)*exp(-xx/0.01)/1.e19;
        Te = tefit.data(idx(i),end)*exp(-xx/0.01);
        Ti = alpha*Te;
        fprintf(ff,'%g %g %g %g\n',xx,Ne,Ne.*Te,Ne*Ti);
    end
    fclose(ff);
end

% set colors
cc{1} ='b'; cc{2}='r';cc{3}='g';cc{4}='k';cc{5}='--b';cc{6}='--r';cc{7}='--g';

hand=figure('Position',[100 100 800 800]);
row = 3; 
col = 2;

title(['Data for shot # ',num2str(shot)]);

subplot(row,col,1)
for i=1:size(times,2)
    plot(R.R,nefit.data(idx(i),:),cc{i});
    hold on
end
legend(ss);
legend('boxoff')
title(['Thomson. #' ,num2str(shot)]);
ylabel('n_e [m^{-3}]')
xlabel('m')
set(gca,'XLim',[R.R(1),R.R(end)]);


subplot(row,col,3)
for i=1:size(times,2)
    plot(R.R,tefit.data(idx(i),:),cc{i});
    hold on
end
legend(ss);
legend('boxoff')
title(['Thomson #' ,num2str(shot)]);
ylabel('T_e [eV]');
xlabel('m')
set(gca,'XLim',[R.R(1),R.R(end)]);

subplot(row,col,5)
e = 1.602e-19;
for i=1:size(times,2)
    plot(R.R,e*nefit.data(idx(i),:).*tefit.data(idx(i),:),cc{i});
    hold on
end
legend(ss);
legend('boxoff')
title(['Thomson. #' ,num2str(shot)]);
ylabel('P_e [Pa]')
xlabel('m')
set(gca,'XLim',[R.R(1),R.R(end)]);


subplot(row,col,2)
plot(time(:,1),nefit.data(:,1),'r');
hold on
plot(time(:,1),nefit.data(:,end),'b');
title('Density'); 
legend('Core','LCFS');
legend('boxoff')
ylabel('n_e [m^{-3}]') 
xlabel('[s]')

subplot(row,col,4)
plot(time(:,1),tefit.data(:,1),'r');
hold on
plot(time(:,1),tefit.data(:,end),'b');
title('Electron temp'); 
legend('Core','LCFS');
legend('boxoff')
ylabel('T_e [eV]');
xlabel('[s]')


subplot(row,col,6)
plot(q95.dim{it},q95.data(:,1),'r');
hold on
plot(qedge.dim{it},qedge.data(:,1),'b');
legend('q_{95}','q_{edge}');
legend('boxoff')
ylabel('q') 
xlabel('[s]')

print('-djpeg',['tcv_ts_profiles_v2.' num2str(shot) '.jpg']);
saveas(hand,   ['tcv_ts_profiles_v2.' num2str(shot) '.fig']);
