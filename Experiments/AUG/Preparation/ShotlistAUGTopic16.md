# Shotlists for TOPIC 16


## Week 15 Shotlist
Available number of Shots allocated for the experiment is **8** to be performed in Calendar week 15/2019.
The plan proposed address two points of the general program

* Check differences between fueling induced and seeding induced detachment in L-Mode to verify different upstream behavior
* Power dependence of H-Mode shoulder

### L-Mode plasmas


### H-Mode plasmas


### Dignostics
- [ ] Midplane manipulator
- [ ] Li-Beam. 
- [ ] Reflectometer also ICRH Reflectometer. 
- [ ] Lower and Upper Divertor probes
- [ ] Camera for neutral analysis
- [ ] Infrared (to monitor probe head)
- [ ] Divertor Spectroscopy
- [ ] AXUV in the divertor region
- [ ] THB
- [ ] GPI
- [ ] Doppler Reflectometry

### List of actions
- [ ] Check for old AUG15-2.2-4 H-Mode shots --> N. Vianello
- [ ] Check for old Leena shot for N2 induced seeding detachment --> N. Vianello
- [ ] Check feasibility of He-filter for THB beam-line in order to use both GPI and THB --> Michael, Istvan
- [ ] Determine exact values and timing of power for power scan --> Nicola and Nick
- [ ] Determine exact values and timing for fueling --> Nicola and Nick
- [ ] Ask for D2 line instead of N2 lines
- [ ] Ask for cryo-off for H-Mode part of the discharge. --> Nicola in the shot proposal
- [ ] Prepare AUG shot-proposal
