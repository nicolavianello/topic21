# Shotlist for TOPIC 21: AUG

Available number of Shots allocated for the experiment is **9** to be performed in Calendar week 9/2019.
We propose the following shotlist which should accomodate the 3 different programs:

* L-Mode DN shoulder formation at different d<sub>sep</sub>
* H-Mode DN shoulder formation at different d<sub>sep</sub> with maximum available power
* L-Mode investigation with different strike point distances from pumping throat.

## Week 12 Shotlist
### DN L-Mode plasmas
1. **USN Conditioning** discharge I<sub>p</sub>=0.8MA,  B<sub>t</sub> = -2.4T. Reference shot 28647 without ICRH. Take Equilibrium at 3s
2. **L-Mode in USN, DN, LSN** I<sub>p</sub>=0.9MA,  B<sub>t</sub> = -2.5T. Reference shot 32064 with 0.5 MW NBI. Programmed discharge **/edge/DP_0MA8_USN_DN_LSN.xml**. MEM strokes 2, 3.35, 4, 4.6, 5, 5.9. Xprobe = 1790. XP-strokes 2, 3, 4s
3. I<sub>p</sub>=0.8MA,  B<sub>t</sub> = -2.5T, NBI=0.5MW from 1-5.4s,  Fueling ramp as 33339, DN configuration with large d<sub>sep</sub> (previous reference around 4.5s). MEM Plunge 1.5, 2.1, 3.3, 4, 4.5,5.2. MEM position = 1790 (as 34102) XP plunge 2.1,3.3,4.5
4. I<sub>p</sub>=0.8MA,  B<sub>t</sub> = -2.5T, NBI=0.5MW from 1-5.4s,  Fueling ramp as 33339, DN configuration with small d<sub>sep</sub> as close as possible to connected DN (like 35710 around 3.3s). MEM Plunge 1.5, 2.1, 3.3, 4, 4.5,5.2. MEM position = 1790 (as 34102) XP plunge 2.1,3.3,4.5

### Large Strike Point Sweep
5. I<sub>p</sub>=0.8MA,  B<sub>t</sub> = -2.5T, reference 33339. Programmed discharge is **/edge/DP_0MA8_SPsweep.xml**. MEM strokes at 1.8, 2.4, 3.1, 3.7, 4.4, 5. XP strokes at 2.4,  3.1,  4.4

### DN In H-Mode plasmas
6. Equilibrium, current and field as in Shot #2. H-Mode plasma with heating (NBI:4.1 mW 2.25-6.3s, ECH: 1.785 MW) and fueling/seeding scheme as shot 34281. Eventually reduce ECH heating if needed in order to avoid damage of upper divertor. MEM Plunge 1.5, 2.1, 3.3, 4, 4.5,5.2. MEM position = 1773 (as 34102), XP plunge 2.1,3.3,4.5
7. Equilibrium, current and field as in Shot #3. H-Mode plasma with heating and fueling/seeding scheme as shot 34281. Eventually reduce ECH heating if needed in order to avoid damage of upper divertor. MEM Plunge 1.5, 2.1, 3.3, 4, 4.5,5.2. MEM position = 1773 (as 34102) XP plunge 2.1,3.3,4.5


### Large Strike Point Sweep
8. Fixed density (0.5 n/nG or 0.8 n/nGw to be decided according to shot #4). Shots with 3 plateau, 1 second long each. In the first plateau SP sweeping at 4 Hz amplitude 5cm. In the second and third plateau keep the SP fixed at closest and more distance position and check for upstream profile variation. MEM strokes at 1.8, 2.4, 3.1, 3.7, 4.4, 5. XP strokes at 2.4,  3.1,  4.4

### Contingency
9. Contingency


## Diagnostics
- [ ] Midplane manipulator
- [ ] Li-Beam. 
- [ ] Reflectometer also ICRH Reflectometer. 
- [ ] Lower and Upper Divertor probes
- [ ] Camera for neutral analysis
- [ ] Infrared (to monitor probe head)
- [ ] Divertor Spectroscopy
- [ ] AXUV in the divertor region
- [ ] THB
- [ ] GPI
- [ ] Doppler Reflectometry

