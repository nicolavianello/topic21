import sys
from collections import OrderedDict
import numpy as np
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib.ticker import AutoMinorLocator
import MDSplus as mds
from scipy import io, constants
import pandas as pd
import gpr1dfusion
import seaborn as sns
import bin_by
import bottleneck as bn
from myThbObject import myThbObject

if int(sys.version_info[0]) < 3:
    import eqtools
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rc("font", size=22)
mpl.rc('font', **{'family': 'sans-serif', 'sans-serif': ['Helvetica']})
mpl.rc("lines", linewidth=2)
import cmocean

if int(sys.version_info[0]) >= 3:
    import plasmapy.physics.parameters as plasma
    from astropy import units as u


def print_menu():
    print(30 * "-", "MENU", 30 * "-")
    print("1. Shot 35879  - Plot with Edge density with upstream profiles ")
    print("2. Shot 35879 - Plot the evolution of lower and upper outer divertor")
    print("3. Plot Radial correlation length from THB with probe from LSN and velocity distributions")
    print("4. Compare shot 33339 and 35879 same greenwald fraction")
    print("99. Exit")
    print(67 * "-")

loop = True

while loop:
    print_menu()
    selection = int(input('Enter your choice [1-99] '))
    # noinspection PyUnboundLocalVariable
    if selection == 1:
        shot = 35879
        tRangesL = ((1.95, 2.1), (2.45, 2.6), (3.09, 3.2))
        colorL = ('#00D3E6', '#8ABA07', '#BC2764')
        fig, ax = mpl.pylab.subplots(figsize=(6, 8.5), nrows=2, ncols=1)
        fig.subplots_adjust(hspace=0.22, bottom=0.1, top=0.95, right=0.95, left=0.2)
        for _ax in ax.flatten():
            _ax.xaxis.set_minor_locator(AutoMinorLocator())
        ax[1].yaxis.set_major_locator(
            mpl.ticker.LogLocator(base=10.0, numticks=15,
                                  subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)))

        conn = mds.Connection('localhost:8001')
        # edge Data
        eN = conn.get(
            'augsignal({}, "TOT", "H-5_corr")'.format(shot)).data() / 1e20
        time = conn.get(
            'dim_of(augsignal({}, "TOT", "H-5_corr")'.format(
                shot) + ')').data()
        ax[0].plot(time, eN, '-k', lw=2)
        _s = 'augdiag(' + str(shot) + ',"LIN","ne", "LIBE")'
        _st = 'dim_of(augdiag(' + str(shot) + ',"LIN","ne", "LIBE"), 0)'
        _sr = 'dim_of(augdiag(' + str(shot) + ',"LIN","ne", "LIBE"), 1)'
        LiBes = conn.get(_s).data()
        LiTime = conn.get(_st).data()
        LiRho = conn.get(_sr).data()
        nG = float(0.8) / (np.pi * np.power(0.5, 2))
        for _idxt, (_t, _c) in enumerate(zip(tRangesL, colorL)):
            ax[0].axvline(np.asarray(_t).mean(), ls='--', color=_c)
            _dummyTime = np.where((LiTime >= _t[0]) & (LiTime <= _t[1]))
            _dummyLib = LiBes[:, _dummyTime].ravel() / 1e19
            _dummyRho = LiRho[:, _dummyTime].ravel()
            _idx = np.where((time >= _t[0]) & (time <= _t[1]))[0]
            nGlabel = eN[_idx].mean() / nG
            yOut, bins, bin_means, bin_width, xOut = bin_by.bin_by(
                _dummyRho, _dummyLib, nbins=30)
            xB = np.asarray([np.nanmean(k) for k in xOut])
            xBE = np.asarray([np.nanstd(k) for k in xOut])
            yB = np.asarray([np.nanmean(k) for k in yOut])
            yBE = np.asarray([np.nanstd(k) for k in yOut])
            xN = np.linspace(0.95, 1.08, 200)
            # be sure to include rho = 1
            xN = np.append(xN, 1)
            xN = xN[np.argsort(xN)]
            # perform the fit
            gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                xB, yB, yBE,
                xe=xBE, nrestarts=100, length_scale=0.5,
                epsilon=5.e-3, regularization_parameter=1)
            gp.GPRFit(xN)
            yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
            enS = yFit[np.argmin(np.abs(xN - 1))]
            ax[1].semilogy(_dummyRho, _dummyLib / enS,
                           '.', color=_c, ms=1, alpha=0.3)
            ax[1].semilogy(xN, yFit / enS, '-', lw=2, color=_c,
                           label=r'n$^e$/n$_G$ = {:.1f}'.format(nGlabel))
            ax[1].fill_between(xN,
                               (yFit - yFitE / 2) / enS,
                               (yFit + yFitE / 2) / enS,
                               facecolor=_c,
                               edgecolor='none',
                               alpha=0.3)
            ax[1].text(0.05, 0.4 - 0.1 * _idxt, r'n$^e$/n$_G$ = {:.2f}'.format(nGlabel),
                       transform=ax[1].transAxes, color=_c)
        ax[1].set_xlabel(r'$\rho$')
        ax[1].set_ylabel(r'n$_e$/n$_e(\rho=1)$')
        ax[0].set_xlabel(r't [s]')
        ax[0].set_ylabel(r'n$^e [10^{19}$m$^{-3}$]')
        ax[1].set_ylim([2e-2, 3])
        ax[1].set_xlim([0.98, 1.1])
        ax[0].set_ylim([0, 2.0])
        ax[0].set_title(r'#{}'.format(shot))
        fig.savefig('../pdfbox/EdgeDensityUpstreamProfileShot{}.pdf'.format(shot),
                    bbox_to_inches='tight')
    elif selection == 2:
        shot = 35879
        OuterLower = OrderedDict([
            ('ua1', {'R': 1.582, 'z': -1.199, 's': 1.045}),
            ('ua2', {'R': 1.588, 'z': -1.175, 's': 1.070}),
            ('ua3', {'R': 1.595, 'z': -1.151, 's': 1.094}),
            ('ua4', {'R': 1.601, 'z': -1.127, 's': 1.126}),
            ('ua5', {'R': 1.608, 'z': -1.103, 's': 1.158}),
            ('ua6', {'R': 1.614, 'z': -1.078, 's': 1.189}),
            ('ua7', {'R': 1.620, 'z': -1.054, 's': 1.213}),
            ('ua8', {'R': 1.627, 'z': -1.030, 's': 1.246}),
            ('ua9', {'R': 1.640, 'z': -0.982, 's': 1.276})])
        InnerLower = OrderedDict([
            ('ui9', {'R': 1.288, 'z': -0.959, 's': 0.339}),
            ('ui8', {'R': 1.281, 'z': -0.993, 's': 0.373}),
            ('ui7', {'R': 1.275, 'z': -1.011, 's': 0.391}),
            ('ui6', {'R': 1.269, 'z': -1.029, 's': 0.411}),
            ('ui5', {'R': 1.262, 'z': -1.047, 's': 0.429}),
            ('ui4', {'R': 1.256, 'z': -1.065, 's': 0.448}),
            ('ui3', {'R': 1.250, 'z': -1.083, 's': 0.468}),
            ('ui2', {'R': 1.244, 'z': -1.101, 's': 0.486}),
            ('ui1', {'R': 1.238, 'z': -1.119, 's': 0.505})])
        OuterUpper = OrderedDict([
            ('oa5', {'R': 1.637, 'z': 1.139}),
            ('oa4', {'R': 1.606, 'z': 1.152}),
            ('oa3', {'R': 1.574, 'z': 1.164}),
            ('oa2', {'R': 1.544, 'z': 1.176}),
            ('oa1', {'R': 1.515, 'z': 1.188})])
        InnerUpper = OrderedDict([
            ('oi1', {'R': 1.477, 'z': 1.172}),
            ('oi2', {'R': 1.460, 'z': 1.162}),
            ('oi3', {'R': 1.443, 'z': 1.153}),
            ('oi4', {'R': 1.427, 'z': 1.144}),
            ('oi5', {'R': 1.410, 'z': 1.135}),
            ('oi6', {'R': 1.377, 'z': 1.116}),
            ('oi7', {'R': 1.360, 'z': 1.107}),
            ('oi8', {'R': 1.344, 'z': 1.098})])

        fig, ax = mpl.pylab.subplots(figsize=(6, 4), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.2, top=0.95, right=0.95, left=0.2)
        c = mds.Connection('localhost:8001')
        # computation of outer target integrated ion flux
        outDivSignal = np.asarray([])
        neTime = c.get(
            'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(
                shot, 'ua1')).data()
        for i, s in enumerate(OuterLower.keys()):
            _ne = c.get(
                'augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
            _te = c.get(
                'augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
            _an = c.get(
                'augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
            _cs = np.sqrt(constants.e * 4 * _te / (2 * constants.m_p))
            # this is the ion flux
            _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

            if i == 0:
                outDivSignal = _s
            else:
                outDivSignal = np.vstack((outDivSignal,
                                          _s))
        # now we compute the total integrate ion flux
        outTarget = np.zeros(neTime.size)
        for i in range(neTime.size):
            # _x = np.asarray([OuterLower[k]['s']
            #                  for k in OuterLower.keys()])
            _xr = np.asarray([OuterLower[k]['R']
                              for k in OuterLower.keys()])
            _zr = np.asarray([OuterLower[k]['z']
                              for k in OuterLower.keys()])
            _x = np.sqrt(np.power(_xr, 2) + np.power(_zr, 2))
            _r = np.asarray([OuterLower[k]['R']
                                 for k in OuterLower.keys()])
            _y = outDivSignal[:, i]
            indices = ~(np.isnan(_x) | np.isnan(_y))
            # _dummy = np.vstack((_x, _y)).transpose()
            # _dummy = _dummy[~np.isnan(_dummy).any(1)]
            _x = _x[indices]
            _y = _y[indices]
            # _dummy = np.vstack((_x, _y)).transpose()
            # _dummy = _dummy[~np.isnan(_dummy).any(1)]
            # _x = _dummy[:, 0]
            # _y = _dummy[:, 1][np.argsort(_x)]
            _x = np.sort(_x)
            outTarget[i] = 2 * np.pi * _r.mean() * np.trapz(
                _y, x=_x)
        ax.plot(neTime, bn.move_mean(outTarget,200)/1e22,'-',color='#182E58')
        ax.text(0.1,0.9,'Outer Lower target', color='#182E58', transform=ax.transAxes)
        # now repeat for upper target
        neTimeU = c.get(
            'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(
                shot, 'oa1')).data()
        for i, s in enumerate(OuterUpper.keys()):
            _ne = c.get(
                'augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
            _te = c.get(
                'augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
            _an = c.get(
                'augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
            _cs = np.sqrt(constants.e * 4 * _te / (2 * constants.m_p))
            # this is the ion flux
            _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

            if i == 0:
                outDivSignal = _s
            else:
                outDivSignal = np.vstack((outDivSignal,
                                          _s))
        # now we compute the total integrate ion flux
        upTarget = np.zeros(neTime.size)
        for i in range(neTime.size):
            _xr = np.asarray([OuterUpper[k]['R']
                             for k in OuterUpper.keys()])
            _zr = np.asarray([OuterUpper[k]['z']
                             for k in OuterUpper.keys()])
            _x = np.sqrt(np.power(_xr,2) + np.power(_zr,2))
            _r = np.asarray([OuterUpper[k]['R']
                             for k in OuterUpper.keys()])
            _y = outDivSignal[:, i]
            indices = ~(np.isnan(_x) | np.isnan(_y))
            # _dummy = np.vstack((_x, _y)).transpose()
            # _dummy = _dummy[~np.isnan(_dummy).any(1)]
            _x = _x[indices]
            _y = _y[indices]
            # _dummy = np.vstack((_x, _y)).transpose()
            # _dummy = _dummy[~np.isnan(_dummy).any(1)]
            # _x = _dummy[:, 0]
            # _y = _dummy[:, 1][np.argsort(_x)]
            _x = np.sort(_x)
            upTarget[i] = 2 * np.pi * _r.mean() * np.trapz(
                _y, x=_x)
        ax.plot(neTimeU, bn.move_mean(upTarget,200)/1e22,'-',color='#F27C1B')
        ax.text(0.1,0.8,'Outer Upper target', color='#F27C1B', transform=ax.transAxes)
        ax.set_xlabel(r't[s]')
        ax.set_ylabel(r'[10$^{22}$ ion/s]')
        ax.set_ylim([0,1.2])
        # sign the timing used for the profiles and the THB investigation
        tRangesL = ((1.95, 2.1), (2.45, 2.6), (3.09, 3.2))
        colorL = ('#00D3E6', '#8ABA07', '#BC2764')
        for _t,_c in zip(tRangesL, colorL):
            ax.axvline(np.asarray(_t).mean(),ls='--', color=_c)
        fig.savefig('../pdfbox/UpperLowerIntegratedFluxShot{}.pdf'.format(shot),
                    bbox_to_inches='tight')

    elif selection == 3:
        shot = 35879
        tRangesL = ((1.95, 2.1), (2.45, 2.6), (3.05, 3.2))
        colorL = ('#00D3E6', '#8ABA07', '#BC2764')
        sel_PMT = 'PMT1'
        sel_wavelen = 587
        conn = mds.Connection('localhost:8001')
        # edge Data
        eN = conn.get(
            'augsignal({}, "TOT", "H-5_corr")'.format(shot)).data() / 1e20
        time = conn.get(
            'dim_of(augsignal({}, "TOT", "H-5_corr")'.format(
                shot) + ')').data()
        fig, ax = mpl.pylab.subplots(figsize=(6, 4), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.2, top=0.95, right=0.95, left=0.2)
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 4), nrows=1, ncols=1)
        fig2.subplots_adjust(bottom=0.2, top=0.95, right=0.95, left=0.2)
        fig3, ax3 = mpl.pylab.subplots(figsize=(6,8), nrows=2, ncols=1)
        fig3. subplots_adjust(hspace=0.3, left=0.2, bottom=0.12,top=0.96,right=0.95)
        DataAUG = pd.read_csv('/Users/vianello/Documents/Fisica/Conferences/IAEA/iaea2018//data/aug/LModeBlobDatabase.csv')
        shotListAug = (34102, 34104, 34105, 34106)
        DataAUG = DataAUG[DataAUG['shots'].isin(shotListAug)]
        ax.errorbar(DataAUG['<n_e^e>']/DataAUG['n/nG'],
                    DataAUG['Size [rhos]'] * DataAUG['Rhos'] * 2*np.sqrt(np.log(2)) * 1e3,
                    yerr= DataAUG['Size Err [rhos]']* DataAUG['Rhos'] * 2*np.sqrt(np.log(2)) * 1e3,
                    fmt='o', ms=15, color='#182E58')
        for _t, _c in zip(tRangesL, colorL):
            _idx = np.where((time >= _t[0]) & (time <= _t[1]))[0]
            nG = float(0.8) / (np.pi * np.power(0.5, 2))
            _x = eN[_idx].mean()/nG
            # load the THB object
            myObj = myThbObject('../data/THB/filament_data/%i_t_%0.3f_%0.3f_%s'
                                                % (shot, _t[0], _t[1], sel_PMT))
            ax.plot(_x, myObj.rad_corr_len*1e3,'*', ms=15, color='#F27C1B')
            _y = myObj.velocity_all/1e3
            sns.distplot(_y[~np.isnan(_y)], bins=None,color=_c, kde=True,
                        ax=ax2,label=r'n$^e$/n$_G$ = {:.2f}'.format(_x))
            ax3[0].plot(_x,myObj.rad_corr_len*1e3,'*', ms=18,color=_c)
            sns.distplot(_y[~np.isnan(_y)], bins=None, color=_c, kde=True,
                         ax=ax3[1], label=r'n$^e$/n$_G$ = {:.2f}'.format(_x))
        ax.text(0.6,5,'LSN Probe', color='#182E58')
        ax.text(0.6,3,'DN THB', color='#F27C1B')
        ax.set_ylabel(r'$\delta_b$ [mm]')
        ax.set_xlabel(r'n$_e^e$/n$_G$')
        ax3[0].set_ylim([0,10])
        ax3[0].set_ylabel(r'$\delta_b$ [mm]')
        ax3[0].set_xlabel(r'n$_e^e$/n$_G$')
        ax3[0].set_ylim([3,10])
        fig.savefig('../pdfbox/BlobSizeVsGreenwaldLSN-DN.pdf',bbox_to_inches='tight')
        ax2.set_xlabel(r'v$_r$ [km/s]')
        ax2.legend(loc='best', numpoints=1,frameon=False,fontsize=14)
        ax2.set_xlim([0, 1.2])
        ax3[1].set_xlabel(r'v$_r$ [km/s]')
        ax3[1].legend(loc='best', numpoints=1,frameon=False,fontsize=14)
        ax3[1].set_xlim([0, 1.2])
        fig2.savefig('../pdfbox/VelocityDistributionTHBShot{}.pdf'.format(shot),bbox_to_inches='tight')
        fig3.savefig('../pdfbox/BlobVelocityDistributionTHBShot{}.pdf'.format(shot),bbox_to_inches='tight')
    elif selection == 4:
        shotList = (33339, 35879)
        trangeList = (((2.59, 2.60), (4.93, 4.96)),
                      ((2.45, 2.6), (2.94, 2.98)))
        colorList = ('#AC0D2B', '#0E1259')
        fig, ax = mpl.pylab.subplots(figsize=(6,9), nrows=2,ncols=1)
        fig.subplots_adjust(hspace=0.05,left=0.2,top=0.95,bottom=0.12)
        for i in np.arange(2,dtype='int'):
            ax[i].xaxis.set_minor_locator(AutoMinorLocator())
            ax[i].yaxis.set_major_locator(
                mpl.ticker.LogLocator(base=10.0, numticks=15,
                                      subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)))
            ax[i].set_xlim([0.98, 1.1])
            ax[i].set_ylim([5e-2, 3])
            ax[i].set_ylabel(r'n$_e$/n$_e (\rho=1)$')
        ax[0].tick_params(labelbottom=False)
        ax[1].set_xlabel(r'$\rho$')
        ax[0].text(0.1,0.15, r'#{} LSN'.format(shotList[0]),transform=ax[0].transAxes)
        ax[1].text(0.1,0.15, r'#{} DN'.format(shotList[1]),transform=ax[1].transAxes)
        conn = mds.Connection('localhost:8001')
        for _idxshot,(shot, tList) in enumerate(zip(shotList, trangeList)):
            eN = conn.get('augsignal({}, "TOT", "H-5_corr")'.format(shot)).data() / 1e20
            time = conn.get(
                'dim_of(augsignal({}, "TOT", "H-5_corr")'.format(shot) + ')').data()
            nG = 0.8 / (np.pi * np.power(0.5, 2))
            # read the Li_bea
            if shot == 33339:
                _s = 'augdiag(' + str(shot) + ',"LIN","ne")'
                _st = 'dim_of(augdiag(' + str(shot) + ',"LIN","ne"), 0)'
                _sr = 'dim_of(augdiag(' + str(shot) + ',"LIN","ne"), 1)'
            else:
                _s = 'augdiag(' + str(shot) + ',"LIN","ne", "LIBE")'
                _st = 'dim_of(augdiag(' + str(shot) + ',"LIN","ne", "LIBE"), 0)'
                _sr = 'dim_of(augdiag(' + str(shot) + ',"LIN","ne", "LIBE"), 1)'
            LiBes = conn.get(_s).data()
            LiTime = conn.get(_st).data()
            LiRho = conn.get(_sr).data()
            for _idxtr, (_tr, _col) in enumerate(zip(tList, colorList)):
                _idx = np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                _dummyTime = LiTime[_idx]
                _dummyLib = LiBes[:, _idx]
                _dummyRho = LiRho[:, _idx]
                xB = np.nanmean(_dummyRho, axis=1)
                xBE = np.nanstd(_dummyRho, axis=1)
                yB = np.nanmean(_dummyLib, axis=1)
                yBE = np.nanstd(_dummyLib, axis=1)
                yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
                # now the fit
                xN = np.linspace(0.9, 1.1, 200)
                # be sure to include rho = 1
                xN = np.append(xN, 1)
                xN = xN[np.argsort(xN)]
                # perform the fit
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB, yB, yBE,
                    xe=xBE, nrestarts=100, length_scale=0.5,
                    epsilon=5.e-3, regularization_parameter=1)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                # determine the edge greenwald fraction
                _idx = np.where((time >= _tr[0]) & (time <= _tr[1]))[0]
                nLabel = np.nanmean(eN[_idx]) / nG
                ax[_idxshot].plot(_dummyRho, _dummyLib / enS, '.', ms=3, color=_col, alpha=0.3)
                ax[_idxshot].semilogy(xN, yFit / enS, '-', lw=2, color=_col)
                ax[_idxshot].fill_between(xN,
                                   (yFit - yFitE / 2) / enS,
                                   (yFit + yFitE / 2) / enS,
                                   facecolor=_col,
                                   edgecolor='none',
                                   alpha=0.3)
                ax[_idxshot].text(0.5, 0.92 - _idxtr * 0.1, r'n$_e^e$/n$_G$ = {0:.2f}'.format(nLabel),
                           transform=ax[_idxshot].transAxes, color=_col)
        fig.savefig('../pdfbox/CompareUpstreamShot{0}_{1}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inches='tight')


    elif selection == 99:
        mpl.pylab.close('all')
        loop = False
    else:
        input("Unknown Option Selected!")
