from aug import libes
import numpy as np
import gpr1dfusion
import socket
if socket.gethostname() == "toki08":
    server = "mdsplus.aug.ipp.mpg.de"
    import dd
    from aug import ddremoveELMData
else:
    server = "localhost:8000"
import matplotlib as mpl
mpl.rcParams["font.family"] = "sans-serif"
mpl.rc("font", size=22)
mpl.rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
mpl.rc("lines", linewidth=2)

shotList = {
    '36341':{'shot':36341,
             'tlist':((2.5,2.7),(3.5,3.7),(4.5,4.7)),
             'rholist':(0.82,0.82,None),
             'tseplist':(0.002,0.002,None)},
    '36342':{'shot':36342,
             'tlist':((2.5,2.7),(3.5,3.7),(4.5,4.7),(5.5,5.7),(6.5,6.7)),
             'rholist':(0.82,0.82,0.95,0.95,0.95),
             'tseplist':(0.002,0.002,0.002,0.002,0.002)},
    '36345': {'shot': 36345,
              'tlist': ((2.4, 2.6), (3.4, 3.6), (4.4, 4.6), (5.4, 5.6), (6.4, 6.5)),
              'rholist': (0.82, 0.92, None, None, None),
              'tseplist': (0.002, 0.002, None, None, None)},
    '36346': {'shot': 36346,
              'tlist': ((2.4, 2.6), (3.4, 3.6), (4.4, 4.6), (5.4, 5.6), (6.4, 6.6)),
              'rholist': (0.82, 0.82, 0.82, None, None),
              'tseplist': (0.002, 0.002, 0.002, None, None)}}

colorList = ('#0C3559', '#DB5461','#FFC857','#57CC99','#624CAB')

for key in shotList.keys():
    shot = shotList[key]['shot']
    trList = shotList[key]['tlist']
    rhoList = shotList[key]['rholist']
    tsepList = shotList[key]['tseplist']
    fig, ax = mpl.pylab.subplots(figsize=(8,6), nrows=1,ncols=1)
    fig.subplots_adjust(bottom=0.17, left=0.17)
    LB = libes.Libes(shot,remote=True, server=server, experiment='LIBE')
    for _idplot, (tr, rho, t_sep, col) in enumerate(zip(trList, rhoList, tsepList,colorList)):
        if rho:
            Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
        else:
            Out = LB.averageProfile(trange=tr)
        _label = r'$\Delta$t = {:.1f} - {:.1f}'.format(tr[0], tr[1])
        _file = '../data/neprofile_gpi_{}_{:.1f}_{:.1f}.z'.format(shot,tr[0],tr[1])
        _idx = np.logical_and(Out["rho"] >= 0.97, Out['rho'] <= 1.08)
        _idxraw = np.logical_and(Out['rhoraw'] >= 0.97, Out['rhoraw'] <= 1.08)
        xB = Out["rho"][_idx]
        yB = Out["ne"][_idx]
        yBE = Out["err_Y"][_idx]
        xBE = Out["err_X"][_idx]
        try:
            gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                xB[np.argsort(xB)],
                yB[np.argsort(xB)],
                yBE[np.argsort(xB)],
                xe=xBE[np.argsort(xB)],
                nrestarts=500,
            )
            xN = np.linspace(0.97, 1.08, 120)
            gp.GPRFit(xN)
            yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
            enS = yFit[np.argmin(np.abs(xN - 1))]
            ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
            ax.fill_between(
                xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.15
            )
            print("Gaussian Process Regression Fit Performed")
        except:
            xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
            enS = yFit[np.argmin(np.abs(xN - 1))]
            ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
            ax.fill_between(
                xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.15
            )
            print("Gaussian Process Regression Fit NOT Performed")
        ax.plot(
            Out["rhoraw"][_idxraw], Out["neraw"][_idxraw] / enS, ".", ms=3, color=col, alpha=0.2
        )
        np.savez(_file,rhoraw=Out["rhoraw"][_idxraw],neraw=Out["neraw"][_idxraw],rhofit=xN,fit=yFit,errfit=yFitE)
    ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
    ax.set_xlabel(r"$\rho$")
    ax.set_title(r"#{}".format(shot))
    ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
    ax.set_yscale("log")
    ax.set_ylim([5e-2, 4])
    fig.savefig(
        "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
    )
