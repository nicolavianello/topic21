import socket
import sys
import matplotlib as mpl
import numpy as np
from collections import OrderedDict
if socket.gethostname() == 'toki01':
    import dd
    import langmuir
    import libes
else:
    import MDSplus as mds
    import gpr1dfusion
from scipy import constants
from scipy.interpolate import interp1d
import smooth
import bin_by
import bottleneck as bn
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rc("font", size=22)
mpl.rc('font', **{'family': 'sans-serif', 'sans-serif': ['Helvetica']})
mpl.rc("lines", linewidth=2)
if int(sys.version_info[0]) >= 3:
    import plasmapy.physics.parameters as plasma
    from astropy import units as u


mpl.rc('font', **{'family': 'sans-serif', 'sans-serif': ['Helvetica']})
mpl.rc("font", size=22)
mpl.rc("lines", linewidth=2)


def print_menu():
    print(30 * "-", "MENU", 30 * "-")
    print("1. Compare H-Mode shots general plot 34281/33475")
    print("2. Compare rollover density 34281/33475")
    print("3. Check evolution of upstream profile in reference 35710")
    print("4. Provide appropriate timing for strokes")
    print("5. Compare L-Mode shots general plot 33339/30801 ")
    print("6. Compare rollover density 33339/30801 ")
    print("7. Upstream/Target/Lambda comparison 33339/30801 Only @TOK cluster")
    print("8. Compare shot 33339/35879")
    print("9. Compare shot 33339/35880")
    print("10. Compare Upstream 33339/35879")
    print("11. Compare Upstream 33339/35880")
    print("12. General Waveform 35882/35884")
    print("13. Upstream 35882/35884")
    print("14. Recycling divertor vs fueling cryo/nocryo (33339/34102)")
    print("15. General Waveform (33339/34102)")
    print("16. Compare shot 30732, 30778 and 36346")
    print("17. Proposed waveform for shot at higher power")
    print("18. Further proposed waveform for Divertor/Midplane high power")
    print("99: End")
    print(67 * "-")


loop = True
while loop:
    print_menu()
    selection = int(input("Enter your choice [1-99] "))
    if selection == 1:
        shotList = (33475, 34281)
        colorList = ('#318BA3', '#FF6000')
        fig, ax = mpl.pylab.subplots(figsize=(8, 13),
                                     nrows=6, ncols=1, sharex=True)
        fig.subplots_adjust(top=0.95, right=0.95, left=0.18, bottom=0.12)
        Diagnostic = OrderedDict([
            ('density', {'experiment': 'TOT', 'signal': 'H-5_corr',
                         'label': r'n$_e^e [10^{19}$m$^{-3}$]',
                         'factor': 1e19}),
            ('D2', {'experiment': 'UVS', 'signal': 'D_tot',
                    'label': r'D$_2$ [$10^{22}$el/s]',
                    'factor': 1e22}),
            ('N2', {'experiment': 'UVS', 'signal': 'N_tot',
                    'label': r'N$_2$ [$10^{22}$el/s]',
                    'factor': 1e22}),
            ('pressure', {'experiment': 'IOC', 'signal': 'F01',
                          'label': r'[10$^{23}$molecules/m$^{2}$/s]',
                          'factor': 1e23}),
            ('nbi', {'experiment': 'TOT', 'signal': 'PNBI_TOT',
                     'label': r'PNBI[MW]',
                     'factor': 1e6}),
            ('ecrh', {'experiment': 'TOT', 'signal': 'PECR_TOT',
                      'label': r'P ECRH[MW]',
                      'factor': 1e6})])
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            for i, k in enumerate(Diagnostic.keys()):
                signal = Diagnostic[k]['signal']
                diag = Diagnostic[k]['experiment']
                _s = 'augdiag('+str(shot)+',"'+diag+'","'+signal+'")'
                _st = 'dim_of(augdiag('+str(shot)+',"'+diag+'","'+signal+'"))'
                data = c.get(_s).data()
                time = c.get(_st).data()
                ax[i].plot(time,
                           data/Diagnostic[k]['factor'],
                           label=r'#{}'.format(shot))
                ax[i].text(0.1, 0.87, Diagnostic[k]['label'], transform=ax[i].transAxes,
                           fontsize=13)
        ax[0].set_ylim([0, 10])
        ax[-1].set_xlim([0, 6.5])
        ax[-1].set_xlabel(r't[s]')
        lg = ax[0].legend(loc='best', numpoints=1, frameon=False)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/CompareGeneralWaveform{}_{}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inchets='tight')

    elif selection == 2:
        shotList = (33475, 34281)
        colorList = ('#318BA3', '#FF6000')
        OuterLower = OrderedDict([
            ('ua1', {'R': 1.582, 'z': -1.199, 's': 1.045}),
            ('ua2', {'R': 1.588, 'z': -1.175, 's': 1.070}),
            ('ua3', {'R': 1.595, 'z': -1.151, 's': 1.094}),
            ('ua4', {'R': 1.601, 'z': -1.127, 's': 1.126}),
            ('ua5', {'R': 1.608, 'z': -1.103, 's': 1.158}),
            ('ua6', {'R': 1.614, 'z': -1.078, 's': 1.189}),
            ('ua7', {'R': 1.620, 'z': -1.054, 's': 1.213}),
            ('ua8', {'R': 1.627, 'z': -1.030, 's': 1.246}),
            ('ua9', {'R': 1.640, 'z': -0.982, 's': 1.276})])

        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(top=0.95, left=0.17, right=0.96, bottom=0.16)
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            outDivSignal = np.asarray([])
            neTime = c.get(
                'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(
                    shot, 'ua1')).data()
            for i, s in enumerate(OuterTarget.keys()):
                _ne = c.get(
                    'augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
                _te = c.get(
                    'augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
                _an = c.get(
                    'augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
                _cs = np.sqrt(constants.e * 4 * _te/(2*constants.m_p))
                # this is the ion flux
                _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

                if i == 0:
                    outDivSignal = _s
                else:
                    outDivSignal = np.vstack((outDivSignal,
                                              _s))
            # now we compute the total integrate ion flux
            outTarget = np.zeros(neTime.size)
            for i in range(neTime.size):
                _x = np.asarray([OuterTarget[k]['s']
                                 for k in OuterTarget.keys()])
                _r = np.asarray([OuterTarget[k]['R']
                                 for k in OuterTarget.keys()])
                _y = outDivSignal[:, i]
                _dummy = np.vstack((_x, _y)).transpose()
                _dummy = _dummy[~np.isnan(_dummy).any(1)]
                _x = _dummy[:, 0]
                _y = _dummy[:, 1][np.argsort(_x)]
                _x = np.sort(_x)
                outTarget[i] = 2*np.pi*_r.mean()*np.trapz(
                    _y, x=_x)
            # each of the quantity is considered only in the
            # flat top
            tmin, tmax = 2, 6
            _idx = np.where((neTime >= tmin) & (neTime <= tmax))[0]
            # smooth theme since they exhibit spikes
            outTarget = smooth.smooth(outTarget[_idx], window_len=200)
            neTime = neTime[_idx]
            # create a Spline object
            SOut = interp1d(neTime,
                            outTarget, fill_value='extrapolate')
            eN = c.get(
                'augsignal({}, "TOT", "H-5_corr")'.format(shot)).data()/1e20
            time = c.get(
                'dim_of(augsignal({}, "TOT", "H-5_corr")'.format(
                    shot)+')').data()
            # limit to the range [tmin, tmax]
            _idx = np.where((time >= tmin) &
                            (time <= tmax))[0]
            time = time[_idx]
            eN = eN[_idx]
            _x = eN
            _y = SOut(time)
            _y = smooth.smooth(_y[np.argsort(_x)], window_len=20)/1e23
            _x = _x[np.argsort(_x)]
            ax.plot(_x, _y, '-', color=col, label=r'#{}'.format(shot))
        ax.set_xlim([0, 1])
        ax.set_xlabel(r'n$_e^e [10^{20}$m$^{-3}$]')
        ax.set_ylabel(r'[10$^{23}$ ion/s]')
        lg = ax.legend(loc='upper left', numpoints=1,
                       frameon=False, fontsize=16)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/CompareIonFlux{}_{}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inches='tight')

    elif selection == 5:
        shotList = (33339, 30801)
        colorList = ('#318BA3', '#FF6000')
        fig, ax = mpl.pylab.subplots(figsize=(8, 13),
                                     nrows=5, ncols=1, sharex=True)
        fig.subplots_adjust(top=0.95, right=0.95, left=0.18, bottom=0.12)
        Diagnostic = OrderedDict([
            ('density', {'experiment': 'TOT', 'signal': 'H-5_corr',
                         'label': r'n$_e^e [10^{19}$m$^{-3}$]',
                         'factor': 1e19}),
            ('D2', {'experiment': 'UVS', 'signal': 'D_tot',
                    'label': r'D$_2$ [$10^{22}$el/s]',
                    'factor': 1e22}),
            ('N2', {'experiment': 'UVS', 'signal': 'N_tot',
                    'label': r'N$_2$ [$10^{22}$el/s]',
                    'factor': 1e22}),
            ('pressure', {'experiment': 'IOC', 'signal': 'F03',
                          'label': r'[10$^{23}$molecules/m$^{2}$/s]',
                          'factor': 1e23}),
            ('teDiv', {'experiment': 'LSD', 'signal': 'te-uab',
                       'label': r'T$_{div}$[eV]',
                       'factor': 1})])
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            for i, k in enumerate(Diagnostic.keys()):
                try:
                    signal = Diagnostic[k]['signal']
                    diag = Diagnostic[k]['experiment']
                    _s = 'augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '")'
                    _st = 'dim_of(augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '"))'
                    data = c.get(_s).data()
                    time = c.get(_st).data()
                    if k == 'teDiv':
                        data = smooth.smooth(data, window_len=50)
                    ax[i].plot(time,
                               data / Diagnostic[k]['factor'],
                               label=r'#{}'.format(shot))
                    ax[i].text(0.1, 0.87, Diagnostic[k]['label'], transform=ax[i].transAxes,
                               fontsize=13)
                except Exception:
                    print('Signal' + k + ' For shot {} not found'.format(shot))
                    continue
        ax[0].set_ylim([0, 10])
        ax[-1].set_xlim([0, 6.5])
        ax[-1].set_xlabel(r't[s]')
        lg = ax[0].legend(loc='best', numpoints=1, frameon=False)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/CompareGeneralWaveform{}_{}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inchets='tight')

    elif selection == 6:
        shotList = (33339, 30801)
        colorList = ('#318BA3', '#FF6000')
        OuterTarget = OrderedDict([
            ('ua1', {'R': 1.582, 'z': -1.199, 's': 1.045}),
            ('ua2', {'R': 1.588, 'z': -1.175, 's': 1.070}),
            ('ua3', {'R': 1.595, 'z': -1.151, 's': 1.094}),
            ('ua4', {'R': 1.601, 'z': -1.127, 's': 1.126}),
            ('ua5', {'R': 1.608, 'z': -1.103, 's': 1.158}),
            ('ua6', {'R': 1.614, 'z': -1.078, 's': 1.189}),
            ('ua7', {'R': 1.620, 'z': -1.054, 's': 1.213}),
            ('ua8', {'R': 1.627, 'z': -1.030, 's': 1.246}),
            ('ua9', {'R': 1.640, 'z': -0.982, 's': 1.276})])
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(top=0.95, left=0.17, right=0.96, bottom=0.16)
        # create a second plot with the integral density vs time
        fig2, ax2 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig2.subplots_adjust(top=0.95, left=0.17, right=0.96, bottom=0.16)
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            outDivSignal = np.asarray([])
            neTime = c.get(
                'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(
                    shot, 'ua1')).data()
            for i, s in enumerate(OuterTarget.keys()):
                _ne = c.get(
                    'augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
                _te = c.get(
                    'augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
                _an = c.get(
                    'augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
                _cs = np.sqrt(constants.e * 4 * _te/(2*constants.m_p))
                # this is the ion flux
                _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

                if i == 0:
                    outDivSignal = _s
                else:
                    outDivSignal = np.vstack((outDivSignal,
                                              _s))
            # now we compute the total integrate ion flux
            outTarget = np.zeros(neTime.size)
            for i in range(neTime.size):
                _x = np.asarray([OuterTarget[k]['s']
                                 for k in OuterTarget.keys()])
                _r = np.asarray([OuterTarget[k]['R']
                                 for k in OuterTarget.keys()])
                _y = outDivSignal[:, i]
                indices = ~(np.isnan(_x) | np.isnan(_y))
                # _dummy = np.vstack((_x, _y)).transpose()
                # _dummy = _dummy[~np.isnan(_dummy).any(1)]
                _x = _x[indices]
                _y = _y[indices]
                _x = np.sort(_x)
                outTarget[i] = 2*np.pi*_r.mean()*np.trapz(
                    _y, x=_x)
            # each of the quantity is considered only in the
            # flat top
            ax2.plot(neTime, smooth.smooth(outTarget, window_len=50)/1e23,
                     color=col, label=r'#{}'.format(shot))
            tmin, tmax = 1.5, 5.45
            _idx = np.where((neTime >= tmin) & (neTime <= tmax))[0]
            # smooth theme since they exhibit spikes
            outTarget = bn.smooth(outTarget[_idx], window_len=200)
            neTime = neTime[_idx]
            # create a Spline object
            SOut = interp1d(neTime,
                            outTarget, fill_value='extrapolate')
            eN = c.get(
                'augsignal({}, "TOT", "H-5_corr")'.format(shot)).data()/1e20
            time = c.get(
                'dim_of(augsignal({}, "TOT", "H-5_corr")'.format(
                    shot)+')').data()
            # limit to the range [tmin, tmax]
            _idx = np.where((time >= tmin) &
                            (time <= tmax))[0]
            time = time[_idx]
            eN = eN[_idx]
            _x = eN
            _y = SOut(time)
            _y = smooth.smooth(_y[np.argsort(_x)], window_len=20)/1e23
            _x = _x[np.argsort(_x)]
            ax.plot(_x, _y, '-', color=col, label=r'#{}'.format(shot))
        ax.set_xlim([0, 0.5])
        ax.set_xlabel(r'n$_e^e [10^{20}$m$^{-3}$]')
        ax.set_ylabel(r'[10$^{23}$ ion/s]')
        lg = ax.legend(loc='upper left', numpoints=1,
                       frameon=False, fontsize=16)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/CompareIonFlux{}_{}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inches='tight')
        ax2.set_xlim([1, 5.5])
        ax2.set_xlabel(r't [s]')
        ax2.set_ylabel(r'[10$^{23}$ ion/s]')
        lg = ax2.legend(loc='upper left', numpoints=1,
                        frameon=False, fontsize=16)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig2.savefig('../pdfbox/CompareIonFluxTime{}_{}.pdf'.format(
            shotList[0], shotList[1]), bbox_to_inches='tight')

    elif selection == 7:
        shotList = (33339, 30801)
        colorList = ('#318BA3', '#FF6000')
        colorTimeList = ('#00548C', '#C70039', '#FF5733', '#05B6AD')
        fig = mpl.pylab.figure(figsize=(10, 10))
        fig.subplots_adjust(hspace=0.28, right=0.96, top=0.94)
        ax1 = mpl.pylab.subplot2grid((2, 2), (0, 0), colspan=2)
        ax1.set_title(r'AUG')
        # these are the panel for upstream profiles
        ax2 = mpl.pylab.subplot2grid((2, 2), (1, 0))
        ax3 = mpl.pylab.subplot2grid((2, 2), (1, 1))
        # list of axes
        axProf = (ax2, ax3)
        # tranges
        tList = ((1.45, 1.55), (2.35, 2.45), (2.8, 2.9), (4.45, 4.55))
        c = mds.Connection('localhost:8001')
        for shot, col, _axp in zip(
                shotList, colorList, axProf):
            _s = 'augdiag('+str(shot)+',"TOT","H-5_corr")'
            _st = 'dim_of(augdiag('+str(shot)+',"TOT","H-5_corr"))'
            data = c.get(_s).data()
            time = c.get(_st).data()
            label = r'#{}'.format(shot)
            ax1.plot(time, data/1e19, color=col,
                     label=label, lw=3)
            # load the libes data
            _s = 'augdiag('+str(shot)+',"LIN","ne")'
            _st = 'dim_of(augdiag('+str(shot)+',"LIN","ne"), 0)'
            _sr = 'dim_of(augdiag('+str(shot)+',"LIN","ne"), 1)'
            LiBes = c.get(_s).data()
            LiTime = c.get(_st).data()
            LiRho = c.get(_sr).data()
            for t, _colorT in zip(
                    tList, colorTimeList):
                _dummyTime = np.where((LiTime >= t[0]) & (LiTime <= t[1]))
                _dummyLib = LiBes[:, _dummyTime].ravel()/1e19
                _dummyRho = LiRho[:, _dummyTime].ravel()

                yOut, bins, bin_means, bin_width, xOut = bin_by.bin_by(
                    _dummyRho, _dummyLib, nbins=30)
                xB = np.asarray([np.nanmean(k) for k in xOut])
                xBE = np.asarray([np.nanstd(k) for k in xOut])
                yB = np.asarray([np.nanmean(k) for k in yOut])
                yBE = np.asarray([np.nanstd(k) for k in yOut])
                xN = np.linspace(0.95, 1.08, 200)
                # be sure to include rho = 1
                xN = np.append(xN, 1)
                xN = xN[np.argsort(xN)]
                # perform the fit
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB, yB, yBE,
                    xe=xBE, nrestarts=100, length_scale=0.5,
                    epsilon=5.e-3, regularization_parameter=1)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN-1))]

                _axp.semilogy(_dummyRho, _dummyLib/enS,
                              '.', color=_colorT, alpha=0.3)
                _axp.semilogy(xN, yFit/enS, '-', lw=2, color=_colorT)
                _axp.fill_between(xN,
                                  (yFit-yFitE/2)/enS,
                                  (yFit+yFitE/2)/enS,
                                  facecolor=_colorT,
                                  edgecolor='none',
                                  alpha=0.3)

        ax1.text(0.9, 0.87, '(a)', transform=ax1.transAxes)
        for _p, _s in zip(axProf, shotList):
            _p.set_xlim([0.98, 1.06])
            _p.set_ylim([1e-1, 3])
            _p.text(0.1, 0.86, r'#%5i' % _s, transform=_p.transAxes)
            _p.set_xlabel(r'$\rho$')
        axProf[1].axes.get_yaxis().set_visible(False)
        axProf[1].set_ylabel(r'n$_e$/n$_e(\rho_p = 1)$')
        for _t, _c in zip(tList, colorTimeList):
            ax1.axvline((_t[0]+_t[1])/2, ls='-', lw=2, color=_c)

        ax1.set_ylabel(r'n$_e$ H-5 [10$^{19}$]')
        ax1.set_xlabel(r't[s]')
        ax1.set_xlim([0, 7])
        ax1.set_ylim([0, 6])
        leg = ax1.legend(loc='best', numpoints=1, frameon=False)
        for t, _c in zip(leg.get_texts(), colorList):
            t.set_color(_c)

        fig.savefig('../pdfbox/UpstreamDivertorProfiles{}_{}.pdf'.format(
                    shotList[0], shotList[1]),
                    bbox_to_inches='tight')

    elif selection == 8:
        shotList = (33339, 35879)
        colorList = ('#318BA3', '#FF6000')
        fig, ax = mpl.pylab.subplots(figsize=(14, 12),
                                     nrows=5, ncols=1, sharex=True)
        fig.subplots_adjust(top=0.95, right=0.95, left=0.18,
                            bottom=0.12, hspace=0.05)
        Diagnostic = OrderedDict([
            ('density', {'experiment': 'TOT', 'signal': 'H-1_corr',
                         'label': r'H-1_corr n$_e [10^{19}$m$^{-3}$]',
                         'factor': 1e19}),
            ('D2', {'experiment': 'UVS', 'signal': 'D_tot',
                    'label': r'D$_2$ [$10^{22}$el/s]',
                    'factor': 1e22}),
            ('pressure', {'experiment': 'IOC', 'signal': 'F01',
                          'label': r'[10$^{23}$molecules/m$^{2}$/s]',
                          'factor': 1e23}),
            ('Wmhd', {'experiment': 'GQI', 'signal': 'Wmhd',
                      'label': r'W$_{MHD}$ [MJ]',
                      'factor': 1e6, 'limit': [0, 0.2]}),
            ('teDiv', {'experiment': 'LSD', 'signal': 'te-uab',
                       'label': r'T$_{div}$[eV]',
                       'factor': 1})])
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            for i, k in enumerate(Diagnostic.keys()):
                try:
                    signal = Diagnostic[k]['signal']
                    diag = Diagnostic[k]['experiment']
                    _s = 'augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '")'
                    _st = 'dim_of(augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '"))'
                    data = c.get(_s).data()
                    time = c.get(_st).data()
                    if k == 'teDiv':
                        data = smooth.smooth(data, window_len=50)
                    ax[i].plot(time,
                               data / Diagnostic[k]['factor'],
                               label=r'#{}'.format(shot))
                    ax[i].text(0.1, 0.87, Diagnostic[k]['label'], transform=ax[i].transAxes,
                               fontsize=13)
                except Exception:
                    print('Signal' + k + ' For shot {} not found'.format(shot))
                    continue
        ax[0].set_ylim([0, 13])
        ax[-1].set_xlim([0, 6.5])
        ax[-1].set_xlabel(r't[s]')
        lg = ax[0].legend(loc='best', numpoints=1, frameon=False)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/CompareGeneralWaveform{}_{}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inchets='tight')

    elif selection == 9:
        shotList = (33339, 35880)
        colorList = ('#318BA3', '#FF6000')
        fig, ax = mpl.pylab.subplots(figsize=(14, 12),
                                     nrows=5, ncols=1, sharex=True)
        fig.subplots_adjust(top=0.95, right=0.95, left=0.18,
                            bottom=0.12, hspace=0.05)
        Diagnostic = OrderedDict([
            ('density', {'experiment': 'TOT', 'signal': 'H-1_corr',
                         'label': r'H-1 corr n$_e [10^{19}$m$^{-3}$]',
                         'factor': 1e19}),
            ('D2', {'experiment': 'UVS', 'signal': 'D_tot',
                    'label': r'D$_2$ [$10^{22}$el/s]',
                    'factor': 1e22}),
            ('pressure', {'experiment': 'IOC', 'signal': 'F01',
                          'label': r'[10$^{23}$molecules/m$^{2}$/s]',
                          'factor': 1e23}),
            ('Wmhd', {'experiment': 'GQI', 'signal': 'Wmhd',
                      'label': r'W$_{MHD}$ [MJ]',
                      'factor': 1e6, 'limit': [0, 0.2]}),
            ('teDiv', {'experiment': 'LSD', 'signal': 'te-uab',
                       'label': r'T$_{div}$[eV]',
                       'factor': 1})])
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            for i, k in enumerate(Diagnostic.keys()):
                try:
                    signal = Diagnostic[k]['signal']
                    diag = Diagnostic[k]['experiment']
                    _s = 'augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '")'
                    _st = 'dim_of(augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '"))'
                    data = c.get(_s).data()
                    time = c.get(_st).data()
                    if k == 'teDiv':
                        data = smooth.smooth(data, window_len=50)
                    ax[i].plot(time,
                               data / Diagnostic[k]['factor'],
                               label=r'#{}'.format(shot))
                    ax[i].text(0.1, 0.87, Diagnostic[k]['label'], transform=ax[i].transAxes,
                               fontsize=13)
                except Exception:
                    print('Signal' + k + ' For shot {} not found'.format(shot))
                    continue
        ax[0].set_ylim([0, 15])
        ax[-1].set_xlim([0, 6.5])
        ax[-1].set_xlabel(r't[s]')
        lg = ax[0].legend(loc='best', numpoints=1, frameon=False)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/CompareGeneralWaveform{}_{}.pdf'.format(
            shotList[0], shotList[1]),
            bbox_to_inchets='tight')

    elif selection == 10:
        shotList = (33339, 35879)
        colorList = ('#318BA3', '#FF6000')
        colorTimeList = ('#00548C', '#C70039', '#FF5733', '#05B6AD')
        fig = mpl.pylab.figure(figsize=(10, 10))
        fig.subplots_adjust(hspace=0.28, right=0.96, top=0.94)
        ax1 = mpl.pylab.subplot2grid((2, 2), (0, 0), colspan=2)
        ax1.set_title(r'AUG')
        # these are the panel for upstream profiles
        ax2 = mpl.pylab.subplot2grid((2, 2), (1, 0))
        ax3 = mpl.pylab.subplot2grid((2, 2), (1, 1))
        # list of axes
        axProf = (ax2, ax3)
        # tranges
        (2, 2.4, 2.8, 3.25)
        tList = ((1.97, 2.03), (2.35, 2.45), (2.77, 2.83), (3.22, 3.28))
        c = mds.Connection('localhost:8001')
        for shot, col, _axp in zip(
                shotList, colorList, axProf):
            _s = 'augdiag('+str(shot)+',"TOT","H-1_corr")'
            _st = 'dim_of(augdiag('+str(shot)+',"TOT","H-1_corr"))'
            data = c.get(_s).data()
            time = c.get(_st).data()
            label = r'#{}'.format(shot)
            ax1.plot(time, data/1e19, color=col,
                     label=label, lw=3)
            # load the libes data
            if shot == 33339:
                _s = 'augdiag('+str(shot)+',"LIN","ne")'
                _st = 'dim_of(augdiag('+str(shot)+',"LIN","ne"), 0)'
                _sr = 'dim_of(augdiag('+str(shot)+',"LIN","ne"), 1)'
            else:
                _s = 'augdiag('+str(shot)+',"LIN","ne", "LIBE")'
                _st = 'dim_of(augdiag('+str(shot)+',"LIN","ne", "LIBE"), 0)'
                _sr = 'dim_of(augdiag('+str(shot)+',"LIN","ne", "LIBE"), 1)'
            LiBes = c.get(_s).data()
            LiTime = c.get(_st).data()
            LiRho = c.get(_sr).data()
            for t, _colorT in zip(
                    tList, colorTimeList):
                _dummyTime = np.where((LiTime >= t[0]) & (LiTime <= t[1]))
                _dummyLib = LiBes[:, _dummyTime].ravel()/1e19
                _dummyRho = LiRho[:, _dummyTime].ravel()

                yOut, bins, bin_means, bin_width, xOut = bin_by.bin_by(
                    _dummyRho, _dummyLib, nbins=30)
                xB = np.asarray([np.nanmean(k) for k in xOut])
                xBE = np.asarray([np.nanstd(k) for k in xOut])
                yB = np.asarray([np.nanmean(k) for k in yOut])
                yBE = np.asarray([np.nanstd(k) for k in yOut])
                xN = np.linspace(0.95, 1.08, 200)
                # be sure to include rho = 1
                xN = np.append(xN, 1)
                xN = xN[np.argsort(xN)]
                # perform the fit
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB, yB, yBE,
                    xe=xBE, nrestarts=100, length_scale=0.5,
                    epsilon=5.e-3, regularization_parameter=1)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN-1))]

                _axp.semilogy(_dummyRho, _dummyLib/enS,
                              '.', color=_colorT, alpha=0.3)
                _axp.semilogy(xN, yFit/enS, '-', lw=2, color=_colorT)
                _axp.fill_between(xN,
                                  (yFit-yFitE/2)/enS,
                                  (yFit+yFitE/2)/enS,
                                  facecolor=_colorT,
                                  edgecolor='none',
                                  alpha=0.3)

        ax1.text(0.9, 0.87, '(a)', transform=ax1.transAxes)
        for _p, _s in zip(axProf, shotList):
            _p.set_xlim([0.98, 1.06])
            _p.set_ylim([1e-1, 3])
            _p.text(0.1, 0.86, r'#%5i' % _s, transform=_p.transAxes)
            _p.set_xlabel(r'$\rho$')
        axProf[1].axes.get_yaxis().set_visible(False)
        axProf[1].set_ylabel(r'n$_e$/n$_e(\rho_p = 1)$')
        for _t, _c in zip(tList, colorTimeList):
            ax1.axvline((_t[0]+_t[1])/2, ls='-', lw=2, color=_c)

        ax1.set_ylabel(r'n$_e$ H-1 corr [10$^{19}$m$^{-3}$]')
        ax1.set_xlabel(r't[s]')
        ax1.set_xlim([0, 6])
        ax1.set_ylim([0, 9])
        leg = ax1.legend(loc='best', numpoints=1, frameon=False)
        for t, _c in zip(leg.get_texts(), colorList):
            t.set_color(_c)

        fig.savefig('../pdfbox/UpstreamDivertorProfiles{}_{}.pdf'.format(
                    shotList[0], shotList[1]),
                    bbox_to_inches='tight')

    elif selection == 11:
        shotList = (33339, 35880)
        colorList = ('#318BA3', '#FF6000')
        colorTimeList = ('#00548C', '#C70039', '#FF5733', '#05B6AD')
        fig = mpl.pylab.figure(figsize=(10, 10))
        fig.subplots_adjust(hspace=0.28, right=0.96, top=0.94)
        ax1 = mpl.pylab.subplot2grid((2, 2), (0, 0), colspan=2)
        ax1.set_title(r'AUG')
        # these are the panel for upstream profiles
        ax2 = mpl.pylab.subplot2grid((2, 2), (1, 0))
        ax3 = mpl.pylab.subplot2grid((2, 2), (1, 1))
        # list of axes
        axProf = (ax2, ax3)
        # tranges
        tList = ((1.97, 2.03), (2.35, 2.45), (2.77, 2.83), (3.05, 3.15))
        c = mds.Connection('localhost:8001')
        for shot, col, _axp in zip(
                shotList, colorList, axProf):
            _s = 'augdiag('+str(shot)+',"TOT","H-1_corr")'
            _st = 'dim_of(augdiag('+str(shot)+',"TOT","H-1_corr"))'
            data = c.get(_s).data()
            time = c.get(_st).data()
            label = r'#{}'.format(shot)
            ax1.plot(time, data/1e19, color=col,
                     label=label, lw=3)
            # load the libes data
            if shot == 33339:
                _s = 'augdiag('+str(shot)+',"LIN","ne")'
                _st = 'dim_of(augdiag('+str(shot)+',"LIN","ne"), 0)'
                _sr = 'dim_of(augdiag('+str(shot)+',"LIN","ne"), 1)'
            else:
                _s = 'augdiag('+str(shot)+',"LIN","ne", "LIBE")'
                _st = 'dim_of(augdiag('+str(shot)+',"LIN","ne", "LIBE"), 0)'
                _sr = 'dim_of(augdiag('+str(shot)+',"LIN","ne", "LIBE"), 1)'
            LiBes = c.get(_s).data()
            LiTime = c.get(_st).data()
            LiRho = c.get(_sr).data()
            for t, _colorT in zip(
                    tList, colorTimeList):
                _dummyTime = np.where((LiTime >= t[0]) & (LiTime <= t[1]))
                _dummyLib = LiBes[:, _dummyTime].ravel()/1e19
                _dummyRho = LiRho[:, _dummyTime].ravel()

                yOut, bins, bin_means, bin_width, xOut = bin_by.bin_by(
                    _dummyRho, _dummyLib, nbins=30)
                xB = np.asarray([np.nanmean(k) for k in xOut])
                xBE = np.asarray([np.nanstd(k) for k in xOut])
                yB = np.asarray([np.nanmean(k) for k in yOut])
                yBE = np.asarray([np.nanstd(k) for k in yOut])
                xN = np.linspace(0.95, 1.08, 200)
                # be sure to include rho = 1
                xN = np.append(xN, 1)
                xN = xN[np.argsort(xN)]
                # perform the fit
                try:
                    gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                        xB[~np.isnan(yB)], yB[~np.isnan(yB)
                                              ], yBE[~np.isnan(yB)],
                        xe=xBE, nrestarts=100, length_scale=0.5,
                        epsilon=5.e-3, regularization_parameter=1)
                    gp.GPRFit(xN)
                    yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                    enS = yFit[np.argmin(np.abs(xN-1))]
                    _axp.semilogy(xN, yFit/enS, '-', lw=2, color=_colorT)
                    _axp.fill_between(xN,
                                      (yFit-yFitE/2)/enS,
                                      (yFit+yFitE/2)/enS,
                                      facecolor=_colorT,
                                      edgecolor='none',
                                      alpha=0.3)
                except Exception:
                    enS = yB[np.argmin(np.abs(xB-1))]
                    continue
                _axp.semilogy(_dummyRho, _dummyLib/enS,
                              '.', color=_colorT, alpha=0.3)

        ax1.text(0.9, 0.87, '(a)', transform=ax1.transAxes)
        for _p, _s in zip(axProf, shotList):
            _p.set_xlim([0.98, 1.06])
            _p.set_ylim([1e-1, 3])
            _p.text(0.1, 0.86, r'#%5i' % _s, transform=_p.transAxes)
            _p.set_xlabel(r'$\rho$')
        axProf[1].axes.get_yaxis().set_visible(False)
        axProf[1].set_ylabel(r'n$_e$/n$_e(\rho_p = 1)$')
        for _t, _c in zip(tList, colorTimeList):
            ax1.axvline((_t[0]+_t[1])/2, ls='-', lw=2, color=_c)

        ax1.set_ylabel(r'n$_e$ H-1 corr [10$^{19}$m$^{-3}$]')
        ax1.set_xlabel(r't[s]')
        ax1.set_xlim([0, 6])
        ax1.set_ylim([0, 9])
        leg = ax1.legend(loc='best', numpoints=1, frameon=False)
        for t, _c in zip(leg.get_texts(), colorList):
            t.set_color(_c)

        fig.savefig('../pdfbox/UpstreamDivertorProfiles{}_{}.pdf'.format(
                    shotList[0], shotList[1]),
                    bbox_to_inches='tight')

    elif selection == 12:
        shotList = (35882, 35883, 35884)
        for shot in shotList:
            fig, ax = mpl.pylab.subplots(
                figsize=(14, 14),
                nrows=6, ncols=1, sharex=True)
            fig.subplots_adjust(top=0.98, right=0.95, left=0.12,
                                bottom=0.1, hspace=0.05)
            Diagnostic = OrderedDict([
                ('density', {'experiment': 'TOT', 'signal': 'H-1_corr',
                             'label': r'n$_e [10^{19}$m$^{-3}$]',
                             'factor': 1e19, 'axis': 0}),
                ('Edensity', {'experiment': 'TOT', 'signal': 'H-5_corr',
                              'label': r'n$_e [10^{19}$m$^{-3}$]',
                              'factor': 1e19, 'axis': 0}),
                ('D2', {'experiment': 'UVS', 'signal': 'D_tot',
                        'label': r'D$_2$ [$10^{22}$el/s]',
                        'factor': 1e22, 'axis': 1}),
                ('pressure', {'experiment': 'IOC', 'signal': 'F01',
                              'label': r'[10$^{23}$molecules/m$^{2}$/s]',
                              'factor': 1e23, 'axis': 2}),
                ('nbi', {'experiment': 'TOT', 'signal': 'PNBI_TOT',
                         'label': r'[MW]',
                         'factor': 1e6, 'axis': 3}),
                ('ecrh', {'experiment': 'TOT', 'signal': 'PECR_TOT',
                          'label': r'[MW]',
                          'factor': 1e6, 'axis': 3}),
                ('Ipol', {'experiment': 'MAC', 'signal': 'Ipolsola',
                          'label': r'I$_{shunt}$ [kA]',
                          'factor': -1e3, 'limit': [0, 5], 'axis':4}),
                ('teDiv', {'experiment': 'LSD', 'signal': 'te-ua5',
                           'label': r'T$_{div}$[eV]',
                           'factor': 1, 'axis': 5})])
            c = mds.Connection('localhost:8001')
            for i, k in enumerate(Diagnostic.keys()):
                try:
                    signal = Diagnostic[k]['signal']
                    diag = Diagnostic[k]['experiment']
                    if k == 'Ipol':
                        _s = 'augsignal(' + str(shot) + ',"' + \
                            diag + '","' + signal + '")'
                        _st = 'dim_of(augsignal(' + str(shot) + ',"' + \
                            diag + '","' + signal + '"))'
                    else:
                        _s = 'augdiag(' + str(shot) + ',"' + \
                            diag + '","' + signal + '")'
                        _st = 'dim_of(augdiag(' + str(shot) + ',"' + \
                            diag + '","' + signal + '"))'
                    data = c.get(_s).data()
                    time = c.get(_st).data()
                    if k == 'teDiv':
                        data = smooth.smooth(data, window_len=50)
                    if k == 'Edensity':
                        ax[Diagnostic[k]['axis']].plot(time,
                                                       data / Diagnostic[k]['factor'], color='#00548C',
                                                       label=r'H-5')
                    elif k == 'ecrh':
                        ax[Diagnostic[k]['axis']].plot(time,
                                                       data / Diagnostic[k]['factor'], color='#00548C',
                                                       label=r'ECRH')
                    else:
                        ax[Diagnostic[k]['axis']].plot(time,
                                                       data / Diagnostic[k]['factor'], color='#C70039')
                        ax[Diagnostic[k]['axis']].text(
                            0.1, 0.87, Diagnostic[k]['label'],
                            transform=ax[Diagnostic[k]['axis']].transAxes,
                            fontsize=13)
                except Exception:
                    print('Signal' + k + ' For shot {} not found'.format(shot))
                    continue
            ax[0].set_ylim([0, 15])
            ax[-1].set_xlim([0, 4])
            ax[4].set_ylim([0, 10])
            ax[-1].set_xlabel(r't[s]')
            lg = ax[0].legend(loc='best', numpoints=1, frameon=False)
            lg = ax[3].legend(loc='best', numpoints=1, frameon=False)
            fig.savefig('../pdfbox/GeneralPlot_{}.pdf'.format(shot),
                        bbox_to_inches='tight')

    elif selection == 13:
        shotList = (35882, 35884)
        colorTimeList = ('#00548C', '#C70039', '#FF5733')
        timeList = (((2.245, 2.255), (2.3, 2.4), (2.65, 2.7)),
                    ((1.75, 1.85), (2.05, 2.15)))
        for shot, tList in zip(shotList, timeList):
            fig, ax = mpl.pylab.subplots(figsize=(7, 10), nrows=3, ncols=1)
            fig.subplots_adjust(hspace=0.28, right=0.96, top=0.94, left=0.16)
            c = mds.Connection('localhost:8001')
            _s = 'augdiag('+str(shot)+',"TOT","H-5_corr")'
            _st = 'dim_of(augdiag('+str(shot)+',"TOT","H-5_corr"))'
            data = c.get(_s).data()
            time = c.get(_st).data()
            ax[0].plot(time, data/1e19, lw=3)
            ax[0].set_ylabel(r'n$_e^e [10^{19}$m$^{-3}$]')
            ax[0].set_ylim([0, 11])
            ax[0].set_xlim([0, 3.5])
            # then plot the Wmhd
            _s = 'augdiag('+str(shot)+',"GQI","Wmhd")'
            _st = 'dim_of(augdiag('+str(shot)+',"GQI","Wmhd"))'
            data = c.get(_s).data()
            time = c.get(_st).data()
            ax[1].plot(time, data/1e6, lw=3)
            ax[1].set_ylabel(r'W$_{\mathrm{MHD}}$ [MJ]')
            ax[1].set_ylim([0, .7])
            ax[1].set_xlim([0, 3.5])
            ax[1].set_xlabel(r't [s]')
            # then read the Li-Be
            _s = 'augdiag('+str(shot)+',"LIN","ne", "LIBE")'
            _st = 'dim_of(augdiag('+str(shot)+',"LIN","ne", "LIBE"), 0)'
            _sr = 'dim_of(augdiag('+str(shot)+',"LIN","ne", "LIBE"), 1)'
            LiBes = c.get(_s).data()
            LiTime = c.get(_st).data()
            LiRho = c.get(_sr).data()
            for t, _colorT in zip(
                    tList, colorTimeList):
                _dummyTime = np.where((LiTime >= t[0]) & (LiTime <= t[1]))
                _dummyLib = LiBes[:, _dummyTime].ravel()/1e19
                _dummyRho = LiRho[:, _dummyTime].ravel()

                yOut, bins, bin_means, bin_width, xOut = bin_by.bin_by(
                    _dummyRho, _dummyLib, nbins=30)
                xB = np.asarray([np.nanmean(k) for k in xOut])
                xBE = np.asarray([np.nanstd(k) for k in xOut])
                yB = np.asarray([np.nanmean(k) for k in yOut])
                yBE = np.asarray([np.nanstd(k) for k in yOut])
                xN = np.linspace(0.95, 1.08, 200)
                # be sure to include rho = 1
                xN = np.append(xN, 1)
                xN = xN[np.argsort(xN)]
                # perform the fit
                try:
                    gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                        xB[~np.isnan(yB)], yB[~np.isnan(yB)
                                              ], yBE[~np.isnan(yB)],
                        xe=xBE, nrestarts=100, length_scale=0.5,
                        epsilon=5.e-3, regularization_parameter=1)
                    gp.GPRFit(xN)
                    yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                    enS = yFit[np.argmin(np.abs(xN-1))]
                    ax[2].semilogy(xN, yFit/enS, '-', lw=2, color=_colorT)
                    ax[2].fill_between(xN,
                                       (yFit-yFitE/2)/enS,
                                       (yFit+yFitE/2)/enS,
                                       facecolor=_colorT,
                                       edgecolor='none',
                                       alpha=0.3)
                except Exception:
                    enS = yB[np.argmin(np.abs(xB-1))]
                    continue
                ax[2].semilogy(_dummyRho, _dummyLib/enS,
                               '.', color=_colorT, alpha=0.3)

            ax[2].set_ylabel(r'n$_e$/n$_e(\rho_p = 1)$')
            ax[2].set_xlabel(r'$\rho$')
            ax[2].set_ylim([1e-2, 3])
            ax[2].set_xlim([0.95, 1.1])
            ax[0].set_title(r'#{}'.format(shot))
            for _t, _c in zip(tList, colorTimeList):
                ax[0].axvline((_t[0]+_t[1])/2, ls='-', lw=2, color=_c)
                ax[1].axvline((_t[0] + _t[1]) / 2, ls='-', lw=2, color=_c)

            fig.savefig('../pdfbox/UpstreamProfiles{}.pdf'.format(
                        shot),
                        bbox_to_inches='tight')

    elif selection == 14:
        shotList = (33339, 34102)
        colorList = ('#318BA3', '#FF6000')
        OuterTarget = OrderedDict([
            ('ua1', {'R': 1.582, 'z': -1.199, 's': 1.045}),
            ('ua2', {'R': 1.588, 'z': -1.175, 's': 1.070}),
            ('ua3', {'R': 1.595, 'z': -1.151, 's': 1.094}),
            ('ua4', {'R': 1.601, 'z': -1.127, 's': 1.126}),
            ('ua5', {'R': 1.608, 'z': -1.103, 's': 1.158}),
            ('ua6', {'R': 1.614, 'z': -1.078, 's': 1.189}),
            ('ua7', {'R': 1.620, 'z': -1.054, 's': 1.213}),
            ('ua8', {'R': 1.627, 'z': -1.030, 's': 1.246}),
            ('ua9', {'R': 1.640, 'z': -0.982, 's': 1.276})])
        # this is the plot vs edge density
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(top=0.95, left=0.17, right=0.96, bottom=0.16)
        # create a second plot vs fueling
        fig2, ax2 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig2.subplots_adjust(top=0.95, left=0.17, right=0.96, bottom=0.16)
        # create a third plot vs neutral pressure
        fig3, ax3 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig3.subplots_adjust(top=0.95, left=0.17, right=0.96, bottom=0.16)
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            outDivSignal = np.asarray([])
            neTime = c.get(
                'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(
                    shot, 'ua1')).data()
            for i, s in enumerate(OuterTarget.keys()):
                _ne = c.get(
                    'augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
                _te = c.get(
                    'augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
                _an = c.get(
                    'augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
                _cs = np.sqrt(constants.e * 4 * _te/(2*constants.m_p))
                # this is the ion flux
                _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

                if i == 0:
                    outDivSignal = _s
                else:
                    outDivSignal = np.vstack((outDivSignal,
                                              _s))
            # now we compute the total integrate ion flux
            outTarget = np.zeros(neTime.size)
            for i in range(neTime.size):
                _x = np.asarray([OuterTarget[k]['s']
                                 for k in OuterTarget.keys()])
                _r = np.asarray([OuterTarget[k]['R']
                                 for k in OuterTarget.keys()])
                _y = outDivSignal[:, i]
                _dummy = np.vstack((_x, _y)).transpose()
                _dummy = _dummy[~np.isnan(_dummy).any(1)]
                _x = _dummy[:, 0]
                _y = _dummy[:, 1][np.argsort(_x)]
                _x = np.sort(_x)
                outTarget[i] = 2*np.pi*_r.mean()*np.trapz(
                    _y, x=_x)
            if shot == 33339:
                tmin, tmax = 1.5, 5.45
            else:
                tmin, tmax = 1.5, 3.9
            _idx = np.where((neTime >= tmin) & (neTime <= tmax))[0]
            # smooth theme since they exhibit spikes
            outTarget = smooth.smooth(outTarget[_idx], window_len=200)
            neTime = neTime[_idx]
            # create a Spline object
            SOut = interp1d(neTime,
                            outTarget, fill_value='extrapolate')
            eN = c.get(
                'augdiag({}, "TOT", "H-5_corr","AUGD",1)'.format(shot)).data()/1e20
            time = c.get(
                'dim_of(augdiag({}, "TOT", "H-5_corr","AUGD",1)'.format(
                    shot)+')').data()
            # limit to the range [tmin, tmax]
            _idx = np.where((time >= tmin) &
                            (time <= tmax))[0]
            time = time[_idx]
            eN = eN[_idx]
            _x = eN
            _y = SOut(time)
            if (_x.size) != (_y.size):
                _imx = np.minimum(_x.size, _y.size)
                _x = _x[:_imx]
                _y = _y[:_imx]
            _y = smooth.smooth(_y[np.argsort(_x)], window_len=20)/1e23
            _x = _x[np.argsort(_x)]
            ax.plot(_x, _y, '-', color=col, label=r'#{}'.format(shot))

            # replicate the plot with evolution vs fueling
            Dtot = c.get(
                'augdiag({}, "UVS", "D_tot","AUGD",1)'.format(shot)).data()/1e22
            time = c.get(
                'dim_of(augdiag({}, "UVS", "D_tot","AUGD",1)'.format(
                    shot)+')').data()
            # limit to the range [tmin, tmax]
            _idx = np.where((time >= tmin) &
                            (time <= tmax))[0]
            time = time[_idx]
            Dtot = Dtot[_idx]
            _x = Dtot
            _y = SOut(time)
            if (_x.size) != (_y.size):
                _imx = np.minimum(_x.size, _y.size)
                _x = _x[:_imx]
                _y = _y[:_imx]
            _y = smooth.smooth(_y[np.argsort(_x)], window_len=20)/1e23
            _x = _x[np.argsort(_x)]
            ax2.plot(_x, _y, '-', color=col, label=r'#{}'.format(shot))

            # replicate the plot with evolution vs neutral pressure
            Dtot = c.get(
                'augdiag({}, "IOC", "F03","AUGD",1)'.format(shot)).data()/1e23
            time = c.get(
                'dim_of(augdiag({}, "IOC", "F03","AUGD",1)'.format(
                    shot)+')').data()
            # limit to the range [tmin, tmax]
            _idx = np.where((time >= tmin) &
                            (time <= tmax))[0]
            time = time[_idx]
            Dtot = Dtot[_idx]
            _x = Dtot
            _y = SOut(time)
            if (_x.size) != (_y.size):
                _imx = np.minimum(_x.size, _y.size)
                _x = _x[:_imx]
                _y = _y[:_imx]
            _y = smooth.smooth(_y[np.argsort(_x)], window_len=20)/1e23
            _x = _x[np.argsort(_x)]
            ax3.plot(_x, _y, '-', color=col, label=r'#{}'.format(shot))

        ax.set_xlim([0, 0.5])
        ax.set_xlabel(r'n$_e^e [10^{20}$m$^{-3}$]')
        ax.set_ylabel(r'[10$^{23}$ ion/s]')
        lg = ax.legend(loc='upper left', numpoints=1,
                       frameon=False, fontsize=16)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/CompareIonFlux{}_{}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inches='tight')
        ax2.set_xlim([0, 2.5])
        ax2.set_xlabel(r'$\Gamma_{D_2}$ [10$^{22}$ el/s]')
        ax2.set_ylabel(r'[10$^{23}$ ion/s]')
        lg = ax2.legend(loc='upper right', numpoints=1,
                        frameon=False, fontsize=16)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig2.savefig('../pdfbox/CompareIonFluxFueling{}_{}.pdf'.format(
            shotList[0], shotList[1]), bbox_to_inches='tight')

        ax3.set_xlim([0, 2.2])
        ax3.set_xlabel(r'[10$^{23}$ molecules/m$^2$/s]')
        ax3.set_ylabel(r'[10$^{23}$ ion/s]')
        lg = ax3.legend(loc='upper left', numpoints=1,
                        frameon=False, fontsize=16)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig3.savefig('../pdfbox/CompareIonFluxNeutralPressure{}_{}.pdf'.format(
            shotList[0], shotList[1]), bbox_to_inches='tight')

    elif selection == 15:
        shotList = (33339, 34102)
        colorList = ('#318BA3', '#FF6000')
        fig, ax = mpl.pylab.subplots(figsize=(8, 13),
                                     nrows=4, ncols=1, sharex=True)
        fig.subplots_adjust(top=0.95, right=0.95, left=0.18, bottom=0.12)
        Diagnostic = OrderedDict([
            ('density', {'experiment': 'TOT', 'signal': 'H-5_corr',
                         'label': r'n$_e^e [10^{19}$m$^{-3}$]',
                         'factor': 1e19}),
            ('D2', {'experiment': 'UVS', 'signal': 'D_tot',
                    'label': r'D$_2$ [$10^{22}$el/s]',
                    'factor': 1e22}),
            ('pressure', {'experiment': 'IOC', 'signal': 'F03',
                          'label': r'[10$^{23}$molecules/m$^{2}$/s]',
                          'factor': 1e23}),
            ('teDiv', {'experiment': 'LSD', 'signal': 'te-uab',
                       'label': r'T$_{div}$[eV]',
                       'factor': 1})])
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            for i, k in enumerate(Diagnostic.keys()):
                try:
                    signal = Diagnostic[k]['signal']
                    diag = Diagnostic[k]['experiment']
                    _s = 'augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '","AUGD",1)'
                    _st = 'dim_of(augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '","AUGD",1))'
                    data = c.get(_s).data()
                    time = c.get(_st).data()
                    if k == 'teDiv':
                        data = smooth.smooth(data, window_len=50)
                    ax[i].plot(time,
                               data / Diagnostic[k]['factor'],
                               label=r'#{}'.format(shot))
                    ax[i].text(0.1, 0.87, Diagnostic[k]['label'], transform=ax[i].transAxes,
                               fontsize=13)
                except Exception:
                    print('Signal' + k + ' For shot {} not found'.format(shot))
                    continue
        ax[0].set_ylim([0, 10])
        ax[-1].set_xlim([0, 6.5])
        ax[-1].set_xlabel(r't[s]')
        lg = ax[0].legend(loc='best', numpoints=1, frameon=False)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/CompareGeneralWaveform{}_{}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inchets='tight')

    elif selection == 16:
        shotList = (30732, 36346)
        colorList = ('#081B42', '#BC2764')
        fig, ax = mpl.pylab.subplots(figsize=(8, 15),
                                     nrows=5, ncols=1, sharex=True)
        fig.subplots_adjust(top=0.95, right=0.95, left=0.1, bottom=0.08)
        Diagnostic = OrderedDict([
            ('density', {'experiment': 'TOT', 'signal': 'H-5_corr',
                         'label': r'n$_e^e [10^{19}$m$^{-3}$]',
                         'factor': 1e19}),
            ('D2', {'experiment': 'UVS', 'signal': 'D_tot',
                    'label': r'D$_2$ [$10^{22}$el/s]',
                    'factor': 1e22}),
            ('pressure', {'experiment': 'IOC', 'signal': 'F01',
                          'label': r'[10$^{23}$molecules/m$^{2}$/s]',
                          'factor': 1e23}),
            ('teDiv', {'experiment': 'LSD', 'signal': 'te-uab',
                       'label': r'T$_{div}$[eV]',
                       'factor': 1}),
            ('power', {'experiment': 'NIS', 'signal': 'PNI', 'label': r'NBI [MW]',
                       'factor': 1e6})])
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            for i, k in enumerate(Diagnostic.keys()):
                try:
                    signal = Diagnostic[k]['signal']
                    diag = Diagnostic[k]['experiment']
                    if diag == 'IOC' and shot != 36346:
                        signal = 'F03'
                    _s = 'augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '","AUGD",1)'
                    _st = 'dim_of(augdiag(' + str(shot) + ',"' + \
                        diag + '","' + signal + '","AUGD",1))'
                    data = c.get(_s).data()
                    time = c.get(_st).data()
                    if k == 'teDiv':
                        time = time[~np.isnan(data)]
                        data = bn.move_mean(data[~np.isnan(data)], 150)
                    if k == 'CH21':
                        data = data[3, :]
                        time = time[~np.isnan(data)]
                        data = bn.move_mean(data[~np.isnan(data)], 400)
                    ax[i].plot(time,
                               data / Diagnostic[k]['factor'],
                               label=r'#{}'.format(shot), color=col)
                    ax[i].text(0.1, 0.87, Diagnostic[k]['label'], transform=ax[i].transAxes,
                               fontsize=16)
                except Exception:
                    print('Signal' + k + ' For shot {} not found'.format(shot))
                    continue
        ax[0].set_ylim([0, 10])
        ax[-1].set_xlim([0, 8.])
        ax[-1].set_xlabel(r't[s]')
        lg = ax[0].legend(loc='best', numpoints=1, frameon=False)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/CompareGeneralWaveform{}_{}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inchets='tight')

        # compare as well the ion-flux to the target
        OuterTarget = OrderedDict([
            ('ua1', {'R': 1.582, 'z': -1.199, 's': 1.045}),
            ('ua2', {'R': 1.588, 'z': -1.175, 's': 1.070}),
            ('ua3', {'R': 1.595, 'z': -1.151, 's': 1.094}),
            ('ua4', {'R': 1.601, 'z': -1.127, 's': 1.126}),
            ('ua5', {'R': 1.608, 'z': -1.103, 's': 1.158}),
            ('ua6', {'R': 1.614, 'z': -1.078, 's': 1.189}),
            ('ua7', {'R': 1.620, 'z': -1.054, 's': 1.213}),
            ('ua8', {'R': 1.627, 'z': -1.030, 's': 1.246}),
            ('ua9', {'R': 1.640, 'z': -0.982, 's': 1.276})])
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(top=0.95, left=0.17, right=0.96, bottom=0.16)
        c = mds.Connection('localhost:8001')
        for shot, col in zip(shotList, colorList):
            outDivSignal = np.asarray([])
            neTime = c.get(
                'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(
                    shot, 'ua1')).data()
            for i, s in enumerate(OuterTarget.keys()):
                _ne = c.get(
                    'augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
                _te = c.get(
                    'augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
                _an = c.get(
                    'augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
                _cs = np.sqrt(constants.e * 4 * _te/(2*constants.m_p))
                # this is the ion flux
                _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

                if i == 0:
                    outDivSignal = _s
                else:
                    outDivSignal = np.vstack((outDivSignal,
                                              _s))
            # now we compute the total integrate ion flux
            outTarget = np.zeros(neTime.size)
            for i in range(neTime.size):
                _x = np.asarray([OuterTarget[k]['s']
                                 for k in OuterTarget.keys()])
                _r = np.asarray([OuterTarget[k]['R']
                                 for k in OuterTarget.keys()])
                _y = outDivSignal[:, i]
                _dummy = np.vstack((_x, _y)).transpose()
                _dummy = _dummy[~np.isnan(_dummy).any(1)]
                _x = _dummy[:, 0]
                _y = _dummy[:, 1][np.argsort(_x)]
                _x = np.sort(_x)
                outTarget[i] = 2*np.pi*_r.mean()*np.trapz(
                    _y, x=_x)
            # each of the quantity is considered only in the
            # flat top
            outTarget = bn.move_mean(outTarget, 400)
            ax.plot(neTime, outTarget, '-', color=col,
                    label=r'#{}'.format(shot))
        ax.set_xlim([0, 9])
        ax.set_xlabel(r't [s]')
        ax.set_ylabel(r'[10$^{23}$ ion/s]')
        lg = ax.legend(loc='upper left', numpoints=1,
                       frameon=False, fontsize=16)
        for t, c in zip(lg.get_texts(), colorList):
            t.set_color(c)
        fig.savefig('../pdfbox/IonFluxComparison{}_{}.pdf'.format(shotList[0], shotList[1]),
                    bbox_to_inches='tight')
    elif selection == 17:
        t = [0, 1.75, 1.75, 2.25, 3.75, 7.5, 7.5, 8]
        D2 = [0, 0, 15, 15, 80, 80, 0, 0]
        tMid = [0, 5.5, 5.5, 7.5, 7.5, 8]
        D2Mid = [0, 0, 30, 30, 0, 0]
        tNBI = [0, 1.75, 1.75, 4.5, 4.5, 6.75, 6.75, 7.5, 7.5, 8]
        PNBI = [0, 0, 7.5, 7.5, 12, 12, 9.5, 9.5, 0, 0]
        tEch = [0, 1.75, 1.75, 7.5, 7.5, 8]
        Ech = [0, 0, 4, 4, 0, 0]
        tMarkers = [1.75, 2.25, 3.75, 4.5, 6.75, 7.5]
        fig, ax = mpl.pylab.subplots(
            figsize=(8, 6), nrows=2, ncols=1, sharex=True)
        fig.subplots_adjust(wspace=0.05, bottom=0.12)
        ax[0].plot(t, D2, lw=2, color='k')
        ax[0].set_ylabel(r'D$_2$ [10$^{21}$ s$^{-1}$]')
        ax[0].tick_params(labelbottom=False)
        ax[1].plot(tNBI, PNBI, lw=2, color='b')
        ax[1].plot(tEch, Ech, lw=2, color='g')
        ax[1].text(2.1, 8, '7.5 MW', color='b', fontsize=15)
        ax[1].text(5.2, 13, '12 MW', color='b', fontsize=15)
        ax[1].text(7, 10, '9.5MW', color='b', fontsize=15)
        ax[1].set_ylim([0, 15])
        ax[1].text(4, 5, '4 MW', color='g', fontsize=15)
        ax[1].set_ylabel(r'[MW]')
        ax[1].set_xlabel(r't [s]')
        for _t in tMarkers:
            ax[0].axvline(_t, ls='--', color='grey', lw=0.7)
            ax[0].text(_t+0.1, 40, str(_t), rotation=90,
                       fontsize=12, color='gray')
            ax[1].axvline(_t, ls='--', color='grey', lw=0.7)
            ax[1].text(_t + 0.1, 12.5, str(_t), rotation=90,
                       fontsize=12, color='gray')
        ax[0].axhline(15, ls='--', color='grey', lw=0.7)
        ax[0].text(0.2, 16, r'1.5$\times 10^{22}$ s$^{-1}$', fontsize=12)
        ax[0].axhline(80, ls='--', color='grey', lw=0.7)
        ax[0].text(0.2, 85, r'8$\times 10^{22}$ s$^{-1}$', fontsize=12)
        ax[0].set_ylim([0, 95])
        fig.savefig('../pdfbox/ProposeHmodeHighPower.pdf',
                    bbox_to_inches='tight')
        fig.savefig('../pngbox/ProposeHmodeHighPower.png',
                    bbox_to_inches='tight', dpi=200)

        t = [0, 1.75, 1.75, 2.25, 3.75, 7.5, 7.5, 8]
        D2 = [0, 0, 15, 15, 80, 80,  0, 0]
        tMid = [0, 5.5, 5.5, 7.5, 7.5, 8]
        D2Mid = [0, 0, 30, 30, 0, 0]
        tNBI = [0, 1.75, 1.75, 4.5, 4.5, 6.75, 6.75, 7.5, 7.5, 8]
        PNBI = [0, 0, 7.5, 7.5, 12, 12, 7.5, 7.5, 0, 0]
        tEch = [0, 1.75, 1.75, 7.5, 7.5, 8]
        Ech = [0, 0, 4, 4, 0, 0]
        tMarkers = [1.75, 2.25, 3.75, 4.5, 5.5, 6.75, 7.5]
        fig, ax = mpl.pylab.subplots(
            figsize=(8, 6), nrows=2, ncols=1, sharex=True)
        fig.subplots_adjust(wspace=0.05, bottom=0.12)
        ax[0].plot(t, D2, lw=2, color='k')
        ax[0].plot(tMid, D2Mid, lw=2, color='r', label='Midplane')
        ax[0].set_ylabel(r'D$_2$ [10$^{21}$ s$^{-1}$]')

        ax[0].tick_params(labelbottom=False)
        ax[1].plot(tNBI, PNBI, lw=2, color='b')
        ax[1].plot(tEch, Ech, lw=2, color='g')
        ax[1].text(2.1, 8, '7.5 MW', color='b')
        ax[1].text(5.2, 13, '12 MW', color='b')
        ax[1].set_ylim([0, 15])
        ax[1].text(4, 5, '4 MW', color='g')
        ax[1].set_ylabel(r'[MW]')
        ax[1].set_xlabel(r't [s]')
        for _t in tMarkers:
            ax[0].axvline(_t, ls='--', color='grey', lw=0.7)
            ax[0].text(_t + 0.1, 70, str(_t), rotation=90,
                       fontsize=12, color='gray')
            ax[1].axvline(_t, ls='--', color='grey', lw=0.7)
            ax[1].text(_t + 0.1, 12.5, str(_t), rotation=90,
                       fontsize=12, color='gray')
        ax[0].axhline(15, ls='--', color='grey', lw=0.7)
        ax[0].text(0.05, 16, r'1.5$\times 10^{22}$ s$^{-1}$', fontsize=12)
        ax[0].axhline(80, ls='--', color='grey', lw=0.7)
        ax[0].text(0.05, 85, r'8$\times 10^{22}$ s$^{-1}$', fontsize=12)
        ax[0].axhline(30, ls='--', color='r', lw=0.7)
        ax[0].text(-0.15, 33,
                   r'Midplane 3$\times 10^{22}$ s$^{-1}$', color='r', fontsize=12)
        ax[0].set_ylim([0, 95])
        fig.savefig('../pdfbox/ProposeHmodeHighPowerMidplane.pdf',
                    bbox_to_inches='tight')
        fig.savefig('../pngbox/ProposeHmodeHighPowerMidplane.png',
                    bbox_to_inches='tight', dpi=200)

        # compare for the N2 foreseen experiment the planned and achieved
        t = [0, 0.25, 0.5, 0.75, 1, 1, 1.25, 1.5, 1.75, 1.75, 2, 2, 2.25, 2.45, 2.75, 2.75, 3, 3.25, 3.5, 3.5, 3.75, 4,
             4.25, 4.5, 4.75, 5, 5, 5.25, 5.45, 5.45, 5.5, 5.75, 6, 6.25, 6.3, 6.3, 6.5, 6.75, 7]

        D2_1 = [0, 0, 0, 0, 0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 2.166666667, 3.5, 5.5, 5.5, 7.166666667, 8.833333333,
                10.5, 10.5, 12.16666667, 13.83333333, 15.5, 17.16666667, 18.83333333, 20.5, 20.5, 22.16666667, 23.5, 23.5,
                0, 0, 0, 0, 0, 0, 0, 0, 0]

        D2_2 = [0, 0, 0, 0, 0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 2.17, 3.5, 3.5, 3.5, 3.5, 3.5, 3.5, 3.5, 3.5, 3.5, 3.5,
                3.5, 3.5, 3.5, 3.5, 3.5, 3.5, 3.5, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        N2p = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1.024390244, 1.024390244, 1.87804878, 2.731707317, 3.585365854,
               3.585365854, 4.43902439, 5.292682927, 6.146341463, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        c = mds.Connection('localhost:8001')
        shot, diag, signal = 36347, 'UVS', 'N_tot'
        _s = 'augdiag(' + str(shot) + ',"' + \
             diag + '","' + signal + '","AUGD",1)'
        _st = 'dim_of(augdiag(' + str(shot) + ',"' + \
              diag + '","' + signal + '","AUGD",1))'
        N2 = c.get(_s).data()/1e21
        tN2 = c.get(_st).data()
        shot, diag, signal = 36347, 'UVS', 'D_tot'
        _s = 'augdiag(' + str(shot) + ',"' + \
             diag + '","' + signal + '","AUGD",1)'
        _st = 'dim_of(augdiag(' + str(shot) + ',"' + \
              diag + '","' + signal + '","AUGD",1))'
        D2 = c.get(_s).data()/1e21
        tD2 = c.get(_st).data()
        fig, ax = mpl.pylab.subplots(
            figsize=(8, 6), nrows=2, ncols=1, sharex=True)
        fig.subplots_adjust(wspace=0.05, bottom=0.12)
        ax[0].plot(t, D2_2, lw=2, color='k', label='planned')
        ax[0].plot(tD2, D2, lw=2, color='r', label=r'#{}'.format(shot))
        ax[0].tick_params(labelbottom=False)
        ax[0].set_ylabel(r'D$_2$ [10$^{21}$ el/s]')
        ax[1].plot(t, N2p, lw=2, color='k', label='planned')
        ax[1].plot(tN2, N2, lw=2, color='r', label=r'#{}'.format(shot))
        ax[1].set_ylabel(r'N$_2$ [10$^{21}$ el/s]')
        ax[1].legend(loc='best', numpoints=1, frameon=False)
        ax[0].legend(loc='best', numpoints=1, frameon=False)
        ax[1].set_xlim([0, 7])
        ax[1].set_xlabel(r't [s]')
        fig.savefig('../pdfbox/PlannedVsShot{}.pdf'.format(shot),
                    bbox_to_inches='tight')
        fig.savefig('../pngbox/PlannedVsShot{}.png'.format(shot),
                    bbox_to_inches='tight', dpi=200)

        tD = [0,  1, 1, 2, 2.5, 5.5, 5.5, 7]
        D2 = [0, 0, 0.5, 0.5, 3.5, 3.5, 0, 0]
        tN = [0, 2.5, 4.5, 5.5, 5.5, 7]
        N2p = [0, 0, 7, 7, 0, 0]
        fig, ax = mpl.pylab.subplots(figsize=(8, 4), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.17)
        ax.plot(tD, D2, lw=2, color='k', label=r'D$_2$')
        ax.set_ylabel(r'[10$^{21}$ el/s]')
        ax.plot(tN, N2p, lw=2, color='r', label=r'N$_2$')
        ax.text(6, 6, r'D$_2$', fontsize=20)
        ax.text(6, 5., r'N$_2$', fontsize=20, color='r')
        ax.set_xlim([0, 7])
        ax.set_xlabel(r't [s]')
        tMarkers = np.unique(np.append(tD, tN))
        tMarkers = tMarkers[1:-1]
        for _t in tMarkers:
            ax.axvline(_t, ls='--', color='grey', lw=0.7)
            ax.text(_t+0.1, 2, str(_t)+' s', color='grey',
                    fontsize=12, rotation=90)
        ax.axhline(0.5, ls='--', color='k', lw=0.7)
        ax.axhline(3.5, ls='--', color='k', lw=0.7)
        ax.axhline(7, ls='--', color='r', lw=0.7)
        ax.text(0.05, 0.7, r'0.5$\times 10^{21}$ el/s', color='k', fontsize=12)
        ax.text(0.05, 3.7, r'3.5$\times 10^{21}$ el/s', color='k', fontsize=12)
        ax.text(0.05, 6.3, r'7$\times 10^{21}$ el/s', color='r', fontsize=12)

        fig.savefig('../pdfbox/PlannedN2Lmode.pdf'.format(shot),
                    bbox_to_inches='tight')
        fig.savefig('../pngbox/PlannedN2Lmode.png'.format(shot),
                    bbox_to_inches='tight', dpi=200)

    elif selection == 18:
        t = [0, 1.75, 1.75, 2.25, 3.75, 4.5, 4.5, 5.1, 5.1, 7.5, 7.5, 8]
        D2 = [0, 0, 15, 15, 80, 80, 80, 80, 50, 50, 0, 0]
        D2Mid = [0, 0, 0, 0, 0, 0, 30, 30, 30, 30, 0, 0]
        tNBI = [0, 1.75, 1.75, 4., 4., 6.75, 6.75, 7.5, 7.5, 8]
        PNBI = [0, 0, 7.5, 7.5, 12, 12, 9.5, 9.5, 0, 0]
        tEch = [0, 1.75, 1.75, 7.5, 7.5, 8]
        Ech = [0, 0, 4, 4, 0, 0]
        tMarkers = [1.75, 2.25, 3.75, 4, 4.5, 5.1, 6.75, 7.5]
        fig, ax = mpl.pylab.subplots(
            figsize=(8, 6), nrows=2, ncols=1, sharex=True )
        fig.subplots_adjust( wspace=0.05, bottom=0.12 )
        ax[0].plot( t, D2, lw=2, color='b' )
        ax[0].plot( t, D2Mid, lw=2, color='r', label='Midplane' )
        ax[0].plot( t, np.asarray(D2) + np.asarray(D2Mid), lw=2,color='k')
        ax[0].set_ylabel( r'D$_2$ [10$^{21}$ s$^{-1}$]' )

        ax[0].tick_params( labelbottom=False )
        ax[1].plot( tNBI, PNBI, lw=2, color='b' )
        ax[1].plot( tEch, Ech, lw=2, color='g' )
        ax[1].text( 2.1, 8, '7.5 MW', color='b', fontsize=14)
        ax[1].text( 5.2, 13, '12 MW', color='b', fontsize=14)
        ax[1].text( 7, 10.5, '9.5 MW', color='b', fontsize=14)
        ax[1].set_ylim( [0, 18] )
        ax[1].text( 4, 5, '4 MW', color='g', fontsize=14)
        ax[1].set_ylabel( r'[MW]' )
        ax[1].set_xlabel( r't [s]' )
        for _t in tMarkers:
            ax[0].axvline( _t, ls='--', color='grey', lw=0.7 )
            ax[0].text( _t + 0.1, 70, str( _t ), rotation=90,
                        fontsize=12, color='gray' )
            ax[1].axvline( _t, ls='--', color='grey', lw=0.7 )
            ax[1].text( _t + 0.1, 2.5, str( _t ), rotation=90,
                        fontsize=12, color='gray' )
        ax[0].axhline( 15, ls='--', color='blue', lw=0.7 )
        ax[0].text( 0.05, 16, r'1.5$\times 10^{22}$ s$^{-1}$', fontsize=12, color='b' )
        ax[0].axhline( 80, ls='--', color='blue', lw=0.7 )
        ax[0].text( 0.05, 85, r'8$\times 10^{22}$ s$^{-1}$', fontsize=12, color='b')
        ax[0].axhline( 50, ls='--', color='blue', lw=0.7 )
        ax[0].text( 0.05, 55, r'5$\times 10^{22}$ s$^{-1}$', fontsize=12, color='b')
        ax[0].axhline( 30, ls='--', color='r', lw=0.7 )
        ax[0].text( -0.15, 33,
                    r'Midplane 3$\times 10^{22}$ s$^{-1}$', color='r', fontsize=12 )
        ax[0].set_ylim( [0, 115] )
        fig.savefig( '../pdfbox/ProposeHmodeHighPowerMidplaneNew.pdf',
                     bbox_to_inches='tight' )
        fig.savefig( '../pngbox/ProposeHmodeHighPowerMidplaneNew.png',
                     bbox_to_inches='tight', dpi=200 )
    elif selection == 99:
        mpl.pylab.close('all')
        loop = False
    else:
        input("Unknown Option Selected!")
