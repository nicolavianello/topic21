import numpy as np
import matplotlib as mpl
import sys
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rc("font", size=22)
mpl.rc('font', **{'family': 'sans-serif', 'sans-serif': ['Helvetica']})
mpl.rc("lines", linewidth=2)

def print_menu():
    print(30 * "-", "MENU", 30 * "-")
    print("1. Waveform shot 1 CW10/2020 midplane fuelling")
    print("2. Waveform shot 3 CW10/2020 N2 seeding FB")
    print(67 * "-")


loop = True
while loop:
    print_menu()
    selection = int(input("Enter your choice [1-99] "))
    if selection == 1:
        fig,ax = mpl.pylab.subplots(figsize=(8,6), nrows=2,ncols=1,sharex='col')
        fig.subplots_adjust(hspace=0.05)
        tHeat = np.asarray([0,1,1,1.4,1.4,7,7])
        nbh = np.asarray([0,0,2.5,2.5,5,5,0])
        ech = np.asarray([0,0,2.,2.,2,2,0])
        ax[0].plot(tHeat, nbh, lw=2,color='k')
        ax[0].plot(tHeat,ech,lw=2,color='g')
        ax[0].text(2.5, 4., 'NBH = 5MW', color='k')
        ax[0].text(2.5, 2.2, 'ECH = 2MW', color='g')
        ax[0].set_ylabel(r'MW')
        ax[0].tick_params(labelbottom=False)

        tDiv = np.asarray([0,0.4,1.1,7,7])
        Div = np.asarray([0,0,6,6,0])
        tMid = np.asarray([0,1.2,1.2,6,7,7])
        Mid = np.asarray([0,0,1.2,24,24,0])
        ax[1].plot(tDiv, Div, color='k', lw=2)
        ax[1].plot(tMid, Mid, color='b', lw=2)
        ax[1].set_ylabel(r'Fuel [10$^{21}$]')
        for l in (1.2, 24):
            ax[1].axhline(l,ls='--',color='b')
        ax[1].axhline(6,ls='--',color='k')
        ax[1].text(0,6.2,r'Div 6$\times 10^{21}$',color='k')
        ax[1].text(4,1.45,r'Mid 1.2$\times 10^{21}$',color='b')
        ax[1].text(0,20.,r'Mid 24$\times 10^{21}$',color='b')
        ax[1].set_xlabel(r't[s]')
        fig.savefig('../pdfbox/CW10_2020_FuelingShot.pdf',bbox_inches='tight')
        fig.savefig('../pngbox/CW10_2020_FuelingShot.png',bbox_inches='tight',dpi=300)
        for _ax in ax:
            _ax.autoscale(enable=True,axis='both')

    elif selection == 2:
        fig,ax = mpl.pylab.subplots(figsize=(8,9), nrows=3,ncols=1,sharex='col')
        fig.subplots_adjust(hspace=0.05)
        tHeat = np.asarray([0,1,1,1.4,1.4,7,7])
        nbh = np.asarray([0,0,2.5,2.5,5,5,0])
        ech = np.asarray([0,0,2.,2.,2,2,0])
        ax[0].plot(tHeat, nbh, lw=2,color='k')
        ax[0].plot(tHeat,ech,lw=2,color='g')
        ax[0].text(2.5, 4., 'NBH = 5MW', color='k')
        ax[0].text(2.5, 2.5, 'ECH = 2MW', color='g')
        ax[0].set_ylabel(r'MW')
        ax[0].tick_params(labelbottom=False)

        tDiv = np.asarray([0,0.4,2,5.1,5.1,7])
        Div = np.asarray([0,0,2,2,0.5,0.5])
        ax[1].plot(tDiv, Div, color='k', lw=2)
        ax[1].set_ylabel(r'Fuel [10$^{21}$]')
        for l in Div:
            ax[1].axhline(l,ls='--',color='k')
        ax[1].axhline(2,ls='--',color='k')
        ax[1].axhline(0.5,ls='--',color='k')
        ax[1].text(0,1.7,r'Div 2$\times 10^{21}$',color='k')
        ax[1].text(2,0.7,r'Div 0.5$\times 10^{21}$',color='k')
        ax[1].tick_params(labelbottom=False)
        tTe = np.asarray([2.1,3.5,5.1])
        Te = np.asarray([15,5,5])
        ax[2].plot(tTe,Te,lw=2,color='orange')
        ax[2].set_ylabel(r'T$_{div}$ FB [eV]')
        ax[2].set_xlabel(r't [s]')

        for _ax in ax:
            _ax.autoscale(enable=True,axis='both')
        fig.savefig('../pdfbox/CW10_2020_SeedingShot.pdf',bbox_inches='tight')
        fig.savefig('../pngbox/CW10_2020_SeedingShot.png',bbox_inches='tight',dpi=300)
    elif selection == 99:
        mpl.pylab.close('all')
        loop = False
    else:
        input("Unknown Option Selected!")