import numpy as np
import matplotlib.pyplot as plt
import map_equ
import dd
import libes


def bin_data(x,Nbins):
    xx = np.array(x)
    slices = np.linspace(0,xx.shape[0],Nbins+1,True).astype(np.int)
    counts = np.diff(slices)
    mean = np.add.reduceat(xx,slices[:-1])/counts
    return mean







if __name__=='__main__':

    times = [5.0,6.0]
    tlim = [0,7.8]
    t_av = 0.2

    Nbins = 500

    shots = [36342,36343,36345,36346]
    c = ['g','r','b','k']
    ls = ['-','--',':',':']
    fig,axs = plt.subplots(6,1,sharex=True)
    fig2,axs2 = plt.subplots(2,1,sharex=True)
    for i,shot in enumerate(shots):
        
        Eq = map_equ.equ_map()
        Eq.Open(shot)


        nis = dd.shotfile('NIS',shot)
        PNbi = nis('PNI')

        axs[0].plot(PNbi.time,PNbi.data/1e6,label=str(shot),c=c[i],lw=2,alpha=0.7)

        #ecs = dd.shotfile('ECS',shot)
        #PEch = ecs('PECRH')
        #axs[0].plot(PEch.time,PEch.data,label=str(shot)+':ECH')

        uvs = dd.shotfile('UVS',shot)
        D2 = uvs('D_tot')
        
        axs[1].plot(D2.time,D2.data/1e22,label=str(shot)+':D2',c=c[i],lw=2,alpha=0.7)

        Mac = dd.shotfile("MAC",shot)
        Tdiv = Mac('Tdiv')
    
        axs[4].plot(Tdiv.time,Tdiv.data,label=str(shot)+':Tdiv',c=c[i],lw=2,alpha=0.7)
        
        fpg = dd.shotfile("FPG",shot)
        wmhd = fpg('Wmhd')

        axs[2].plot(wmhd.time,wmhd.data/1e6,label=str(shot)+':WMHD',c=c[i],lw=2,alpha=0.7)

        dcn = dd.shotfile('DCN',shot)
        neh5 = dcn('H-5')
        
        axs[3].plot(neh5.time,neh5.data/1e19,c=c[i],lw=2,alpha=0.7)
        

        lsf = dd.shotfile('LSF',shot)

        lsf_t = bin_data(lsf('CH21').time,Nbins)
        #ua1 = bin_data(lsf('CH21').data[1],Nbins)
        #ua2 = bin_data(lsf('CH21').data[2],Nbins) 
        ua3 = bin_data(lsf('CH21').data[3],Nbins)
        #ua4 = bin_data(lsf('CH22').data[1],Nbins)
        #ua5 = bin_data(lsf('CH22').data[2],Nbins)
        #ua6 = bin_data(lsf('CH22').data[4],Nbins)
        #ua7 = bin_data(lsf('CH23').data[0],Nbins)
        #ua8 = bin_data(lsf('CH23').data[2],Nbins)
        #ua9 = bin_data(lsf('CH23').data[4],Nbins)


        axs[5].plot(lsf_t,ua3,c=c[i],lw=2,alpha=0.7)

        Lib = libes.Libes(shot)
        for j,t in enumerate(times):
            ne,err,efold,neN,errN = Lib.averageProfile([t-t_av,t+t_av],interelm=False)
            if i == 0:
                axs2[0].semilogy(Lib.rho,ne,c=c[i],ls=ls[j],lw=3,alpha=0.7,label='t = '+str(t)+'s')
                axs2[1].semilogy(Lib.rho,neN,c=c[i],ls=ls[j],lw=3,alpha=0.7,label='t = '+str(t)+'s')
            else:
                axs2[0].semilogy(Lib.rho,ne,c=c[i],ls=ls[j],lw=3,alpha=0.7)
                axs2[1].semilogy(Lib.rho,neN,c=c[i],ls=ls[j],lw=3,alpha=0.7)


axs[0].legend(fancybox=True,loc=2)
axs[0].set_xlim(tlim)

axs2[0].set_ylim(1e19,5e19)

axs2[1].set_xlim(1.0,1.06)
axs2[1].set_ylim(0.3,1.0)
axs[4].set_ylim(0,40)
axs2[0].legend(fancybox=True)  

axs2[1].set_ylabel(r'ne ($1\times10^{19} m^{-3}$)')
axs2[1].set_xlabel(r'$\rho_{pol}$')
axs2[1].set_ylabel('ne/ne$_{sep}$')

axs[0].set_ylabel('NBI Power (MW)')
axs[1].set_ylabel('D2 $(10^{22}el/s)$')
axs[2].set_ylabel('WMHD (MJ)')
axs[3].set_ylabel('ne-H5 ($10^{19}m^{-3}$)')
axs[4].set_ylabel('Tdiv (eV)')
axs[5].set_ylabel('Isat at SP (arb)')
axs[5].set_xlabel('Time (s)')

fig.set_size_inches(8,12)
fig.savefig('T16_W22_H-mode_overview.pdf',bbox_inches='tight')
fig2.set_size_inches(8,12)
fig2.savefig('T16_W22_H-mode_LiB_profiles.pdf',bbox_inches='tight')

plt.show()


