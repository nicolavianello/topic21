# Plot Filament Statistics from HEC raw data

"""
peak_start_time # list (rad_channels, fil. #), fil. start times
peak_stop_time  # list (rad_channels, fil. #), fil. stop times
fil_duration    # list (rad_channels, fil. #), fil. duration
fil_amplitude   # list (rad_channels, fil. #), fil. amplitude
waiting_time    # list (rad_channels, fil. # ), times waiting times

rad_corr_len    # float, radial correlation length
pol_corr_len    # float, poliodal correlation length
v_calc          # float, calculated mean velocity over self and rad. correlation

LOS_names_rad   # array (rad_channels), LOS names
rho_pol_mean    # array (rad_channels), mean pol. position over time
R_MAJ_mean      # array (rad_channels), mean radial position over time

self_corr_time  # array (rad_channels), self correlation time
N_peak          # array (rad_channels), number of filaments
blob_freq       # array (rad_channels), filament frequency
skew            # array (rad_channels), skewness

velocity_pol_all# array (fil. #), pol velocity for filament number at outermost channel
velocity_all    # array fil. #), rad velocity for filament number at HEB+0+0
"""
# Michael Griener
# 06.06.2019

#import pdb
from matplotlib.pyplot import * 
from myObject import myObject

#from matplotlib.widgets import MultiCursor # make a cursor for the plots
import matplotlib.offsetbox as offsetbox # text box which looks like legend

import numpy as np


sys.path.append('/afs/ipp/home/m/mgrien/Python/functions')
import Farben	# own color commands
reload(Farben)

from smooth import smooth # smooth(data,smoothingwindow)

def find_index(array, value): # returns index of first value <= value
        index = array[array <= value].size
        if index == array.size:
            index = index -1
        return index
	
def nan_helper(y):
    """
    Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """
    return np.isnan(y), lambda z: z.nonzero()[0]
    
def cm2inch(tupl):
    inch = 2.54
    return tupl/inch	
#%% plot options -----------------------------------------------------------------------
close('all') 	# close all open figures
interactive(True) # enables interactive mode

rcParams['text.usetex'] = False

fsize = 14
lsize = fsize # legend size

#matplotlib.use('GTK3Agg')

matplotlib.rc('font', family='sans-serif') 
matplotlib.rc('font', size=fsize) 
matplotlib.rc('font', weight='normal') # normal, bold, light
matplotlib.rc('axes', labelweight='normal') # same for labels
matplotlib.rc('font', style='normal') # or italic
matplotlib.rc('legend', fontsize= lsize)

matplotlib.rc('text', usetex= False) 
#matplotlib.rc('text.latex', preamble='\usepackage{sfmath}')  # load this package for sans serif fonts
    
matplotlib.rcParams['lines.linewidth'] = 2

fig_width = cm2inch(15)           # cm
fig_high = cm2inch(13)            # cm 
my_dpi = 100                      # cm              # cm

y_tick_nr = 3
#%% ============================================================================================================
## --- put in shot number here --------------------------------------------------------------------------------

shot_Nr = 35880

# start and stop of the profile
t_start_1 = 1.95
t_stop_1  = 2.1

t_start_2 = 2.45
t_stop_2  = 2.6

t_start_3 = 3.05
t_stop_3  = 3.2
'''
shot_Nr = 35822

# start and stop of the profile
t_start_1 = 2.05
t_stop_1  = 2.2

t_start_2 = 2.35
t_stop_2  = 2.5

t_start_3 = 2.65
t_stop_3  = 2.8
'''
sel_PMT = 'PMT1'

times_start = [t_start_1, t_start_2, t_start_3]
times_stop = [t_stop_1, t_stop_2, t_stop_3]

if sel_PMT == 'PMT1':  sel_wavelen = 587 
if sel_PMT == 'PMT2':  sel_wavelen = 667 
if sel_PMT == 'PMT3':  sel_wavelen = 706 
if sel_PMT == 'PMT4':  sel_wavelen = 728 
#%% ====================================================================================================================
# --- load the results in the object ------------------------------------------------------------------------------------
myObj = myThbObject.myThbObject('%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_1, t_stop_1, sel_PMT))

LOS_names_rad_1 = myObj.LOS_names_rad 
rho_pol_mean_1 = myObj.rho_pol_mean
R_MAJ_mean_1 = myObj.R_MAJ_mean 

peak_start_time_1 = myObj.peak_start_time
peak_stop_time_1 = myObj.peak_stop_time
fil_duration_1 = myObj.fil_duration 
fil_amplitude_1 = myObj.fil_amplitude 
waiting_time_1 = myObj.waiting_time 

self_corr_time_1 = myObj.self_corr_time 
N_peak_1 = myObj.N_peak 
blob_freq_1 = myObj.blob_freq 
skew_1 = myObj.skew 

rad_corr_len_1 = myObj.rad_corr_len 
pol_corr_len_1 = myObj.pol_corr_len
v_calc_1 = myObj.v_calc

emission_profile_1 = myObj.emission_profile 
velocity_pol_1 = myObj.velocity_pol_all
velocity_1 = myObj.velocity_all 
'''
# if available, you can average over the results from different wavelength
myObj = myObject('/afs/ipp/home/m/mgrien/Python/Filaments/filament_data/%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_1, t_stop_1, 'PMT1'))
velocity_pol_1_1  = myObj.velocity_pol_all
velocity_1_1 = myObj.velocity_all 
myObj = myObject('/afs/ipp/home/m/mgrien/Python/Filaments/filament_data/%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_1, t_stop_1, 'PMT2'))
velocity_pol_1_2  = myObj.velocity_pol_all
velocity_1_2 = myObj.velocity_all 
myObj = myObject('/afs/ipp/home/m/mgrien/Python/Filaments/filament_data/%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_1, t_stop_1, 'PMT3'))
velocity_pol_1_3  = myObj.velocity_pol_all
velocity_1_3 = myObj.velocity_all 

velocity_1 = np.concatenate((velocity_1_1, velocity_1_2, velocity_1_3))
velocity_pol_1 = np.concatenate((velocity_pol_1_1, velocity_pol_1_2, velocity_pol_1_3))
'''
# --- load the results in the object -----------------------------------------------------------------------------------
myObj = myThbObject.myThbObject('%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_2, t_stop_2, sel_PMT))
LOS_names_rad_2  = myObj.LOS_names_rad
rho_pol_mean_2  = myObj.rho_pol_mean
R_MAJ_mean_2  = myObj.R_MAJ_mean

peak_start_time_2  = myObj.peak_start_time
peak_stop_time_2  = myObj.peak_stop_time
fil_duration_2  = myObj.fil_duration 
fil_amplitude_2  = myObj.fil_amplitude 
waiting_time_2  = myObj.waiting_time 

self_corr_time_2  = myObj.self_corr_time 
N_peak_2  = myObj.N_peak 
blob_freq_2  = myObj.blob_freq 
skew_2  = myObj.skew 

rad_corr_len_2  = myObj.rad_corr_len 
pol_corr_len_2  = myObj.pol_corr_len
v_calc_2  = myObj.v_calc

emission_profile_2 = myObj.emission_profile 
velocity_pol_2 = myObj.velocity_pol_all
velocity_2 = myObj.velocity_all 
'''
myObj = myObject('/afs/ipp/home/m/mgrien/Python/Filaments/filament_data/%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_2, t_stop_2, 'PMT1'))
velocity_pol_2_1  = myObj.velocity_pol_all
velocity_2_1 = myObj.velocity_all 
myObj = myObject('/afs/ipp/home/m/mgrien/Python/Filaments/filament_data/%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_2, t_stop_2, 'PMT2'))
velocity_pol_2_2  = myObj.velocity_pol_all
velocity_2_2 = myObj.velocity_all 
myObj = myObject('/afs/ipp/home/m/mgrien/Python/Filaments/filament_data/%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_2, t_stop_2, 'PMT3'))
velocity_pol_2_3  = myObj.velocity_pol_all
velocity_2_3 = myObj.velocity_all 

velocity_2 = np.concatenate((velocity_2_1, velocity_2_2, velocity_2_3))
velocity_pol_2 = np.concatenate((velocity_pol_2_1, velocity_pol_2_2, velocity_pol_2_3))
'''
# --- load the results in the object -----------------------------------------------------------------------------------
myObj = myThbObject.myThbObject('%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_3, t_stop_3, sel_PMT))
LOS_names_rad_3  = myObj.LOS_names_rad 
rho_pol_mean_3  = myObj.rho_pol_mean
R_MAJ_mean_3  = myObj.R_MAJ_mean 

peak_start_time_3  = myObj.peak_start_time
peak_stop_time_3  = myObj.peak_stop_time
fil_duration_3  = myObj.fil_duration 
fil_amplitude_3  = myObj.fil_amplitude 
waiting_time_3  = myObj.waiting_time 

self_corr_time_3  = myObj.self_corr_time 
N_peak_3  = myObj.N_peak 
blob_freq_3  = myObj.blob_freq 
skew_3  = myObj.skew 

rad_corr_len_3  = myObj.rad_corr_len 
pol_corr_len_3  = myObj.pol_corr_len
v_calc_3  = myObj.v_calc

emission_profile_3 = myObj.emission_profile 
velocity_pol_3 = myObj.velocity_pol_all
velocity_3 = myObj.velocity_all 
'''
myObj = myObject('/afs/ipp/home/m/mgrien/Python/Filaments/filament_data/%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_3, t_stop_3, 'PMT1'))
velocity_pol_3_1  = myObj.velocity_pol_all
velocity_3_1 = myObj.velocity_all 
myObj = myObject('/afs/ipp/home/m/mgrien/Python/Filaments/filament_data/%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_3, t_stop_3, 'PMT2'))
velocity_pol_3_2  = myObj.velocity_pol_all
velocity_3_2 = myObj.velocity_all 
peak_start_time_3_PMT2 = myObj.peak_start_time
myObj = myObject('/afs/ipp/home/m/mgrien/Python/Filaments/filament_data/%i_t_%0.3f_%0.3f_%s'
                 %(shot_Nr,t_start_3, t_stop_3, 'PMT3'))
velocity_pol_3_3  = myObj.velocity_pol_all
velocity_3_3 = myObj.velocity_all 

velocity_3 = np.concatenate((velocity_3_1, velocity_3_2, velocity_3_3))
velocity_pol_3 = np.concatenate((velocity_pol_3_1, velocity_pol_3_2, velocity_pol_3_3))
'''
#%% =============================================================================================================
# --- plot the mean emission profile along the radius ---   
rho_pol_mean = [rho_pol_mean_1, rho_pol_mean_2, rho_pol_mean_3]
emission_profile = [emission_profile_1, emission_profile_2,emission_profile_3]
for i in range(len(times_start)): 
    fig = figure('Mean emission profile # %05d HEC, t = %0.3f-%0.3f s'
             %(shot_Nr, times_start[i], times_stop[i]))
    clf()
    fig.suptitle('Emission profile %i nm\n # %05d, $t$ = %0.3f-%0.3f s'
             %(sel_wavelen, shot_Nr, times_start[i], times_stop[i]))
    plot1 = fig.add_subplot(111)
    
    plot1.plot(rho_pol_mean[i], emission_profile[i], marker = 'x') 
    
    plot1.set_xlabel('$\\rho_{\mathrm{pol}}$')
    plot1.set_ylabel('emission (arb. unit)')
  
    # --- set figure properties ---
    fig.set_size_inches(fig_width, fig_high)
    mgr = get_current_fig_manager()
    mgr.window.setGeometry(cm2inch(2)*my_dpi,cm2inch(2)*my_dpi,fig_width*my_dpi,fig_high*my_dpi)
    subplots_adjust(top = 0.85, left = 0.14, right = 0.85, bottom = 0.13, wspace = 0.1, hspace = 0.2) 
    fig.savefig('output/Vianello/emission_profile_%05d_t_%0.2f_%0.2f_s.png'%(shot_Nr, times_start[i], times_stop[i]), dpi=my_dpi)
    #fig.savefig('output/spectra/spec_%05d_%i.eps'%(shot_Nr, channel), format='eps', dpi=my_dpi)
        
#%% =============================================================================================================
# --- skewness and blob frequency ---
skew = [skew_1, skew_2, skew_3]
blob_freq = [blob_freq_1, blob_freq_2, blob_freq_3]
for i in range(len(times_start)):
    # --- plot the skewness and the blob frequency in one plot ---
    fig = figure('Skewness and blob frequency # %05d t = %0.2f-%0.2f s'%(shot_Nr, times_start[i], times_stop[i]))
    clf()
    fig.suptitle('Skewness and blob frequency # %05d\n $t$ = %0.3f-%0.3f s'%(shot_Nr, times_start[i], times_stop[i]))
    plot1 = fig.add_subplot(111)
    color = 'tab:red'
    plot1.plot(rho_pol_mean[i], skew[i], color=color)
    plot1.set_ylabel('skewness', color=color)
    plot1.tick_params(axis='y', labelcolor=color)
    
    plot2 = plot1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:blue'
    plot2.plot(rho_pol_mean[i], blob_freq[i], color=color)
    plot2.set_ylabel('blob freq. (Hz)', color=color)
    plot2.tick_params(axis='y', labelcolor=color)
    
    plot1.grid()
    plot1.set_xlabel('$\\rho_{\mathrm{pol}}$')
    plot1.set_xlim(np.min(rho_pol_mean),np.max(rho_pol_mean))
    # --- set figure properties ---
    fig.set_size_inches(fig_width, fig_high)
    mgr = get_current_fig_manager()
    mgr.window.setGeometry(cm2inch(2)*my_dpi,cm2inch(2)*my_dpi,fig_width*my_dpi,fig_high*my_dpi)
    subplots_adjust(top = 0.85, left = 0.14, right = 0.85, bottom = 0.13, wspace = 0.1, hspace = 0.2) 
    fig.savefig('output/Vianello/skewness_%05d_t_%0.2f_%0.2f_s.png'%(shot_Nr, times_start[i], times_stop[i]), dpi=my_dpi)
    #fig.savefig('output/spectra/spec_%05d_%i.eps'%(shot_Nr, channel), format='eps', dpi=my_dpi)
        
#%% =============================================================================================================
#  --- plot a histogram of the velocity, L- and H-mode seperated -----------------------------------------------
fig = figure('radial velocity histogram')
clf()
plot1 = fig.add_subplot(111)

# inter ELM
v_1 = velocity_1/1e3
v_2 = velocity_2/1e3
v_3 = velocity_3/1e3
hist_bins = np.linspace(0,1.250, 26)
weights_1 = np.ones_like(v_1)/float(len(v_1))
weights_2 = np.ones_like(v_2)/float(len(v_2))
weights_3 = np.ones_like(v_3)/float(len(v_3))

vel = [v_1, v_2, v_3]
plot1.hist(vel, hist_bins,  weights=[weights_1,weights_2,weights_3], 
    color = [Farben.co[0], Farben.co[1], Farben.co[2]], label = 
    ['%0.2f-%0.2f s'%(t_start_1,t_stop_1),'%0.2f-%0.2f s'%(t_start_2,t_stop_2),'%0.2f-%0.2f s'%(t_start_3,t_stop_3)])
#plot1.hist(velocity_L, hist_bins,  weights=weights_L, color = 'green', label = 'L-mode')
#plot1.hist(velocity_H, hist_bins,  weights=weights_H, color = 'red', label = 'H-mode')

plot1.set_xlabel('velocity (km/s)')
plot1.set_ylabel('probability')
#plot1.yaxis.set_label_coords(-0.15,0.5) # set the y-label distance to plot
plot1.tick_params(axis='both', which='major', pad=6) # distance of tickmarks to axis
plot1.legend(loc = 5)
#plot1.set_ylim(0,0.3) 
#ob = offsetbox.AnchoredText('a)', loc=2, frameon = False)
#plot1.add_artist(ob)
ob = offsetbox.AnchoredText('radial filament velocity\n                      # %i'%shot_Nr, loc=1, frameon = False)
plot1.add_artist(ob)

# --- set figure properties --- 
fig_width = cm2inch(24)           # cm
fig_high = cm2inch(10)            # cm 

fig.set_size_inches(fig_width, fig_high)
mgr = get_current_fig_manager()

mgr.window.setGeometry(cm2inch(2)*my_dpi,cm2inch(2)*my_dpi,fig_width*my_dpi,fig_high*my_dpi)
subplots_adjust(top = 0.95, left = 0.10, right = 0.96, bottom = 0.18, wspace = 0.28, hspace = 0.0) 
fig.savefig('output/Vianello/rad_vel_hist_%i.png'%shot_Nr, dpi=my_dpi)
#fig.savefig('output/rad_vel_hist_L_H.eps', format='eps', dpi=my_dpi)  

#%% =============================================================================================================
#  --- print results -----------------------------------------------
# --- timespan 1 ---
print('=== Summarized Results ===\n')
print('#%i, time = %0.2f-%0.2f s'%(shot_Nr, t_start_1, t_stop_1))
print('Number of filaments: %i'%N_peak_1[-1])
print('Blob frequency: %i Hz'%blob_freq_1[-1])
print('Self correltation time at rho_pol = %0.2f: %0.1f micro s'%(rho_pol_mean_1[-1],self_corr_time_1[-1]*1e6))
print('Radial correltation length : %0.1f mm'%(rad_corr_len_1*1e3))
print('Poliodal correltation length : %0.1f mm'%(pol_corr_len_1*1e3))
print('Calculated velocity from self corr. time and rad. corr time: %0.0f m/s'%v_calc_1)
print('Velocity by radial correlation of signals: Median: %0.0f m/s, Mean: %0.0f+-%0.0f'
      %(np.nanmedian(velocity_1), np.nanmean(velocity_1), np.nanstd(velocity_1)))
if len(velocity_pol_1)>0:
    print('Poliodal velocity at rho_pol = %0.2f: Median: %0.0f m/s, Mean: %0.0f+-%0.0f'
          %(rho_pol_mean_1[4], np.nanmedian(velocity_pol_1), np.nanmean(velocity_pol_1), np.nanstd(velocity_pol_1)))
else:
    print('Poliodal velocity at rho_pol = %0.2f: NaN'%rho_pol_mean_1[4])
print('Fil. amplitude at rho_pol = %0.2f:  %0.1f +- %0.1f (arb. unit)'
      %(rho_pol_mean_1[-1], np.mean(fil_amplitude_1[-1]), np.std(fil_amplitude_1[-1])))
print('Fil. duration at rho_pol = %0.2f:  %0.0f +- %0.0f (micros s)'
      %(rho_pol_mean_1[-1], np.mean(fil_duration_1[-1])*1e6, np.std(fil_duration_1[-1])*1e6))

# --- timespan 2 ---
print('\n#%i, time = %0.2f-%0.2f s'%(shot_Nr, t_start_2, t_stop_2))
print('Number of filaments: %i'%N_peak_2[-1])
print('Blob frequency: %i Hz'%blob_freq_2[-1])
print('Self correltation time at rho_pol = %0.2f: %0.1f micro s'%(rho_pol_mean_2[-1],self_corr_time_2[-1]*1e6))
print('Radial correltation length : %0.1f mm'%(rad_corr_len_2*1e3))
print('Poliodal correltation length : %0.1f mm'%(pol_corr_len_2*1e3))
print('Calculated velocity from self corr. time and rad. corr time: %0.0f m/s'%v_calc_2)
print('Velocity by radial correlation of signals: Median: %0.0f m/s, Mean: %0.0f+-%0.0f'
      %(np.nanmedian(velocity_2), np.nanmean(velocity_2), np.nanstd(velocity_2)))
if len(velocity_pol_2)>0:
    print('Poliodal velocity at rho_pol = %0.2f: Median: %0.0f m/s, Mean: %0.0f+-%0.0f'
          %(rho_pol_mean_2[4], np.nanmedian(velocity_pol_2), np.nanmean(velocity_pol_2), np.nanstd(velocity_pol_2)))
else:
    print('Poliodal velocity at rho_pol = %0.2f: NaN'%rho_pol_mean_2[4])
print('Fil. amplitude at rho_pol = %0.2f:  %0.1f +- %0.1f (arb. unit)'
      %(rho_pol_mean_2[-1], np.mean(fil_amplitude_2[-1]), np.std(fil_amplitude_2[-1])))
print('Fil. duration at rho_pol = %0.2f:  %0.0f +- %0.0f (micros s)'
      %(rho_pol_mean_2[-1], np.mean(fil_duration_2[-1])*1e6, np.std(fil_duration_2[-1])*1e6))

# --- timespan 3 ---
print('\n#%i, time = %0.2f-%0.2f s'%(shot_Nr, t_start_3, t_stop_3))
print('Number of filaments: %i'%N_peak_3[-1])
print('Blob frequency: %i Hz'%blob_freq_3[-1])
print('Self correltation time at rho_pol = %0.2f: %0.1f micro s'%(rho_pol_mean_3[-1],self_corr_time_3[-1]*1e6))
print('Radial correltation length : %0.1f mm'%(rad_corr_len_3*1e3))
print('Poliodal correltation length : %0.1f mm'%(pol_corr_len_3*1e3))
print('Calculated velocity from self corr. time and rad. corr time: %0.0f m/s'%v_calc_3)
print('Velocity by radial correlation of signals: Median: %0.0f m/s, Mean: %0.0f+-%0.0f'
      %(np.nanmedian(velocity_3), np.nanmean(velocity_3), np.nanstd(velocity_3)))
if len(velocity_pol_3)>0:
    print('Poliodal velocity at rho_pol = %0.2f: Median: %0.0f m/s, Mean: %0.0f+-%0.0f'
          %(rho_pol_mean_3[4], np.nanmedian(velocity_pol_3), np.nanmean(velocity_pol_3), np.nanstd(velocity_pol_3)))
else:
    print('Poliodal velocity at rho_pol = %0.2f: NaN'%rho_pol_mean_3[4])
print('Fil. amplitude at rho_pol = %0.2f:  %0.1f +- %0.1f (arb. unit)'
      %(rho_pol_mean_3[-1], np.mean(fil_amplitude_3[-1]), np.std(fil_amplitude_3[-1])))
print('Fil. duration at rho_pol = %0.2f:  %0.0f +- %0.0f (micros s)'
      %(rho_pol_mean_3[-1], np.mean(fil_duration_3[-1])*1e6, np.std(fil_duration_3[-1])*1e6))