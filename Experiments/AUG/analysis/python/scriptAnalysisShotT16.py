# script in order to compare heating fueling and equilibria
# for shot at different current/bt/q95 for the proper scan
from __future__ import print_function
import sys
import numpy as np
import matplotlib as mpl
from matplotlib.ticker import AutoMinorLocator
import MDSplus as mds
from collections import OrderedDict
import bin_by
from scipy.interpolate import UnivariateSpline
from scipy.signal import savgol_filter
from scipy import constants
import matplotlib.gridspec as gridspec
import bottleneck as bn

mpl.rcParams["font.family"] = "sans-serif"
mpl.rc("font", size=22)
mpl.rc("font", **{"family": "sans-serif", "sans-serif": ["Tahoma"]})


def print_menu():
    print(30 * "-", "MENU", 30 * "-")
    print("1. General plot H-Mode shots CW 22")
    print("2. General plot L-Mode shots CW 22")
    print(
        "3. Target ion flux evolution and upstream profiles for H-modes with power steps"
    )
    print("4. General plot H-Mode shots CW27")
    print("5. Compare general and profiles shot 36573 and 36574")
    print("6. Check target temperature 36574-36577 and profiles")
    print("7. Compare general profiles shot 36574 and 36605")
    print("8. Single profiles 36573, 36574, 36606")
    print("9 Single profiles 36573, 36574, 36606 with target profiles")
    print("10 Single profiles 34276 with target profiles")
    print("11. Single profiles 36573, 36574, 36606 upstream, downstream ne and Te")
    print("12 Single profiles 34276 with target profiles both ne and te")
    print("13. Target flux behavior shot 37444, 37447")
    print("99: End")
    print(67 * "-")


loop = True

while loop:
    print_menu()
    selection = int(input("Enter your choice [1-99] "))
    if selection == 1:
        # h-mode list shot for CW 22
        shotList = (36342, 36343, 36345, 36346)
        colorList = ("#BC1AF0", "#324D5C", "#60A65F", "#FF3B30")
        colorList = ("#BC1AF0", "#324D5C", "#F29E38", "#FF3B30")
        fig2, ax2 = mpl.pylab.subplots(figsize=(8, 15), nrows=7, ncols=1, sharex=True)
        fig2.subplots_adjust(hspace=0.05, top=0.94, bottom=0.1, left=0.1)
        exp = ("DCN", "UVS", "FPG", "NIS", "LSD", "LSF", "IOC")
        diag = ("H-5", "D_tot", "Wmhd", "PNI", "te-uab", "CH21", "F01")
        lim = (1e19, 1e21, 1e6, 1e6, 1, 1, 1e23)
        _label = ("(a)", "(b)", "(c)", "(d)", "(e)", "(f)", "(g)")
        for shot, col in zip(shotList, colorList):
            c = mds.Connection("localhost:8001")
            for i, (a, b, _norm, _t) in enumerate(zip(exp, diag, lim, _label)):
                _s = 'augsignal({},"'.format(shot) + a + '", "' + b + '")'
                y = c.get(_s).data()
                _s = 'dim_of(augsignal({},"'.format(shot) + a + '", "' + b + '"))'
                x = c.get(_s).data()
                if b == "te-uab":
                    x = x[~np.isnan(y)]
                    y = bn.move_mean(y[~np.isnan(y)], 400)
                if b == "CH21":
                    y = y[3, :]
                    x = x[~np.isnan(y)]
                    y = bn.move_mean(y[~np.isnan(y)], 400)
                label = r"#{}".format(shot)
                line = "-"

                ax2[i].plot(x, y / _norm, line, color=col, lw=3, label=label, alpha=0.7)
                ax2[i].text(0.9, 0.82, _t, transform=ax2[i].transAxes)

        for i in range(5):
            ax2[i].axes.get_xaxis().set_visible(False)
        label = (
            r"n$_e$ H-5 [10$^{19}$m$^{-2}$]",
            r"D$_2 [10^{21}$e$^{-}$/s]",
            r"W$_{MHD}$ [MJ]",
            r"P$_{\mathrm{NBI}}$ [MW]",
            r"T$_{div}$ [eV]",
            r"I$_{\mathrm{s}}$ [A]",
            r"[10$^{23}$ molecules/m$^2$/s]",
        )
        for i, l in enumerate(label):
            if i != 0:
                ax2[i].text(0.1, 0.8, l, transform=ax2[i].transAxes)

        ax2[0].text(0.5, 0.2, label[0], transform=ax2[0].transAxes)
        ax2[0].set_ylim([0, 8])
        ax2[1].set_ylim([0, 70])
        ax2[2].set_ylim([0, 0.8])
        ax2[3].set_ylim([0, 8])
        ax2[4].set_ylim([0, 20])
        ax2[5].set_ylim([0, 5])
        ax2[6].set_ylim([0, 4])
        ax2[6].set_xlim([0, 8])
        ax2[6].set_xlabel(r"t [s]")
        l = ax2[0].legend(
            loc="best", numpoints=1, markerscale=0, fontsize=16, frameon=False
        )
        for t, _c in zip(l.get_texts(), colorList):
            t.set_color(_c)
        ax2[0].set_title(r"AUG")
        fig2.savefig("../pdfbox/FigHModeCW22_2019.pdf", bbox_to_inches="tight", dpi=300)

    elif selection == 3:
        shotList = (36342, 36343, 36345, 36346)
        #        colorList = ('#BC1AF0', '#324D5C', '#60A65F', '#FF3B30')
        colorList = ("#BC1AF0", "#324D5C", "#F29E38", "#FF3B30")
        colorPrList = ("#3E9DD3", "#9A8D84", "#2BFF00", "#3B4859")
        thrListAll = (
            (1200, 900, 700, 700),
            (1200, 700, 700, 700),
            (1000, 700, 700, 500),
            (1200, 800, 800, 600),
        )
        trList = ((2.8, 2.9), (5.0, 5.1), (5.8, 5.9), (6.85, 6.95))
        c = mds.Connection("localhost:8001")
        OuterTarget = OrderedDict(
            [
                ("ua1", {"R": 1.582, "z": -1.199, "s": 1.045}),
                ("ua2", {"R": 1.588, "z": -1.175, "s": 1.070}),
                ("ua3", {"R": 1.595, "z": -1.151, "s": 1.094}),
                ("ua4", {"R": 1.601, "z": -1.127, "s": 1.126}),
                ("ua5", {"R": 1.608, "z": -1.103, "s": 1.158}),
                ("ua6", {"R": 1.614, "z": -1.078, "s": 1.189}),
                ("ua7", {"R": 1.620, "z": -1.054, "s": 1.213}),
                ("ua8", {"R": 1.627, "z": -1.030, "s": 1.246}),
                ("ua9", {"R": 1.640, "z": -0.982, "s": 1.276}),
            ]
        )


        # build the other figure with 4 panels for each shots
        fig4P, ax4P = mpl.pylab.subplots(
            figsize=(16, 8), nrows=2, ncols=4, sharey="row"
        )
        fig4P.subplots_adjust(
            left=0.1, right=0.95, wspace=0.05, hspace=0.23, bottom=0.1, top=0.95
        )
        for _i in range(4):
            ax4P[0, _i].xaxis.set_minor_locator(AutoMinorLocator())
            ax4P[0, _i].yaxis.set_minor_locator(AutoMinorLocator())
            ax4P[0, _i].set_xlabel(r"t[s]")
            ax4P[0, _i].set_xlim([0, 8])
            ax4P[0, _i].set_ylim([0, 2])
            ax4P[1, _i].xaxis.set_minor_locator(AutoMinorLocator())
            ax4P[1, _i].yaxis.set_major_locator(
                mpl.ticker.LogLocator(
                    base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
                )
            )
            ax4P[1, _i].set_xlabel(r"$\rho$")
            ax4P[1, _i].set_yscale("log")
            ax4P[1, _i].set_xlim([0.98, 1.1])
            ax4P[1, _i].set_ylim([5e-2, 3])
        for _i in np.linspace(1, 3, 3, dtype="int"):
            ax4P[0, _i].tick_params(labelleft=False)
            ax4P[1, _i].tick_params(labelleft=False)

        # build the figure
        fig = mpl.pylab.figure(figsize=(13, 11))
        gs1 = gridspec.GridSpec(
            nrows=3, ncols=4, right=0.96, wspace=0.05, hspace=0.27, top=0.95
        )
        # NBI
        axN = fig.add_subplot(gs1[0, 0:])
        # Ion flux
        axS = fig.add_subplot(gs1[1, 0:])
        # Profiles
        ax2 = fig.add_subplot(gs1[2, 0])
        ax3 = fig.add_subplot(gs1[2, 1])
        ax4 = fig.add_subplot(gs1[2, 2])
        ax5 = fig.add_subplot(gs1[2, 3])
        axList = (ax2, ax3, ax4, ax5)
        lineStyle = (":", "-.", "--", "-")

        # build a second plot where we determine outer-target behavior
        fig3, ax3 = mpl.pylab.subplots(figsize=(6, 8), nrows=2, ncols=1, sharex=True)
        fig3.subplots_adjust(left=0.17, hspace=0.05, top=0.95, bottom=0.13)
        for _ax in ax3.flatten():
            _ax.xaxis.set_minor_locator(AutoMinorLocator())
            _ax.yaxis.set_minor_locator(AutoMinorLocator())
        ax3[0].tick_params(labelbottom=False)
        ax3[1].set_xlim([0, 8])
        ax3[1].set_ylim([0, 2])
        ax3[0].set_ylim([0, 7])
        ax3[0].set_ylabel(r"P$_{\mathrm{NBI}}$ [MW]")
        ax3[1].set_ylabel(r"ion flux [10$^{23}$ /s]")
        ax3[1].set_xlabel(r"t [s]")
        for shotid, (shot, _c, _lst, _ax, thrList) in enumerate(
            zip(shotList, colorList, lineStyle, axList, thrListAll)
        ):
            # read the NBI and plot it
            _s = 'augsignal({},"NIS","PNI")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"NIS","PNI"))'.format(shot)
            axN.plot(
                c.get(_st).data(),
                Data / 1e6,
                _lst,
                color=_c,
                alpha=0.7,
                label=r"# {}".format(shot),
                lw=2.5,
            )
            ax3[0].plot(
                c.get(_st).data(),
                Data / 1e6,
                _lst,
                color=_c,
                alpha=0.7,
                label=r"# {}".format(shot),
                lw=2.5,
            )

            # collect also the data of LiBes and Ipol
            LiBes = c.get('augsignal({}, "LIN", "ne")'.format(shot)).data() / 1e19
            LiRho = c.get('dim_of(augsignal({},"LIN","ne"),1)'.format(shot)).data()
            LiTime = c.get('dim_of(augsignal({},"LIN","ne"))'.format(shot)).data()

            # read the IpolSolA per il masking del

            Ipol = c.get('augsignal({}, "MAC", "Ipolsoli")'.format(shot)).data()
            IpolTime = c.get(
                'dim_of(augsignal({}, "MAC", "Ipolsoli"))'.format(shot)
            ).data()
            # now compute the ion flux to the target
            outDivSignal = np.asarray([])
            neTime = c.get(
                'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(shot, "ua1")
            ).data()
            for i, s in enumerate(OuterTarget.keys()):
                _ne = c.get('augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
                _te = c.get('augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
                _an = c.get('augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
                _cs = np.sqrt(constants.e * 4 * _te / (2 * constants.m_p))
                # this is the ion flux
                _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

                if i == 0:
                    outDivSignal = _s
                else:
                    outDivSignal = np.vstack((outDivSignal, _s))
            outTarget = np.zeros(outDivSignal.shape[1])
            for i in range(neTime.size):
                _x = np.asarray([OuterTarget[k]["s"] for k in OuterTarget.keys()])
                _r = np.asarray([OuterTarget[k]["R"] for k in OuterTarget.keys()])
                _y = outDivSignal[:, i]
                _dummy = np.vstack((_x, _y)).transpose()
                _dummy = _dummy[~np.isnan(_dummy).any(1)]
                _x = _dummy[:, 0]
                _y = _dummy[:, 1][np.argsort(_x)]
                _x = np.sort(_x)
                outTarget[i] = 2 * np.pi * _r.mean() * np.trapz(_y, x=_x)
            outTarget = bn.move_mean(outTarget, 600) / 1e23
            axS.plot(neTime, outTarget, "-", color=_c, alpha=0.8)
            ax3[1].plot(neTime, outTarget, "-", color=_c, alpha=0.8)
            ax4P[0, shotid].plot(neTime, outTarget, "-", color=_c)
            ax4P[0, shotid].set_title(r"#{}".format(shot), color=_c)
            for i, (tr, _thr, _cp) in enumerate(zip(trList, thrList, colorPrList)):
                axN.axvline(np.mean(tr), ls="--", lw=2, color=_cp)
                axS.axvline(np.mean(tr), ls="--", lw=2, color=_cp)
                ax4P[0, shotid].axvline(np.mean(tr), ls="--", lw=2, color=_cp)
                _idxT = np.where(((IpolTime >= tr[0]) & (IpolTime <= tr[1])))[0]
                # now create an appropriate savgolfile
                IpolS = savgol_filter(Ipol[_idxT], 301, 3)
                IpolT = IpolTime[_idxT]
                IpolO = Ipol[_idxT]
                # we generate an UnivariateSpline object
                _dummyTime = LiTime[np.where((LiTime >= tr[0]) & (LiTime <= tr[1]))[0]]
                _dummyLib = LiBes[:, np.where((LiTime >= tr[0]) & (LiTime <= tr[1]))[0]]
                _dummyRho = LiRho[:, np.where((LiTime >= tr[0]) & (LiTime <= tr[1]))[0]]
                IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                # on these we choose a threshold
                # which can be set as also set as keyword
                ElmMask = np.zeros(IpolSp.size, dtype="bool")
                ElmMask[np.where(IpolSp > _thr)[0]] = True
                _interElm = np.where(ElmMask == False)[0]
                # in this way they are
                _dummyLib = _dummyLib[:, _interElm].ravel()
                _dummyRho = _dummyRho[:, _interElm].ravel()
                yOut, bins, bin_means, bin_width, xOut = bin_by.bin_by(
                    _dummyRho, _dummyLib, nbins=40
                )
                xB = np.asarray([np.nanmean(k) for k in xOut])
                xBE = np.asarray([np.nanstd(k) for k in xOut])
                yB = np.asarray([np.nanmean(k) for k in yOut])
                yBE = np.asarray([np.nanstd(k) for k in yOut])
                enS = yB[np.argmin(np.abs(xB - 1))]
                _ax.errorbar(
                    xB,
                    yB / enS,
                    xerr=xBE,
                    yerr=yBE / enS,
                    fmt="-",
                    color=_cp,
                    alpha=0.7,
                    lw=2.5,
                )
                ax4P[1, shotid].errorbar(
                    xB,
                    yB / enS,
                    xerr=xBE,
                    yerr=yBE / enS,
                    fmt="-",
                    color=_cp,
                    alpha=0.7,
                    lw=2.5,
                )

        for i, _ax in enumerate(axList):
            _ax.set_xlim([0.99, 1.1])
            _ax.set_ylim([1e-2, 3])
            _ax.set_xlabel(r"$\rho$")
            _ax.set_yscale("log")
            if i != 0:
                _ax.axes.get_yaxis().set_visible(False)
        ax2.set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        ax4P[1, 0].set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        axN.set_xlim([1, 8])
        axN.set_ylim([0, 7])
        axN.set_ylabel(r"P$_{\mathrm{NBI}}$ [MW]")
        lg = axN.legend(loc="upper left", frameon=False, fontsize=14, ncol=2)
        for _t, _c in zip(lg.get_texts(), colorList):
            _t.set_color(_c)
        lg = ax3[0].legend(loc="upper left", frameon=False, fontsize=14, ncol=2)
        for _t, _c in zip(lg.get_texts(), colorList):
            _t.set_color(_c)

        axS.set_xlim([1, 8])
        axS.set_ylim([0.7, 2.3])
        axS.set_ylabel(r"ion flux [s$^{-1}$]")
        axS.set_xlabel(r"t [s]")
        ax4P[0, 0].set_xlabel(r"t [s]")
        ax4P[0, 0].set_ylabel(r"ion flux [s$^{-1}$]")
        fig.savefig("../pdfbox/CW22_NBI_IonFlux_Upstream.pdf", bbox_to_inches="tight")
        fig3.savefig("../pdfbox/CW22_NBI_IonFlux.pdf", bbox_to_inches="tight")
        fig4P.savefig(
            "../pdfbox/C22_IonFlux_Upstream_4Panels.pdf", bbox_to_inches="tight"
        )
        fig2, axDummy = mpl.pylab.subplots(
            figsize=(6, 10), nrows=4, ncols=1, sharex=True, sharey=True
        )
        fig2.subplots_adjust(hspace=0.05, left=0.17, bottom=0.12, top=0.95)
        for i, (shot, _c) in enumerate(zip(shotList, colorList)):
            _s = 'augsignal({},"NIS","PNI")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"NIS","PNI"))'.format(shot)
            axDummy[i].plot(c.get(_st).data(), Data / 1e6, "-", lw=2, color=_c)
            axDummy[i].set_ylabel(r"P$_{\mathrm{NBI}}$ [MW]")
            axDummy[i].text(
                0.1, 0.85, r"#{}".format(shot), transform=axDummy[i].transAxes, color=_c
            )
            # overplot the pulse of the He valves in dashed gray
            _s = 'augsignal({},"HEB","S13VALVE")'.format(shot)
            _st = 'dim_of(augsignal({},"HEB","S13VALVE"))'.format(shot)
            Data = c.get(_s).data()
            time = c.get(_st).data()
            Data -= Data[np.where(time <= 0.5)[0]].mean()
            Data /= Data.max()
            axDummy[i].plot(time, Data, "--", color="grey")
        for i in range(3):
            axDummy[i].axes.get_xaxis().set_visible(False)
        axDummy[-1].set_xlabel(r"t [s]")
        axDummy[-1].set_ylim([0, 6.5])
        axDummy[-1].set_xlim([0, 8])
        axDummy[-1].set_yticks([0, 2.5, 5])
        fig2.savefig("../pdfbox/CW22_OnlyBeam.pdf", bbox_to_inches="tight")

    elif selection == 4:
        shotList = (36573, 36574, 36605)
        colorList = ("#BC1AF0", "#324D5C", "#F29E38", "#FF3B30")
        fig2, ax2 = mpl.pylab.subplots(figsize=(8, 15), nrows=7, ncols=1, sharex=True)
        fig2.subplots_adjust(hspace=0.05, top=0.94, bottom=0.1, left=0.1)
        exp = ("DCN", "UVS", "FPG", "TOT", "LSD", "LSF", "IOC")
        diag = ("H-5", "D_tot", "Wmhd", "P_TOT", "te-uab", "CH21", "F01")
        lim = (1e19, 1e21, 1e6, 1e6, 1, 1, 1e23)
        _label = ("(a)", "(b)", "(c)", "(d)", "(e)", "(f)", "(g)")
        for shot, col in zip(shotList, colorList):
            c = mds.Connection("localhost:8001")
            for i, (a, b, _norm, _t) in enumerate(zip(exp, diag, lim, _label)):
                _s = 'augsignal({},"'.format(shot) + a + '", "' + b + '")'
                y = c.get(_s).data()
                _s = 'dim_of(augsignal({},"'.format(shot) + a + '", "' + b + '"))'
                x = c.get(_s).data()
                if b == "te-uab":
                    x = x[~np.isnan(y)]
                    y = bn.move_mean(y[~np.isnan(y)], 400)
                if b == "CH21":
                    y = y[3, :]
                    x = x[~np.isnan(y)]
                    y = bn.move_mean(y[~np.isnan(y)], 400)
                label = r"#{}".format(shot)
                line = "-"

                ax2[i].plot(x, y / _norm, line, color=col, lw=3, label=label, alpha=0.7)
                ax2[i].text(0.9, 0.82, _t, transform=ax2[i].transAxes)

        for i in range(5):
            ax2[i].axes.get_xaxis().set_visible(False)
        label = (
            r"n$_e$ H-5 [10$^{19}$m$^{-2}$]",
            r"D$_2 [10^{21}$e$^{-}$/s]",
            r"W$_{MHD}$ [MJ]",
            r"P$_{\mathrm{tot}}$ [MW]",
            r"T$_{div}$ [eV]",
            r"I$_{\mathrm{s}}$ [A]",
            r"[10$^{23}$ molecules/m$^2$/s]",
        )
        for i, l in enumerate(label):
            if i != 0:
                ax2[i].text(0.1, 0.8, l, transform=ax2[i].transAxes)

        ax2[0].text(0.5, 0.2, label[0], transform=ax2[0].transAxes)
        ax2[0].set_ylim([0, 8])
        ax2[1].set_ylim([0, 70])
        ax2[2].set_ylim([0, 0.8])
        ax2[3].set_ylim([0, 8])
        ax2[4].set_ylim([0, 20])
        ax2[5].set_ylim([0, 5])
        ax2[6].set_ylim([0, 4])
        ax2[6].set_xlim([0, 8])
        ax2[6].set_xlabel(r"t [s]")
        l = ax2[0].legend(
            loc="best", numpoints=1, markerscale=0, fontsize=16, frameon=False
        )
        for t, _c in zip(l.get_texts(), colorList):
            t.set_color(_c)
        ax2[0].set_title(r"AUG")
        fig2.savefig("../pdfbox/FigHModeCW27_2019.pdf", bbox_to_inches="tight", dpi=300)

    elif selection == 5:
        shotList = (36573, 36574)
        colorShot = ("#3B4859", "#F20505")
        tList = ((1.9, 2.1), (3.9, 4.1), (4.9, 5.1), (6.15, 6.35))
        thresLAll = ((3000, 1000, 1000, -99), (3000, 1000, 1000, 1000))
        colorList = ("#2679B1", "#BC2764", "#00805E", "#F2811D")

        gs_top = mpl.pylab.GridSpec(
            5, 1, hspace=0.001, top=0.97, bottom=0.05, right=0.95, left=0.2
        )
        gs_base = mpl.pylab.GridSpec(
            5, 2, wspace=0.19, hspace=0.05, top=0.85, bottom=0.05, right=0.95, left=0.2
        )
        fig = mpl.pylab.figure(figsize=(10, 17))

        # Bottom axes with the same x and y axis
        ax = fig.add_subplot(gs_base[-1, 0])
        botAxes = [ax] + [fig.add_subplot(gs_base[-1, 1], sharey=ax)]
        for _ax in botAxes:
            _ax.yaxis.set_major_locator(
                mpl.ticker.LogLocator(
                    base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
                )
            )
            _ax.xaxis.set_minor_locator(AutoMinorLocator())
            _ax.set_xlabel(r"$\rho$")
            _ax.set_ylim([8e-2, 2])
            _ax.set_xlim([0.98, 1.1])
            _ax.set_yscale("log")
        botAxes[0].set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        botAxes[1].tick_params(labelleft=False)
        # The shared in time axes
        ax = fig.add_subplot(gs_top[0])  # Need to create the first one to share...
        other_axes = [
            fig.add_subplot(gs_top[i], sharex=ax)
            for i in np.linspace(1, 3, 3, dtype="int")
        ]
        topAxes = [ax] + other_axes
        # Hide shared x-tick labels
        for ax in topAxes[:-1]:
            ax.tick_params(labelbottom=False)
        for ax in topAxes:
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())
            ax.set_xlim([0, 8])
        OuterTarget = OrderedDict(
            [
                ("ua1", {"R": 1.582, "z": -1.199, "s": 1.045}),
                ("ua2", {"R": 1.588, "z": -1.175, "s": 1.070}),
                ("ua3", {"R": 1.595, "z": -1.151, "s": 1.094}),
                ("ua4", {"R": 1.601, "z": -1.127, "s": 1.126}),
                ("ua5", {"R": 1.608, "z": -1.103, "s": 1.158}),
                ("ua6", {"R": 1.614, "z": -1.078, "s": 1.189}),
                ("ua7", {"R": 1.620, "z": -1.054, "s": 1.213}),
                ("ua8", {"R": 1.627, "z": -1.030, "s": 1.246}),
                ("ua9", {"R": 1.640, "z": -0.982, "s": 1.276}),
            ]
        )
        c = mds.Connection("localhost:8001")

        for _idxshot, (shot, _cShot, thresL) in enumerate(
            zip(shotList, colorShot, thresLAll)
        ):
            _s = 'augsignal({},"TOT","P_TOT")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","P_TOT"))'.format(shot)
            topAxes[0].plot(
                c.get(_st).data(), bn.move_mean(Data / 1e6, 200), color=_cShot, lw=2.5
            )
            topAxes[0].set_ylabel(r"P$_{\mathrm{TOT}}$ [MW]")
            topAxes[0].text(
                0.1,
                0.9 - _idxshot * 0.1,
                r"#{}".format(shot),
                color=_cShot,
                transform=topAxes[0].transAxes,
            )

            _s = 'augsignal({},"UVS","D_tot")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"UVS","D_tot"))'.format(shot)
            topAxes[1].plot(c.get(_st).data(), Data / 1e22, color=_cShot, lw=2.5)
            topAxes[1].set_ylabel(r"D$_2 [10^{22}]$")
            topAxes[1].set_ylim([0, 12])

            _s = 'augsignal({},"TOT","n/nGW")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","n/nGW"))'.format(shot)
            topAxes[2].plot(c.get(_st).data(), Data, color=_cShot, lw=2.5)
            topAxes[2].set_ylabel(r"n/n$_G$")
            topAxes[2].set_ylim([0, 1.05])

            if shot != 36573:
                outDivSignal = np.asarray([])
                neTime = c.get(
                    'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(shot, "ua1")
                ).data()
                for i, s in enumerate(OuterTarget.keys()):
                    _ne = c.get('augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
                    _te = c.get('augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
                    _an = c.get('augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
                    _cs = np.sqrt(constants.e * 4 * _te / (2 * constants.m_p))
                    # this is the ion flux
                    _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

                    if i == 0:
                        outDivSignal = _s
                    else:
                        outDivSignal = np.vstack((outDivSignal, _s))
                # now we compute the total integrate ion flux
                # compute also the peakDensity signal
                peakTarget = bn.move_mean(np.nanmean(outDivSignal, axis=0), 600)
                outTarget = np.zeros(neTime.size)
                for i in range(neTime.size):
                    _x = np.asarray([OuterTarget[k]["s"] for k in OuterTarget.keys()])
                    _r = np.asarray([OuterTarget[k]["R"] for k in OuterTarget.keys()])
                    _y = outDivSignal[:, i]
                    _dummy = np.vstack((_x, _y)).transpose()
                    _dummy = _dummy[~np.isnan(_dummy).any(1)]
                    _x = _dummy[:, 0]
                    _y = _dummy[:, 1][np.argsort(_x)]
                    _x = np.sort(_x)
                    outTarget[i] = 2 * np.pi * _r.mean() * np.trapz(_y, x=_x)
                outerTarget = bn.move_mean(outTarget, 300)
                topAxes[3].plot(neTime, outerTarget / 1e23, lw=2.5, color=_cShot)
                # topAxes[2].plot( neTime, peakTarget / 1e23, lw=2.5, color='r')
                topAxes[3].set_xlabel(r"t [s]")
                topAxes[3].set_ylabel(r"$[10^{23}$ion/s$]$")
            for ax in topAxes:
                for t, _c in zip(tList, colorList):
                    ax.axvline(np.mean(t), ls="-", lw=2, color=_c)
            # read the Li-Beam data
            LiBes = c.get('augsignal({}, "LIN", "ne")'.format(shot)).data() / 1e19
            LiRho = c.get('dim_of(augsignal({},"LIN","ne"),1)'.format(shot)).data()
            LiTime = c.get('dim_of(augsignal({},"LIN","ne"))'.format(shot)).data()
            # read the IpolSolA per il masking del
            # LiB
            Ipol = c.get('augsignal({}, "MAC", "Ipolsola")'.format(shot)).data()
            IpolTime = c.get(
                'dim_of(augsignal({}, "MAC", "Ipolsola"))'.format(shot)
            ).data()

            # now the profiles which is more subtle and long
            for _tr, _thr, _col in zip(tList, thresL, colorList):
                if _thr != -99:
                    _idxT = np.where(((IpolTime >= _tr[0]) & (IpolTime <= _tr[1])))[0]
                    # now create an appropriate savgolfile
                    IpolS = savgol_filter(Ipol[_idxT], 301, 3)
                    IpolT = IpolTime[_idxT]
                    IpolO = Ipol[_idxT]
                    _dummyTime = LiTime[
                        np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                    # on these we choose a threshold
                    # which can be set as also set as keyword
                    ElmMask = np.zeros(IpolSp.size, dtype="bool")
                    ElmMask[np.where(IpolSp > _thr)[0]] = True
                    _interElm = np.where(ElmMask == False)[0]
                    # in this way they are
                    _dummyLib = _dummyLib[:, _interElm].ravel()
                    _dummyRho = _dummyRho[:, _interElm].ravel()
                else:
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ].ravel()
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ].ravel()
                yOut, bins, bin_means, bin_width, xOut = bin_by.bin_by(
                    _dummyRho, _dummyLib, nbins=20
                )
                xB = np.asarray([np.nanmean(k) for k in xOut])
                xBE = np.asarray([np.nanstd(k) for k in xOut])
                yB = np.asarray([np.nanmean(k) for k in yOut])
                yBE = np.asarray([np.nanstd(k) for k in yOut])
                enS = yB[np.argmin(np.abs(xB - 1))]
                botAxes[_idxshot].errorbar(
                    xB, yB / enS, xerr=xBE, yerr=yBE / enS, fmt="-", color=_col, lw=2.5
                )
        fig.savefig(
            "../pdfbox/GeneralProfilesShot{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inches="tight",
        )
    elif selection == 6:
        pass

    elif selection == 7:
        shotList = (36574, 36605)
        tList = (
            (2.1, 2.2),
            (3.1, 3.2),
            (4.1, 4.2),
            (4.9, 5.0),
            (5.5, 5.6),
            (6.25, 6.35),
            (7.4, 7.5),
        )
        thresholdList = (
            (3000, 1800, 1000, 800, 1000, 500, -99),
            (3000, 2000, 1000, 1000, 1000, 1000, 500),
        )
        colorShot = ("#3B4859", "#F20505")
        colorList = [
            "#e41a1c",
            "#377eb8",
            "#4daf4a",
            "#984ea3",
            "#ff7f00",
            "#ffff33",
            "#a65628",
        ]
        gs_top = mpl.pylab.GridSpec(
            5, 1, hspace=0.001, top=0.99, bottom=0.05, right=0.95, left=0.2
        )
        gs_base = mpl.pylab.GridSpec(
            5, 2, wspace=0.19, hspace=0.05, top=0.8, bottom=0.05, right=0.95, left=0.2
        )
        fig = mpl.pylab.figure(figsize=(10, 17))

        # Bottom axes with the same x and y axis
        ax = fig.add_subplot(gs_base[-1, 0])
        botAxes = [ax] + [fig.add_subplot(gs_base[-1, 1], sharey=ax)]
        for _ax in botAxes:
            _ax.yaxis.set_major_locator(
                mpl.ticker.LogLocator(
                    base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
                )
            )
            _ax.xaxis.set_minor_locator(AutoMinorLocator())
            _ax.set_xlabel(r"$\rho$")
            _ax.set_ylim([8e-2, 2])
            _ax.set_xlim([0.98, 1.1])
            _ax.set_yscale("log")
        botAxes[0].set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        botAxes[1].tick_params(labelleft=False)
        # The shared in time axes
        ax = fig.add_subplot(gs_top[0])  # Need to create the first one to share...
        other_axes = [
            fig.add_subplot(gs_top[i], sharex=ax)
            for i in np.linspace(1, 3, 3, dtype="int")
        ]
        topAxes = [ax] + other_axes
        # Hide shared x-tick labels
        for ax in topAxes[:-1]:
            ax.tick_params(labelbottom=False)
        for ax in topAxes:
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())
            ax.set_xlim([0, 8])
        OuterTarget = OrderedDict(
            [
                ("ua1", {"R": 1.582, "z": -1.199, "s": 1.045}),
                ("ua2", {"R": 1.588, "z": -1.175, "s": 1.070}),
                ("ua3", {"R": 1.595, "z": -1.151, "s": 1.094}),
                ("ua4", {"R": 1.601, "z": -1.127, "s": 1.126}),
                ("ua5", {"R": 1.608, "z": -1.103, "s": 1.158}),
                ("ua6", {"R": 1.614, "z": -1.078, "s": 1.189}),
                ("ua7", {"R": 1.620, "z": -1.054, "s": 1.213}),
                ("ua8", {"R": 1.627, "z": -1.030, "s": 1.246}),
                ("ua9", {"R": 1.640, "z": -0.982, "s": 1.276}),
            ]
        )
        c = mds.Connection("localhost:8001")

        for _idxshot, (shot, _cShot, thresL) in enumerate(
            zip(shotList, colorShot, thresholdList)
        ):
            _s = 'augsignal({},"TOT","P_TOT")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","P_TOT"))'.format(shot)
            topAxes[0].plot(
                c.get(_st).data(), bn.move_mean(Data / 1e6, 200), color=_cShot, lw=2.5
            )
            topAxes[0].set_ylabel(r"P$_{\mathrm{TOT}}$ [MW]")
            topAxes[0].text(
                0.1,
                0.9 - _idxshot * 0.1,
                r"#{}".format(shot),
                color=_cShot,
                transform=topAxes[0].transAxes,
            )

            _s = 'augsignal({},"UVS","D_tot")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"UVS","D_tot"))'.format(shot)
            topAxes[1].plot(c.get(_st).data(), Data / 1e22, color=_cShot, lw=2.5)
            topAxes[1].set_ylabel(r"D$_2 [10^{22}]$")
            topAxes[1].set_ylim([0, 12])

            _s = 'augsignal({},"TOT","n/nGW")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","n/nGW"))'.format(shot)
            topAxes[2].plot(c.get(_st).data(), Data, color=_cShot, lw=2.5)
            topAxes[2].set_ylabel(r"n/n$_G$")
            topAxes[2].set_ylim([0, 1.05])

            if shot != 36573:
                outDivSignal = np.asarray([])
                neTime = c.get(
                    'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(shot, "ua1")
                ).data()
                for i, s in enumerate(OuterTarget.keys()):
                    _ne = c.get('augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
                    _te = c.get('augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
                    _an = c.get('augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
                    _cs = np.sqrt(constants.e * 4 * _te / (2 * constants.m_p))
                    # this is the ion flux
                    _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

                    if i == 0:
                        outDivSignal = _s
                    else:
                        outDivSignal = np.vstack((outDivSignal, _s))
                # now we compute the total integrate ion flux
                # compute also the peakDensity signal
                peakTarget = bn.move_mean(np.nanmean(outDivSignal, axis=0), 600)
                outTarget = np.zeros(neTime.size)
                for i in range(neTime.size):
                    _x = np.asarray([OuterTarget[k]["s"] for k in OuterTarget.keys()])
                    _r = np.asarray([OuterTarget[k]["R"] for k in OuterTarget.keys()])
                    _y = outDivSignal[:, i]
                    _dummy = np.vstack((_x, _y)).transpose()
                    _dummy = _dummy[~np.isnan(_dummy).any(1)]
                    _x = _dummy[:, 0]
                    _y = _dummy[:, 1][np.argsort(_x)]
                    _x = np.sort(_x)
                    outTarget[i] = 2 * np.pi * _r.mean() * np.trapz(_y, x=_x)
                outerTarget = bn.move_mean(outTarget, 300)
                topAxes[3].plot(neTime, outerTarget / 1e23, lw=2.5, color=_cShot)
                # topAxes[2].plot( neTime, peakTarget / 1e23, lw=2.5, color='r')
                topAxes[3].set_xlabel(r"t [s]")
                topAxes[3].set_ylabel(r"$[10^{23}$ion/s$]$")
            for ax in topAxes:
                for t, _c in zip(tList, colorList):
                    ax.axvline(np.mean(t), ls="-", lw=2, color=_c)
            # read the Li-Beam data
            LiBes = c.get('augsignal({}, "LIN", "ne")'.format(shot)).data() / 1e19
            LiRho = c.get('dim_of(augsignal({},"LIN","ne"),1)'.format(shot)).data()
            LiTime = c.get('dim_of(augsignal({},"LIN","ne"))'.format(shot)).data()
            # read the IpolSolA per il masking del
            # LiB
            Ipol = c.get('augsignal({}, "MAC", "Ipolsola")'.format(shot)).data()
            IpolTime = c.get(
                'dim_of(augsignal({}, "MAC", "Ipolsola"))'.format(shot)
            ).data()

            # now the profiles which is more subtle and long
            for _tr, _thr, _col in zip(tList, thresL, colorList):
                if _thr != -99:
                    _idxT = np.where(((IpolTime >= _tr[0]) & (IpolTime <= _tr[1])))[0]
                    # now create an appropriate savgolfile
                    IpolS = savgol_filter(Ipol[_idxT], 301, 3)
                    IpolT = IpolTime[_idxT]
                    IpolO = Ipol[_idxT]
                    _dummyTime = LiTime[
                        np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                    # on these we choose a threshold
                    # which can be set as also set as keyword
                    ElmMask = np.zeros(IpolSp.size, dtype="bool")
                    ElmMask[np.where(IpolSp > _thr)[0]] = True
                    _interElm = np.where(ElmMask == False)[0]
                    # in this way they are
                    _dummyLib = _dummyLib[:, _interElm].ravel()
                    _dummyRho = _dummyRho[:, _interElm].ravel()
                else:
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ].ravel()
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ].ravel()
                yOut, bins, bin_means, bin_width, xOut = bin_by.bin_by(
                    _dummyRho, _dummyLib, nbins=20
                )
                xB = np.asarray([np.nanmean(k) for k in xOut])
                xBE = np.asarray([np.nanstd(k) for k in xOut])
                yB = np.asarray([np.nanmean(k) for k in yOut])
                yBE = np.asarray([np.nanstd(k) for k in yOut])
                enS = yB[np.argmin(np.abs(xB - 1))]
                botAxes[_idxshot].errorbar(
                    xB, yB / enS, xerr=xBE, yerr=yBE / enS, fmt="-", color=_col, lw=2.5
                )
                botAxes[_idxshot].text(
                    0.7,
                    0.87,
                    r"#{}".format(shot),
                    color=_cShot,
                    transform=botAxes[_idxshot].transAxes,
                )
        fig.savefig(
            "../pdfbox/GeneralProfilesShot{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inches="tight",
        )

    elif selection == 8:
        shotList = (36573, 36574, 36605)
        tList = (
            (2.1, 2.2),
            (3.1, 3.2),
            (4.1, 4.2),
            (4.9, 5.0),
            (5.5, 5.6),
            (6.25, 6.35),
            (7.4, 7.5),
        )

        thresholdList = (
            (3000, 1800, 1800, 1800, 1500, 1500, 800),
            (3000, 1800, 1000, 800, 1000, 500, -99),
            (3000, 2000, 1000, 1000, 1000, 1000, 500),
        )
        colorShot = ("#3B4859", "#F20505", "#049DD9")
        colorList = [
            "#e41a1c",
            "#377eb8",
            "#4daf4a",
            "#984ea3",
            "#ff7f00",
            "#ffff33",
            "#a65628",
        ]
        OuterTarget = OrderedDict(
            [
                ("ua1", {"R": 1.582, "z": -1.199, "s": 1.045}),
                ("ua2", {"R": 1.588, "z": -1.175, "s": 1.070}),
                ("ua3", {"R": 1.595, "z": -1.151, "s": 1.094}),
                ("ua4", {"R": 1.601, "z": -1.127, "s": 1.126}),
                ("ua5", {"R": 1.608, "z": -1.103, "s": 1.158}),
                ("ua6", {"R": 1.614, "z": -1.078, "s": 1.189}),
                ("ua7", {"R": 1.620, "z": -1.054, "s": 1.213}),
                ("ua8", {"R": 1.627, "z": -1.030, "s": 1.246}),
                ("ua9", {"R": 1.640, "z": -0.982, "s": 1.276}),
            ]
        )
        c = mds.Connection("localhost:8001")

        for _idxshot, (shot, _cShot, thresL) in enumerate(
            zip(shotList, colorShot, thresholdList)
        ):
            gs_top = mpl.pylab.GridSpec(
                5, 1, hspace=0.001, top=0.99, bottom=0.05, right=0.95, left=0.2
            )
            gs_base = mpl.pylab.GridSpec(
                5,
                1,
                wspace=0.19,
                hspace=0.05,
                top=0.8,
                bottom=0.05,
                right=0.95,
                left=0.2,
            )
            fig = mpl.pylab.figure(figsize=(8, 17))

            # Bottom axes with the same x and y axis
            botAxes = fig.add_subplot(gs_base[-1])
            # botAxes = [ax] + [fig.add_subplot( gs_base[-1, 1], sharey=ax )]
            # for _ax in botAxes:
            botAxes.yaxis.set_major_locator(
                mpl.ticker.LogLocator(
                    base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
                )
            )
            botAxes.xaxis.set_minor_locator(AutoMinorLocator())
            botAxes.set_xlabel(r"$\rho$")
            botAxes.set_ylim([8e-2, 2])
            botAxes.set_xlim([0.98, 1.1])
            botAxes.set_yscale("log")
            botAxes.set_ylabel(r"n$_e$/n$_e(\rho=1)$")
            # botAxes[1].tick_params( labelleft=False )
            # The shared in time axes
            ax = fig.add_subplot(gs_top[0])  # Need to create the first one to share...
            other_axes = [
                fig.add_subplot(gs_top[i], sharex=ax)
                for i in np.linspace(1, 3, 3, dtype="int")
            ]
            topAxes = [ax] + other_axes
            # Hide shared x-tick labels
            for ax in topAxes[:-1]:
                ax.tick_params(labelbottom=False)
            for ax in topAxes:
                ax.xaxis.set_minor_locator(AutoMinorLocator())
                ax.yaxis.set_minor_locator(AutoMinorLocator())
                ax.set_xlim([0, 8])
            _s = 'augsignal({},"TOT","P_TOT")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","P_TOT"))'.format(shot)
            topAxes[0].plot(
                c.get(_st).data(), bn.move_mean(Data / 1e6, 200), color=_cShot, lw=2.5
            )
            topAxes[0].set_ylabel(r"P$_{\mathrm{TOT}}$ [MW]")
            topAxes[0].text(
                0.1,
                0.9,
                r"#{}".format(shot),
                color=_cShot,
                transform=topAxes[0].transAxes,
            )

            _s = 'augsignal({},"UVS","D_tot")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"UVS","D_tot"))'.format(shot)
            topAxes[1].plot(c.get(_st).data(), Data / 1e22, color=_cShot, lw=2.5)
            topAxes[1].set_ylabel(r"D$_2 [10^{22}]$")
            topAxes[1].set_ylim([0, 12])

            _s = 'augsignal({},"TOT","n/nGW")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","n/nGW"))'.format(shot)
            topAxes[2].plot(c.get(_st).data(), Data, color=_cShot, lw=2.5)
            topAxes[2].set_ylabel(r"n/n$_G$")
            topAxes[2].set_ylim([0, 1.05])

            if shot != 36573:
                outDivSignal = np.asarray([])
                neTime = c.get(
                    'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(shot, "ua1")
                ).data()
                for i, s in enumerate(OuterTarget.keys()):
                    _ne = c.get('augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
                    _te = c.get('augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
                    _an = c.get('augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
                    _cs = np.sqrt(constants.e * 4 * _te / (2 * constants.m_p))
                    # this is the ion flux
                    _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

                    if i == 0:
                        outDivSignal = _s
                    else:
                        outDivSignal = np.vstack((outDivSignal, _s))
                # now we compute the total integrate ion flux
                # compute also the peakDensity signal
                peakTarget = bn.move_mean(np.nanmean(outDivSignal, axis=0), 600)
                outTarget = np.zeros(neTime.size)
                for i in range(neTime.size):
                    _x = np.asarray([OuterTarget[k]["s"] for k in OuterTarget.keys()])
                    _r = np.asarray([OuterTarget[k]["R"] for k in OuterTarget.keys()])
                    _y = outDivSignal[:, i]
                    _dummy = np.vstack((_x, _y)).transpose()
                    _dummy = _dummy[~np.isnan(_dummy).any(1)]
                    _x = _dummy[:, 0]
                    _y = _dummy[:, 1][np.argsort(_x)]
                    _x = np.sort(_x)
                    outTarget[i] = 2 * np.pi * _r.mean() * np.trapz(_y, x=_x)
                outerTarget = bn.move_mean(outTarget, 300)
                topAxes[3].plot(neTime, outerTarget / 1e23, lw=2.5, color=_cShot)
                # topAxes[2].plot( neTime, peakTarget / 1e23, lw=2.5, color='r')
                topAxes[3].set_xlabel(r"t [s]")
                topAxes[3].set_ylabel(r"$[10^{23}$ion/s$]$")
            for ax in topAxes:
                for t, _c in zip(tList, colorList):
                    ax.axvline(np.mean(t), ls="-", lw=2, color=_c)
            # read the Li-Beam data
            LiBes = c.get('augsignal({}, "LIN", "ne")'.format(shot)).data() / 1e19
            LiRho = c.get('dim_of(augsignal({},"LIN","ne"),1)'.format(shot)).data()
            LiTime = c.get('dim_of(augsignal({},"LIN","ne"))'.format(shot)).data()
            # read the IpolSolA per il masking del
            # LiB
            Ipol = c.get('augsignal({}, "MAC", "Ipolsola")'.format(shot)).data()
            IpolTime = c.get(
                'dim_of(augsignal({}, "MAC", "Ipolsola"))'.format(shot)
            ).data()

            # now the profiles which is more subtle and long
            for _tr, _thr, _col in zip(tList, thresL, colorList):
                if _thr != -99:
                    _idxT = np.where(((IpolTime >= _tr[0]) & (IpolTime <= _tr[1])))[0]
                    # now create an appropriate savgolfile
                    IpolS = savgol_filter(Ipol[_idxT], 301, 3)
                    IpolT = IpolTime[_idxT]
                    IpolO = Ipol[_idxT]
                    _dummyTime = LiTime[
                        np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                    # on these we choose a threshold
                    # which can be set as also set as keyword
                    ElmMask = np.zeros(IpolSp.size, dtype="bool")
                    ElmMask[np.where(IpolSp > _thr)[0]] = True
                    _interElm = np.where(ElmMask == False)[0]
                    # in this way they are
                    _dummyLib = _dummyLib[:, _interElm].ravel()
                    _dummyRho = _dummyRho[:, _interElm].ravel()
                else:
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ].ravel()
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ].ravel()
                yOut, bins, bin_means, bin_width, xOut = bin_by.bin_by(
                    _dummyRho, _dummyLib, nbins=20
                )
                xB = np.asarray([np.nanmean(k) for k in xOut])
                xBE = np.asarray([np.nanstd(k) for k in xOut])
                yB = np.asarray([np.nanmean(k) for k in yOut])
                yBE = np.asarray([np.nanstd(k) for k in yOut])
                enS = yB[np.argmin(np.abs(xB - 1))]
                # botAxes.errorbar(
                #     xB, yB / enS, xerr=xBE,
                #     yerr=yBE / enS, fmt='-', color=_col, lw=2.5 )
                botAxes.plot(xB, yB / enS, "-", lw=2.5, color=_col)
                botAxes.fill_between(
                    xB,
                    (yB - yBE / 2) / enS,
                    (yB + yBE / 2) / enS,
                    color=_col,
                    alpha=0.1,
                )
                fig.savefig(
                    "../pdfbox/GeneralProfilesShot{}.pdf".format(shot),
                    bbox_to_inches="tight",
                )

    elif selection == 9:
        shotList = (36573, 36574, 36605)
        tList = (
            (2.1, 2.2),
            (3.1, 3.2),
            (4.1, 4.2),
            (4.9, 5.0),
            (5.5, 5.6),
            (6.25, 6.35),
            (7.4, 7.5),
        )

        thresholdList = (
            (3000, 1800, 1800, 1800, 1500, 1500, 800),
            (3000, 1800, 1000, 800, 1000, 500, -99),
            (3000, 2000, 1000, 1000, 1000, 1000, 500),
        )
        colorShot = ("#3B4859", "#F20505", "#049DD9")
        colorList = [
            "#e41a1c",
            "#377eb8",
            "#4daf4a",
            "#984ea3",
            "#ff7f00",
            "#ffff33",
            "#a65628",
        ]
        c = mds.Connection("localhost:8001")

        for _idxshot, (shot, _cShot, thresL) in enumerate(
            zip(shotList, colorShot, thresholdList)
        ):
            gs_top = mpl.pylab.GridSpec(
                5, 1, hspace=0.001, top=0.99, bottom=0.05, right=0.95, left=0.2
            )
            gs_base = mpl.pylab.GridSpec(
                5,
                1,
                wspace=0.19,
                hspace=0.05,
                top=0.8,
                bottom=0.05,
                right=0.95,
                left=0.2,
            )
            fig = mpl.pylab.figure(figsize=(8, 17))

            # Bottom axes with the same x and y axis
            ax = fig.add_subplot(gs_base[-2])
            botAxes = [ax] + [fig.add_subplot(gs_base[-1], sharex=ax)]
            # for _ax in botAxes:
            botAxes[1].yaxis.set_major_locator(
                mpl.ticker.LogLocator(
                    base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
                )
            )
            botAxes[1].xaxis.set_minor_locator(AutoMinorLocator())
            botAxes[1].set_xlabel(r"$\rho$")
            botAxes[1].set_ylim([8e-2, 2])
            botAxes[1].set_xlim([0.96, 1.1])
            botAxes[1].set_yscale("log")
            botAxes[1].set_ylabel(r"n$_e$/n$_e(\rho=1)$")
            botAxes[0].xaxis.set_minor_locator(AutoMinorLocator())
            botAxes[0].yaxis.set_minor_locator(AutoMinorLocator())
            botAxes[0].tick_params(labelbottom=False)
            botAxes[0].set_ylabel(r"n$_e^e$ [10$^{20}$m$^{-3}$]")
            botAxes[0].set_xlim([0.96, 1.1])

            # botAxes[1].tick_params( labelleft=False )
            # The shared in time axes
            ax = fig.add_subplot(gs_top[0])  # Need to create the first one to share...
            other_axes = [
                fig.add_subplot(gs_top[i], sharex=ax)
                for i in np.linspace(1, 2, 2, dtype="int")
            ]
            topAxes = [ax] + other_axes
            # Hide shared x-tick labels
            for ax in topAxes[:-1]:
                ax.tick_params(labelbottom=False)
            for ax in topAxes:
                ax.xaxis.set_minor_locator(AutoMinorLocator())
                ax.yaxis.set_minor_locator(AutoMinorLocator())
                ax.set_xlim([0, 8])
            _s = 'augsignal({},"TOT","P_TOT")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","P_TOT"))'.format(shot)
            topAxes[0].plot(
                c.get(_st).data(), bn.move_mean(Data / 1e6, 200), color=_cShot, lw=2.5
            )
            topAxes[0].set_ylabel(r"P$_{\mathrm{TOT}}$ [MW]")
            topAxes[0].text(
                0.1,
                0.9,
                r"#{}".format(shot),
                color=_cShot,
                transform=topAxes[0].transAxes,
            )

            _s = 'augsignal({},"UVS","D_tot")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"UVS","D_tot"))'.format(shot)
            topAxes[1].plot(c.get(_st).data(), Data / 1e22, color=_cShot, lw=2.5)
            topAxes[1].set_ylabel(r"D$_2 [10^{22}]$")
            topAxes[1].set_ylim([0, 12])

            _s = 'augsignal({},"TOT","n/nGW")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","n/nGW"))'.format(shot)
            topAxes[2].plot(c.get(_st).data(), Data, color=_cShot, lw=2.5)
            topAxes[2].set_ylabel(r"n/n$_G$")
            topAxes[2].set_ylim([0, 1.05])

            topAxes[2].set_xlabel(r"t [s]")
            for ax in topAxes:
                for t, _c in zip(tList, colorList):
                    ax.axvline(np.mean(t), ls="-", lw=2, color=_c)
            # read the Li-Beam data
            LiBes = c.get('augsignal({}, "LIN", "ne")'.format(shot)).data() / 1e19
            LiRho = c.get('dim_of(augsignal({},"LIN","ne"),1)'.format(shot)).data()
            LiTime = c.get('dim_of(augsignal({},"LIN","ne"))'.format(shot)).data()
            # read the IpolSolA per il masking del
            # LiB
            Ipol = c.get('augsignal({}, "MAC", "Ipolsola")'.format(shot)).data()
            IpolTime = c.get(
                'dim_of(augsignal({}, "MAC", "Ipolsola"))'.format(shot)
            ).data()
            # read also the file with the target profile
            if shot != 36573:
                Target = np.load("../data/OuterTarget{}.npz".format(shot))
            # now the profiles which is more subtle and long
            for _tr, _thr, _col in zip(tList, thresL, colorList):
                if _thr != -99:
                    _idxT = np.where(((IpolTime >= _tr[0]) & (IpolTime <= _tr[1])))[0]
                    # now create an appropriate savgolfile
                    IpolS = savgol_filter(Ipol[_idxT], 301, 3)
                    IpolT = IpolTime[_idxT]
                    IpolO = Ipol[_idxT]
                    _dummyTime = LiTime[
                        np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                    # on these we choose a threshold
                    # which can be set as also set as keyword
                    ElmMask = np.zeros(IpolSp.size, dtype="bool")
                    ElmMask[np.where(IpolSp > _thr)[0]] = True
                    _interElm = np.where(ElmMask == False)[0]
                    # in this way they are
                    _dummyLib = _dummyLib[:, _interElm]
                    _dummyRho = _dummyRho[:, _interElm]
                else:
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                xB = np.nanmean(_dummyRho, axis=1)
                xBE = np.nanstd(_dummyRho, axis=1)
                yB = np.nanmean(_dummyLib, axis=1)
                yBE = np.nanstd(_dummyLib, axis=1)
                yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
                enS = yB[np.argmin(np.abs(xB - 1))]
                # botAxes.errorbar(
                #     xB, yB / enS, xerr=xBE,
                #     yerr=yBE / enS, fmt='-', color=_col, lw=2.5 )
                botAxes[1].plot(xB, yB / enS, "-", lw=2.5, color=_col)
                botAxes[1].fill_between(
                    xB,
                    (yB - yBE / 2) / enS,
                    (yB + yBE / 2) / enS,
                    color=_col,
                    alpha=0.1,
                )
                if shot != 36573:
                    # do similar stuff for the target
                    _dummyTime = Target["time"][
                        np.where(
                            (Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1])
                        )[0]
                    ]
                    _dummyLib = (
                        Target["en"][
                            :,
                            np.where(
                                (Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1])
                            )[0],
                        ]
                        / 1e20
                    )
                    _dummyRho = Target["rho"][
                        :,
                        np.where(
                            (Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1])
                        )[0],
                    ]
                    if _thr != -99:
                        IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                        # on these we choose a threshold
                        # which can be set as also set as keyword
                        ElmMask = np.zeros(IpolSp.size, dtype="bool")
                        ElmMask[np.where(IpolSp > _thr)[0]] = True
                        _interElm = np.where(ElmMask == False)[0]
                        _dummyLib = _dummyLib[:, _interElm]
                        _dummyRho = _dummyRho[:, _interElm]
                    xB = np.nanmean(_dummyRho, axis=1)
                    xBE = np.nanstd(_dummyRho, axis=1)
                    yB = np.nanmean(_dummyLib, axis=1)
                    yBE = np.nanstd(_dummyLib, axis=1)
                    yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
                    _sort = np.argsort(xB)
                    botAxes[0].errorbar(
                        xB[_sort],
                        yB[_sort],
                        xerr=xBE[_sort],
                        yerr=yBE[_sort],
                        fmt="-o",
                        color=_col,
                        ms=12,
                    )
            fig.savefig(
                "../pdfbox/GeneralProfilesShotTarget{}.pdf".format(shot),
                bbox_to_inches="tight",
            )

    elif selection == 10:
        shot = 34276
        tList = ((2.245, 2.345), (3.0, 3.1), (4.63, 4.73), (5.4, 5.5))
        thresL = (-99.0, 1200, 500, 500)
        _cShot = "k"
        colorList = ["#e41a1c", "#377eb8", "#4daf4a", "#984ea3"]
        c = mds.Connection("localhost:8001")
        # repat for shot 34276 which has lower points in profiles
        gs_top = mpl.pylab.GridSpec(
            5, 1, hspace=0.001, top=0.99, bottom=0.05, right=0.95, left=0.2
        )
        gs_base = mpl.pylab.GridSpec(
            5, 1, wspace=0.19, hspace=0.05, top=0.86, bottom=0.05, right=0.95, left=0.2
        )
        fig = mpl.pylab.figure(figsize=(8, 17))

        # Bottom axes with the same x and y axis
        ax = fig.add_subplot(gs_base[-2])
        botAxes = [ax] + [fig.add_subplot(gs_base[-1], sharex=ax)]
        # for _ax in botAxes:
        botAxes[1].yaxis.set_major_locator(
            mpl.ticker.LogLocator(
                base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
            )
        )
        botAxes[1].xaxis.set_minor_locator(AutoMinorLocator())
        botAxes[1].set_xlabel(r"$\rho$")
        botAxes[1].set_ylim([8e-2, 2])
        botAxes[1].set_xlim([0.96, 1.1])
        botAxes[1].set_yscale("log")
        botAxes[1].set_ylabel(r"n$_e$/n$_e(\rho=1)$")
        botAxes[0].xaxis.set_minor_locator(AutoMinorLocator())
        botAxes[0].yaxis.set_minor_locator(AutoMinorLocator())
        botAxes[0].tick_params(labelbottom=False)
        botAxes[0].set_ylabel(r"n$_e^e$ [10$^{20}$m$^{-3}$]")
        botAxes[0].set_xlim([0.96, 1.1])

        # botAxes[1].tick_params( labelleft=False )
        # The shared in time axes
        ax = fig.add_subplot(gs_top[0])  # Need to create the first one to share...
        other_axes = [
            fig.add_subplot(gs_top[i], sharex=ax)
            for i in np.linspace(1, 2, 2, dtype="int")
        ]
        topAxes = [ax] + other_axes
        # Hide shared x-tick labels
        for ax in topAxes[:-1]:
            ax.tick_params(labelbottom=False)
        for ax in topAxes:
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())
            ax.set_xlim([0, 8])

        _s = 'augsignal({},"TOT","P_TOT")'.format(shot)
        Data = c.get(_s).data()
        _st = 'dim_of(augsignal({},"TOT","P_TOT"))'.format(shot)
        topAxes[0].plot(
            c.get(_st).data(), bn.move_mean(Data / 1e6, 200), color=_cShot, lw=2.5
        )
        topAxes[0].set_ylabel(r"P$_{\mathrm{TOT}}$ [MW]")
        topAxes[0].text(
            0.1, 0.9, r"#{}".format(shot), color=_cShot, transform=topAxes[0].transAxes
        )

        _s = 'augsignal({},"UVS","D_tot")'.format(shot)
        Data = c.get(_s).data()
        _st = 'dim_of(augsignal({},"UVS","D_tot"))'.format(shot)
        topAxes[1].plot(c.get(_st).data(), Data / 1e22, color=_cShot, lw=2.5)
        topAxes[1].set_ylabel(r"D$_2 [10^{22}]$")
        topAxes[1].set_ylim([0, 5])
        topAxes[1].text(0.1, 0.9, "No cryo", transform=topAxes[1].transAxes)

        _s = 'augsignal({},"TOT","n/nGW")'.format(shot)
        Data = c.get(_s).data()
        _st = 'dim_of(augsignal({},"TOT","n/nGW"))'.format(shot)
        topAxes[2].plot(c.get(_st).data(), Data, color=_cShot, lw=2.5)
        topAxes[2].set_ylabel(r"n/n$_G$")
        topAxes[2].set_ylim([0, 1.05])

        topAxes[2].set_xlabel(r"t [s]")
        for ax in topAxes:
            for t, _c in zip(tList, colorList):
                ax.axvline(np.mean(t), ls="-", lw=2, color=_c)
        # read the Li-Beam data
        LiBes = c.get('augsignal({}, "LIN", "ne")'.format(shot)).data() / 1e19
        LiRho = c.get('dim_of(augsignal({},"LIN","ne"),1)'.format(shot)).data()
        LiTime = c.get('dim_of(augsignal({},"LIN","ne"))'.format(shot)).data()
        # read the IpolSolA per il masking del
        # LiB
        Ipol = c.get('augsignal({}, "MAC", "Ipolsola")'.format(shot)).data()
        IpolTime = c.get('dim_of(augsignal({}, "MAC", "Ipolsola"))'.format(shot)).data()
        # read also the file with the target profile
        Target = np.load("../data/OuterTarget{}.npz".format(shot))
        # now the profiles which is more subtle and long
        for _tr, _thr, _col in zip(tList, thresL, colorList):
            if _thr != -99:
                _idxT = np.where(((IpolTime >= _tr[0]) & (IpolTime <= _tr[1])))[0]
                # now create an appropriate savgolfile
                IpolS = savgol_filter(Ipol[_idxT], 301, 3)
                IpolT = IpolTime[_idxT]
                IpolO = Ipol[_idxT]
                _dummyTime = LiTime[
                    np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
                _dummyLib = LiBes[
                    :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
                _dummyRho = LiRho[
                    :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
                IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                # on these we choose a threshold
                # which can be set as also set as keyword
                ElmMask = np.zeros(IpolSp.size, dtype="bool")
                ElmMask[np.where(IpolSp > _thr)[0]] = True
                _interElm = np.where(ElmMask == False)[0]
                # in this way they are
                _dummyLib = _dummyLib[:, _interElm]
                _dummyRho = _dummyRho[:, _interElm]
            else:
                _dummyLib = LiBes[
                    :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
                _dummyRho = LiRho[
                    :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
            xB = np.nanmean(_dummyRho, axis=1)
            xBE = np.nanstd(_dummyRho, axis=1)
            yB = np.nanmean(_dummyLib, axis=1)
            yBE = np.nanstd(_dummyLib, axis=1)
            yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
            enS = yB[np.argmin(np.abs(xB - 1))]
            # botAxes.errorbar(
            #     xB, yB / enS, xerr=xBE,
            #     yerr=yBE / enS, fmt='-', color=_col, lw=2.5 )
            botAxes[1].plot(xB, yB / enS, "-", lw=2.5, color=_col)
            botAxes[1].fill_between(
                xB, (yB - yBE / 2) / enS, (yB + yBE / 2) / enS, color=_col, alpha=0.1
            )
            # do similar stuff for the target
            _dummyTime = Target["time"][
                np.where((Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1]))[0]
            ]
            _dummyLib = (
                Target["en"][
                    :,
                    np.where((Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1]))[
                        0
                    ],
                ]
                / 1e20
            )
            _dummyRho = Target["rho"][
                :, np.where((Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1]))[0]
            ]
            if _thr != -99:
                IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                # on these we choose a threshold
                # which can be set as also set as keyword
                ElmMask = np.zeros(IpolSp.size, dtype="bool")
                ElmMask[np.where(IpolSp > _thr)[0]] = True
                _interElm = np.where(ElmMask == False)[0]
                _dummyLib = _dummyLib[:, _interElm]
                _dummyRho = _dummyRho[:, _interElm]
            xB = np.nanmean(_dummyRho, axis=1)
            xBE = np.nanstd(_dummyRho, axis=1)
            yB = np.nanmean(_dummyLib, axis=1)
            yBE = np.nanstd(_dummyLib, axis=1)
            yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
            _sort = np.argsort(xB)
            botAxes[0].errorbar(
                xB[_sort],
                yB[_sort],
                xerr=xBE[_sort],
                yerr=yBE[_sort],
                fmt="-o",
                color=_col,
                ms=12,
            )
        fig.savefig(
            "../pdfbox/GeneralProfilesShotTarget{}.pdf".format(shot),
            bbox_to_inches="tight",
        )

    elif selection == 11:
        shotList = (36573, 36574, 36605)
        tList = (
            (2.1, 2.2),
            (3.1, 3.2),
            (4.1, 4.2),
            (4.9, 5.0),
            (5.5, 5.6),
            (6.25, 6.35),
            (7.4, 7.5),
        )

        thresholdList = (
            (3000, 1800, 1800, 1800, 1500, 1500, 800),
            (3000, 1800, 1000, 800, 1000, 500, -99),
            (3000, 2000, 1000, 1000, 1000, 1000, 500),
        )
        colorShot = ("#3B4859", "#F20505", "#049DD9")
        colorList = [
            "#e41a1c",
            "#377eb8",
            "#4daf4a",
            "#984ea3",
            "#ff7f00",
            "#ffff33",
            "#a65628",
        ]
        c = mds.Connection("localhost:8001")

        for _idxshot, (shot, _cShot, thresL) in enumerate(
            zip(shotList, colorShot, thresholdList)
        ):
            gs_top = mpl.pylab.GridSpec(
                5, 1, hspace=0.001, top=0.99, bottom=0.05, right=0.95, left=0.14
            )
            gs_base = mpl.pylab.GridSpec(
                5,
                2,
                wspace=0.22,
                hspace=0.2,
                top=0.83,
                bottom=0.05,
                right=0.95,
                left=0.14,
            )
            fig = mpl.pylab.figure(figsize=(8, 17))

            # Bottom axes with the same x and y axis
            ax = fig.add_subplot(gs_base[-2, :])
            botAxes = [ax] + [
                fig.add_subplot(gs_base[-1, k], sharex=ax)
                for k in np.arange(2, dtype="int")
            ]
            # botAxes[0] is the upstream density profile
            # botAxes[1] is the target density botAxes[2] is the target temperature
            # for _ax in botAxes:
            for _axid, _ax in enumerate(botAxes):
                _ax.xaxis.set_minor_locator(AutoMinorLocator())
                if _axid != 0:
                    _ax.yaxis.set_minor_locator(AutoMinorLocator())
                    _ax.set_xlabel(r"$\rho$")
                else:
                    _ax.yaxis.set_major_locator(
                        mpl.ticker.LogLocator(
                            base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
                        )
                    )
                    _ax.set_ylim([8e-2, 2])
                    _ax.set_yscale("log")
                    _ax.set_ylabel(r"n$_e$/n$_e(\rho=1)$")
                    # _ax.tick_params(labelbottom=False)
                _ax.set_xlim([0.96, 1.1])
                _ax.set_xticks([0.96, 1, 1.04, 1.08])
            botAxes[1].set_ylabel(r"n$_e^t [10^{20}$m$^{-3}$]")
            botAxes[2].set_ylabel(r"T$_e$[eV]")
            # botAxes[1].tick_params( labelleft=False )
            # The shared in time axes
            ax = fig.add_subplot(gs_top[0])  # Need to create the first one to share...
            other_axes = [
                fig.add_subplot(gs_top[i], sharex=ax)
                for i in np.linspace(1, 2, 2, dtype="int")
            ]
            topAxes = [ax] + other_axes
            # Hide shared x-tick labels
            for ax in topAxes[:-1]:
                ax.tick_params(labelbottom=False)
            for ax in topAxes:
                ax.xaxis.set_minor_locator(AutoMinorLocator())
                ax.yaxis.set_minor_locator(AutoMinorLocator())
                ax.set_xlim([0, 8])
            _s = 'augsignal({},"TOT","P_TOT")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","P_TOT"))'.format(shot)
            topAxes[0].plot(
                c.get(_st).data(), bn.move_mean(Data / 1e6, 200), color=_cShot, lw=2.5
            )
            topAxes[0].set_ylabel(r"P$_{\mathrm{TOT}}$ [MW]")
            topAxes[0].text(
                0.1,
                0.9,
                r"#{}".format(shot),
                color=_cShot,
                transform=topAxes[0].transAxes,
            )

            _s = 'augsignal({},"UVS","D_tot")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"UVS","D_tot"))'.format(shot)
            topAxes[1].plot(c.get(_st).data(), Data / 1e22, color=_cShot, lw=2.5)
            topAxes[1].set_ylabel(r"D$_2 [10^{22}]$")
            topAxes[1].set_ylim([0, 12])

            _s = 'augsignal({},"TOT","n/nGW")'.format(shot)
            Data = c.get(_s).data()
            _st = 'dim_of(augsignal({},"TOT","n/nGW"))'.format(shot)
            topAxes[2].plot(c.get(_st).data(), Data, color=_cShot, lw=2.5)
            topAxes[2].set_ylabel(r"n/n$_G$")
            topAxes[2].set_ylim([0, 1.05])

            topAxes[2].set_xlabel(r"t [s]")
            for ax in topAxes:
                for t, _c in zip(tList, colorList):
                    ax.axvline(np.mean(t), ls="-", lw=2, color=_c)
            # read the Li-Beam data
            LiBes = c.get('augsignal({}, "LIN", "ne")'.format(shot)).data() / 1e19
            LiRho = c.get('dim_of(augsignal({},"LIN","ne"),1)'.format(shot)).data()
            LiTime = c.get('dim_of(augsignal({},"LIN","ne"))'.format(shot)).data()
            # read the IpolSolA per il masking del
            # LiB
            Ipol = c.get('augsignal({}, "MAC", "Ipolsola")'.format(shot)).data()
            IpolTime = c.get(
                'dim_of(augsignal({}, "MAC", "Ipolsola"))'.format(shot)
            ).data()
            # read also the file with the target profile
            if shot != 36573:
                Target = np.load("../data/OuterTarget{}.npz".format(shot))
            # now the profiles which is more subtle and long
            for _tr, _thr, _col in zip(tList, thresL, colorList):
                if _thr != -99:
                    _idxT = np.where(((IpolTime >= _tr[0]) & (IpolTime <= _tr[1])))[0]
                    # now create an appropriate savgolfile
                    IpolS = savgol_filter(Ipol[_idxT], 301, 3)
                    IpolT = IpolTime[_idxT]
                    IpolO = Ipol[_idxT]
                    _dummyTime = LiTime[
                        np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                    # on these we choose a threshold
                    # which can be set as also set as keyword
                    ElmMask = np.zeros(IpolSp.size, dtype="bool")
                    ElmMask[np.where(IpolSp > _thr)[0]] = True
                    _interElm = np.where(ElmMask == False)[0]
                    # in this way they are
                    _dummyLib = _dummyLib[:, _interElm]
                    _dummyRho = _dummyRho[:, _interElm]
                else:
                    _dummyLib = LiBes[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                    _dummyRho = LiRho[
                        :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                    ]
                xB = np.nanmean(_dummyRho, axis=1)
                xBE = np.nanstd(_dummyRho, axis=1)
                yB = np.nanmean(_dummyLib, axis=1)
                yBE = np.nanstd(_dummyLib, axis=1)
                yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
                enS = yB[np.argmin(np.abs(xB - 1))]
                # botAxes[0].errorbar(
                #     xB, yB / enS, xerr=xBE,
                #     yerr=yBE / enS, fmt='-', color=_col, lw=2.5 )
                # botAxes[0].plot(xB, yB / enS, '-', lw=2.5, color=_col)
                # botAxes[0].fill_between(xB, (yB - yBE / 2) / enS, (yB + yBE / 2) / enS,
                #                      color=_col, alpha=0.1)
                botAxes[0].errorbar(
                    xB,
                    yB / enS,
                    yerr=yBE / 2 / enS,
                    fmt="-o",
                    ms=12,
                    lw=2.5,
                    alpha=0.5,
                    color=_col,
                )
                # botAxes[0].fill_between(xB, (yB - yBE / 2) / enS, (yB + yBE / 2) / enS,
                #                      color=_col, alpha=0.1)
                if shot != 36573:
                    # do similar stuff for the target
                    _dummyTime = Target["time"][
                        np.where(
                            (Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1])
                        )[0]
                    ]
                    _dummyLib = (
                        Target["en"][
                            :,
                            np.where(
                                (Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1])
                            )[0],
                        ]
                        / 1e20
                    )
                    _dummyRho = Target["rho"][
                        :,
                        np.where(
                            (Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1])
                        )[0],
                    ]
                    if _thr != -99:
                        IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                        # on these we choose a threshold
                        # which can be set as also set as keyword
                        ElmMask = np.zeros(IpolSp.size, dtype="bool")
                        ElmMask[np.where(IpolSp > _thr)[0]] = True
                        _interElm = np.where(ElmMask == False)[0]
                        _dummyLib = _dummyLib[:, _interElm]
                        _dummyRho = _dummyRho[:, _interElm]
                    xB = np.nanmean(_dummyRho, axis=1)
                    xBE = np.nanstd(_dummyRho, axis=1)
                    yB = np.nanmean(_dummyLib, axis=1)
                    yBE = np.nanstd(_dummyLib, axis=1)
                    yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
                    _sort = np.argsort(xB)
                    botAxes[1].errorbar(
                        xB[_sort],
                        yB[_sort],
                        xerr=xBE[_sort],
                        yerr=yBE[_sort],
                        fmt="-o",
                        color=_col,
                        ms=12,
                        alpha=0.4,
                    )
                    # repeat for the temperature
                    _dummyTime = Target["time"][
                        np.where(
                            (Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1])
                        )[0]
                    ]
                    _dummyLib = Target["te"][
                        :,
                        np.where(
                            (Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1])
                        )[0],
                    ]
                    _dummyRho = Target["rho"][
                        :,
                        np.where(
                            (Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1])
                        )[0],
                    ]
                    if _thr != -99:
                        _dummyLib = _dummyLib[:, _interElm]
                        _dummyRho = _dummyRho[:, _interElm]
                    xB = np.nanmean(_dummyRho, axis=1)
                    xBE = np.nanstd(_dummyRho, axis=1)
                    yB = np.nanmean(_dummyLib, axis=1)
                    yBE = np.nanstd(_dummyLib, axis=1)
                    yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
                    _sort = np.argsort(xB)
                    botAxes[2].errorbar(
                        xB[_sort],
                        yB[_sort],
                        xerr=xBE[_sort],
                        yerr=yBE[_sort],
                        fmt="-o",
                        color=_col,
                        ms=12,
                        alpha=0.4,
                    )
            fig.savefig(
                "../pdfbox/GeneralProfilesShotTargetNeTe{}.pdf".format(shot),
                bbox_to_inches="tight",
            )
            fig.savefig(
                "../pngbox/GeneralProfilesShotTargetNeTe{}.png".format(shot),
                bbox_to_inches="tight",
                dpi=300,
            )
    elif selection == 12:
        shot = 34276
        tList = ((2.245, 2.345), (3.0, 3.1), (4.63, 4.73), (5.4, 5.5))
        thresL = (-99.0, 1200, 500, 500)
        _cShot = "k"
        colorList = ["#e41a1c", "#377eb8", "#4daf4a", "#984ea3"]
        c = mds.Connection("localhost:8001")
        # repat for shot 34276 which has lower points in profiles
        gs_top = mpl.pylab.GridSpec(
            5, 1, hspace=0.001, top=0.99, bottom=0.05, right=0.95, left=0.14
        )
        gs_base = mpl.pylab.GridSpec(
            5, 2, wspace=0.19, hspace=0.2, top=0.86, bottom=0.05, right=0.95, left=0.14
        )
        fig = mpl.pylab.figure(figsize=(12, 17))
        ax = fig.add_subplot(gs_base[-2, :])
        botAxes = [ax] + [
            fig.add_subplot(gs_base[-1, k], sharex=ax)
            for k in np.arange(2, dtype="int")
        ]
        # botAxes[0] is the upstream density profile
        # botAxes[1] is the target density botAxes[2] is the target temperature
        # for _ax in botAxes:
        for _axid, _ax in enumerate(botAxes):
            _ax.xaxis.set_minor_locator(AutoMinorLocator())
            if _axid != 0:
                _ax.yaxis.set_minor_locator(AutoMinorLocator())
                _ax.set_xlabel(r"$\rho$")
            else:
                _ax.yaxis.set_major_locator(
                    mpl.ticker.LogLocator(
                        base=10.0, numticks=15, subs=(1, 2, 3, 4, 5, 6, 7, 8, 9)
                    )
                )
                _ax.set_ylim([8e-2, 2])
                _ax.set_yscale("log")
                _ax.set_ylabel(r"n$_e$/n$_e(\rho=1)$")
            _ax.set_xlim([0.96, 1.1])
            _ax.set_xticks([0.96, 1, 1.04, 1.08])
        botAxes[1].set_ylabel(r"n$_e^t [10^{20}$m$^{-3}$]")
        botAxes[2].set_ylabel(r"T$_e$[eV]")
        # The shared in time axes
        ax = fig.add_subplot(gs_top[0])  # Need to create the first one to share...
        other_axes = [
            fig.add_subplot(gs_top[i], sharex=ax)
            for i in np.linspace(1, 2, 2, dtype="int")
        ]
        topAxes = [ax] + other_axes
        # Hide shared x-tick labels
        for ax in topAxes[:-1]:
            ax.tick_params(labelbottom=False)
        for ax in topAxes:
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())
            ax.set_xlim([0, 8])

        _s = 'augsignal({},"TOT","P_TOT")'.format(shot)
        Data = c.get(_s).data()
        _st = 'dim_of(augsignal({},"TOT","P_TOT"))'.format(shot)
        topAxes[0].plot(
            c.get(_st).data(), bn.move_mean(Data / 1e6, 200), color=_cShot, lw=2.5
        )
        topAxes[0].set_ylabel(r"P$_{\mathrm{TOT}}$ [MW]")
        topAxes[0].text(
            0.1, 0.9, r"#{}".format(shot), color=_cShot, transform=topAxes[0].transAxes
        )

        _s = 'augsignal({},"UVS","D_tot")'.format(shot)
        Data = c.get(_s).data()
        _st = 'dim_of(augsignal({},"UVS","D_tot"))'.format(shot)
        topAxes[1].plot(c.get(_st).data(), Data / 1e22, color=_cShot, lw=2.5)
        topAxes[1].set_ylabel(r"D$_2 [10^{22}]$")
        topAxes[1].set_ylim([0, 5])
        topAxes[1].text(0.1, 0.9, "No cryo", transform=topAxes[1].transAxes)

        _s = 'augsignal({},"TOT","n/nGW")'.format(shot)
        Data = c.get(_s).data()
        _st = 'dim_of(augsignal({},"TOT","n/nGW"))'.format(shot)
        topAxes[2].plot(c.get(_st).data(), Data, color=_cShot, lw=2.5)
        topAxes[2].set_ylabel(r"n/n$_G$")
        topAxes[2].set_ylim([0, 1.05])

        topAxes[2].set_xlabel(r"t [s]")
        for ax in topAxes:
            for t, _c in zip(tList, colorList):
                ax.axvline(np.mean(t), ls="-", lw=2, color=_c)
        # read the Li-Beam data
        LiBes = c.get('augsignal({}, "LIN", "ne")'.format(shot)).data() / 1e19
        LiRho = c.get('dim_of(augsignal({},"LIN","ne"),1)'.format(shot)).data()
        LiTime = c.get('dim_of(augsignal({},"LIN","ne"))'.format(shot)).data()
        # read the IpolSolA per il masking del
        # LiB
        Ipol = c.get('augsignal({}, "MAC", "Ipolsola")'.format(shot)).data()
        IpolTime = c.get('dim_of(augsignal({}, "MAC", "Ipolsola"))'.format(shot)).data()
        # read also the file with the target profile
        Target = np.load("../data/OuterTarget{}.npz".format(shot))
        # now the profiles which is more subtle and long
        for _tr, _thr, _col in zip(tList, thresL, colorList):
            if _thr != -99:
                _idxT = np.where(((IpolTime >= _tr[0]) & (IpolTime <= _tr[1])))[0]
                # now create an appropriate savgolfile
                IpolS = savgol_filter(Ipol[_idxT], 301, 3)
                IpolT = IpolTime[_idxT]
                IpolO = Ipol[_idxT]
                _dummyTime = LiTime[
                    np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
                _dummyLib = LiBes[
                    :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
                _dummyRho = LiRho[
                    :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
                IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                # on these we choose a threshold
                # which can be set as also set as keyword
                ElmMask = np.zeros(IpolSp.size, dtype="bool")
                ElmMask[np.where(IpolSp > _thr)[0]] = True
                _interElm = np.where(ElmMask == False)[0]
                # in this way they are
                _dummyLib = _dummyLib[:, _interElm]
                _dummyRho = _dummyRho[:, _interElm]
            else:
                _dummyLib = LiBes[
                    :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
                _dummyRho = LiRho[
                    :, np.where((LiTime >= _tr[0]) & (LiTime <= _tr[1]))[0]
                ]
            xB = np.nanmean(_dummyRho, axis=1)
            xBE = np.nanstd(_dummyRho, axis=1)
            yB = np.nanmean(_dummyLib, axis=1)
            yBE = np.nanstd(_dummyLib, axis=1)
            yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
            enS = yB[np.argmin(np.abs(xB - 1))]
            # botAxes.errorbar(
            #     xB, yB / enS, xerr=xBE,
            #     yerr=yBE / enS, fmt='-', color=_col, lw=2.5 )
            botAxes[0].plot(xB, yB / enS, "-", lw=2.5, color=_col)
            botAxes[0].fill_between(
                xB, (yB - yBE / 2) / enS, (yB + yBE / 2) / enS, color=_col, alpha=0.1
            )
            # do similar stuff for the target
            _dummyTime = Target["time"][
                np.where((Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1]))[0]
            ]
            _dummyLib = (
                Target["en"][
                    :,
                    np.where((Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1]))[
                        0
                    ],
                ]
                / 1e20
            )
            _dummyRho = Target["rho"][
                :, np.where((Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1]))[0]
            ]
            if _thr != -99:
                IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
                # on these we choose a threshold
                # which can be set as also set as keyword
                ElmMask = np.zeros(IpolSp.size, dtype="bool")
                ElmMask[np.where(IpolSp > _thr)[0]] = True
                _interElm = np.where(ElmMask == False)[0]
                _dummyLib = _dummyLib[:, _interElm]
                _dummyRho = _dummyRho[:, _interElm]
            xB = np.nanmean(_dummyRho, axis=1)
            xBE = np.nanstd(_dummyRho, axis=1)
            yB = np.nanmean(_dummyLib, axis=1)
            yBE = np.nanstd(_dummyLib, axis=1)
            yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
            _sort = np.argsort(xB)
            botAxes[1].errorbar(
                xB[_sort],
                yB[_sort],
                xerr=xBE[_sort],
                yerr=yBE[_sort],
                fmt="-o",
                color=_col,
                ms=12,
                alpha=0.6,
            )
            # repeat for the temperature
            _dummyTime = Target["time"][
                np.where((Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1]))[0]
            ]
            _dummyLib = Target["te"][
                :, np.where((Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1]))[0]
            ]
            _dummyRho = Target["rho"][
                :, np.where((Target["time"] >= _tr[0]) & (Target["time"] <= _tr[1]))[0]
            ]
            if _thr != -99:
                _dummyLib = _dummyLib[:, _interElm]
                _dummyRho = _dummyRho[:, _interElm]
            xB = np.nanmean(_dummyRho, axis=1)
            xBE = np.nanstd(_dummyRho, axis=1)
            yB = np.nanmean(_dummyLib, axis=1)
            yBE = np.nanstd(_dummyLib, axis=1)
            yBE[~np.isfinite(yBE)] = np.mean(yBE[np.isfinite(yBE)])
            _sort = np.argsort(xB)
            botAxes[2].errorbar(
                xB[_sort],
                yB[_sort],
                xerr=xBE[_sort],
                yerr=yBE[_sort],
                fmt="-o",
                color=_col,
                ms=12,
                alpha=0.4,
            )
        fig.savefig(
            "../pdfbox/GeneralProfilesShotTargetNeTe{}.pdf".format(shot),
            bbox_to_inches="tight",
        )

    elif selection == 13:
        shotList = (37444, 37447)
        c = mds.Connection("localhost:8000")
        OuterTarget = OrderedDict(
            [
                ("ua1", {"R": 1.582, "z": -1.199, "s": 1.045}),
                ("ua2", {"R": 1.588, "z": -1.175, "s": 1.070}),
                ("ua3", {"R": 1.595, "z": -1.151, "s": 1.094}),
                ("ua4", {"R": 1.601, "z": -1.127, "s": 1.126}),
                ("ua5", {"R": 1.608, "z": -1.103, "s": 1.158}),
                ("ua6", {"R": 1.614, "z": -1.078, "s": 1.189}),
                ("ua7", {"R": 1.620, "z": -1.054, "s": 1.213}),
                ("ua8", {"R": 1.627, "z": -1.030, "s": 1.246}),
                ("ua9", {"R": 1.640, "z": -0.982, "s": 1.276}),
            ]
        )
        for shot in shotList:
            fig, ax = mpl.pylab.subplots(figsize=(9,7), nrows=2,ncols=1,sharex='col')
            fig.subplots_adjust(wspace=0.05)
            ax[1].set_xlim([0,7])
            ax[1].set_xlabel(r't [s]')
            ax[0].set_ylabel(r'D$_2 [10^{21}$e/s]')
            ax[1].set_ylabel(r"ion flux [10$^{23}$ /s]")
            ax[0].tick_params(labelbottom=False)
            ax[0].set_title(r'#{}'.format(shot))
            outDivSignal = np.asarray([])
            neTime = c.get(
                'dim_of(augsignal({}, "LSD", "ne-{}"))'.format(shot, "ua1")
            ).data()
            for i, s in enumerate(OuterTarget.keys()):
                _ne = c.get('augsignal({}, "LSD", "ne-{}")'.format(shot, s)).data()
                _te = c.get('augsignal({}, "LSD", "te-{}")'.format(shot, s)).data()
                _an = c.get('augsignal({}, "LSD", "ang-{}")'.format(shot, s)).data()
                _cs = np.sqrt(constants.e * 4 * _te / (2 * constants.m_p))
                # this is the ion flux
                _s = _ne * _cs * np.abs(np.sin(np.radians(_an)))

                if i == 0:
                    outDivSignal = _s
                else:
                    outDivSignal = np.vstack((outDivSignal, _s))
            outTarget = np.zeros(outDivSignal.shape[1])
            for i in range(neTime.size):
                _x = np.asarray([OuterTarget[k]["s"] for k in OuterTarget.keys()])
                _r = np.asarray([OuterTarget[k]["R"] for k in OuterTarget.keys()])
                _y = outDivSignal[:, i]
                _dummy = np.vstack((_x, _y)).transpose()
                _dummy = _dummy[~np.isnan(_dummy).any(1)]
                _x = _dummy[:, 0]
                _y = _dummy[:, 1][np.argsort(_x)]
                _x = np.sort(_x)
                outTarget[i] = 2 * np.pi * _r.mean() * np.trapz(_y, x=_x)
            outTarget = bn.move_mean(outTarget, 600) / 1e23
            ax[1].plot(neTime,outTarget,'-',color='k')
            # read also the total fuelling
            _s = 'augsignal({},"UVS","D_tot")'.format(shot)
            Data = c.get(_s).data()/1e21
            _st = 'dim_of(augsignal({},"UVS","D_tot"))'.format(shot)
            ax[0].plot(_st,Data,'-',color='k')
            for _ax in ax:
                _ax.autoscale(enable='True',axis='y')
            fig.savefig('../pdfbox/OuterTargetFluxShot{}.pdf',bbox_to_inches='tight')
    elif selection == 99:
        loop = False
    else:
        input("Unknown Option Selected!")
