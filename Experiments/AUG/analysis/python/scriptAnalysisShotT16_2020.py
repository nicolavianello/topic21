import socket
import warnings
import matplotlib as mpl
import numpy as np
from collections import OrderedDict
import gpr1dfusion
import seaborn as sns
from scipy import constants
from scipy.stats import sigmaclip
import bottleneck
import MDSplus as mds
from aug import libes, mdslangmuir, mdsthomson, myThbObject, mdselmremoval
from general import bin_by, elm_detection
from astropy import units
from statsmodels.stats.weightstats import DescrStatsW
from plasmapy.formulary import collisions, parameters
try:
    from plasmapy.atomic import Particle
except:
    from plasmapy.particles import Particle
if socket.gethostname() == "toki08":
    server = "mdsplus.aug.ipp.mpg.de"
    import dd
    from aug import ddremoveELMData
else:
    server = "localhost:8000"
mpl.rcParams["font.family"] = "sans-serif"
mpl.rc("font", size=22)
mpl.rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
mpl.rc("lines", linewidth=2)

def print_menu():
    print(30 * "-", "MENU", 30 * "-")
    print("1. General Plot 37466 with rollover density")
    print("2. Upstream/Divertor profiles 37466")
    print("3. General Plot 37468 with rollover density")
    print("4. Upstream/Divertor profiles 37468")
    print("5. Evolution of Lambda_div shot 37466")
    print("6. Compare shot 34276/37750")
    print("7. Compare shot 37466/37752")
    print("8. Compare shot 37468/37753")
    print("9. Upstream profiles 37750")
    print("10. Upstream profiles 37752")
    print("11. Upstream profiles 37753")
    print("12. Upstream profiles 37754")
    print("13. General plot 37754")
    print("14. Target profiles 34276")
    print("15. Target/Lambda profiles profiles 37466")
    print("16. Target profiles 37468")
    print("17. General plot 37550 and 37552")
    print("18. Target profile/Lambda profiles 37750")
    print("19. Target profiles/Lambda profiles 37752")
    print("20. Fluctuation velocity 37752")
    print("21. Filament analysis 37750")
    print("22. Upstream profiles 37468")
    print("23. Target profiles/Lambda profiles 37753")
    print("24. Target profiles/Lambda profiles 37468")
    print(
        "25. Vr boxplot and fb vs Lambda Div and Pmain for 37466, 37468, 37550, 37552, 37753 "
    )
    print("26. Evaluation 37466")
    print("27. Evaluation 37468")
    print("28. Evaluation 37750")
    print("29. Evaluation 37752")
    print("30. Evaluation 37753")
    print(
        "31. Vr and fb time traces vs LambdaDiv and Pmain for 37466, 37468, 37550, 37552, 37753"
    )
    print("32. Compare filaments velocity 37466 and 37752 at different pressure")
    print("33. Time blob_freq evolution in shot 37468")
    print("34. Far SOL Ti vs lambda_div and pMain")
    print("35. Combine upstream profiles 37466 and 37752")
    print("36. Combine fluctuation properties 37466, 37752")
    print("99: End")
    print(67 * "-")


loop = True
while loop:
    print_menu()
    selection = int(input("Enter your choice [1-99] "))
    if selection == 1:
        shot = 37466
        Conn = mds.Connection(server)
        Diagnostic = OrderedDict(
            [
                (
                    "density",
                    {
                        "experiment": "TOT",
                        "signal": "H-5_corr",
                        "label": r"n$_e^e [10^{19}$m$^{-3}$]",
                        "factor": 1e19,
                    },
                ),
                (
                    "D2",
                    {
                        "experiment": "UVS",
                        "signal": "D_tot",
                        "label": r"D$_2$ [$10^{22}$el/s]",
                        "factor": 1e22,
                    },
                ),
                (
                    "Ipol",
                    {
                        "experiment": "Mac",
                        "signal": "Ipolsola",
                        "label": r"I$_s$ [kA/m$^2$]",
                        "factor": -1e3,
                    },
                ),
                (
                    "pressureDiv",
                    {
                        "experiment": "DDS",
                        "signal": "nDivIst",
                        "label": r"[10$^{23}$m$^{2}$/s]",
                        "factor": 1e21,
                    },
                ),
            ]
        )

        fig, ax = mpl.pylab.subplots(figsize=(7, 9), nrows=5, ncols=1, sharex="col")
        fig.subplots_adjust(hspace=0.05, wspace=0.05, top=0.92, bottom=0.12)
        for i, k in enumerate(Diagnostic.keys()):
            signal = Diagnostic[k]["signal"]
            diag = Diagnostic[k]["experiment"]
            _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
            _st = "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
            data = Conn.get(_s).data()
            time = Conn.get(_st).data()
            ax[i].plot(time, data / Diagnostic[k]["factor"], color="k")
            ax[i].text(
                0.1,
                0.87,
                Diagnostic[k]["label"],
                transform=ax[i].transAxes,
                fontsize=13,
            )

        # ADD also the midplane pressure
        _s = 'augdiag({}, "DDS", "nMainIst")'.format(shot)
        _st = 'dim_of(augdiag({}, "DDS", "nMainIst"))'.format(shot)
        data = Conn.get(_s).data()
        time = Conn.get(_st).data()
        ax[3].plot(time, data / 1e19, color="orange")
        ax[3].text(
            0.1,
            0.7,
            r"Midplane $\times$ 100",
            color="orange",
            transform=ax[3].transAxes,
            fontsize=13,
        )
        ax[3].text(
            0.1, 0.6, r"Divertor", color="k", transform=ax[3].transAxes, fontsize=13
        )
        LP = mdslangmuir.Target(shot, server=server)
        TotIon, _ = LP.TotalIonFlux()
        ax[4].plot(LP.time, TotIon / 1e23, "k")
        ax[4].text(
            0.1,
            0.82,
            r"$\int$ J$_s [10^{23}$s$^{-1}$]",
            transform=ax[4].transAxes,
            fontsize=13,
        )
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")
            _ax.set_xlim([0, 6.9])
        ax[0].set_ylim([0, 8])
        ax[2].set_ylim([-5, 13])
        ax[3].set_ylim([0, 1.0])
        ax[0].set_title(r"AUG #{}".format(shot))
        ax[-1].set_xlabel(r"t[s]")
        fig.savefig(
            "../pdfbox/GeneralPlotShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 2:
        shot = 37466
        trangeList = ((2.2, 3.0), (4.0, 4.9), (5.2, 6.9))
        rhoList = (0.83, 0.83, 0.9)
        tSepList = (0.00015, 0.0003, 0.0006)
        colorList = ("#BF0C2B", "#353B40", "#862C91")
        fig, ax = mpl.pylab.subplots(figsize=(6, 12), nrows=4, ncols=1)
        fig.subplots_adjust(hspace=0.28, right=0.8, top=0.94)
        Conn = mds.Connection(server)
        diag, signal = "UVS", "D_tot"
        _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
        _st = "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
        data = Conn.get(_s).data()
        time = Conn.get(_st).data()
        ax[0].plot(time, data / 1e22, "k", lw=2)
        ax[0].set_ylabel(r"D$_2$ [$10^{22}$el/s]")
        axt = ax[0].twinx()
        diag, signal = "DDS", "nMainIst"
        _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
        _st = "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
        data = Conn.get(_s).data()
        time = Conn.get(_st).data()
        axt.plot(time, data / 1e19, color="orange")
        axt.set_ylabel(r"P$_{main}$ [10$^{19}$m$^{2}$/s]", color="orange")
        axt.tick_params(axis="y", colors="orange")
        axt.spines["right"].set_color("orange")
        ax[0].set_xlim([0, 7])
        ax[0].set_ylim([0, 3])
        axt.set_xlim([0, 7])
        axt.set_ylim([0, 2])

        edgeTh = mdsthomson.Thomson(shot)
        # load also the core thomson which is needed to get a better estimate of the shift
        coreTh = mdsthomson.Thomson(
            shot, equilibrium=edgeTh._equilibrium, location="core"
        )
        Lb = libes.Libes(shot, remote=True, edition=2, experiment="LIBE")
        LP = mdslangmuir.Target(shot, equilibrium=edgeTh._equilibrium)
        for tr, col, rho, t_l in zip(trangeList, colorList, rhoList, tSepList):
            ax[0].axvline(np.average(tr), ls="-", lw=2, color=col)
            _eTh = edgeTh.getTSprofiles(trange=tr, interelm=True, rho=rho, t_sep=t_l)
            _cTh = coreTh.getTSprofiles(trange=tr, interelm=True, rho=rho, t_sep=t_l)
            _TSp = edgeTh.combineprofiles(_eTh, _cTh)
            # compute the 100 eV shift
            _shift, _nesep = coreTh.computeshift(_TSp, nbins=50, clip=True)
            # consider the TS profiles
            _cA = np.logical_and(
                _TSp["rho"] + _shift >= 0.97, _TSp["rho"] + _shift <= 1.06,
            )
            rhoTS, enTS, denTS = (
                _TSp["rho"][_cA] + _shift,
                _TSp["ne"][_cA],
                _TSp["dne"][_cA],
            )
            # better to bin them since they are too much
            OutBinned = bin_by.binned_weighted_statistics(
                rhoTS, enTS / 1e19, denTS / 1e19, nbins=20
            )
            # now the combined Thomson/Li-Be profiles
            Out = Lb.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_l)
            _idx, _idx2 = (
                np.where(Out["rho"] - 0.002 >= 1.06)[0],
                np.where(Out["rhoraw"] - 0.002 >= 1.06)[0],
            )
            xB = np.append(OutBinned["x"], Out["rho"][_idx])
            yB = np.append(OutBinned["y_median"], Out["ne"][_idx])
            yBE = np.append(OutBinned["y_err"], Out["err_Y"][_idx])
            xBE = np.append(OutBinned["x_err"], Out["err_X"][_idx])
            gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                xB[np.argsort(xB)],
                yB[np.argsort(xB)],
                yBE[np.argsort(xB)],
                xe=xBE[np.argsort(xB)],
                nrestarts=500,
            )
            xN = np.linspace(0.97, 1.1, 120)
            gp.GPRFit(xN)
            yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
            enS = yFit[np.argmin(np.abs(xN - 1))]
            ax[1].errorbar(
                rhoTS,
                enTS / 1e19 / enS,
                yerr=denTS / 1e19 / enS,
                fmt="P",
                color=col,
                alpha=0.2,
                ms=7,
            )
            ax[1].plot(
                Out["rhoraw"][_idx2],
                Out["neraw"][_idx2],
                ".",
                color=col,
                alpha=0.2,
                ms=1,
            )
            ax[1].plot(
                xN,
                yFit / enS,
                "-",
                color=col,
                lw=2,
                label=r"$\Delta$t ={:1.1f} - {:1.1f}".format(tr[0], tr[1]),
            )
            ax[1].fill_between(
                xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
            )
            # add also the raw Li-Be data

            _LPp = LP.getprofile(trange=tr, rho=rho, t_sep=t_l)
            # need to bin it otherwise not clear
            _neBW = bin_by.bin_by(_LPp["x"], _LPp["ne"] / 1e19, nbins=12, clip=True)
            ax[2].errorbar(
                _neBW["x"],
                _neBW["y_median"],
                yerr=_neBW["y_err"],
                fmt="P",
                ms=13,
                color=col,
            )
            _teBW = bin_by.bin_by(_LPp["x"], _LPp["te"], nbins=12, clip=True)
            ax[3].errorbar(
                _teBW["x"],
                _teBW["y_median"],
                yerr=_teBW["y_err"],
                fmt="P",
                ms=13,
                color=col,
            )
        ax[1].set_xlim([0.97, 1.1])
        ax[2].set_xlim([0.97, 1.1])
        ax[3].set_xlim([0.97, 1.1])
        ax[1].tick_params(labelbottom=False)
        ax[2].tick_params(labelbottom=False)
        ax[1].set_ylabel(r"n$_e$/n$_e^s$")
        ax[1].set_yscale("log")
        ax[1].set_ylim([1e-1, 3])
        ax[2].set_ylabel(r"n$_e^t [10^{19}$m$^{-3}$]")
        ax[3].set_ylabel(r"T$_e$ [eV]")
        ax[3].set_xlabel(r"$\rho$")
        ax[2].autoscale(enable=True, axis="y")
        ax[3].autoscale(enable=True, axis="y")
        fig.savefig(
            "../pdfbox/DivertorUpstreamShot{}.pdf".format(shot), bbox_inches="tight"
        )
    elif selection == 3:
        shot = 37468
        Conn = mds.Connection(server)
        Diagnostic = OrderedDict(
            [
                (
                    "density",
                    {
                        "experiment": "TOT",
                        "signal": "H-5_corr",
                        "label": r"n$_e^e [10^{19}$m$^{-3}$]",
                        "factor": 1e19,
                    },
                ),
                (
                    "power",
                    {
                        "experiment": "TOT",
                        "signal": "P_TOT",
                        "label": r"P$_{tot}$ [MW]",
                        "factor": 1e6,
                    },
                ),
                (
                    "D2",
                    {
                        "experiment": "UVS",
                        "signal": "D_tot",
                        "label": r"D$_2$ [$10^{22}$el/s]",
                        "factor": 1e22,
                    },
                ),
                (
                    "Ipol",
                    {
                        "experiment": "Mac",
                        "signal": "Ipolsola",
                        "label": r"I$_s$ [kA/m$^2$]",
                        "factor": -1e3,
                    },
                ),
                (
                    "pressureDiv",
                    {
                        "experiment": "DDS",
                        "signal": "nDivIst",
                        "label": r"[10$^{23}$m$^{2}$/s]",
                        "factor": 1e21,
                    },
                ),
            ]
        )

        fig, ax = mpl.pylab.subplots(figsize=(7, 12), nrows=6, ncols=1, sharex="col")
        fig.subplots_adjust(hspace=0.05, wspace=0.05, top=0.92, bottom=0.12)
        for i, k in enumerate(Diagnostic.keys()):
            signal = Diagnostic[k]["signal"]
            diag = Diagnostic[k]["experiment"]
            _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
            _st = "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
            data = Conn.get(_s).data()
            time = Conn.get(_st).data()
            ax[i].plot(time, data / Diagnostic[k]["factor"], color="k")
            ax[i].text(
                0.05,
                0.87,
                Diagnostic[k]["label"],
                transform=ax[i].transAxes,
                fontsize=13,
            )

        # add the total radiated power
        _s = 'augdiag({}, "BPD", "Pradtot")'.format(shot)
        _st = 'dim_of(augdiag({}, "BPD", "Pradtot"))'.format(shot)
        data = Conn.get(_s).data()
        time = Conn.get(_st).data()
        ax[1].plot(time, data / 1e6, color="orange")
        ax[1].text(
            0.05,
            0.7,
            r"P$_{rad}$ [MW]",
            color="orange",
            transform=ax[1].transAxes,
            fontsize=13,
        )

        # add the Nitrogen
        _s = 'augdiag({}, "UVS", "N_tot")'.format(shot)
        _st = 'dim_of(augdiag({}, "UVS", "N_tot"))'.format(shot)
        data = Conn.get(_s).data()
        time = Conn.get(_st).data()
        ax[2].plot(time, data / 1e22, color="magenta")
        ax[2].text(
            0.05,
            0.7,
            r"N$_{tot}$ [10$^{22}$ el/s]",
            color="magenta",
            transform=ax[2].transAxes,
            fontsize=13,
        )

        # ADD also the midplane pressure
        _s = 'augdiag({}, "DDS", "nMainIst")'.format(shot)
        _st = 'dim_of(augdiag({}, "DDS", "nMainIst"))'.format(shot)
        data = Conn.get(_s).data()
        time = Conn.get(_st).data()
        ax[4].plot(time, data / 1e19, color="orange")
        ax[4].text(
            0.05,
            0.7,
            r"Midplane $\times$ 100",
            color="orange",
            transform=ax[4].transAxes,
            fontsize=13,
        )
        ax[4].text(
            0.1, 0.6, r"Divertor", color="k", transform=ax[4].transAxes, fontsize=13
        )

        LP = mdslangmuir.Target(shot, server=server)
        TotIon, _ = LP.TotalIonFlux()
        ax[5].plot(LP.time, TotIon / 1e23, "k")
        ax[5].text(
            0.05,
            0.82,
            r"$\int$ J$_s [10^{23}$s$^{-1}$]",
            transform=ax[5].transAxes,
            fontsize=13,
        )
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")
            _ax.set_xlim([0, 8])
        ax[0].set_ylim([0, 6])
        ax[2].set_ylim([0, 2.5])
        ax[3].set_ylim([0, 15])
        ax[4].set_ylim([0, 0.2])
        ax[0].set_title(r"AUG #{}".format(shot))
        ax[-1].set_xlabel(r"t[s]")
        fig.savefig(
            "../pdfbox/GeneralPlotShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 4:
        shot = 37468
        trangeList = ((1.5, 2.0), (3.0, 4.5), (5.4, 6.5))
        rhoList = (0.83, 0.83, 0.9)
        tSepList = (0.00015, 0.0003, 0.0006)
        colorList = ("#BF0C2B", "#353B40", "#862C91")
        fig, ax = mpl.pylab.subplots(figsize=(6, 12), nrows=4, ncols=1)
        fig.subplots_adjust(hspace=0.28, right=0.8, top=0.94)
        edgeTh = mdsthomson.Thomson(shot)
        # load also the core thomson which is needed to get a better estimate of the shift
        coreTh = mdsthomson.Thomson(
            shot, equilibrium=edgeTh._equilibrium, location="core"
        )
        Lb = libes.Libes(shot, remote=True, edition=2, experiment="LIBE")
        LP = mdslangmuir.Target(shot, equilibrium=edgeTh._equilibrium)
        TotIon, _ = LP.TotalIonFlux()
        ax[0].plot(LP.time, TotIon / 1e23, "k")
        ax[0].text(
            0.05,
            0.82,
            r"$\int$ J$_s [10^{23}$s$^{-1}$]",
            transform=ax[0].transAxes,
            fontsize=13,
        )
        ax[0].set_ylim([0, 0.4])
        ax[0].set_xlim([0, 8])
        for tr, col, rho, t_l in zip(trangeList, colorList, rhoList, tSepList):
            ax[0].axvline(np.average(tr), ls="-", lw=2, color=col)
            _eTh = edgeTh.getTSprofiles(trange=tr, interelm=True, rho=rho, t_sep=t_l)
            # _cTh = coreTh.getTSprofiles(trange=tr, interelm=True, rho=rho, t_sep=t_l)
            # _TSp = edgeTh.combineprofiles(_eTh, _cTh)
            # # compute the 100 eV shift
            # _shift, _nesep = coreTh.computeshift(_TSp, nbins=50, clip=True)
            # # consider the TS profiles
            # _cA = np.logical_and(
            #     _TSp["rho"] + _shift >= 0.97, _TSp["rho"] + _shift <= 1.06,
            # )
            rhoTS, enTS, denTS = (
                _eTh["rho"] + 0.02,
                _eTh["ne"],
                _eTh["dne"],
            )
            # better to bin them since they are too much
            OutBinned = bin_by.binned_weighted_statistics(
                rhoTS, enTS / 1e19, denTS / 1e19, nbins=20
            )
            # now the combined Thomson/Li-Be profiles
            Out = Lb.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_l)
            _idx, _idx2 = (
                np.where(Out["rho"] <= 0.97)[0],
                np.where(Out["rhoraw"] <= 0.97)[0],
            )
            xB = np.append(OutBinned["x"], Out["rho"][_idx])
            yB = np.append(OutBinned["y_median"], Out["ne"][_idx])
            yBE = np.append(OutBinned["y_err"], Out["err_Y"][_idx])
            xBE = np.append(OutBinned["x_err"], Out["err_X"][_idx])
            gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                xB[np.argsort(xB)],
                yB[np.argsort(xB)],
                yBE[np.argsort(xB)],
                xe=xBE[np.argsort(xB)],
                nrestarts=500,
            )
            xN = np.linspace(0.97, 1.1, 120)
            gp.GPRFit(xN)
            yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
            enS = yFit[np.argmin(np.abs(xN - 1))]
            ax[1].errorbar(
                rhoTS,
                enTS / 1e19 / enS,
                yerr=denTS / 1e19 / enS,
                fmt="P",
                color=col,
                alpha=0.1,
                ms=7,
            )
            ax[1].plot(
                Out["rhoraw"][_idx2],
                Out["neraw"][_idx2] / enS,
                ".",
                color=col,
                alpha=0.1,
                ms=1,
            )
            ax[1].plot(
                xN,
                yFit / enS,
                "-",
                color=col,
                lw=3,
                label=r"$\Delta$t ={:1.1f} - {:1.1f}".format(tr[0], tr[1]),
            )
            ax[1].fill_between(
                xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.1
            )
            # add also the raw Li-Be data

            _LPp = LP.getprofile(trange=tr, rho=rho, t_sep=t_l)
            # need to bin it otherwise not clear
            _neBW = bin_by.bin_by(_LPp["x"], _LPp["ne"] / 1e19, nbins=12, clip=True)
            ax[2].errorbar(
                _neBW["x"],
                _neBW["y_median"],
                yerr=_neBW["y_err"],
                fmt="P",
                ms=13,
                color=col,
            )
            _teBW = bin_by.bin_by(_LPp["x"], _LPp["te"], nbins=12, clip=True)
            ax[3].errorbar(
                _teBW["x"],
                _teBW["y_median"],
                yerr=_teBW["y_err"],
                fmt="P",
                ms=13,
                color=col,
            )
        ax[1].set_xlim([0.97, 1.1])
        ax[2].set_xlim([0.97, 1.1])
        ax[3].set_xlim([0.97, 1.1])
        ax[1].tick_params(labelbottom=False)
        ax[2].tick_params(labelbottom=False)
        ax[1].set_ylabel(r"n$_e$/n$_e^s$")
        ax[1].set_yscale("log")
        ax[1].set_ylim([1e-1, 3])
        ax[2].set_ylim([0, 5])
        ax[3].set_ylim([-1, 25])
        ax[2].set_ylabel(r"n$_e^t [10^{19}$m$^{-3}$]")
        ax[3].set_ylabel(r"T$_e$ [eV]")
        ax[3].set_xlabel(r"$\rho$")
        fig.savefig(
            "../pdfbox/DivertorUpstreamShot{}.pdf".format(shot), bbox_inches="tight"
        )
    elif selection == 6:
        shotList = (34276, 37750)
        colorList = ("#BF1B39", "#1B2A59")
        Conn = mds.Connection(server)
        Diagnostic = OrderedDict(
            [
                (
                    "density",
                    {
                        "experiment": "TOT",
                        "signal": "H-5_corr",
                        "label": r"n$_e^e [10^{19}$m$^{-3}$]",
                        "factor": 1e19,
                    },
                ),
                (
                    "D2",
                    {
                        "experiment": "UVS",
                        "signal": "D_tot",
                        "label": r"D$_2$ [$10^{22}$el/s]",
                        "factor": 1e22,
                    },
                ),
                (
                    "Ipol",
                    {
                        "experiment": "Mac",
                        "signal": "Ipolsola",
                        "label": r"I$_s$ [kA/m$^2$]",
                        "factor": -1e3,
                    },
                ),
                (
                    "pressureDiv",
                    {
                        "experiment": "DDS",
                        "signal": "nMainIst",
                        "label": r"nMain [10$^{20}$m$^{2}$/s]",
                        "factor": 1e20,
                    },
                ),
            ]
        )

        fig, ax = mpl.pylab.subplots(figsize=(7, 9), nrows=5, ncols=1, sharex="col")
        fig.subplots_adjust(hspace=0.05, wspace=0.05, top=0.92, bottom=0.12)
        for shot, col in zip(shotList, colorList):
            for i, k in enumerate(Diagnostic.keys()):
                signal = Diagnostic[k]["signal"]
                diag = Diagnostic[k]["experiment"]
                _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
                _st = (
                    "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
                )
                data = Conn.get(_s).data()
                time = Conn.get(_st).data()
                ax[i].plot(time, data / Diagnostic[k]["factor"], color=col)
                ax[i].text(
                    0.1,
                    0.87,
                    Diagnostic[k]["label"],
                    transform=ax[i].transAxes,
                    fontsize=13,
                )
            try:
                LP = mdslangmuir.Target(shot, server=server)
                TotIon, _ = LP.TotalIonFlux()
                ax[4].plot(LP.time, TotIon / 1e23, col)
                ax[4].text(
                    0.1,
                    0.82,
                    r"$\int$ J$_s [10^{23}$s$^{-1}$]",
                    transform=ax[4].transAxes,
                    fontsize=13,
                )
            except:
                pass
        ax[0].text(
            0.5,
            0.45,
            r"#{}".format(shotList[0]),
            transform=ax[0].transAxes,
            color=colorList[0],
        )
        ax[0].text(
            0.5,
            0.15,
            r"#{}".format(shotList[1]),
            transform=ax[0].transAxes,
            color=colorList[1],
        )
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04")
        tList = (2.75, 3.75, 5.75)
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")
            _ax.set_xlim([0, 6.9])
            for t, c in zip(tList, colorList):
                _ax.axvline(t, ls="-", lw=2, color=c)
        ax[0].set_ylim([0, 11])
        ax[2].set_ylim([-5, 13])
        ax[3].set_ylim([0, 0.5])
        ax[-1].set_xlabel(r"t[s]")
        fig.savefig(
            "../pdfbox/GeneralPlotShot{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inches="tight",
        )
    elif selection == 7:
        shotList = (37466, 37752)
        colorList = ("#BF1B39", "#1B2A59")
        Conn = mds.Connection(server)
        Diagnostic = OrderedDict(
            [
                (
                    "density",
                    {
                        "experiment": "TOT",
                        "signal": "H-5_corr",
                        "label": r"n$_e^e [10^{19}$m$^{-3}$]",
                        "factor": 1e19,
                    },
                ),
                (
                    "D2",
                    {
                        "experiment": "UVS",
                        "signal": "D_tot",
                        "label": r"D$_2$ [$10^{22}$el/s]",
                        "factor": 1e22,
                    },
                ),
                (
                    "Ipol",
                    {
                        "experiment": "Mac",
                        "signal": "Ipolsola",
                        "label": r"I$_s$ [kA/m$^2$]",
                        "factor": -1e3,
                    },
                ),
                (
                    "pressureDiv",
                    {
                        "experiment": "DDS",
                        "signal": "nMainIst",
                        "label": r" nMain [10$^{19}$m$^{2}$/s]",
                        "factor": 1e19,
                    },
                ),
            ]
        )

        fig, ax = mpl.pylab.subplots(figsize=(7, 9), nrows=5, ncols=1, sharex="col")
        fig.subplots_adjust(hspace=0.05, wspace=0.05, top=0.92, bottom=0.12)
        for shot, col in zip(shotList, colorList):
            for i, k in enumerate(Diagnostic.keys()):
                signal = Diagnostic[k]["signal"]
                diag = Diagnostic[k]["experiment"]
                _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
                _st = (
                    "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
                )
                data = Conn.get(_s).data()
                time = Conn.get(_st).data()
                ax[i].plot(time, data / Diagnostic[k]["factor"], color=col)
                ax[i].text(
                    0.1,
                    0.87,
                    Diagnostic[k]["label"],
                    transform=ax[i].transAxes,
                    fontsize=13,
                )
            try:
                LP = mdslangmuir.Target(shot, server=server)
                TotIon, _ = LP.TotalIonFlux()
                ax[4].plot(LP.time, TotIon / 1e23, col)
                ax[4].text(
                    0.1,
                    0.82,
                    r"$\int$ J$_s [10^{23}$s$^{-1}$]",
                    transform=ax[4].transAxes,
                    fontsize=13,
                )
            except:
                pass
        ax[0].text(
            0.5,
            0.45,
            r"#{}".format(shotList[0]),
            transform=ax[0].transAxes,
            color=colorList[0],
        )
        ax[0].text(
            0.5,
            0.15,
            r"#{}".format(shotList[1]),
            transform=ax[0].transAxes,
            color=colorList[1],
        )
        tList = (2.25, 4.2, 5.85, 6.65)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")
            _ax.set_xlim([0, 7.4])
            for t, c in zip(tList, colorList):
                _ax.axvline(t, ls="-", lw=2, color=c)
        ax[0].set_ylim([0, 8])
        ax[2].set_ylim([-5, 13])
        ax[3].set_ylim([0, 1.0])
        ax[-1].set_xlabel(r"t[s]")
        fig.savefig(
            "../pdfbox/GeneralPlotShot{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inches="tight",
        )
    elif selection == 8:
        shotList = (37468, 37753)
        colorList = ("#BF1B39", "#1B2A59")
        Conn = mds.Connection(server)
        Diagnostic = OrderedDict(
            [
                (
                    "density",
                    {
                        "experiment": "TOT",
                        "signal": "H-5_corr",
                        "label": r"n$_e^e [10^{19}$m$^{-3}$]",
                        "factor": 1e19,
                    },
                ),
                (
                    "D2",
                    {
                        "experiment": "UVS",
                        "signal": "D_tot",
                        "label": r"D$_2$ [$10^{22}$el/s]",
                        "factor": 1e22,
                    },
                ),
                (
                    "N2",
                    {
                        "experiment": "UVS",
                        "signal": "N_tot",
                        "label": r"N$_2$ [$10^{22}$el/s]",
                        "factor": 1e22,
                    },
                ),
                (
                    "Ipol",
                    {
                        "experiment": "Mac",
                        "signal": "Ipolsola",
                        "label": r"I$_s$ [kA/m$^2$]",
                        "factor": -1e3,
                    },
                ),
                (
                    "pressureDiv",
                    {
                        "experiment": "DDS",
                        "signal": "nMainIst",
                        "label": r"nMain [10$^{19}$m$^{2}$/s]",
                        "factor": 1e19,
                    },
                ),
            ]
        )

        fig, ax = mpl.pylab.subplots(figsize=(7, 9), nrows=6, ncols=1, sharex="col")
        fig.subplots_adjust(hspace=0.05, wspace=0.05, top=0.92, bottom=0.12)
        for shot, col in zip(shotList, colorList):
            for i, k in enumerate(Diagnostic.keys()):
                signal = Diagnostic[k]["signal"]
                diag = Diagnostic[k]["experiment"]
                _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
                _st = (
                    "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
                )
                data = Conn.get(_s).data()
                time = Conn.get(_st).data()
                ax[i].plot(time, data / Diagnostic[k]["factor"], color=col)
                ax[i].text(
                    0.1,
                    0.87,
                    Diagnostic[k]["label"],
                    transform=ax[i].transAxes,
                    fontsize=13,
                )
            try:
                LP = mdslangmuir.Target(shot, server=server)
                TotIon, _ = LP.TotalIonFlux()
                ax[5].plot(LP.time, TotIon / 1e23, col)
                ax[5].text(
                    0.1,
                    0.82,
                    r"$\int$ J$_s [10^{23}$s$^{-1}$]",
                    transform=ax[5].transAxes,
                    fontsize=13,
                )
            except:
                pass
        ax[0].text(
            0.5,
            0.65,
            r"#{}".format(shotList[0]),
            transform=ax[0].transAxes,
            color=colorList[0],
        )
        ax[0].text(
            0.5,
            0.15,
            r"#{}".format(shotList[1]),
            transform=ax[0].transAxes,
            color=colorList[1],
        )
        tList = (1.9, 4.65, 6.4)
        colorList = ("#1C6C8C", "#F2B90F", "#276323")
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")
            _ax.set_xlim([0, 7.4])
            for t, c in zip(tList, colorList):
                _ax.axvline(t, ls="-", lw=2, color=c)
        ax[0].set_ylim([0, 8])
        ax[3].set_ylim([-5, 13])
        ax[4].set_ylim([0, 0.3])
        ax[-1].set_xlabel(r"t[s]")
        fig.savefig(
            "../pdfbox/GeneralPlotShot{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inches="tight",
        )
    elif selection == 9:
        shot = 37750
        #        trangeList = ((2.5, 3.0), (3.5, 4.0), (5.5, 6.0))
        trangeList = ((2.3, 2.6), (3.7, 4), (4.7, 5), (5.7, 6))
        #        rhoList = (0.9, 0.83, None)
        rhoList = (0.83, 0.85, 0.9, None)
        #        tSepList = (0.00015, 0.0003, None)
        tSepList = (0.00015, 0.002, 0.002, None)
        #        colorList = ("#1C6C8C", "#F2B90F", "#D92B04")
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        LB = libes.Libes(shot, remote=True, experiment="AUGD", server=server)
        # Eq = eqtools.AUGMDSTree(34276, server=server, tspline=True)
        # _ = Eq.getBCentr()
        # Target = mdslangmuir.Target(34276, equilibrium=Eq,  server=server)
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.1])
        ax.set_ylim([5e-2, 5])
        for tr, rho, t_sep, col in zip(trangeList, rhoList, tSepList, colorList):
            if rho:
                Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
                # OutTarget = Target.getprofile(
                #     trange=tr, interelm=True, rho=rho, t_sep=t_sep
                # )
            else:
                Out = LB.averageProfile(trange=tr)
            #     OutTarget = Target.getprofile(trange=tr)
            # OutLambda = Target.getLambda(OutTarget, np.average(tr))
            # _aa = np.logical_and(
            #     OutLambda["rho_Lpar"] >= 1.02, OutLambda["rho_Lpar"] <= 1.04
            # )
            # _label = r"$\Lambda_div$ = {:.2f} $\pm$ {:3f} ".format(
            #     np.average(OutLambda["LambdaDiv"][_aa]),
            #     np.std(OutLambda["LambdaDiv"][_aa]),
            # )
            _label = r"t  = {:.2f} - {:.2f} s".format(tr[0], tr[1])
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )

        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 10:
        shot = 37752
        # trangeList = ((2.5, 3.3), (4, 4.8), (5.2, 6.0))
        # rhoList = (0.9, 0.8, None)
        # tSepList = (0.002, 0.0006, None)
        # colorList = ("#1C6C8C", "#F2B90F", "#D92B04")
        trangeList = ((2.1, 2.4), (4, 4.4), (5.7, 6), (6.5, 6.8))
        rhoList = (0.9, 0.9, None, None)
        tSepList = (0.00015, 0.002, None, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        LB = libes.Libes(shot, remote=True, experiment="AUGE", server=server)
        # Target = mdslangmuir.Target(37466, server=server, tspline=True)
        # _ = Target.Eq.getBCentr()
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.075])
        ax.set_ylim([0.1, 2])
        for tr, rho, t_sep, col in zip(trangeList, rhoList, tSepList, colorList):
            if rho:
                Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
                # OutTarget = Target.getprofile(
                #     trange=tr, interelm=True, rho=rho, t_sep=t_sep
                # )
            else:
                Out = LB.averageProfile(trange=tr)
                # OutTarget = Target.getprofile(trange=tr)
            # OutLambda = Target.getLambda(OutTarget, np.average(tr))
            # _aa = np.logical_and(
            #     OutLambda["rho_Lpar"] >= 1.02, OutLambda["rho_lpar"] <= 1.04
            # )
            # _label = r"$\Lambda_div$ = {:.2f} $\pm$ {:3f} ".format(
            #     np.average(OutLambda["LambdaDiv"][_aa]),
            #     np.std(OutLambda["LambdaDiv"][_aa]),
            # )
            _label = r"t  = {:.2f} - {:.2f} s".format(tr[0], tr[1])
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 11:
        shot = 37753
        trangeList = ((1.5, 2.3), (4.3, 5), (6.0, 6.8))
        rhoList = (0.83, 0.9, 0.9)
        tSepList = (0.00015, 0.002, 0.002)
        colorList = ("#1C6C8C", "#F2B90F", "#276323")
        LB = libes.Libes(shot, remote=True, experiment="AUGD", server=server)
        LP = mdslangmuir.Target(shot, server=server)
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.1])
        ax.set_ylim([5e-2, 5])
        for tr, rho, t_sep, col in zip(trangeList, rhoList, tSepList, colorList):
            Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            # compute the target profiles to get the Lambda-parameter
            inDictionary = LP.getprofile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            outLambda = LP.getLambda(
                inDictionary, np.average(tr), nbins=8, inLparallel=inParallel, clip=True
            )
            # drop the zero values of LambdaDiv
            mask = np.logical_and(
                outLambda["rho_Lpar"] >= 1.025, outLambda["LambdaDiv"] != 0
            )
            _st = r"$\langle \Lambda_{div}\rangle_{\rho \leq 1.025}$ = "
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                warnings.warn("Computed the GP fit")
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                warnings.warn("Not Computed the GP fit")

            ax.plot(Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2)
            ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
            ax.fill_between(
                xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
            )
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 12:
        shot = 37754
        trangeList = ((1.5, 2), (3, 4.0), (4.7, 5.1))
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04")
        LB = libes.Libes(shot, remote=True, experiment="AUGD", server=server)
        # Eq = eqtools.AUGMDSTree(37466, tspline=True, server=server)
        # Target = mdslangmuir.Target(37466)
        # _ = Target.Eq.getBCentr()
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.1])
        ax.set_ylim([5e-2, 5])
        for tr, col in zip(trangeList, colorList):
            Out = LB.averageProfile(trange=tr)
            #     OutTarget = Target.getprofile(trange=tr)
            # OutLambda = Target.getLambda(OutTarget, np.average(tr))
            # _aa = np.logical_and(
            #     OutLambda["rho_Lpar"] >= 1.02, OutLambda["rho_lpar"] <= 1.04
            # )
            # _label = r"$\Lambda_div$ = {:.2f} $\pm$ {:3f} ".format(
            #     np.average(OutLambda["LambdaDiv"][_aa]),
            #     np.std(OutLambda["LambdaDiv"][_aa]),
            # )
            _label = r"t ={:.2f} - {:.2f} s".format(tr[0], tr[1])
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]

            ax.plot(Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2)
            ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
            ax.fill_between(
                xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
            )
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 13:
        shot = 37754
        col = "#1B2A59"
        Conn = mds.Connection(server)
        Diagnostic = OrderedDict(
            [
                (
                    "density",
                    {
                        "experiment": "TOT",
                        "signal": "H-5_corr",
                        "label": r"n$_e^e [10^{19}$m$^{-3}$]",
                        "factor": 1e19,
                    },
                ),
                (
                    "D2",
                    {
                        "experiment": "UVS",
                        "signal": "D_tot",
                        "label": r"D$_2$ [$10^{22}$el/s]",
                        "factor": 1e22,
                    },
                ),
                (
                    "N2",
                    {
                        "experiment": "UVS",
                        "signal": "N_tot",
                        "label": r"N$_2$ [$10^{22}$el/s]",
                        "factor": 1e22,
                    },
                ),
                (
                    "pressureDiv",
                    {
                        "experiment": "DDS",
                        "signal": "nMainIst",
                        "label": r"nMain [10$^{19}$m$^{2}$/s]",
                        "factor": 1e19,
                    },
                ),
            ]
        )

        fig, ax = mpl.pylab.subplots(figsize=(7, 9), nrows=4, ncols=1, sharex="col")
        fig.subplots_adjust(hspace=0.05, wspace=0.05, top=0.92, bottom=0.12)
        for i, k in enumerate(Diagnostic.keys()):
            signal = Diagnostic[k]["signal"]
            diag = Diagnostic[k]["experiment"]
            _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
            _st = "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
            data = Conn.get(_s).data()
            time = Conn.get(_st).data()
            ax[i].plot(time, data / Diagnostic[k]["factor"], color=col)
            ax[i].text(
                0.1,
                0.87,
                Diagnostic[k]["label"],
                transform=ax[i].transAxes,
                fontsize=13,
            )
        ax[0].text(
            0.5, 0.65, r"#{}".format(shot), transform=ax[0].transAxes, color=col,
        )
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04")
        tList = (1.75, 3.5, 4.9)
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")
            _ax.set_xlim([0, 7.4])
            for t, c in zip(tList, colorList):
                _ax.axvline(t, ls="-", lw=2, color=c)
        ax[0].set_ylim([0, 5])
        ax[3].set_ylim([0, 0.15])
        ax[-1].set_xlabel(r"t[s]")
        fig.savefig(
            "../pdfbox/GeneralPlotShot{}.pdf".format(shot), bbox_to_inches="tight",
        )
    elif selection == 14:
        shot = 34276
        trList = ((2.5, 3), (3.5, 4), (5.5, 6))
        rhoList = (0.9, 0.83, None)
        tsepList = (0.00015, 0.0003, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04")
        Target = mdslangmuir.Target(shot, server=server)
        fig, ax = mpl.pylab.subplots(figsize=(16, 5), nrows=1, ncols=2, sharex="row")
        fig.subplots_adjust(bottom=0.15, left=0.17, wspace=0.25)
        ax[0].set_xlim([0.97, 1.1])
        nbins = 10
        for tr, rho, t_sep, col in zip(trList, rhoList, tsepList, colorList):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
            else:
                inDictionary = Target.getprofile(trange=tr)

            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )

            ax[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
        ax[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax[1].set_ylabel(r"T$_e$ [eV]")
        for _ax in ax:
            _ax.set_title(r"#{}".format(shot))
            _ax.set_xlabel(r"$\rho$")
            _ax.autoscale(enable=True, axis="y")

        fig.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 15:
        shot = 37466
        trList = ((2.5, 3.3), (4, 4.8), (5.2, 6))
        rhoList = (0.9, 0.8, None)
        tsepList = (0.002, 0.0006, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04")
        Target = mdslangmuir.Target(shot, server=server)
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        fig, ax = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig.subplots_adjust(bottom=0.12, left=0.15, hspace=0.05, right=0.95, top=0.95)
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        ax[0].set_xlim([0.97, 1.1])
        nbins = 8
        for _idx, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
            else:
                inDictionary = Target.getprofile(trange=tr)

            OutLambda = Target.getLambda(
                inDictionary,
                np.average(tr),
                nbins=nbins,
                inLparallel=inParallel,
                clip=True,
            )
            mask = np.logical_and(OutLambda["rho_Lpar"] > 1, OutLambda["LambdaDiv"] > 0)
            ax2.semilogy(
                OutLambda["rho_Lpar"], OutLambda["LambdaDiv"], "-", color=col, lw=2
            )
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )

            ax[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            _label = r"$\Delta$t = {:.2f} $\pm$ {:.2f} s".format(tr[0], tr[1])
            ax[0].text(
                0.45, 0.9 - 0.1 * _idx, _label, color=col, transform=ax[0].transAxes
            )
            # ax[0].text(0.05, 0.9-_idx*0.1, _label, color=col,transform=ax[0].transAxes )

        ax[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax[1].set_ylabel(r"T$_e$ [eV]")
        ax[0].set_title(r"#{}".format(shot))
        ax[0].tick_params(labelbottom=False)
        ax[1].set_xlabel(r"$\rho$")
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")

        fig.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        ax2.set_xlabel(r"$\rho$")
        ax2.set_xlim([1, 1.06])
        ax2.set_ylabel(r"$\Lambda_{div}$")
        ax2.set_title(r"#{}".format(shot))
        ax2.axhline(1, ls="--", lw=1.5, color="gray")
        fig2.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 16:
        shot = 37468
        trList = ((1.5, 2), (3, 4), (5, 6))
        rhoList = (0.83, 0.9, 0.9)
        tsepList = (0.00015, 0.002, 0.002)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04")
        Target = mdslangmuir.Target(shot, server=server)
        fig, ax = mpl.pylab.subplots(figsize=(16, 5), nrows=1, ncols=2, sharex="row")
        fig.subplots_adjust(bottom=0.15, left=0.17, wspace=0.25)
        ax[0].set_xlim([0.97, 1.1])
        nbins = 10
        for _idx, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
            else:
                inDictionary = Target.getprofile(trange=tr)

            # OutLambda = Target.getLambda(inDictionary, np.average(tr))
            # _aa = np.logical_and(
            #     OutLambda["rho_Lpar"] >= 1.02, OutLambda["rho_Lpar"] <= 1.04
            # )
            # _label = r"$\Lambda_div$ @ $\rho=1.03$ = {:.2f} $\pm$ {:3f} ".format(
            #     np.average(OutLambda["LambdaDiv"][_aa]),
            #     np.std(OutLambda["LambdaDiv"][_aa]),
            # )

            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )

            ax[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            # ax[0].text(0.05, 0.9-_idx*0.1, _label, color=col,transform=ax[0].transAxes )

        ax[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax[1].set_ylabel(r"T$_e$ [eV]")
        for _ax in ax:
            _ax.set_title(r"#{}".format(shot))
            _ax.set_xlabel(r"$\rho$")
            _ax.autoscale(enable=True, axis="y")

        fig.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 17:
        shotList = (37750, 37752)
        trList = ((2.45, 3.85, 4.85, 5.85), (2.25, 4.2, 5.85, 6.65))
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        Conn = mds.Connection(server)
        Diagnostic = OrderedDict(
            [
                (
                    "density",
                    {
                        "experiment": "TOT",
                        "signal": "H-5_corr",
                        "label": r"n$_e^e [10^{19}$m$^{-3}$]",
                        "factor": 1e19,
                    },
                ),
                (
                    "D2",
                    {
                        "experiment": "UVS",
                        "signal": "D_tot",
                        "label": r"D$_2$ [$10^{22}$el/s]",
                        "factor": 1e22,
                    },
                ),
                (
                    "Ipol",
                    {
                        "experiment": "Mac",
                        "signal": "Ipolsola",
                        "label": r"I$_s$ [kA/m$^2$]",
                        "factor": -1e3,
                    },
                ),
                (
                    "pressureDiv",
                    {
                        "experiment": "DDS",
                        "signal": "nDivIst",
                        "label": r"[10$^{21}$m$^{2}$/s]",
                        "factor": 1e21,
                    },
                ),
            ]
        )
        for shot, tr in zip(shotList, trList):
            fig, ax = mpl.pylab.subplots(figsize=(7, 9), nrows=5, ncols=1, sharex="col")
            fig.subplots_adjust(hspace=0.05, wspace=0.05, top=0.92, bottom=0.12)
            for i, k in enumerate(Diagnostic.keys()):
                signal = Diagnostic[k]["signal"]
                diag = Diagnostic[k]["experiment"]
                _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
                _st = (
                    "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
                )
                data = Conn.get(_s).data()
                time = Conn.get(_st).data()
                ax[i].plot(time, data / Diagnostic[k]["factor"], color="k")
                ax[i].text(
                    0.1,
                    0.87,
                    Diagnostic[k]["label"],
                    transform=ax[i].transAxes,
                    fontsize=13,
                )

            # ADD also the midplane pressure
            _s = 'augdiag({}, "DDS", "nMainIst")'.format(shot)
            _st = 'dim_of(augdiag({}, "DDS", "nMainIst"))'.format(shot)
            data = Conn.get(_s).data()
            time = Conn.get(_st).data()
            ax[3].plot(time, data / 1e19, color="orange")
            ax[3].text(
                0.1,
                0.7,
                r"Midplane $\times$ 100",
                color="orange",
                transform=ax[3].transAxes,
                fontsize=13,
            )
            ax[3].text(
                0.1, 0.6, r"Divertor", color="k", transform=ax[3].transAxes, fontsize=13
            )
            LP = mdslangmuir.Target(shot, server=server)
            TotIon, _ = LP.TotalIonFlux()
            ax[4].plot(LP.time, TotIon / 1e23, "k")
            ax[4].text(
                0.1,
                0.82,
                r"$\int$ J$_s [10^{23}$s$^{-1}$]",
                transform=ax[4].transAxes,
                fontsize=13,
            )
            for _ax in ax:
                _ax.autoscale(enable=True, axis="y")
                _ax.set_xlim([0, 6.9])
            ax[0].set_ylim([0, 10])
            ax[2].set_ylim([-5, 13])
            if shot == 37750:
                ax[3].set_ylim([0, 4])
            else:
                ax[3].set_ylim([0, 1.2])
            ax[0].set_title(r"AUG #{}".format(shot))
            ax[-1].set_xlabel(r"t[s]")
            for _ax in ax:
                for t, c in zip(tr, colorList):
                    _ax.axvline(t, linewidth=2, color=c)
            fig.savefig(
                "../pdfbox/GeneralPlotShot{}.pdf".format(shot), bbox_to_inches="tight"
            )
    elif selection == 18:
        shot = 37750
        trList = ((2.3, 2.6), (3.7, 4), (4.7, 5), (5.7, 6))
        rhoList = (0.83, 0.85, 0.9, None)
        tsepList = (0.00015, 0.002, 0.002, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        Target = mdslangmuir.Target(shot, server=server)
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        fig, ax = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig.subplots_adjust(bottom=0.12, left=0.15, hspace=0.05, right=0.95, top=0.95)
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        ax[0].set_xlim([0.97, 1.1])
        nbins = 8
        for _idx, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
            else:
                inDictionary = Target.getprofile(trange=tr)

            OutLambda = Target.getLambda(
                inDictionary,
                np.average(tr),
                nbins=nbins,
                inLparallel=inParallel,
                clip=True,
            )
            mask = np.logical_and(OutLambda["rho_Lpar"] > 1, OutLambda["LambdaDiv"] > 0)
            ax2.semilogy(
                OutLambda["rho_Lpar"], OutLambda["LambdaDiv"], "-", color=col, lw=2
            )
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )

            ax[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            _label = r"$\Delta$t = {:.2f} $\pm$ {:.2f} s".format(tr[0], tr[1])
            ax[0].text(
                0.45, 0.9 - 0.1 * _idx, _label, color=col, transform=ax[0].transAxes
            )
            # ax[0].text(0.05, 0.9-_idx*0.1, _label, color=col,transform=ax[0].transAxes )

        ax[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax[1].set_ylabel(r"T$_e$ [eV]")
        ax[0].set_title(r"#{}".format(shot))
        ax[0].tick_params(labelbottom=False)
        ax[1].set_xlabel(r"$\rho$")
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")

        fig.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        ax2.set_xlabel(r"$\rho$")
        ax2.set_xlim([1, 1.06])
        ax2.set_ylabel(r"$\Lambda_{div}$")
        ax2.set_title(r"#{}".format(shot))
        ax2.axhline(1, ls="--", lw=1.5, color="gray")
        fig2.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 19:
        shot = 37752
        trList = ((2.1, 2.4), (4, 4.4), (5.7, 6), (6.5, 6.8))
        rhoList = (0.9, 0.9, None, None)
        tsepList = (0.00015, 0.002, None, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        Target = mdslangmuir.Target(shot, server=server)
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        fig, ax = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig.subplots_adjust(bottom=0.12, left=0.15, hspace=0.05, top=0.95)
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        ax[0].set_xlim([0.97, 1.1])
        nbins = 8
        for _idx, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
            else:
                inDictionary = Target.getprofile(trange=tr)

            OutLambda = Target.getLambda(
                inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
            )
            mask = np.logical_and(OutLambda["rho_Lpar"] > 1, OutLambda["LambdaDiv"] > 0)
            ax2.semilogy(
                OutLambda["rho_Lpar"], OutLambda["LambdaDiv"], "-", color=col, lw=2
            )
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )

            ax[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            _label = r"$\Delta$t = {:.2f} $\pm$ {:.2f} s".format(tr[0], tr[1])
            ax[0].text(
                0.45, 0.9 - 0.1 * _idx, _label, color=col, transform=ax[0].transAxes
            )
            # ax[0].text(0.05, 0.9-_idx*0.1, _label, color=col,transform=ax[0].transAxes )

        ax[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax[1].set_ylabel(r"T$_e$ [eV]")
        ax[0].tick_params(labelbottom=False)
        ax[1].set_xlabel(r"$\rho$")
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")
        ax[0].set_ylim([-1, 15])
        fig.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        ax2.set_xlabel(r"$\rho$")
        ax2.set_ylabel(r"$\Lambda_{div}$")
        ax2.set_title(r"#{}".format(shot))
        ax2.set_xlim([1, 1.06])
        ax2.axhline(1, ls="--", lw=1.5, color="gray")
        fig2.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 20:
        shot = 37752
        myObj = myThbObject.myObject(
            "../data/THB/filament_data/{}_t_2.500_5.200_PMT1".format(shot)
        )

        time = np.asarray(myObj.peak_start_time[myObj.reference_channel])
        vr = np.asarray(myObj.velocity_all) / 1e3
        # drop the nan
        _finite = np.isfinite(vr)
        time, vr = time[_finite], vr[_finite]
        # trim them using the saved ELM detected
        mask = mdselmremoval.mdsremoveELMData(
            shot, time, preft=0.002, suft=0.002, elm_exper="mgrien", server=server
        )
        time, vr = time[mask], vr[mask]
        # trList = ((2.6, 3.), (3.3, 3.7), (4.1, 4.5), (4.8, 5.2))
        # colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        trList = ((2.6, 3.0), (4.8, 5.2))
        colorList = ("#1C6C8C", "#D92B04")
        # load the parallel Connection length
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        LP = mdslangmuir.Target(shot)
        rhoList = (0.9, None)
        tSepList = (0.00015, None)
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig2, ax2 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.14, left=0.15)
        fig2.subplots_adjust(bottom=0.14, left=0.15)
        for tr, _c, rho, t_sep in zip(trList, colorList, rhoList, tSepList):
            _idx = np.logical_and(time >= tr[0], time <= tr[1])
            _dummy = vr[_idx]
            _dummyTime = time[_idx]
            _y, _, _ = sigmaclip(_dummy, 3.5, 3.5)
            if rho:
                inDictionary = LP.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
            else:
                inDictionary = LP.getprofile(trange=tr)
            outLambda = LP.getLambda(
                inDictionary, np.average(tr), nbins=8, inLparallel=inParallel, clip=True
            )
            # drop the zero values of LambdaDiv
            mask = np.logical_and(
                outLambda["rho_Lpar"] >= 1.03, outLambda["LambdaDiv"] != 0
            )
            _st = r"$\langle \Lambda_{div}\rangle_{\rho \leq 1.03}$ = "
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            sns.distplot(
                _y,
                bins="auto",
                color=_c,
                ax=ax,
                label=_label,
                norm_hist=False,
                kde=False,
            )
            _y, _, _ = sigmaclip(_dummy[_dummy >= 0.5], 4, 4)
            sns.distplot(
                _y,
                bins="auto",
                color=_c,
                ax=ax2,
                label=_label,
                norm_hist=False,
                kde=False,
            )

        ax.legend(loc="best", frameon=False)
        ax.set_title(
            r"#{} $\rho =$ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax.set_xlabel(r"v$_r$ [km/s]")
        ax.set_ylabel("#")
        fig.savefig(
            "../pdfbox/FilamentVelocityComparison_{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
        ax2.legend(loc="best", frameon=False)
        ax2.set_title(
            r"#{} $\rho =$ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax2.set_xlabel(r"v$_r$ [km/s]")
        ax2.set_ylabel("#")
        fig2.savefig(
            "../pdfbox/FilamentVelocityComparisonZoom_{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
        # now plot the frequency of the filaments in the ref-channel asa function of Lambda_div or
        # Pressure Divertor or Pressure Midplane
        neReference = LP.Ne[list(LP.signal.keys()).index("ua8"), :]
        teReference = LP.Te[list(LP.signal.keys()).index("ua8"), :]
        # check the LP around rho = 1.03
        _idx = np.argmin(np.abs(Lpar[:, 0] - 1.03))
        LpReference = Lpar[_idx, 2]
        _ne = np.zeros(myObj.int_time.size)
        _te = np.zeros(myObj.int_time.size)
        for _idx, t in enumerate(myObj.int_time):
            mask = np.logical_and(LP.time >= t - 0.05, LP.time <= t + 0.05)
            _ne[_idx] = np.nanmedian(neReference[mask])
            _te[_idx] = np.nanmedian(teReference[mask])
        _collisionFreq = collisions.fundamental_electron_collision_freq(
            _te * units.eV, _ne * units.m ** -3, ion_particle=LP._ionicsymbol,
        )
        # then we need the computation of the ion sound speed
        _Cs = parameters.ion_sound_speed(
            T_e=_te * units.eV, T_i=_te * units.eV, ion=LP._ionicsymbol,
        )
        LambdaTime = (
            (LpReference * _collisionFreq.value / _Cs.value)
            * LP.Particle.integer_charge
            * constants.m_e
            / LP.Particle.mass.value
        )
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.2, left=0.15)
        mask = np.logical_and(
            LambdaTime >= 0.1, myObj.blob_freq_trace[myObj.reference_channel, :] != 0
        )
        ax.semilogx(
            LambdaTime[mask],
            myObj.blob_freq_trace[myObj.reference_channel, mask] / 1e3,
            "-",
            lw=2,
            color="k",
        )
        ax.set_xlabel(r"$\Lambda_{div,\rho\approx 1.03}$")
        ax.set_ylabel(r"f$_b$ [kHz]")
        ax.set_title(
            r"#{} $\rho \simeq$ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax.axvline(1, ls="--", lw=2, color="grey")
        fig.savefig(
            "../pdfbox/FilamentFrequencyLambdaDivShot{}.pdf".format(shot),
            bbox_to_inches="tight",
        )

        # perform the same analysis with the pmid
        Conn = mds.Connection(server)
        _s = 'augdiag({}, "DDS", "nMainIst")'.format(shot)
        _st = 'dim_of(augdiag({}, "DDS", "nMainIst"))'.format(shot)
        data = Conn.get(_s).data()
        time = Conn.get(_st).data()
        pTime = np.zeros(myObj.int_time.size)
        for _idx, t in enumerate(myObj.int_time):
            mask = np.logical_and(time >= t - 0.05, time <= t + 0.05)
            pTime[_idx] = np.nanmean(data[mask])
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.2, left=0.15)
        mask = np.where(myObj.blob_freq_trace[myObj.reference_channel, :] != 0)[0]
        ax.semilogx(
            pTime[mask] / 1e19,
            myObj.blob_freq_trace[myObj.reference_channel, mask] / 1e3,
            "-",
            lw=2,
            color="k",
        )
        ax.set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")
        ax.set_ylabel(r"f$_b$ [kHz]")
        ax.set_title(
            r"#{} $\rho \simeq$ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        fig.savefig(
            "../pdfbox/FilamentFrequencyPMainShot{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
    elif selection == 21:
        shot = 37750
        myObj = myThbObject.myObject(
            "../data/THB/filament_data/{}_t_1.500_6.300_PMT1".format(shot)
        )

        time = np.asarray(myObj.peak_start_time[myObj.reference_channel])
        vr = np.asarray(myObj.velocity_all) / 1e3
        # drop the nan
        _finite = np.isfinite(vr)
        time, vr = time[_finite], vr[_finite]
        # trim them using the saved ELM detected
        mask = mdselmremoval.mdsremoveELMData(
            shot, time, preft=0.002, suft=0.002, elm_exper="AUGD", server=server
        )
        time, vr = time[mask], vr[mask]
        trList = ((2.3, 2.6), (3.7, 4), (4.7, 5), (5.7, 6))
        rhoList = (0.9, 0.9, None, None)
        tsepList = (0.00015, 0.002, None, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        trList = ((2.5, 2.8), (3.7, 4), (5.7, 6))
        rhoList = (0.9, 0.9, None)
        tsepList = (0.00015, 0.002, None)
        colorList = ("#1C6C8C", "#F2B90F", "#276323")
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        LP = mdslangmuir.Target(shot, server=server)
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.14, left=0.15)
        for _count, (tr, _c, rho, t_sep) in enumerate(
            zip(trList, colorList, rhoList, tsepList)
        ):
            _idx = np.logical_and(time >= tr[0], time <= tr[1])
            _dummy = vr[_idx]
            _dummyTime = time[_idx]
            _y, _, _ = sigmaclip(_dummy, 4.0, 4.0)
            if rho:
                inDictionary = LP.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
            else:
                inDictionary = LP.getprofile(trange=tr)
            outLambda = LP.getLambda(
                inDictionary, np.average(tr), nbins=8, inLparallel=inParallel, clip=True
            )
            # drop the zero values of LambdaDiv
            mask = np.logical_and(
                outLambda["rho_Lpar"] >= 1.03, outLambda["LambdaDiv"] != 0
            )
            _st = r"$\langle \Lambda_{div}\rangle_{\rho \leq 1.03}$ = "
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            sns.distplot(
                _y,
                bins="auto",
                color=_c,
                ax=ax,
                norm_hist=False,
                kde=False,
                hist_kws={"alpha": 0.4},
            )
            ax.text(
                0.2,
                0.9 - _count * 0.1,
                _label,
                color=_c,
                transform=ax.transAxes,
                fontsize=20,
            )

        ax.legend(loc="best", frameon=False)
        ax.set_title(
            r"#{} $\rho \simeq $ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax.set_xlabel(r"v$_r$ [km/s]")
        ax.set_ylabel("#")
        fig.savefig(
            "../pdfbox/FilamentVelocityComparison_{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
        # now plot the frequency of the filaments in the ref-channel asa function of Lambda_div or
        # Pressure Divertor or Pressure Midplane
        neReference = LP.Ne[list(LP.signal.keys()).index("ua8"), :]
        teReference = LP.Te[list(LP.signal.keys()).index("ua8"), :]
        # check the LP around rho = 1.03
        _idx = np.argmin(np.abs(Lpar[:, 0] - 1.03))
        LpReference = Lpar[_idx, 2]
        _ne = np.zeros(myObj.int_time.size)
        _te = np.zeros(myObj.int_time.size)
        for _idx, t in enumerate(myObj.int_time):
            mask = np.logical_and(LP.time >= t - 0.05, LP.time <= t + 0.05)
            _ne[_idx] = np.nanmedian(neReference[mask])
            _te[_idx] = np.nanmedian(teReference[mask])
        _collisionFreq = collisions.fundamental_electron_collision_freq(
            _te * units.eV, _ne * units.m ** -3, ion_particle=LP._ionicsymbol,
        )
        # then we need the computation of the ion sound speed
        _Cs = parameters.ion_sound_speed(
            T_e=_te * units.eV, T_i=_te * units.eV, ion=LP._ionicsymbol,
        )
        LambdaTime = (
            (LpReference * _collisionFreq.value / _Cs.value)
            * LP.Particle.integer_charge
            * constants.m_e
            / LP.Particle.mass.value
        )
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.2, left=0.15)
        # combine LambdaDiv and Pmain
        fig2, ax2 = mpl.pylab.subplots(figsize=(7, 10), nrows=2, ncols=1)
        fig2.subplots_adjust(bottom=0.15, left=0.15, hspace=0.28, top=0.93)

        mask = np.logical_and(
            LambdaTime >= 0.1, myObj.blob_freq_trace[myObj.reference_channel, :] != 0
        )
        ax.semilogx(
            LambdaTime[mask],
            myObj.blob_freq_trace[myObj.reference_channel, mask] / 1e3,
            "-",
            lw=2,
            color="k",
        )
        ax.set_xlabel(r"$\Lambda_{div,\rho\approx 1.03}$")
        ax.set_ylabel(r"f$_b$ [kHz]")
        ax.set_title(
            r"#{} $\rho \simeq$ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax.axvline(1, ls="--", lw=2, color="grey")

        fig.savefig(
            "../pdfbox/FilamentFrequencyLambdaDivShot{}.pdf".format(shot),
            bbox_to_inches="tight",
        )


        ax2[0].semilogx(
            LambdaTime[mask],
            myObj.blob_freq_trace[myObj.reference_channel, mask] / 1e3,
            "-",
            lw=2,
            color="k",
        )
        ax2[0].set_xlabel(r"$\Lambda_{div,\rho\approx 1.03}$")
        ax2[0].set_ylabel(r"f$_b$ [kHz]")
        ax2[0].set_title(
            r"#{} $\rho \simeq$ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax2[0].axvline(1, ls="--", lw=2, color="grey")

        # perform the same analysis with the pmid
        Conn = mds.Connection(server)
        _s = 'augdiag({}, "DDS", "nMainIst")'.format(shot)
        _st = 'dim_of(augdiag({}, "DDS", "nMainIst"))'.format(shot)
        data = Conn.get(_s).data()
        time = Conn.get(_st).data()
        pTime = np.zeros(myObj.int_time.size)
        for _idx, t in enumerate(myObj.int_time):
            mask = np.logical_and(time >= t - 0.05, time <= t + 0.05)
            pTime[_idx] = np.nanmean(data[mask])
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.2, left=0.15)
        mask = np.where(myObj.blob_freq_trace[myObj.reference_channel, :] != 0)[0]
        ax.semilogx(
            pTime[mask] / 1e19,
            myObj.blob_freq_trace[myObj.reference_channel, mask] / 1e3,
            "-",
            lw=2,
            color="k",
        )
        ax.set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")
        ax.set_ylabel(r"f$_b$ [kHz]")
        ax.set_title(
            r"#{} $\rho \simeq$ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        # --- combined plot
        ax2[1].semilogx(
            pTime[mask] / 1e19,
            myObj.blob_freq_trace[myObj.reference_channel, mask] / 1e3,
            "-",
            lw=2,
            color="k",
        )
        ax2[1].set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")
        ax2[1].set_ylabel(r"f$_b$ [kHz]")
        fig2.savefig('../pdfbox/FilamentFrequencyLambdaPMainShot{}.pdf'.format(shot), bbox_to_inches='tight')

        fig.savefig(
            "../pdfbox/FilamentFrequencyPMainShot{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
    elif selection == 22:
        shot = 37468
        trangeList = ((1.5, 2.3), (4.3, 5), (6.0, 6.8))
        rhoList = (0.83, 0.9, 0.9)
        tSepList = (0.00015, 0.002, 0.002)
        colorList = ("#1C6C8C", "#F2B90F", "#276323")
        LB = libes.Libes(shot, remote=True, experiment="AUGD", server=server)
        LP = mdslangmuir.Target(shot, server=server)
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.1])
        ax.set_ylim([5e-2, 5])
        for tr, rho, t_sep, col in zip(trangeList, rhoList, tSepList, colorList):
            Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            # compute the target profiles to get the Lambda-parameter
            inDictionary = LP.getprofile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            outLambda = LP.getLambda(
                inDictionary, np.average(tr), nbins=8, inLparallel=inParallel, clip=True
            )
            # drop the zero values of LambdaDiv
            mask = np.logical_and(
                outLambda["rho_Lpar"] >= 1.025, outLambda["LambdaDiv"] != 0
            )
            _st = r"$\langle \Lambda_{div}\rangle_{\rho \leq 1.025}$ = "
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                warnings.warn("Computed the GP fit")
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                warnings.warn("Not Computed the GP fit")

            ax.plot(Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2)
            ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
            ax.fill_between(
                xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
            )
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 23:
        shot = 37753
        trList = ((1.5, 2.3), (4.3, 5), (6.0, 6.8))
        rhoList = (0.83, 0.9, 0.9)
        tSepList = (0.00015, 0.002, 0.002)
        colorList = ("#1C6C8C", "#F2B90F", "#276323")
        Target = mdslangmuir.Target(shot, server=server)
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        fig, ax = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig.subplots_adjust(bottom=0.12, left=0.15, hspace=0.05, right=0.95, top=0.95)
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        ax[0].set_xlim([0.97, 1.1])
        nbins = 8
        for _idx, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tSepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
            else:
                inDictionary = Target.getprofile(trange=tr)

            OutLambda = Target.getLambda(
                inDictionary,
                np.average(tr),
                nbins=nbins,
                inLparallel=inParallel,
                clip=True,
            )
            mask = np.logical_and(OutLambda["rho_Lpar"] > 1, OutLambda["LambdaDiv"] > 0)
            ax2.semilogy(
                OutLambda["rho_Lpar"], OutLambda["LambdaDiv"], "-", color=col, lw=2
            )
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )

            ax[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            mask = np.logical_and(
                OutLambda["rho_Lpar"] >= 1.025, OutLambda["LambdaDiv"] != 0
            )
            _st = r"$\langle \Lambda_{div}\rangle_{\rho \leq 1.025}$ = "
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(OutLambda["LambdaDiv"][mask]),
                np.nanstd(OutLambda["LambdaDiv"][mask]),
            )
            ax[0].text(
                0.4,
                0.9 - 0.1 * _idx,
                _label,
                color=col,
                transform=ax[0].transAxes,
                fontsize=18,
            )
            # ax[0].text(0.05, 0.9-_idx*0.1, _label, color=col,transform=ax[0].transAxes )

        ax[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax[1].set_ylabel(r"T$_e$ [eV]")
        ax[0].set_title(r"#{}".format(shot))
        ax[0].tick_params(labelbottom=False)
        ax[1].set_xlabel(r"$\rho$")
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")

        fig.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        ax2.set_xlabel(r"$\rho$")
        ax2.set_xlim([1, 1.06])
        ax2.set_ylabel(r"$\Lambda_{div}$")
        ax2.set_title(r"#{}".format(shot))
        ax2.axhline(1, ls="--", lw=1.5, color="gray")
        fig2.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 24:
        shot = 37468
        trList = ((1.5, 2.3), (4.3, 5), (6.0, 6.8))
        rhoList = (0.83, 0.9, 0.9)
        tSepList = (0.00015, 0.002, 0.002)
        colorList = ("#1C6C8C", "#F2B90F", "#276323")
        Target = mdslangmuir.Target(shot, server=server)
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        fig, ax = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig.subplots_adjust(bottom=0.12, left=0.15, hspace=0.05, right=0.95, top=0.95)
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        ax[0].set_xlim([0.97, 1.1])
        nbins = 8
        for _idx, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tSepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
            else:
                inDictionary = Target.getprofile(trange=tr)

            OutLambda = Target.getLambda(
                inDictionary,
                np.average(tr),
                nbins=nbins,
                inLparallel=inParallel,
                clip=True,
            )
            mask = np.logical_and(OutLambda["rho_Lpar"] > 1, OutLambda["LambdaDiv"] > 0)
            ax2.semilogy(
                OutLambda["rho_Lpar"], OutLambda["LambdaDiv"], "-", color=col, lw=2
            )
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )

            ax[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            mask = np.logical_and(
                OutLambda["rho_Lpar"] >= 1.025, OutLambda["LambdaDiv"] != 0
            )
            _st = r"$\langle \Lambda_{div}\rangle_{\rho \leq 1.025}$ = "
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(OutLambda["LambdaDiv"][mask]),
                np.nanstd(OutLambda["LambdaDiv"][mask]),
            )
            ax[0].text(
                0.4,
                0.9 - 0.1 * _idx,
                _label,
                color=col,
                transform=ax[0].transAxes,
                fontsize=18,
            )
            # ax[0].text(0.05, 0.9-_idx*0.1, _label, color=col,transform=ax[0].transAxes )

        ax[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax[1].set_ylabel(r"T$_e$ [eV]")
        ax[0].set_title(r"#{}".format(shot))
        ax[0].tick_params(labelbottom=False)
        ax[1].set_xlabel(r"$\rho$")
        for _ax in ax:
            _ax.autoscale(enable=True, axis="y")

        fig.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        ax2.set_xlabel(r"$\rho$")
        ax2.set_xlim([1, 1.06])
        ax2.set_ylabel(r"$\Lambda_{div}$")
        ax2.set_title(r"#{}".format(shot))
        ax2.axhline(1, ls="--", lw=1.5, color="gray")
        fig2.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
    elif selection == 25:
        Dictionary = {
            "37466": {
                "shot": 37466,
                "trlist": ((2.1, 2.4), (4, 4.4), (5.7, 6), (6.5, 6.8)),
                "rhoList": (0.9, 0.9, None, None),
                "tsepList": (0.00015, 0.002, None, None),
                "libe": "AUGE",
                "ELMdd": "nvianell",
                "file": "../data/THB/filament_data/37466_t_1.800_7.000_PMT1",
            },
            "37468": {
                "shot": 37468,
                "trlist": ((1.5, 2.3), (4.3, 5), (6.0, 6.8)),
                "rhoList": (0.83, 0.9, 0.9),
                "tsepList": (0.00015, 0.002, 0.002),
                "libe": "AUGD",
                "ELMdd": "augd",
                "file": "../data/THB/filament_data/37468_t_1.500_7.000_PMT1",
            },
            "37750": {
                "shot": 37750,
                "trlist": ((2.3, 2.6), (3.7, 4), (4.7, 5), (5.7, 6)),
                "rhoList": (0.83, 0.85, 0.9, None),
                "tsepList": (0.00015, 0.002, 0.002, None),
                "libe": "AUGD",
                "ELMdd": "augd",
                "file": "../data/THB/filament_data/37750_t_1.500_6.300_PMT1",
            },
            "37752": {
                "shot": 37752,
                "trlist": ((2.1, 2.4), (4, 4.4), (5.7, 6), (6.5, 6.8)),
                "rhoList": (0.9, 0.9, None, None),
                "tsepList": (0.00015, 0.002, None, None),
                "libe": "AUGD",
                "ELMdd": "mgrien",
                "file": "../data/THB/filament_data/37752_t_1.000_7.000_PMT1",
            },
            "37753": {
                "shot": 37753,
                "trlist": ((1.5, 2.3), (4.3, 5), (6.0, 6.8)),
                "rhoList": (0.83, 0.9, 0.9),
                "tsepList": (0.00015, 0.002, 0.002),
                "libe": "AUGD",
                "ELMdd": "nvianell",
                "file": "../data/THB/filament_data/37753_t_1.500_7.000_PMT1",
            },
        }
        #
        colorList = ("#023E73", "#38A3A5", "#624CAB", "#FFC857", "#8C8C8C")
        # open the two figures \
        # figure of vr as a function of LambdaDiv and Pressure
        fig, ax = mpl.pylab.subplots(figsize=(7, 12), nrows=2, ncols=1, sharey="col")
        fig.subplots_adjust(hspace=0.25, left=0.18, right=0.97, bottom=0.16)
        # figure of fb as a function of LambdaDiv and Pressure
        fig2, ax2 = mpl.pylab.subplots(figsize=(7, 12), nrows=2, ncols=1, sharey="col")
        fig2.subplots_adjust(hspace=0.25, left=0.18, right=0.97, bottom=0.16)
        # lambdaDiv average calculation
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        # read the pressure
        for _idxshot, (key, col) in enumerate(zip(Dictionary.keys(), colorList)):
            shot = Dictionary[key]["shot"]
            Target = mdslangmuir.Target(shot, server=server)
            trList = Dictionary[key]["trlist"]
            rhoList = Dictionary[key]["rhoList"]
            tsepList = Dictionary[key]["tsepList"]
            # parallel connection length
            Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
            inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
            # filament properties
            myObj = myThbObject.myObject(Dictionary[key]["file"])
            time = np.asarray(
                myObj.peak_start_time[myObj.reference_channel]
                + myObj.peak_stop_time[myObj.reference_channel]
            )
            vr = np.asarray(myObj.velocity_all) / 1e3
            time = time[np.isfinite(time)]
            # drop the nan
            _finite = np.isfinite(vr)
            time, vr = time[_finite], vr[_finite]
            # trim them using the saved ELM detected
            if server == "locahost:8000":
                mask = mdselmremoval.mdsremoveELMData(
                    shot,
                    time,
                    preft=0.002,
                    suft=0.002,
                    elm_exper=Dictionary[key]["ELMdd"],
                    server=server,
                )
            elif server == "mdsplus.aug.ipp.mpg.de":
                mask = ddremoveELMData.ddremoveELMData(
                    shot,
                    time,
                    preft=0.002,
                    suft=0.002,
                    elm_exper=Dictionary[key]["ELMdd"],
                )
            else:
                mask = mdselmremoval.mdsremoveELMData(
                    shot,
                    time,
                    preft=0.002,
                    suft=0.002,
                    elm_exper=Dictionary[key]["ELMdd"],
                    server=server,
                )
            time, vr = time[mask], vr[mask]
            timeFreq, blobFreq = (
                myObj.int_time,
                myObj.blob_freq_trace[myObj.reference_channel, :] / 1e3,
            )
            # read the pressure
            Conn = mds.Connection(server)
            diag, signal = "DDS", "nMainIst"
            _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
            _st = "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
            pMain = Conn.get(_s).data() / 1e19
            tpMain = Conn.get(_st).data()
            nbins = 8
            for tr, rho, t_sep in zip(trList, rhoList, tsepList):
                if rho:
                    inDictionary = Target.getprofile(
                        trange=tr, interelm=True, rho=rho, t_sep=t_sep
                    )
                else:
                    inDictionary = Target.getprofile(trange=tr)
                # ------------------
                # Lambda Evaluation
                outLambda = Target.getLambda(
                    inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
                )
                maskA = np.logical_and(
                    outLambda["rho_Lpar"] >= _lambdaDivMin,
                    outLambda["rho_Lpar"] <= _lambdaDivMax,
                )
                maskB = outLambda["LambdaDiv"] != 0
                mask = np.logical_and(maskA, maskB)
                lambdaPlot = np.nanmean(outLambda["LambdaDiv"][mask])
                lambdaWidth = np.nanstd(outLambda["LambdaDiv"][mask])
                # pressure point
                _idxtime = np.logical_and(tpMain >= tr[0], tpMain <= tr[1])
                pMainPlot = np.nanmean(pMain[_idxtime])
                pMainWidth = np.nanstd(pMain[_idxtime])
                #
                _idxtime = np.logical_and(time >= tr[0], time <= tr[1])
                _dummy = vr[_idxtime]
                _dummyTime = time[_idxtime]
                _y, _, _ = sigmaclip(_dummy, 3.5, 3.5)
                # now the boxplot for the velocity
                _box = ax[0].boxplot(
                    _y,
                    notch=True,
                    showfliers=False,
                    patch_artist=True,
                    showcaps=False,
                    whis=[5, 85],
                    positions=[lambdaPlot],
                    widths=[lambdaWidth],
                    medianprops={"color": "black"},
                    boxprops={"edgecolor": col, "facecolor": col, "color": col},
                    whiskerprops={"color": col},
                    manage_xticks=False,
                )

                _box = ax[1].boxplot(
                    _y,
                    notch=True,
                    showfliers=False,
                    patch_artist=True,
                    showcaps=False,
                    whis=[5, 85],
                    positions=[pMainPlot],
                    widths=[pMainWidth],
                    medianprops={"color": "black"},
                    boxprops={"edgecolor": col, "facecolor": col, "color": col},
                    whiskerprops={"color": col},
                    manage_xticks=False,
                )
                _idxtime = np.logical_and(timeFreq >= tr[0], timeFreq <= tr[1])
                y, e = np.nanmean(blobFreq[_idxtime]), np.nanstd(blobFreq[_idxtime])
                ax2[0].errorbar(
                    lambdaPlot, y, xerr=lambdaWidth, yerr=e, fmt="o", ms=13, color=col
                )
                ax2[1].errorbar(
                    pMainPlot, y, xerr=pMainWidth, yerr=e, fmt="o", ms=13, color=col
                )
            ax[0].text(
                0.1,
                0.9 - 0.1 * _idxshot,
                r"#".format(shot),
                transform=ax[0].transAxes,
                fontsize=13,
                color=col,
            )
            ax2[0].text(
                0.1,
                0.9 - 0.1 * _idxshot,
                r"#".format(shot),
                transform=ax2[0].transAxes,
                fontsize=13,
                color=col,
            )
        for _ax in ax:
            _ax.set_xscale("log")
            _ax.set_ylabel(r"v$_r$ [km/s]")
        ax[0].set_xlabel(r"$\Lambda_{div}$")
        ax[1].set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")

        for _ax in ax2:
            _ax.set_xscale("log")
            _ax.set_ylabel(r"f$_b$ [kHz]")
        ax2[0].set_xlabel(r"$\Lambda_{div}$")
        ax2[1].set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")
        fig.savefig("../pdfbox/VrBoxPlot.pdf", bbox_to_inchest="tight")
        fig2.savefig(
            "../pdfbox/FrequencyPlotVsLambdaPressure.pdf", bbox_to_inchest="tight"
        )
    elif selection == 26:
        shot = 37466
        trList = ((2.1, 2.4), (4, 4.4), (5.7, 6), (6.5, 6.8))
        rhoList = (0.9, 0.9, None, None)
        tsepList = (0.00015, 0.002, None, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        # Li-beam profiles
        LB = libes.Libes(shot, remote=True, experiment="AUGE", server=server)
        # target profiles
        Target = mdslangmuir.Target(shot, server=server)
        # parallel connection length
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        # filament properties
        myObj = myThbObject.myObject(
            "../data/THB/filament_data/{}_t_1.800_7.000_PMT1".format(shot)
        )
        time = np.asarray(myObj.peak_start_time[myObj.reference_channel])
        vr = np.asarray(myObj.velocity_all) / 1e3
        time = time[np.isfinite(time)]
        # drop the nan
        _finite = np.isfinite(vr)
        time, vr = time[_finite], vr[_finite]
        # trim them using the saved ELM detected
        if server == "locahost:8000":
            mask = mdselmremoval.mdsremoveELMData(
                shot, time, preft=0.002, suft=0.002, elm_exper="nvianell", server=server
            )
        elif server == "mdsplus.aug.ipp.mpg.de":
            mask = ddremoveELMData.ddremoveELMData(
                shot, time, preft=0.002, suft=0.002, elm_exper="nvianell"
            )
        else:
            mask = mdselmremoval.mdsremoveELMData(
                shot, time, preft=0.002, suft=0.002, elm_exper="nvianell", server=server
            )

        time, vr = time[mask], vr[mask]
        # ----- figure settings
        # upstream profiles
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.075])
        ax.set_ylim([0.1, 2])
        # target profiles
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.12, left=0.2, hspace=0.05, top=0.95)
        ax2[0].set_xlim([0.97, 1.1])
        nbins = 8
        # lambda profiles
        fig3, ax3 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1)
        fig3.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        # filament velocities
        fig4, ax4 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig4.subplots_adjust(bottom=0.14, left=0.15)
        for _idplot, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
                Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            else:
                inDictionary = Target.getprofile(trange=tr)
            Out = LB.averageProfile(trange=tr)
            # ------------------
            # Lambda Evaluation
            outLambda = Target.getLambda(
                inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
            )
            mask = np.logical_and(outLambda["rho_Lpar"] > 1, outLambda["LambdaDiv"] > 0)
            ax3.semilogy(
                outLambda["rho_Lpar"], outLambda["LambdaDiv"], "-", color=col, lw=2
            )
            # ------------------
            # Target Evaluation
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )
            ax2[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax2[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            maskA = np.logical_and(
                outLambda["rho_Lpar"] >= _lambdaDivMin,
                outLambda["rho_Lpar"] <= _lambdaDivMax,
            )
            maskB = outLambda["LambdaDiv"] != 0
            mask = np.logical_and(maskA, maskB)
            _st = r"$\langle \Lambda_{div}\rangle_{%3.2f \leq \rho \leq %3.2f}$ = " % (
                _lambdaDivMin,
                _lambdaDivMax,
            )
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            ax2[0].text(
                0.1,
                0.9 - 0.1 * _idplot,
                _label,
                color=col,
                transform=ax2[0].transAxes,
                fontsize=16,
            )
            # -----------------
            # upstream profiles
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit Performed")
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit NOT Performed")
            if (_idplot == 0) or (_idplot == 3):
                _idxtime = np.logical_and(time >= tr[0], time <= tr[1])
                _dummy = vr[_idxtime]
                _dummyTime = time[_idxtime]
                _y, _, _ = sigmaclip(_dummy, 4.0, 4.0)
                sns.distplot(
                    _y,
                    bins="auto",
                    color=col,
                    ax=ax4,
                    norm_hist=False,
                    kde=False,
                    hist_kws={"alpha": 0.4},
                )
                ax4.text(
                    0.2,
                    0.9 - _idplot * 0.05,
                    _label,
                    color=col,
                    transform=ax4.transAxes,
                    fontsize=18,
                )
        # ----- Upstream
        # now the plot definitions
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Target
        ax2[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax2[1].set_ylabel(r"T$_e$ [eV]")
        ax2[0].set_title(r"#{}".format(shot))
        ax2[0].tick_params(labelbottom=False)
        ax2[1].set_xlabel(r"$\rho$")
        for _ax in ax2:
            _ax.autoscale(enable=True, axis="y")

        fig2.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Lambda
        ax3.set_xlabel(r"$\rho$")
        ax3.set_xlim([1, 1.06])
        ax3.set_ylabel(r"$\Lambda_{div}$")
        ax3.set_title(r"#{}".format(shot))
        ax3.axhline(1, ls="--", lw=1.5, color="gray")
        fig3.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Filaments
        ax4.legend(loc="best", frameon=False)
        ax4.set_title(
            r"#{} $\rho \simeq $ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax4.set_xlabel(r"v$_r$ [km/s]")
        ax4.set_ylabel("#")
        fig4.savefig(
            "../pdfbox/FilamentVelocityComparison_{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
    elif selection == 27:
        shot = 37468
        trList = ((1.5, 2.3), (4.3, 5), (6.0, 6.8))
        rhoList = (0.83, 0.9, 0.9)
        tsepList = (0.00015, 0.002, 0.002)
        colorList = ("#1C6C8C", "#F2B90F", "#276323")
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        # Li-beam profiles
        LB = libes.Libes(shot, remote=True, experiment="AUGD", server=server)
        # target profiles
        Target = mdslangmuir.Target(shot, server=server)
        # parallel connection length
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        # filament properties
        myObj = myThbObject.myObject(
            "../data/THB/filament_data/{}_t_1.500_7.000_PMT1".format(shot)
        )
        time = np.asarray(myObj.peak_start_time[myObj.reference_channel])
        vr = np.asarray(myObj.velocity_all) / 1e3
        time = time[np.isfinite(time)]
        # drop the nan
        _finite = np.isfinite(vr)
        time, vr = time[_finite], vr[_finite]
        # trim them using the saved ELM detected
        mask = mdselmremoval.mdsremoveELMData(
            shot, time, preft=0.002, suft=0.002, elm_exper="augd"
        )
        time, vr = time[mask], vr[mask]
        # ----- figure settings
        # upstream profiles
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.075])
        ax.set_ylim([0.1, 2])
        # target profiles
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.12, left=0.15, hspace=0.05, top=0.95)
        ax2[0].set_xlim([0.97, 1.1])
        nbins = 8
        # lambda profiles
        fig3, ax3 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1)
        fig3.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        # filament velocities
        fig4, ax4 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig4.subplots_adjust(bottom=0.14, left=0.15)
        for _idplot, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
                Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            else:
                inDictionary = Target.getprofile(trange=tr)
            Out = LB.averageProfile(trange=tr)
            # ------------------
            # Lambda Evaluation
            outLambda = Target.getLambda(
                inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
            )
            mask = np.logical_and(outLambda["rho_Lpar"] > 1, outLambda["LambdaDiv"] > 0)
            ax3.semilogy(
                outLambda["rho_Lpar"], outLambda["LambdaDiv"], "-", color=col, lw=2
            )
            # ------------------
            # Target Evaluation
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )
            ax2[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax2[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            maskA = np.logical_and(
                outLambda["rho_Lpar"] >= _lambdaDivMin,
                outLambda["rho_Lpar"] <= _lambdaDivMax,
            )
            maskB = outLambda["LambdaDiv"] != 0
            mask = np.logical_and(maskA, maskB)
            _st = r"$\langle \Lambda_{div}\rangle_{%3.2f \leq \rho \leq %3.2f}$ = " % (
                _lambdaDivMin,
                _lambdaDivMax,
            )
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            ax2[0].text(
                0.2,
                0.9 - 0.1 * _idplot,
                _label,
                color=col,
                transform=ax2[0].transAxes,
                fontsize=18,
            )
            # -----------------
            # upstream profiles
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=2, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit Performed")
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=2, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit NOT Performed")
            if (_idplot == 0) or (_idplot == 2):
                _idxtime = np.logical_and(time >= tr[0], time <= tr[1])
                _dummy = vr[_idxtime]
                _dummyTime = time[_idxtime]
                _y, _, _ = sigmaclip(_dummy, 4.0, 4.0)
                sns.distplot(
                    _y,
                    bins="auto",
                    color=col,
                    ax=ax4,
                    norm_hist=False,
                    kde=False,
                    hist_kws={"alpha": 0.4},
                )
                ax4.text(
                    0.2,
                    0.9 - _idplot * 0.1,
                    _label,
                    color=col,
                    transform=ax4.transAxes,
                    fontsize=18,
                )
        # ----- Upstream
        # now the plot definitions
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Target
        ax2[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax2[1].set_ylabel(r"T$_e$ [eV]")
        ax2[0].set_title(r"#{}".format(shot))
        ax2[0].tick_params(labelbottom=False)
        ax2[1].set_xlabel(r"$\rho$")
        for _ax in ax2:
            _ax.autoscale(enable=True, axis="y")

        fig2.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Lambda
        ax3.set_xlabel(r"$\rho$")
        ax3.set_xlim([1, 1.06])
        ax3.set_ylabel(r"$\Lambda_{div}$")
        ax3.set_title(r"#{}".format(shot))
        ax3.axhline(1, ls="--", lw=1.5, color="gray")
        fig3.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Filaments
        ax4.legend(loc="best", frameon=False)
        ax4.set_title(
            r"#{} $\rho \simeq $ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax4.set_xlabel(r"v$_r$ [km/s]")
        ax4.set_ylabel("#")
        fig4.savefig(
            "../pdfbox/FilamentVelocityComparison_{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
    elif selection == 28:
        shot = 37750
        trList = ((2.3, 2.6), (3.7, 4), (4.7, 5), (5.7, 6))
        rhoList = (0.83, 0.85, 0.9, None)
        tsepList = (0.00015, 0.002, 0.002, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        # Li-beam profiles
        LB = libes.Libes(shot, remote=True, experiment="AUGD", server=server)
        # target profiles
        Target = mdslangmuir.Target(shot, server=server)
        # parallel connection length
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        # filament properties
        myObj = myThbObject.myObject(
            "../data/THB/filament_data/{}_t_1.500_6.300_PMT1".format(shot)
        )
        time = np.asarray(myObj.peak_start_time[myObj.reference_channel])
        vr = np.asarray(myObj.velocity_all) / 1e3
        time = time[np.isfinite(time)]
        # drop the nan
        _finite = np.isfinite(vr)
        time, vr = time[_finite], vr[_finite]
        # trim them using the saved ELM detected
        mask = mdselmremoval.mdsremoveELMData(
            shot, time, preft=0.002, suft=0.002, elm_exper="augd"
        )
        time, vr = time[mask], vr[mask]
        # ----- figure settings
        # upstream profiles
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.075])
        ax.set_ylim([0.1, 2])
        # target profiles
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.12, left=0.15, hspace=0.05, top=0.95)
        ax2[0].set_xlim([0.97, 1.1])
        nbins = 8
        # lambda profiles
        fig3, ax3 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1)
        fig3.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        # filament velocities
        fig4, ax4 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig4.subplots_adjust(bottom=0.14, left=0.15)
        for _idplot, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
                Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            else:
                inDictionary = Target.getprofile(trange=tr)
            Out = LB.averageProfile(trange=tr)
            # ------------------
            # Lambda Evaluation
            outLambda = Target.getLambda(
                inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
            )
            mask = np.logical_and(outLambda["rho_Lpar"] > 1, outLambda["LambdaDiv"] > 0)
            ax3.semilogy(
                outLambda["rho_Lpar"], outLambda["LambdaDiv"], "-", color=col, lw=2
            )
            # ------------------
            # Target Evaluation
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )
            ax2[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax2[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            maskA = np.logical_and(
                outLambda["rho_Lpar"] >= _lambdaDivMin,
                outLambda["rho_Lpar"] <= _lambdaDivMax,
            )
            maskB = outLambda["LambdaDiv"] != 0
            mask = np.logical_and(maskA, maskB)
            _st = r"$\langle \Lambda_{div}\rangle_{%3.2f \leq \rho \leq %3.2f}$ = " % (
                _lambdaDivMin,
                _lambdaDivMax,
            )
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            ax2[0].text(
                0.2,
                0.9 - 0.1 * _idplot,
                _label,
                color=col,
                transform=ax2[0].transAxes,
                fontsize=18,
            )
            # -----------------
            # upstream profiles
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit Performed")
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit NOT Performed")
            if (_idplot == 0) or (_idplot == 3):
                _idxtime = np.logical_and(time >= tr[0], time <= tr[1])
                _dummy = vr[_idxtime]
                _dummyTime = time[_idxtime]
                _y, _, _ = sigmaclip(_dummy, 4.0, 4.0)
                sns.distplot(
                    _y,
                    bins="auto",
                    color=col,
                    ax=ax4,
                    norm_hist=False,
                    kde=False,
                    hist_kws={"alpha": 0.4},
                )
                ax4.text(
                    0.2,
                    0.9 - _idplot * 0.1,
                    _label,
                    color=col,
                    transform=ax4.transAxes,
                    fontsize=18,
                )
        # ----- Upstream
        # now the plot definitions
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Target
        ax2[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax2[1].set_ylabel(r"T$_e$ [eV]")
        ax2[0].set_title(r"#{}".format(shot))
        ax2[0].tick_params(labelbottom=False)
        ax2[1].set_xlabel(r"$\rho$")
        for _ax in ax2:
            _ax.autoscale(enable=True, axis="y")

        fig2.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Lambda
        ax3.set_xlabel(r"$\rho$")
        ax3.set_xlim([1, 1.06])
        ax3.set_ylabel(r"$\Lambda_{div}$")
        ax3.set_title(r"#{}".format(shot))
        ax3.axhline(1, ls="--", lw=1.5, color="gray")
        fig3.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Filaments
        ax4.legend(loc="best", frameon=False)
        ax4.set_title(
            r"#{} $\rho \simeq $ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax4.set_xlabel(r"v$_r$ [km/s]")
        ax4.set_ylabel("#")
        fig4.savefig(
            "../pdfbox/FilamentVelocityComparison_{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
    elif selection == 29:
        shot = 37752
        trList = ((2.1, 2.4), (4, 4.4), (5.7, 6), (6.5, 6.8))
        rhoList = (0.9, 0.9, None, None)
        tsepList = (0.00015, 0.002, None, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        # Li-beam profiles
        LB = libes.Libes(shot, remote=True, experiment="AUGE", server=server)
        # target profiles
        Target = mdslangmuir.Target(shot, server=server)
        # parallel connection length
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        # filament properties
        myObj = myThbObject.myObject(
            "../data/THB/filament_data/{}_t_1.000_7.000_PMT1".format(shot)
        )
        time = np.asarray(myObj.peak_start_time[myObj.reference_channel])
        vr = np.asarray(myObj.velocity_all) / 1e3
        time = time[np.isfinite(time)]
        # drop the nan
        _finite = np.isfinite(vr)
        time, vr = time[_finite], vr[_finite]
        # trim them using the saved ELM detected
        if server == 'mdsplus.aug.ipp.mpg.de':
            mask = ddremoveELMData.ddremoveELMData(
                shot, time, preft=0.002, suft=0.002, elm_exper="mgrien"
            )
        else:
            mask = mdselmremoval.mdsremoveELMData(
                shot, time, preft=0.002, suft=0.002, elm_exper="mgrien"
            )
        time, vr = time[mask], vr[mask]
        # ----- figure settings
        # upstream profiles
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.075])
        ax.set_ylim([0.1, 2])
        # target profiles
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.12, left=0.15, hspace=0.05, top=0.95)
        ax2[0].set_xlim([0.97, 1.1])
        nbins = 8
        # lambda profiles
        fig3, ax3 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1)
        fig3.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        # filament velocities
        fig4, ax4 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig4.subplots_adjust(bottom=0.14, left=0.15)
        for _idplot, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
                Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            else:
                inDictionary = Target.getprofile(trange=tr)
            Out = LB.averageProfile(trange=tr)
            # ------------------
            # Lambda Evaluation
            outLambda = Target.getLambda(
                inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
            )
            mask = np.logical_and(outLambda["rho_Lpar"] > 1, outLambda["LambdaDiv"] > 0)
            ax3.semilogy(
                outLambda["rho_Lpar"], outLambda["LambdaDiv"], "-", color=col, lw=2
            )
            # ------------------
            # Target Evaluation
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )
            ax2[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax2[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            maskA = np.logical_and(
                outLambda["rho_Lpar"] >= _lambdaDivMin,
                outLambda["rho_Lpar"] <= _lambdaDivMax,
            )
            maskB = outLambda["LambdaDiv"] != 0
            mask = np.logical_and(maskA, maskB)
            _st = r"$\langle \Lambda_{div}\rangle_{%3.2f \leq \rho \leq %3.2f}$ = " % (
                _lambdaDivMin,
                _lambdaDivMax,
            )
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            ax2[0].text(
                0.2,
                0.9 - 0.1 * _idplot,
                _label,
                color=col,
                transform=ax2[0].transAxes,
                fontsize=18,
            )
            # -----------------
            # upstream profiles
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit Performed")
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit NOT Performed")
            if (_idplot == 0) or (_idplot == 3):
                _idxtime = np.logical_and(time >= tr[0], time <= tr[1])
                _dummy = vr[_idxtime]
                _dummyTime = time[_idxtime]
                _y, _, _ = sigmaclip(_dummy, 4.0, 4.0)
                sns.distplot(
                    _y,
                    bins="auto",
                    color=col,
                    ax=ax4,
                    norm_hist=False,
                    kde=False,
                    hist_kws={"alpha": 0.4},
                )
                ax4.text(
                    0.2,
                    0.9 - _idplot * 0.1,
                    _label,
                    color=col,
                    transform=ax4.transAxes,
                    fontsize=20,
                )
        # ----- Upstream
        # now the plot definitions
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Target
        ax2[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax2[1].set_ylabel(r"T$_e$ [eV]")
        ax2[0].set_title(r"#{}".format(shot))
        ax2[0].tick_params(labelbottom=False)
        ax2[1].set_xlabel(r"$\rho$")
        for _ax in ax2:
            _ax.autoscale(enable=True, axis="y")

        fig2.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Lambda
        ax3.set_xlabel(r"$\rho$")
        ax3.set_xlim([1, 1.06])
        ax3.set_ylabel(r"$\Lambda_{div}$")
        ax3.set_title(r"#{}".format(shot))
        ax3.axhline(1, ls="--", lw=1.5, color="gray")
        fig3.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Filaments
        ax4.legend(loc="best", frameon=False)
        ax4.set_title(
            r"#{} $\rho \simeq $ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax4.set_xlabel(r"v$_r$ [km/s]")
        ax4.set_ylabel("#")
        fig4.savefig(
            "../pdfbox/FilamentVelocityComparison_{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
    elif selection == 30:
        shot = 37753
        trList = ((1.5, 2.3), (4.3, 5), (6.0, 6.8))
        rhoList = (0.83, 0.9, 0.9)
        tsepList = (0.00015, 0.002, 0.002)
        colorList = ("#1C6C8C", "#F2B90F", "#276323")
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        # Li-beam profiles
        LB = libes.Libes(shot, remote=True, experiment="AUGD", server=server)
        # target profiles
        Target = mdslangmuir.Target(shot, server=server)
        # parallel connection length
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        # filament properties
        myObj = myThbObject.myObject(
            "../data/THB/filament_data/{}_t_1.500_7.000_PMT1".format(shot)
        )
        time = np.asarray(myObj.peak_start_time[myObj.reference_channel])
        vr = np.asarray(myObj.velocity_all) / 1e3
        time = time[np.isfinite(time)]
        # drop the nan
        _finite = np.isfinite(vr)
        time, vr = time[_finite], vr[_finite]
        # trim them using the saved ELM detected
        if server == "localhost:8000":
            mask = mdselmremoval.mdsremoveELMData(
                shot, time, preft=0.002, suft=0.002, elm_exper="nvianell", server=server
            )
        elif server == "mdsplus.aug.ipp.mpg.de":
            mask = ddremoveELMData.ddremoveELMData(
                shot, time, preft=0.002, suft=0.002, elm_exper="nvianell"
            )
        else:
            mask = mdselmremoval.mdsremoveELMData(
                shot, time, preft=0.002, suft=0.002, elm_exper="nvianell", server=server
            )

        time, vr = time[mask], vr[mask]
        # ----- figure settings
        # upstream profiles
        fig, ax = mpl.pylab.subplots(figsize=(8, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.075])
        ax.set_ylim([0.1, 2])
        # target profiles
        fig2, ax2 = mpl.pylab.subplots(figsize=(6, 10), nrows=2, ncols=1, sharex="col")
        fig2.subplots_adjust(bottom=0.12, left=0.2, hspace=0.05, top=0.95)
        ax2[0].set_xlim([0.97, 1.1])
        nbins = 8
        # lambda profiles
        fig3, ax3 = mpl.pylab.subplots(figsize=(6, 5), nrows=1, ncols=1)
        fig3.subplots_adjust(bottom=0.15, left=0.2, right=0.95, top=0.92)
        # filament velocities
        fig4, ax4 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig4.subplots_adjust(bottom=0.14, left=0.15)
        for _idplot, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
                Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            else:
                inDictionary = Target.getprofile(trange=tr)
            Out = LB.averageProfile(trange=tr)
            # ------------------
            # Lambda Evaluation
            outLambda = Target.getLambda(
                inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
            )
            mask = np.logical_and(outLambda["rho_Lpar"] > 1, outLambda["LambdaDiv"] > 0)
            ax3.semilogy(
                outLambda["rho_Lpar"], outLambda["LambdaDiv"], "-", color=col, lw=2
            )
            # ------------------
            # Target Evaluation
            outNe = bin_by.bin_by(
                inDictionary["x"], inDictionary["ne"] / 1e19, nbins=nbins, clip=True,
            )
            outTe = bin_by.bin_by(
                inDictionary["x"], inDictionary["te"], nbins=nbins, clip=True
            )
            ax2[0].errorbar(
                outNe["x"],
                outNe["y"],
                yerr=outNe["y_err"],
                xerr=outNe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            ax2[1].errorbar(
                outTe["x"],
                outTe["y"],
                yerr=outTe["y_err"],
                xerr=outTe["x_err"],
                fmt="P",
                color=col,
                ms=13,
            )
            maskA = np.logical_and(
                outLambda["rho_Lpar"] >= _lambdaDivMin,
                outLambda["rho_Lpar"] <= _lambdaDivMax,
            )
            maskB = outLambda["LambdaDiv"] != 0
            mask = np.logical_and(maskA, maskB)
            _st = r"$\langle \Lambda_{div}\rangle_{%3.2f \leq \rho \leq %3.2f}$ = " % (
                _lambdaDivMin,
                _lambdaDivMax,
            )
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            ax2[0].text(
                0.1,
                0.9 - 0.1 * _idplot,
                _label,
                color=col,
                transform=ax2[0].transAxes,
                fontsize=13,
            )
            # -----------------
            # upstream profiles
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit Performed")
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit NOT Performed")
            if (_idplot == 0) or (_idplot == 2):
                _idxtime = np.logical_and(time >= tr[0], time <= tr[1])
                _dummy = vr[_idxtime]
                _dummyTime = time[_idxtime]
                _y, _, _ = sigmaclip(_dummy, 4.0, 4.0)
                sns.distplot(
                    _y,
                    bins="auto",
                    color=col,
                    ax=ax4,
                    norm_hist=False,
                    kde=False,
                    hist_kws={"alpha": 0.4},
                )
                ax4.text(
                    0.2,
                    0.9 - _idplot * 0.05,
                    _label,
                    color=col,
                    transform=ax4.transAxes,
                    fontsize=20,
                )
        # ----- Upstream
        # now the plot definitions
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r"#{}".format(shot))
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        fig.savefig(
            "../pdfbox/UpstreamProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Target
        ax2[0].set_ylabel(r"n$_e$ [10$^{19}$m$^{-3}$]")
        ax2[1].set_ylabel(r"T$_e$ [eV]")
        ax2[0].set_title(r"#{}".format(shot))
        ax2[0].tick_params(labelbottom=False)
        ax2[1].set_xlabel(r"$\rho$")
        for _ax in ax2:
            _ax.autoscale(enable=True, axis="y")

        fig2.savefig(
            "../pdfbox/TargetProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Lambda
        ax3.set_xlabel(r"$\rho$")
        ax3.set_xlim([1, 1.06])
        ax3.set_ylabel(r"$\Lambda_{div}$")
        ax3.set_title(r"#{}".format(shot))
        ax3.axhline(1, ls="--", lw=1.5, color="gray")
        fig3.savefig(
            "../pdfbox/LambdaDivProfileShot{}.pdf".format(shot), bbox_to_inches="tight"
        )
        # ----- Filaments
        ax4.legend(loc="best", frameon=False)
        ax4.set_title(
            r"#{} $\rho \simeq $ {:.2f}".format(
                shot, myObj.rho_pol_mean[myObj.reference_channel]
            )
        )
        ax4.set_xlabel(r"v$_r$ [km/s]")
        ax4.set_ylabel("#")
        fig4.savefig(
            "../pdfbox/FilamentVelocityComparison_{}.pdf".format(shot),
            bbox_to_inches="tight",
        )
    elif selection == 31:
        Dictionary = {
            # "37466": {
            #     "shot": 37466,
            #     "trlist": ((2.1, 2.4), (3.1,3.4), (4, 4.4), (4.5, 4.9), (5.7, 6), (6.5, 6.8)),
            #     "rhoList": (0.9, 0.9,0.9, 0.9,  None, None),
            #     "tsepList": (0.00015, 0.002,0.002, 0.002, None, None),
            #     "libe": "AUGE",
            #     "ELMdd": "nvianell",
            #     "file": "../data/THB/filament_data/37466_t_1.800_7.000_PMT1",
            # },
            "37468": {
                "shot": 37468,
                "trlist": ((1.5, 2.3), (3,3.5), (4.3, 5), (5.3, 6.0), (6.2, 6.5)),
                "rhoList": (0.83, 0.9, 0.9, 0.9, 0.9),
                "tsepList": (0.00015, 0.002, 0.002, 0.002, 0.002),
                "libe": "AUGD",
                "ELMdd": "augd",
                "file": "../data/THB/filament_data/37468_t_1.500_7.000_PMT1",
            },
            "37750": {
                "shot": 37750,
                "trlist": ((2.3, 2.6), (2.9, 3.2), (3.7, 4), (4.7, 5), (5.7, 6)),
                "rhoList": (0.83, 0.85, 0.9, None, None),
                "tsepList": (0.00015, 0.002, 0.002, None, None),
                "libe": "AUGD",
                "ELMdd": "augd",
                "file": "../data/THB/filament_data/37750_t_1.500_6.300_PMT1",
            },
            "37752": {
                "shot": 37752,
                "trlist": ((2.1, 2.4), (3, 3.4), (4, 4.4), (5.7, 6), (6.5, 6.8)),
                "rhoList": (0.9, 0.9, 0.9, None, None),
                "tsepList": (0.00015, 0.002,0.002, None, None),
                "libe": "AUGD",
                "ELMdd": "mgrien",
                "file": "../data/THB/filament_data/37752_t_1.000_7.000_PMT1",
            },
            "37753": {
                "shot": 37753,
                "trlist": ((1.5, 2.3), (2.4, 2.7), (3., 3.5), (4.3, 5), (6.0, 6.8)),
                "rhoList": (0.83, 0.9, 0.9, 0.9, 0.9),
                "tsepList": (0.00015, 0.002, 0.002, 0.002, 0.002),
                "libe": "AUGD",
                "ELMdd": "nvianell",
                "file": "../data/THB/filament_data/37753_t_1.500_7.000_PMT1",
            },
        }
        #
        colorList = ("#023E73", "#38A3A5", "#624CAB", "#FFC857", "#8C8C8C")
        # open the two figures \
        # figure of vr as a function of LambdaDiv and Pressure
        fig, ax = mpl.pylab.subplots(figsize=(7, 12), nrows=2, ncols=1, sharey="col")
        fig.subplots_adjust(hspace=0.25, left=0.18, right=0.97, bottom=0.16)
        # figure of fb as a function of LambdaDiv and Pressure
        fig2, ax2 = mpl.pylab.subplots(figsize=(7, 12), nrows=2, ncols=1, sharey="col")
        fig2.subplots_adjust(hspace=0.25, left=0.18, right=0.97, bottom=0.16)
        # lambdaDiv average calculation
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        for _idxshot, (key, col) in enumerate(zip(Dictionary.keys(), colorList)):
            shot = Dictionary[key]["shot"]
            trList = Dictionary[key]['trlist']
            rhoList = Dictionary[key]['rhoList']
            tsepList = Dictionary[key]['tsepList']
            LP = mdslangmuir.Target(shot, server=server)
            # parallel connection length
            Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
            inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
            # check the LP around rho = 1.03
            _idx = np.argmin(np.abs(Lpar[:, 0] - 1.03))
            LpReference = Lpar[_idx, 2]
            # filament properties
            myObj = myThbObject.myObject(Dictionary[key]["file"])
            time = np.asarray(
                myObj.peak_start_time[myObj.reference_channel]
                + myObj.peak_stop_time[myObj.reference_channel]
            )
            vr = np.asarray(myObj.velocity_all) / 1e3
            time = time[np.isfinite(time)]
            # drop the nan
            _finite = np.isfinite(vr)
            time, vr = time[_finite], vr[_finite]
            # trim them using the saved ELM detected
            if server == "mdsplus.aug.ipp.mpg.de":
                mask = ddremoveELMData.ddremoveELMData(
                    shot,
                    time,
                    preft=0.002,
                    suft=0.002,
                    elm_exper=Dictionary[key]["ELMdd"],
                )
                mask2 = ddremoveELMData.ddremoveELMData(
                    shot,
                    LP.time,
                    preft=0.002,
                    suft=0.002,
                    elm_exper=Dictionary[key]["ELMdd"],
                )
            else:
                mask = mdselmremoval.mdsremoveELMData(
                    shot,
                    time,
                    preft=0.002,
                    suft=0.002,
                    elm_exper=Dictionary[key]["ELMdd"],
                    server=server,
                )
                mask2 = mdselmremoval.mdsremoveELMData(
                    shot,
                    LP.time,
                    preft=0.002,
                    suft=0.002,
                    elm_exper=Dictionary[key]["ELMdd"],
                    server=server,
                )
            time, vr = time[mask], vr[mask]
            timeFreq, blobFreq = (
                myObj.int_time,
                myObj.blob_freq_trace[myObj.reference_channel, :] / 1e3,
            )
            mask = (blobFreq != 0)
            timeFreq, blobFreq = timeFreq[mask], blobFreq[mask]
            # read the pressure
            Conn = mds.Connection(server)
            diag, signal = "DDS", "nMainIst"
            _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
            _st = "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
            pMain = Conn.get(_s).data() / 1e19
            tpMain = Conn.get(_st).data()
            neReference = LP.Ne[list(LP.signal.keys()).index("ua8"), :][mask2]
            teReference = LP.Te[list(LP.signal.keys()).index("ua8"), :][mask2]
            _lambdaDivMin, _lambdaDivMax, nbins = 1.02, 1.03, 8
            for tr, rho, t_sep in zip(trList, rhoList, tsepList):
                # computation of Lambda
                if rho:
                    inDictionary = LP.getprofile(
                        trange=tr, interelm=True, rho=rho, t_sep=t_sep
                    )
                else:
                    inDictionary = LP.getprofile(trange=tr)
                outLambda = LP.getLambda(
                    inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
                )
                mask = np.logical_and(outLambda["rho_Lpar"] >= _lambdaDivMin, outLambda["rho_Lpar"]<= _lambdaDivMax)
                LambdaTime = np.nanmean(outLambda['LambdaDiv'][mask])
                # Computation of Vr
                mask = np.logical_and(time >= tr[0],time <= tr[1])
                if np.count_nonzero(mask) != 0:
                    _vr = np.nanmedian(sigmaclip(vr[mask],3.5,3.5)[0])
                    _vrE = np.nanstd(sigmaclip(vr[mask],3.5, 3.5)[0])/2
                else:
                    _vr = np.nan
                    _vrE = np.nan
                    # Computation of Pr
                mask = np.logical_and(tpMain >= tr[0], tpMain <= tr[1])
                _pr = np.nanmean(pMain[mask])
                _prE = np.nanmean(pMain[mask])
                # computation of fb
                mask = np.logical_and(timeFreq >= tr[0], timeFreq <= tr[1])
                _fb = np.nanmean(blobFreq[mask])
                _fbE = np.nanstd(blobFreq[mask])

                # plot lambda vr vs Lambda
                ax[0].errorbar(LambdaTime, _vr, yerr=_vrE/2, fmt='o', ms=13, color=col)
                ax[1].errorbar(_pr, _vr, yerr=_vrE/2, fmt='o', ms=13, color=col)
                # now the blot of the blob-frequency
                ax2[0].errorbar(LambdaTime, _fb, yerr=_fbE, fmt='o', ms=13, color=col)
                ax2[1].errorbar(_pr, _fb, yerr=_fbE, fmt='o', ms=13, color=col)
            # now the plot with the f_b
            ax[0].text(
                0.1,
                0.9 - 0.06 * _idxshot,
                r"#{}".format(shot),
                transform=ax[0].transAxes,
                fontsize=13,
                color=col,
            )
            ax2[0].text(
                0.1,
                0.9 - 0.06 * _idxshot,
                r"#{}".format(shot),
                transform=ax2[0].transAxes,
                fontsize=13,
                color=col,
            )
        for _ax in ax:
            _ax.set_xscale("log")
            _ax.set_ylabel(r"v$_r$ [km/s]")
        ax[0].set_xlabel(r"$\Lambda_{div}$")
        ax[1].set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")

        for _ax in ax2:
            _ax.set_xscale("log")
            _ax.set_ylabel(r"f$_b$ [kHz]")
        ax2[0].set_xlabel(r"$\Lambda_{div}$")
        ax2[1].set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")
        fig.savefig("../pdfbox/Vr_vsLambdaDiv_vsPmain.pdf", bbox_to_inchest="tight")
        fig2.savefig("../pdfbox/fb_vsLambdaDiv_vsPmain.pdf", bbox_to_inchest="tight")
    elif selection == 32:
        shotList = (37466, 37752)
        fileList = ("../data/THB/filament_data/37466_t_1.800_7.000_PMT1",
                    "../data/THB/filament_data/37752_t_1.000_7.000_PMT1")
        colorList = ("#276323", '#FF008E')
        elmDD = ('nvianell','mgrien')
        # filament velocity
        fig, ax = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.17)
        # pressure f_b
        fig2, ax2 = mpl.pylab.subplots(figsize=(7, 5), nrows=1, ncols=1)
        fig2.subplots_adjust(bottom=0.17)
        # now a combine plot with 2 histograms and filament pressure scaling
        fig3, ax3 = mpl.pylab.subplots(figsize=(7, 13), nrows=3, ncols=1)
        fig3.subplots_adjust(hspace=0.25, bottom=0.16, top=0.96, right=0.97, left=0.17)

        tmin, tmax = 6.4, 7
        # add also the early point for shot 37466 for a combined plot
        tminLow, tmaxLow = 3.7, 4
        # hard coded estimate of LambdaDiv to speed up the computation
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        for _idplot, (shot, file, col, _dds) in enumerate(zip(shotList, fileList, colorList, elmDD)):
            myObj = myThbObject.myObject(file)
            time = np.asarray(
                myObj.peak_start_time[myObj.reference_channel]
                + myObj.peak_stop_time[myObj.reference_channel]
            )
            vr = np.asarray(myObj.velocity_all) / 1e3
            time = time[np.isfinite(time)]
            # drop the nan
            _finite = np.isfinite(vr)
            time, vr = time[_finite], vr[_finite]
            # trim them using the saved ELM detected
            if server == "mdsplus.aug.ipp.mpg.de":
                mask = ddremoveELMData.ddremoveELMData(
                    shot,
                    time,
                    preft=0.002,
                    suft=0.002,
                    elm_exper=_dds,
                )
            else:
                mask = mdselmremoval.mdsremoveELMData(
                    shot,
                    time,
                    preft=0.002,
                    suft=0.002,
                    elm_exper=_dds,
                    server=server,
                )
            time, vr = time[mask], vr[mask]
            timeFreq, blobFreq = (
                myObj.int_time,
                myObj.blob_freq_trace[myObj.reference_channel, :] / 1e3,
            )
            mask = (blobFreq >= 1)
            timeFreq, blobFreq = timeFreq[mask], blobFreq[mask]
            _idxtime = np.logical_and(time >= tmin, time <= tmax)
            _dummy = vr[_idxtime]
            _dummyTime = time[_idxtime]
            _y, _, _ = sigmaclip(_dummy, 4.0, 4.0)
            sns.distplot(
                _y,
                bins="auto",
                color=col,
                ax=ax,
                norm_hist=False,
                kde=False,
                hist_kws={"alpha": 0.4, "range":[_y.min(), 5]},
            )
            ax.text(
                0.2,
                0.9 - _idplot * 0.07,
                r'#{}'.format(shot), 
                color=col,
                transform=ax.transAxes,
                fontsize=20,
            )
            # ---- repeat also for the 3 panel figure

            sns.distplot(
                _y,
                bins="auto",
                color=col,
                ax=ax3[1],
                norm_hist=False,
                kde=False,
                hist_kws={"alpha": 0.4, "range":[_y.min(), 5]},
            )
            ax3[1].text(
                0.2,
                0.9 - _idplot * 0.07,
                r'#{}'.format(shot),
                color=col,
                transform=ax3[1].transAxes,
                fontsize=20,
            )
            if shot == 37466:
                _st = r" $\langle \Lambda_{div}\rangle_{%3.2f \leq \rho \leq %3.2f}$ = " % (
                    _lambdaDivMin,
                    _lambdaDivMax)
                # add also the other plot at lower Lambda Div
                # --
                # _idxtime = np.logical_and(time >= tmin, time <= tmax)
                # _dummy = vr[_idxtime]
                # _dummyTime = time[_idxtime]
                # _y, _, _ = sigmaclip(_dummy, 4.0, 4.0)
                sns.distplot(
                    _y,
                    bins="auto",
                    color=col,
                    ax=ax3[0],
                    norm_hist=False,
                    kde=False,
                    hist_kws={"alpha": 0.4, "range":[_y.min(), 5]}
                )
                _label =r'#{}'.format(shot) +  _st + r" 2.13 "
                
                ax3[0].text(
                    0.2,
                    0.8,
                    _label, 
                    color=col,
                    transform=ax3[0].transAxes,
                    fontsize=16,
                )
                # ----- 
                # time = np.asarray(
                #     myObj.peak_start_time[myObj.reference_channel]
                #     + myObj.peak_stop_time[myObj.reference_channel]
                # )
                # vr = np.asarray(myObj.velocity_all) / 1e3
                # time = time[np.isfinite(time)]
                # # drop the nan
                # _finite = np.isfinite(vr)
                # time, vr = time[_finite], vr[_finite]                
                _idxtime = np.logical_and(time >= tminLow, time <= tmaxLow)
                _dummy = vr[_idxtime]
                _dummyTime = time[_idxtime]
                _y, _, _ = sigmaclip(_dummy, 4.0, 4.0)
                sns.distplot(
                    _y,
                    bins="auto",
                    color='#1C6C8C',
                    ax=ax3[0],
                    norm_hist=False,
                    kde=False,
                    hist_kws={"alpha": 0.4, "range":[_y.min(), 5]},
                )
                _label = r'#{}'.format(shot)+ _st + r" 0.49 "
                ax3[0].text(
                    0.2,
                    0.9,
                    _label, 
                    color='#1C6C8C',
                    transform=ax3[0].transAxes,
                    fontsize=16,
                )

            # ----------------
            # read the pressure
            Conn = mds.Connection(server)
            diag, signal = "DDS", "nMainIst"
            _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
            _st = "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
            pMain = Conn.get(_s).data() / 1e19
            tpMain = Conn.get(_st).data()
            pSub = np.zeros(timeFreq.size)
            for _idxtime, t in enumerate(timeFreq):
                mask = np.logical_and(tpMain >= t-0.05, tpMain <= t+0.5)
                pSub[_idxtime] = np.nanmean(pMain[mask])
            ax2.plot(pSub[np.argsort(pSub)], blobFreq[np.argsort(pSub)],'-',lw=2,color=col)
            ax2.text(
                0.5,
                0.4 - _idplot * 0.07,
                r'#{}'.format(shot),
                color=col,
                transform=ax2.transAxes,
                fontsize=20,
            )
            ax3[2].plot(pSub[np.argsort(pSub)], blobFreq[np.argsort(pSub)],'-',lw=2,color=col)
        ax.set_xlabel(r"v$_r$ [km/s]")
        ax.set_ylabel("#")
        fig.savefig(
            "../pdfbox/FilamentVelocityComparison_{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inches="tight",
        )
        ax2.set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")
        ax2.set_ylabel(r"f$_b$ [kHz]")
        fig2.savefig(
            "../pdfbox/FilamentFrequencyPMainShot{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inches="tight",
        )
        # -- this is the 3 panels
        ax3[0].set_xlabel(r"v$_r$ [km/s]")
        ax3[0].set_ylabel("#")
        ax3[1].set_xlabel(r"v$_r$ [km/s]")
        ax3[1].set_ylabel("#")
        ax3[2].set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")
        ax3[2].set_ylabel(r"f$_b$ [kHz]")
        fig3.savefig(
            "../pdfbox/FilamentVelocityFrequencyPMainShotCombined{}_{}.pdf".format(shotList[0], shotList[1]),
            bbox_to_inches="tight",
        )

    elif selection == 33:
        shot = 37468
        myObj= myThbObject.myObject("../data/THB/filament_data/37468_t_1.500_7.000_PMT1")
        mask = np.logical_and(myObj.rho_pol_mean >= 1.03, myObj.rho_pol_mean <= 1.07)
        fig, ax = mpl.pylab.subplots(figsize=(7,5), nrows=1,ncols=1)
        fig.subplots_adjust(bottom=0.17,left=0.17)
        fb = myObj.blob_freq_trace[mask]/1e3
        rho = myObj.rho_pol_mean[mask]
        for _idx, (f,rr) in enumerate(zip(fb,rho)):
            p,=ax.plot(myObj.int_time[f !=0], f[f!=0], '-', lw=2)
            _label = r'$\rho$ = {:.3f}'.format(rr)
            ax.text(0.1,0.9-0.05*_idx,_label,color=p.get_color(), fontsize=13)
        ax.set_xlabel(r't[s]')
        ax.set_ylabel(r'f$_b$ [kHz]')
        ax.set_title(r'#{}'.format(shot))
        fig.savefig('../pdfbox/FilamentFrequencyTimeShot{}.pdf'.format(shot), bbox_to_inches='tight')
    elif selection == 34:
        shotList = (37750, 37752)
        colorList = ("#38A3A5", "#624CAB")
        rhoList = ((0.82,0.82,0.9),
                   (0.9, 0.82, 0.82, None, None))
        nPlunges = (3, 5)
        fig, ax = mpl.pylab.subplots(figsize=(7,10), nrows=2, ncols=1, sharey='row')
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        for _idplot, (shot, col, rho, nP) in enumerate(zip(shotList, colorList, rhoList, nPlunges)):
            RFA = np.loadtxt('../data/RFA/RFA_{}'.format(shot)).transpose()
            Target = mdslangmuir.Target(shot, server=server)
            Conn = mds.Connection(server)
            diag, signal = "DDS", "nMainIst"
            _s = "augdiag(" + str(shot) + ',"' + diag + '","' + signal + '")'
            _st = "dim_of(augdiag(" + str(shot) + ',"' + diag + '","' + signal + '"))'
            pMain = Conn.get(_s).data() / 1e19
            tpMain = Conn.get(_st).data()
            Plunge = np.arange(nP)
            nMax = 6 * (Plunge.max() + 1)
            _positions = np.linspace(0, nMax, )
            Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
            inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
            nbins=8
            for p,rr in zip(Plunge,rho):
                _pp = _positions+p
                tr = (RFA[_pp[-3],:].min(), RFA[_pp[-3],:].max())
                if rr:
                    inDictionary = LP.getprofile(
                        trange=tr, interelm=True, rho=rr, t_sep=0.002
                    )
                else:
                    inDictionary = LP.getprofile(trange=tr)
                outLambda = LP.getLambda(
                    inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
                )
                mask = np.logical_and(outLambda["rho_Lpar"] >= _lambdaDivMin, outLambda["rho_Lpar"]<= _lambdaDivMax)
                LambdaTime = np.nanmean(outLambda['LambdaDiv'][mask])
                mask = np.logical_and(tpMain >= tr[0], tpMain <= tr[1])
                _pr = np.nanmean(pMain[mask])
                _prE = np.nanmean(pMain[mask])
                # now compute the appropriate temperature
                rho = RFA[_pp[-1],:]
                mask = (rho <= 1.10)
                tia, stia = RFA[_pp[0],mask], RFA[_pp[1], mask]
                tib, stib = RFA[_pp[2],mask], RFA[_pp[3], mask]
                mask = np.logical_and(stia/tia < .9, stib/tib < .9)
                TiArr, errArr = np.append(tia[mask],tib[mask]), np.append(stia[mask],stib[mask])
                Out = DescrStatsW(TiArr,weights=errArr)
                ax[0].errorbar(LambdaTime, Out.mean,yerr=Out.std,fmt='P', ms=13,color=col)
                ax[1].errorbar(_pr,Out.mean,yerr=Out.std,fmt='P', ms=13,color=col)
            ax[0].text(0.1,0.9-0.05*_idplot,r'#'.format(shot),transform=ax[0].transAxes,fontsize=13)
        for _ax in ax:
            _ax.set_xscale("log")
            _ax.set_ylabel(r"T$_i$ [eV]")
        ax[0].set_xlabel(r"$\Lambda_{div}$")
        ax[1].set_xlabel(r"P$_{main} [10^{19}$m$^{2}$/s]")
        fig.savefig('../pdfbox/TiFarSOL_vsLambda_vsPmain.pdf',bbox_to_inches='tight')
    elif selection == 35:
        shot = 37466
        nbins = 8
        trList = ((2.1, 2.4), (4, 4.4), (5.7, 6), (6.5, 6.8))
        rhoList = (0.9, 0.9, None, None)
        tsepList = (0.00015, 0.002, None, None)
        colorList = ("#1C6C8C", "#F2B90F", "#D92B04", "#276323")
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        # Li-beam profiles
        LB = libes.Libes(shot, remote=True, experiment="AUGE", server=server)
        # target profiles
        Target = mdslangmuir.Target(shot, server=server)
        # parallel connection length
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        # ----- figure settings
        # upstream profiles
        fig, ax = mpl.pylab.subplots(figsize=(10, 6), nrows=1, ncols=1)
        fig.subplots_adjust(bottom=0.15, left=0.17)
        ax.set_xlim([0.97, 1.075])
        ax.set_ylim([0.3, 2])
        for _idplot, (tr, rho, t_sep, col) in enumerate(
            zip(trList, rhoList, tsepList, colorList)
        ):
            if rho:
                inDictionary = Target.getprofile(
                    trange=tr, interelm=True, rho=rho, t_sep=t_sep
                )
                Out = LB.averageProfile(trange=tr, interelm=True, rho=rho, t_sep=t_sep)
            else:
                inDictionary = Target.getprofile(trange=tr)
            Out = LB.averageProfile(trange=tr)
            # ------------------
            # Lambda Evaluation
            outLambda = Target.getLambda(
                inDictionary, np.average(tr), nbins=8, inLparallel=inParallel
            )
            mask = np.logical_and(outLambda["rho_Lpar"] > 1, outLambda["LambdaDiv"] > 0)
            maskA = np.logical_and(
                outLambda["rho_Lpar"] >= _lambdaDivMin,
                outLambda["rho_Lpar"] <= _lambdaDivMax,
            )
            maskB = outLambda["LambdaDiv"] != 0
            mask = np.logical_and(maskA, maskB)
            _st = r"$\langle \Lambda_{div}\rangle_{%3.2f \leq \rho \leq %3.2f}$ = " % (
                _lambdaDivMin,
                _lambdaDivMax,
            )
            _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
                np.nanmean(outLambda["LambdaDiv"][mask]),
                np.nanstd(outLambda["LambdaDiv"][mask]),
            )
            # -----------------
            # upstream profiles
            _idx = np.where(Out["rho"] >= 0.97)
            xB = Out["rho"][_idx]
            yB = Out["ne"][_idx]
            yBE = Out["err_Y"][_idx]
            xBE = Out["err_X"][_idx]
            try:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    xB[np.argsort(xB)],
                    yB[np.argsort(xB)],
                    yBE[np.argsort(xB)],
                    xe=xBE[np.argsort(xB)],
                    nrestarts=500,
                )
                xN = np.linspace(0.97, 1.1, 120)
                gp.GPRFit(xN)
                yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit Performed")
            except:
                xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
                enS = yFit[np.argmin(np.abs(xN - 1))]
                ax.plot(
                    Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
                )
                ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
                ax.fill_between(
                    xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
                )
                print("Gaussian Process Regression Fit NOT Performed")
        # ----------------------------------
        # now make the computation for 37752
        shot = 37752
        tr = (6.5, 6.8)
        rho = None
        tsepList = None
        col = '#FF008E'
        _lambdaDivMin, _lambdaDivMax = 1.02, 1.03
        # Li-beam profiles
        LB = libes.Libes(shot, remote=True, experiment="AUGE", server=server)
        # target profiles
        Target = mdslangmuir.Target(shot, server=server)
        # parallel connection length
        Lpar = np.loadtxt("../equilibriadata/LparallelShot{:.1f}.txt".format(shot))
        inParallel = {"rho": Lpar[:, 0], "lpar": Lpar[:, 1], "lparX": Lpar[:, 2]}
        #
        inDictionary = Target.getprofile(trange=tr)
        Out = LB.averageProfile(trange=tr)
        # ------------------
        # Lambda Evaluation
        outLambda = Target.getLambda(
            inDictionary, np.average(tr), nbins=nbins, inLparallel=inParallel
        )
        mask = np.logical_and(outLambda["rho_Lpar"] > 1, outLambda["LambdaDiv"] > 0)
        maskA = np.logical_and(
            outLambda["rho_Lpar"] >= _lambdaDivMin,
            outLambda["rho_Lpar"] <= _lambdaDivMax,
        )
        maskB = outLambda["LambdaDiv"] != 0
        mask = np.logical_and(maskA, maskB)
        _st = r"# 37752, $\langle \Lambda_{div}\rangle_{%3.2f \leq \rho \leq %3.2f}$ = " % (
            _lambdaDivMin,
            _lambdaDivMax,
        )
        _label = _st + r" {:.2f} $\pm$ {:.2f} ".format(
            np.nanmean(outLambda["LambdaDiv"][mask]),
            np.nanstd(outLambda["LambdaDiv"][mask]),
        )
        _idx = np.where(Out["rho"] >= 0.97)
        xB = Out["rho"][_idx]
        yB = Out["ne"][_idx]
        yBE = Out["err_Y"][_idx]
        xBE = Out["err_X"][_idx]
        try:
            gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                xB[np.argsort(xB)],
                yB[np.argsort(xB)],
                yBE[np.argsort(xB)],
                xe=xBE[np.argsort(xB)],
                nrestarts=500,
            )
            xN = np.linspace(0.97, 1.1, 120)
            gp.GPRFit(xN)
            yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
            enS = yFit[np.argmin(np.abs(xN - 1))]
            ax.plot(
                Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
            )
            ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
            ax.fill_between(
                xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
            )
            print("Gaussian Process Regression Fit Performed")
        except:
            xN, yFit, yFitE = Out["rho"][_idx], Out["ne"][_idx], Out["err_Y"][_idx]
            enS = yFit[np.argmin(np.abs(xN - 1))]
            ax.plot(
                Out["rhoraw"], Out["neraw"] / enS, ".", ms=3, color=col, alpha=0.2
            )
            ax.plot(xN, yFit / enS, "-", color=col, lw=3, label=_label)
            ax.fill_between(
                xN, (yFit - yFitE) / enS, (yFit + yFitE) / enS, color=col, alpha=0.3
            )
            print("Gaussian Process Regression Fit NOT Performed")
        ax.legend(loc="best", numpoints=1, frameon=False, fontsize=13)
        ax.set_xlabel(r"$\rho$")
        ax.set_title(r'#37466 - #37752')
        ax.set_ylabel(r"n$_e$/n$_e^{\rho=1}$")
        ax.set_yscale("log")
        ax.set_yticks([0.3, 1, 2])
        ax.set_yticklabels([0.3,  1.,  2])
        fig.savefig(
            "../pdfbox/UpstreamProfileShot37466_37752.pdf".format(shot), bbox_to_inches="tight"
        )

    elif selection == 99:
        mpl.pylab.close("all")
        loop = False
    else:
        input("Unknown Option Selected!")
