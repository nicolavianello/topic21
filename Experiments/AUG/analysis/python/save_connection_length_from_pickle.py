import pickle
import os
import numpy as np
directory = "/afs/ipp-garching.mpg.de/home/b/btal/Pool/NV"
fileList = []
for file in os.listdir(directory):
    if file.endswith(".p"):
        Data = pickle.load(open(os.path.join(directory, file), "rb"))
        rho = Data[1]["rhop"]
        LpBX = Data[1]["Lcoutbelowx"]
        LpAX = Data[1]["Lcoutabovex"]
        shot = float(file[16:21])
        savefile = "../equilibriadata/LparallelShot{}.txt".format(
            shot
        )
        np.savetxt(savefile, np.c_[rho,LpAX,LpBX])