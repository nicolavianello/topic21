import numpy as np
from scipy.stats import sigmaclip
from statsmodels.stats.weightstats import DescrStatsW
from collections import OrderedDict


def bin_by(x, y, nbins=30, **kwargs):
    """
        Computed the binned statistics on the values of y
        based on a linear binning on the values of x.
        :arg x: x value where we compute the binning
        :arg y: values where we compute the statistics
        :param nbin: number of bins.
        :param clip: boolean default is False. If true we computed a sigma-clip average values
            and all the keywords for scipy.stats.sigmaclip can be passed
        :param **kwargs: We can give the minimum (mn) and maximum (mx) for the values of x
            as well as all the keyword accepted by scipy sigma clip
        :return OutDictionary: Dictionary containing the computed statistics
    """
    # avoid the NaN
    clip = kwargs.get("clip", False)
    mask = np.logical_and(np.isfinite(x), np.isfinite(y))
    x, y = x[mask], y[mask]
    mn = kwargs.get("mn", x.min())
    mx = kwargs.get("mx", x.max())
    bins = np.linspace(mn, mx, nbins + 1)
    ind = np.digitize(x, bins)
    outY = []
    outX = []
    for i in range(1, len(bins)):
        outY.append(y[ind == i])
        outX.append(x[ind == i])
    # now the appropriate statistics
    wAvg, wStd, xAvg, xStd, wMedian = (
        np.zeros(len(outY)),
        np.zeros(len(outY)),
        np.zeros(len(outY)),
        np.zeros(len(outY)),
        np.zeros(len(outY)),
    )
    for _idx, (x, y) in enumerate(zip(outX, outY)):
        if clip:
            yN, low, high = sigmaclip(
                np.asarray(y), low=kwargs.get("low", 4), high=kwargs.get("high", 4)
            )
            xE = np.asarray(x)[np.logical_and(y >= low, y <= high)]
        else:
            yN, xE = y, x
        wAvg[_idx] = np.average(yN)
        wStd[_idx] = np.std(yN)
        xAvg[_idx] = np.average(xE)
        xStd[_idx] = np.std(xE)
        wMedian[_idx] = np.median(yN)

    Out = OrderedDict(
        [
            ("x", xAvg),
            ("x_err", xStd),
            ("y", wAvg),
            ("y_err", wStd),
            ("y_median", wMedian),
            ("x_all", outX),
            ("y_all", outY),
        ]
    )
    return Out


def binned_weighted_statistics(x, y, err, nbins=30, **kwargs):
    """
        Computed the binned weighted statistics on the values of y
        based on a linear binning on the values of x.
        :arg x: x value where we compute the binning
        :arg y: values where we compute the statistics
        :arg err: values of error to compute the weighted statistics
        :param nbin: number of bins.
        :param clip: boolean default is False. If true we computed a sigma-clip
            weighted average values and all the keywords for scipy.stats.sigmaclip can be passed
        :param **kwargs: We can give the minimum (mn) and maximum (mx) for the values of x
            as well as all the keyword accepted by
            scipy sigma clip
        :return OutDictionary: Dictionary containing the computed statistics

    """

    clip = kwargs.get("clip", False)
    # avoid the NaN and zero errors
    mask = np.logical_and(np.isfinite(x), np.isfinite(y)) + np.isfinite(err)
    x, y, err = x[mask], y[mask], err[mask]
    # sobstitue the eventually zero values with the std deviation of y with non zero
    if np.size(np.where(err == 0)[0]):
        err[np.where(err == 0)] = np.std(y[np.where(err != 0)])

    mn = kwargs.get("mn", x.min())
    mx = kwargs.get("mx", x.max())
    bins = np.linspace(mn, mx, nbins + 1)
    # To avoid extra bin for the max value
    # bins[-1] += 1

    ind = np.digitize(x, bins)
    outY = []
    outX = []
    outE = []
    for i in range(1, len(bins)):
        outY.append(y[ind == i])
        outX.append(x[ind == i])
        outE.append(err[ind == i])
    # now the appropriate statistics
    wAvg, wStd, xAvg, xStd, wMedian = (
        np.asarray([]),
        np.asarray([]),
        np.asarray([]),
        np.asarray([]),
        np.asarray([]),
    )
    for _idx, (x, y, _e) in enumerate(zip(outX, outY, outE)):
        if clip:
            yN, low, high = sigmaclip(
                np.asarray(y), low=kwargs.get("low", 4), high=kwargs.get("high", 4)
            )
            eE = np.asarray(_e)[np.logical_and(y >= low, y <= high)]
            xE = np.asarray(x)[np.logical_and(y >= low, y <= high)]
        else:
            yN, eE, xE = y, _e, x
        Stats = DescrStatsW(yN, weights=eE)
        if np.isfinite(Stats.mean):
            wAvg = np.append(wAvg, Stats.mean)
            wStd = np.append(wStd, Stats.std)
            xAvg = np.append(xAvg, np.average(xE))
            xStd = np.append(xStd, np.std(xE))
            wMedian = np.append(wMedian, np.median(yN))

    Out = OrderedDict(
        [
            ("x", xAvg),
            ("x_err", xStd),
            ("y", wAvg),
            ("y_err", wStd),
            ("y_median", wMedian),
            ("x_all", outX),
            ("y_all", outY),
        ]
    )
    return Out
