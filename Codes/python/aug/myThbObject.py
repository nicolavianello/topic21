import pickle
try:
    from scipy.io import savemat, loadmat
except: 
    pass

class myObject(object):
    def __init__(self, arg=None):
        if type(arg)==str:
            self.load(arg)

    def clear(self):
        self.__dict__.clear()

    def keys(self):
        return self.__dict__.keys()

    @property
    def status(self):
        return len(self.__dict__)!=0

    def __getitem__(self, item):
        return self.__dict__[item]

    def __setitem__(self, item, value):
        self.__dict__[item] = value
        
    def __delitem__(self, item):
        del self.__dict__[item]            

    def save(self, filename):
        if not self.status:
            raise Exception('myObject not loaded.')
        pickle.dump(self.__dict__, open(filename, 'wb'), pickle.HIGHEST_PROTOCOL, encoding='latin1')#version2)

    def savemat(self, filename):
        if not self.status:
            raise Exception('myObject not loaded.')
        savemat(filename, self.__dict__)
        
    def load(self, filename):
        self.__dict__ = pickle.load(open(filename, 'rb'), encoding='latin1')                 

    def loadmat(self, filename):
        self.clear()
        loadmat(filename, self.__dict__)

    @classmethod
    def fromFile(cls, filename):
        temp = cls()
        temp.__dict__ = pickle.load(open(filename), encoding='latin1')       
        return temp

    @classmethod
    def fromMatlabFile(cls, filename):
        temp = cls()
        loadmat(filename, temp.__dict__)
        return temp

    def __iter__(self):
        for key in self.keys():
            yield self[key]
        

