from scipy import constants, interpolate
import numpy as np
from general import elm_detection, bin_by
import MDSplus as mds
import bottleneck
import logging
import matplotlib as mpl
import eqtools
import warnings
from collections import OrderedDict
from cyfieldlineTracer import get_fieldline_tracer
from astropy import units
from plasmapy.formulary import collisions, parameters

try:
    from plasmapy.atomic import Particle
except:
    from plasmapy.particles import Particle


class Target(object):
    def __init__(
        self,
        shot,
        server="localhost:8000",
        plate="Outer Target",
        equilibrium=None,
        gas="D2",
    ):
        """
                Class to deal with already processed data from divertor probes. It basically
        remap the position on the appropriate equilibrium,  compute the
        profiles

        :param shot: Shot number
        :param server: MDSplus server. On AUG cluster just use mdsplus.aug.mpg.de otherwise
            implement an MDSplus tunnel
        :param plate: Type of Target to be considered. Should be a string. Available options are
            "Outer Target", "Outer Limiter", "Dome", "Inner Target", "Inner Limiter",
            "Upper Outer", "Upper Inner". Default value is Outer Target
        :param equilibrium: If passed it should be an object created by eqtools, otherwise it is computed
        :param gas: String indicating the type of gas used. Can be either 'H', 'D2', 'He'
        """

        self.gas = gas
        if self.gas == "H":
            self.Particle = Particle("H+")
            self._ionicsymbol = (
                self.Particle.ionic_symbol[0] + self.Particle.ionic_symbol[-1]
            )
        elif self.gas == "D2":
            self.Particle = Particle("D+")
            self._ionicsymbol = (
                self.Particle.ionic_symbol[0] + self.Particle.ionic_symbol[-1]
            )
        elif gas == "He":
            # assuming single ionized Helium in the SOL
            self.Particle = Particle("He+")
            self._ionicsymbol = (
                self.Particle.ionic_symbol[0:1] + self.Particle.ionic_symbol[-1]
            )
        else:
            print("Gas not found, assuming Deuterium")
            self.Particle = Particle("D+")
            self._ionicsymbol = (
                self.Particle.ionic_symbol[0] + self.Particle.ionic_symbol[-1]
            )

        self._shot = shot
        self._server = server
        self._tokamak = "AUG"
        self.plate = plate
        self._connection = mds.Connection(self._server)
        # defining the positions of the probes
        self.OuterTarget = {
            "ua1": {"R": 1.582, "z": -1.199, "s": 1.045},
            "ua2": {"R": 1.588, "z": -1.175, "s": 1.070},
            "ua3": {"R": 1.595, "z": -1.151, "s": 1.094},
            "ua4": {"R": 1.601, "z": -1.127, "s": 1.126},
            "ua5": {"R": 1.608, "z": -1.103, "s": 1.158},
            "ua6": {"R": 1.614, "z": -1.078, "s": 1.189},
            "ua7": {"R": 1.620, "z": -1.054, "s": 1.213},
            "ua8": {"R": 1.627, "z": -1.030, "s": 1.246},
            "ua9": {"R": 1.640, "z": -0.982, "s": 1.276},
        }

        self.OuterLimiter = {
            "uaa": {"R": 1.659, "z": -0.936, "s": 1.326},
            "uab": {"R": 1.674, "z": -0.909, "s": 1.356},
            "uac": {"R": 1.689, "z": -0.884, "s": 1.387},
            "uad": {"R": 1.729, "z": -0.843, "s": 1.447},
            "uae": {"R": 1.786, "z": -0.788, "s": 1.526},
            "uaf": {"R": 1.857, "z": -0.707, "s": 1.634},
            "uag": {"R": 1.899, "z": -0.660, "s": 1.696},
            "uah": {"R": 1.963, "z": -0.587, "s": 1.793},
        }

        self.InnerTarget = {
            "ui9": {"R": 1.288, "z": -0.959, "s": 0.339},
            "ui8": {"R": 1.281, "z": -0.993, "s": 0.373},
            "ui7": {"R": 1.275, "z": -1.011, "s": 0.391},
            "ui6": {"R": 1.269, "z": -1.029, "s": 0.411},
            "ui5": {"R": 1.262, "z": -1.047, "s": 0.429},
            "ui4": {"R": 1.256, "z": -1.065, "s": 0.448},
            "ui3": {"R": 1.250, "z": -1.083, "s": 0.468},
            "ui2": {"R": 1.244, "z": -1.101, "s": 0.486},
            "ui1": {"R": 1.238, "z": -1.119, "s": 0.505},
        }

        self.InnerLimiter = {
            "uig": {"R": 1.151, "z": -0.671, "s": 0.012},
            "uif": {"R": 1.176, "z": -0.710, "s": 0.059},
            "uie": {"R": 1.198, "z": -0.744, "s": 0.099},
            "uid": {"R": 1.233, "z": -0.798, "s": 0.163},
            "uic": {"R": 1.267, "z": -0.858, "s": 0.230},
            "uib": {"R": 1.281, "z": -0.902, "s": 0.278},
            "uia": {"R": 1.286, "z": -0.929, "s": 0.309},
        }

        self.Dome = {
            "um1": {"R": 1.297, "z": -1.084, "s": 0.617},
            "um2": {"R": 1.344, "z": -1.062, "s": 0.673},
            "um3": {"R": 1.372, "z": -1.062, "s": 0.701},
            "um4": {"R": 1.405, "z": -1.062, "s": 0.732},
            "um5": {"R": 1.429, "z": -1.062, "s": 0.757},
            "um6": {"R": 1.457, "z": -1.062, "s": 0.786},
            "um7": {"R": 1.508, "z": -1.119, "s": 0.862},
            "um8": {"R": 1.537, "z": -1.153, "s": 0.906},
        }

        self.UpperOuter = {
            "oa5": {"R": 1.637, "z": 1.139},
            "oa4": {"R": 1.606, "z": 1.152},
            "oa3": {"R": 1.574, "z": 1.164},
            "oa2": {"R": 1.544, "z": 1.176},
            "oa1": {"R": 1.515, "z": 1.188},
        }

        self.UpperInner = {
            "oi1": {"R": 1.477, "z": 1.172},
            "oi2": {"R": 1.460, "z": 1.162},
            "oi3": {"R": 1.443, "z": 1.153},
            "oi4": {"R": 1.427, "z": 1.144},
            "oi5": {"R": 1.410, "z": 1.135},
            "oi6": {"R": 1.377, "z": 1.116},
            "oi7": {"R": 1.360, "z": 1.107},
            "oi8": {"R": 1.344, "z": 1.098},
        }

        if self.plate == "Outer Target":
            self.signal = self.OuterTarget
        elif self.plate == "Outer Limiter":
            self.signal = self.OuterLimiter
        elif self.plate == "Dome":
            self.signal = self.Dome
        elif self.plate == "Inner Limiter":
            self.signal = self.InnerLimiter
        elif self.plate == "Inner Target":
            self.signal = self.InnerTarget
        elif self.plate == "Upper Outer":
            self.signal = self.UpperOuter
        elif self.plate == "Upper Inner":
            self.signal = self.UpperInner
        else:
            print("Plate not found assuming Outer Target")
            self.signal = self.OuterTarget
        self._loadsignal()
        self._loadauxiliarydata()
        if not equilibrium:
            self._loadeq()
        else:
            self.Eq = equilibrium

    def _loadsignal(self):
        """
        Hidden method to read the data of density and temperature time and incident
        angle useful for the computation of total ion flux
        :return: none but define attribute to the class
        """

        self.Ne = np.asarray(
            [
                self._connection.get(
                    'augdiag({}, "LSD",'.format(self._shot) + '"ne-' + key + '")'
                ).data()
                for key in list(self.signal)
            ]
        )
        self.Te = np.asarray(
            [
                self._connection.get(
                    'augdiag({}, "LSD",'.format(self._shot) + '"te-' + key + '")'
                ).data()
                for key in list(self.signal)
            ]
        )

        self.angle = np.asarray(
            [
                self._connection.get(
                    'augdiag({}, "LSD",'.format(self._shot) + '"ang-' + key + '")'
                ).data()
                for key in self.signal.keys()
            ]
        )

        self.time = self._connection.get(
            'dim_of(augdiag({},"LSD",'.format(self._shot)
            + '"ne-'
            + list(self.signal)[0]
            + '"))'
        ).data()

    def _loadauxiliarydata(self):
        """
        Method to load auxiliary data to perform ELM filtering
        :return: outer target Ipolsola signal defined as attribute to the class
        """

        self._rawipol = -(
            self._connection.get(
                'augdiag({}, "Mac", "Ipolsola")'.format(self._shot)
            ).data()
            / 1e3
        )
        self._rawipoltime = self._connection.get(
            'dim_of(augdiag({}, "Mac", "Ipolsola"))'.format(self._shot)
        ).data()

    def _loadeq(self):
        """
        Load the equilibrium through the eqtools classes 
        """
        self.Eq = eqtools.AUGMDSTree(self._shot, server=self._server, tspline=True)

    def TotalIonFlux(self, npoint=500):
        """
        Compute the total ion flux does not produce an ELM filtering but rather
        a moving median average over the number of points give
        :param npoint: Number of point for moving median done using bottleneck class
        :return: The Total Ion flux as a function of time both the moving median and the raw signal
        """

        _cs = np.sqrt(constants.e * 4 * self.Te / (2 * constants.m_p))
        # this is the ion flux
        _s = self.Ne * _cs * np.abs(np.sin(np.radians(self.angle)))
        # now we compute the total integrate ion flux
        _rawTotalIonFlux = np.zeros(self.time.size)
        for i in range(self.time.size):
            _x = np.asarray([self.signal[k]["s"] for k in self.signal.keys()])
            _r = np.asarray([self.signal[k]["R"] for k in self.signal.keys()])
            _y = _s[:, i]
            _dummy = np.vstack((_x, _y)).transpose()
            _dummy = _dummy[~np.isnan(_dummy).any(1)]
            _x = _dummy[:, 0]
            _y = _dummy[:, 1][np.argsort(_x)]
            _x = np.sort(_x)
            _rawTotalIonFlux[i] = 2 * np.pi * _r.mean() * np.trapz(_y, x=_x)
        _mMedianIonFlux = bottleneck.move_median(_rawTotalIonFlux, npoint)

        return _mMedianIonFlux, _rawTotalIonFlux

    def getprofile(
        self, trange=[2.0, 2.3], interelm=False, check=False, abscissa="rhop", **kwargs
    ):
        """
        Compute the profile in the given time range by properly determining the
        values of abscissa through eqtools. We can provide the abscissa keyword
        :param trange: time range
        :param interelm: If set then compute the interelm profiles based on the function elm_detection
        :param check: Boolean, default is False. If True plot Ipolsola with chosen time windows for inter-ELM
        :param abscissa: String to be passed to eqtools for coordinate conversion. Default values is "rho" for
            normalized poloidal flux but could be as well as Rmid for upstream remapped distance to the
            separatrix. Rmid not yet implemented in eqtools
        :param **kwargs: keywords argument which need to be passed to elm_detection
        :return: An ordered Dictionary with rho, density and temperature
        """
        # first of all filter the corresponding times
        _idxtime = np.where((self.time >= trange[0]) & (self.time <= trange[1]))[0]
        _neprof, _teprof = self.Ne[:, _idxtime], self.Te[:, _idxtime]
        # now compute the correct rho poloidal
        _abscissa = np.zeros((self.Ne.shape[0], _idxtime.size))
        for _idx, key in enumerate(self.signal.keys()):
            if abscissa == "rho":
                _abscissa[_idx, :] = self.Eq.rz2psinorm(
                    self.signal[key]["R"], self.signal[key]["z"], self.time[_idxtime]
                )
            else:
                warnings.warn("Rmid not yet implemented in eqtools using rhop")
                _abscissa[_idx, :] = self.Eq.rz2psinorm(
                    self.signal[key]["R"], self.signal[key]["z"], self.time[_idxtime]
                )

        if interelm:
            try:
                _idxipol = np.logical_and(
                    self._rawipoltime >= trange[0], self._rawipoltime <= trange[1]
                )
                _xElm, _yElm = self._rawipoltime[_idxipol], self._rawipol[_idxipol]
                ED = elm_detection.elm_detection(
                    _xElm,
                    _yElm,
                    rho=kwargs.get("rho", 0.92),
                    width=kwargs.get("width", 0.2),
                    t_sep=kwargs.get("t_sep", 0.002),
                    mode=kwargs.get("mode", "fractional"),
                    mtime=kwargs.get("mtime", 500),
                    hwidth=kwargs.get("hwidth", 8),
                    dnpoint=kwargs.get("dnpoint", 5),
                )
                ED.run()
                elm_range = kwargs.get("elm_range", [0.7, 0.9])
                maskElm = ED.filter_signal(self.time[_idxtime], trange, elm_range)
                logging.warning("Computing inter-ELM profiles")
            except:
                # for some reason does not work for a time window below 0.2 seconds
                # so in case of failure I increas the time window and then drop the elm_times and
                # elm_dsep outside of he time window
                logging.warning("Need to increase the time window")
                _tCenter = np.asarray(trange).mean()
                _idxipol = np.logical_and(
                    self._rawipoltime >= _tCenter - 0.1,
                    self._rawipoltime <= _tCenter + 0.1,
                )
                _xElm, _yElm = self._rawipoltime[_idxipol], self._rawipol[_idxipol]
                ED = elm_detection.elm_detection(
                    _xElm,
                    _yElm,
                    rho=kwargs.get("rho", 0.92),
                    width=kwargs.get("width", 0.2),
                    t_sep=kwargs.get("t_sep", 0.002),
                    mode=kwargs.get("mode", "fractional"),
                    mtime=kwargs.get("mtime", 500),
                    hwidth=kwargs.get("hwidth", 8),
                    dnpoint=kwargs.get("dnpoint", 5),
                )
                ED.run()
                elm_range = kwargs.get("elm_range", [0.7, 0.9])
                maskElm = ED.filter_signal(self.time[_idxtime], trange, elm_range)

            if check:
                fig, ax = mpl.pylab.subplots(figsize=(7, 4), nrows=1, ncols=1)
                ax.plot(_xElm, _yElm, lw=1.5, color="k")
                for _t, _dt in zip(ED.elm_times, ED.elm_sep):
                    ax.axvline(_t, ls="--", color="k", lw=2)
                    ax.axvspan(
                        _t - (1 - elm_range[0]) * _dt,
                        _t - (1 - elm_range[1]) * _dt,
                        color="gray",
                        alpha=0.5,
                    )
                    ax.set_title(r"#{}".format(self._shot))
                    ax.set_xlabel(r"t [s]")
        else:
            maskElm = np.ones(_idxtime.size, dtype=bool)

        Out = OrderedDict(
            [
                ("x", _abscissa[:, maskElm].ravel()),
                ("ne", _neprof[:, maskElm].ravel()),
                ("te", _teprof[:, maskElm].ravel()),
            ]
        )
        return Out

    def getLambda(self, inDictionary, time, nbins=10, inLparallel=None, **kwargs):
        """
        :param inDictionary: receive in input the OrderedDictionary given as output from get profile
        :param time: time instants used for the computation of the parallel connection length
        :param nbins: Number of bins in rho used to average the profiles at the target. Default is 10
        :param inLparallel: It is possible to provide directly the Lparallel connection length
            in a form of Dictionary with keys (rho, lpar, lparX). Default is None
        :param kwargs: keyword argument which can be passed to bin_by (for example for clipping)
        :return: Ordered Dictionary containing values of LambdaDiv, Lambda, parallel connection length
            from midplane to target and from x-point to target, Binning data of Ne and Te, Error on Lambda Div
            and corresponding error
        """

        # first of all compute the binning procedure for rho >= 1
        outNe = bin_by.bin_by(
            inDictionary["x"], inDictionary["ne"], nbins=nbins, mn=1, **kwargs
        )
        outTe = bin_by.bin_by(
            inDictionary["x"], inDictionary["te"], nbins=nbins, mn=1, **kwargs
        )
        _neObject = interpolate.interp1d(
            outNe["x"][np.isfinite(outNe['y_median'])], outNe["y_median"][np.isfinite(outNe['y_median'])], fill_value="extrapolate"
        )
        _teObject = interpolate.interp1d(
            outTe["x"][np.isfinite(outTe['y_median'])], outTe["y_median"][np.isfinite(outTe['y_median'])], fill_value="extrapolate"
        )
        if not inLparallel:
            # now compute the parallel connection length
            rho, lpar, lparX = self._computeLparallel(time)
        else:
            rho, lpar, lparX = (
                inLparallel["rho"],
                inLparallel["lpar"],
                inLparallel["lparX"],
            )
        # limit the rho for Lparallel to the region 1< rho < rho.max() of outNe
        mask = np.logical_and(rho >= 1, rho <= np.nanmax(outNe["x"]))
        rho, lpar, lparX = rho[mask], lpar[mask], lparX[mask]
        # compute the profiles of Te and ne on rho basis
        _teProf = _teObject(rho)
        _neProf = _neObject(rho)
        # mask for possible negative values
        mask = np.logical_and(_teProf > 0, _neProf > 0)
        rho, _teProf, _neProf, lpar, lparX = rho[mask], _teProf[mask], _neProf[mask], lpar[mask], lparX[mask]
        # compute now the collision frequency
        _collisionFreq = collisions.fundamental_electron_collision_freq(
            _teProf * units.eV,
            _neProf * units.m ** -3,
            ion_particle=self._ionicsymbol,
        )
        # then we need the computation of the ion sound speed
        _Cs = parameters.ion_sound_speed(
            T_e= _teProf * units.eV,
            T_i= _teProf * units.eV,
            ion=self._ionicsymbol,
        )
        Lambda = (
            (lpar * _collisionFreq.value / _Cs.value)
            * self.Particle.integer_charge
            * constants.m_e
            / self.Particle.mass.value
        )

        LambdaDiv = (
            (lparX * _collisionFreq.value / _Cs.value)
            * self.Particle.integer_charge
            * constants.m_e
            / self.Particle.mass.value
        )

        Out = OrderedDict(
            [
                ("teDict", outTe),
                ("neDict", outNe),
                ("Lpar", lpar),
                ("LparX", lparX),
                ("rho_Lpar", rho),
                ("Lambda", Lambda),
                ("LambdaDiv", LambdaDiv),
            ]
        )
        return Out

    def _computeLparallel(self, time):
        """
        Compute the parallel connection length from midplane to the target as well as from X-point to the
        target at a given time
        :param time: Time where Lparallel needs to be computed
        :retur out: Tuple with rho, LparMidplane, LparX
        """

        myTracer = get_fieldline_tracer(
            "RK4", machine=self._tokamak, shot=self._shot, time=time, eqobject=self.Eq
        )
        zMAxis = myTracer.eq.axis.z
        # height of Xpoint
        zXPoint = myTracer.eq.xpoints["xpl"].z
        rXPoint = myTracer.eq.xpoints["xpl"].r
        # now determine at the height of the zAxis the R of the LCFS
        _idTime = np.argmin(np.abs(self.Eq.getTimeBase() - time))
        RLcfs = self.Eq.getRLCFS()[_idTime, :]
        ZLcfs = self.Eq.getZLCFS()[_idTime, :]
        # onlye the part greater then xaxis
        ZLcfs = ZLcfs[RLcfs > myTracer.eq.axis.r]
        RLcfs = RLcfs[RLcfs > myTracer.eq.axis.r]
        Rout = RLcfs[np.argmin(np.abs(ZLcfs[~np.isnan(ZLcfs)] - zMAxis))]
        rmin = np.linspace(Rout + 0.001, 2.19, num=30)
        # this is R-Rsep
        rMid = rmin - Rout
        # this is Rho
        rho = self.Eq.rz2psinorm(rmin, np.repeat(zMAxis, rmin.size), time, sqrt=True)
        # now create the Traces and lines
        fieldLines = [
            myTracer.trace(r, zMAxis, mxstep=100000, ds=1e-2, tor_lim=20.0 * np.pi)
            for r in rmin
        ]
        fieldLinesZ = [
            line.filter(["R", "Z"], [[rXPoint, 2], [-10, zXPoint]])
            for line in fieldLines
        ]
        Lpar = np.array([])
        for line in fieldLinesZ:
            try:
                _dummy = np.abs(line.S[0] - line.S[-1])
            except:
                _dummy = np.nan
            Lpar = np.append(Lpar, _dummy)

        # compute also the other one
        LparAll = np.array([])
        for line in fieldLines:
            try:
                _dummy = np.abs(line.S[0] - line.S[-1])
            except:
                _dummy = np.nan
            LparAll = np.append(LparAll, _dummy)

        return rho, LparAll, Lpar
