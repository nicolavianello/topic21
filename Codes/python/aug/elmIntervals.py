from scipy.interpolate import UnivariateSpline
import numpy as np
from scipy.signal import savgol_filter
import matplotlib as mpl
import logging
try:
    import MDSplus as mds
except ImportError as error:
    print('MDSplus class not find')
    pass
try:
    import dd
except ImportError as error:
    print('dd class not find')
    pass

class elmIntervals(object):
    """
    Class for simple determination of the ELM and interELM intervals in a
    given time windows. Should work both remotely and on toki
    """

    def __init__(self, shot, remote=True,usedd=False):
        """

        :param shot: Shot Numeber
        :param remote: Boolean in order to read remotely or on toki. Default is True
        :param usedd: Boolen in order to used the save dd inter ELM or to determine according to threshold
        """

        self.shot = shot
        self.remote = remote
        self.usedd = usedd

        if self.remote:
            print('Remember to open an MDSplus tunnel on port 8001')
            self.usedd = False

        self._read()

    def _read(self):

        if self.remote:
            conn = mds.Connection('localhost:8001')
            _s = 'augdiag(' + str(self.shot) + ',"MAC","Ipolsola")'
            _st = 'dim_of(augdiag(' + str(self.shot) + ',"MAC","Ipolsola"))'
            self.Ipol = -conn.get(_s).data()
            self.t = conn.get(_st).data()
        else:
            if self.usedd:
                logging.warning("Using ELM dda")
                ELM = dd.shotfile("ELM", self.shot, experiment='AUGD')
                elmd = ELM("t_endELM", tBegin=trange[0], tEnd=trange[1])
                # limit to the ELM included in the trange
                _idx = np.where((elmd.time>= trange[0]) & (elmd.time <= trange[1]))[0]
                self.tBegElm = eldm.time[_idx]
                self.tEndElm = elmd.data[_idx]
                ELM.close()
            else:
                logging.warning("Using IpolSolI")
                Mac = dd.shotfile("MAC", self.shot, experiment='AUGD')
                Ipol = Mac('Ipolsola')
                self.Ipol = -Ipol.data
                self.t = Ipol.time


    def _maskElm(self, threshold=3000, trange=[2, 3],
                 check=False):
        """
        Provide an appropriate mask where we identify
        both the ELM and inter-ELM regime

        Parameters
        ----------
        threshold : :obj: `float`
            If we choose to detect as threshold in the
            SOL current then this is the threshold chosen
        Returns
        -------
        None

        Attributes
        ----------
        Define the class hidden attributes
        self._elm
        self._interelm
        which are the indexes of the ELM and inter
        ELM intervals
        """

        if not self.usedd:
            _idxT = np.where(((self.t >= trange[0]) & (self.t <= trange[1])))[0]
            # now create an appropriate savgolfile
            IpolS = savgol_filter(self.Ipol[_idxT], 301, 3)
            IpolT = self.t[_idxT]
            IpolO = self.Ipol[_idxT]
            # which can be set as also set as keyword
            self._Elm = np.where(IpolS > threshold)
            # generate a fake interval
            ElmMask = np.zeros(_idxT.size, dtype='bool')
            ElmMask[self._Elm] = True
            self._interElm = np.where(ElmMask == False)[0]
            if check:
                fig, ax = mpl.pylab.subplots(nrows=1, ncols=1, figsize=(6, 4))
                fig.subplots_adjust(bottom=0.15, left=0.15)
                ax.plot(IpolT, IpolO, color='gray', alpha=0.5)
                ax.plot(IpolT, IpolS, 'k', lw=1.2, alpha=0.5)
                ax.plot(IpolT[self._interElm], IpolS[self._interElm], 'g', lw=1.5)
                ax.set_xlabel(r't[s]')
                ax.set_ylabel(r'Ipol SOL I')
                ax.axhline(threshold, ls='--', color='#d62728')