from scipy.interpolate import UnivariateSpline
import numpy as np
from scipy.signal import savgol_filter
from general import elm_detection, bin_by
import matplotlib as mpl
from collections import OrderedDict
import logging
import warnings

try:
    import dd
except ModuleNotFoundError:
    warnings.warn("dd module not imported can only work with MDSplus")
try:
    import MDSplus as mds
except ModuleNotFoundError:
    warnings.warn("MDSplus module not worked ")


class Libes(object):
    """
    Class to deal with Li-beam data. It compute the Li-Beam on
    an uniform rho-Grid and time,  and save as attributes
    the value normalize to the value at the separatrix
    
    """

    def __init__(
        self, shot, remote=False, experiment="AUGD", edition=0, server="localhost:8000"
    ):
        self._shot = shot
        self.remote = remote

        if self.remote:
            self._server = server
        else:
            self._servr = "mdsplus.aug.ipp.mpg.de"
        self._connection = mds.Connection(self._server)
        self._edition = edition
        self._experiment = experiment
        self._loaddata()
        # self.eq = eqtools.AUGDDData(self.shot)
        # self.eq.remapLCFS()

    def _loaddata(self):

        _s = (
            'augdiag({}, "LIN", "ne"'.format(self._shot)
            + ', "'
            + self._experiment
            + '", {})'.format(self._edition)
        )
        _st = "dim_of(" + _s + ")"
        _sr = "dim_of(" + _s + ", 1)"
        self.neraw = self._connection.get(_s).data().transpose()[:, ::-1] / 1e19
        self.rhoraw = self._connection.get(_sr).data().transpose()[:, ::-1]
        self.time = self._connection.get(_st).data()
        # load also the modelled light to check the correctness of the profile
        _s = (
            'augdiag({}, "LIN", "lib_mod"'.format(self._shot)
            + ', "'
            + self._experiment
            + '", {})'.format(self._edition)
        )
        self.lightraw = self._connection.get(_s).data().transpose()[:,::-1]

    def _loadauxiliarydata(self):
        """
        Method to load auxialy data to perform ELM filtering
        :return: outer target Ipolsola signal defined as attribute to the class
        """

        self._rawipol = -(
            self._connection.get(
                'augdiag({}, "Mac", "Ipolsola")'.format(self._shot)
            ).data()
            / 1e3
        )
        self._rawipoltime = self._connection.get(
            'dim_of(augdiag({}, "Mac", "Ipolsola"))'.format(self._shot)
        ).data()

    def averageProfile(self, trange=[2, 3], interelm=False, check=False, **kwargs):
        """
        Compute average profiles eventually inter-ELM based on elm_detection class on Ipolsola
        :param trange: trange where profiles should be evaluated
        :param interelm: boolean, if True compute the inter-ELM profiles. Default is False
        :param check: Boolean. Provide a check of the quality of inter-ELM determination
        :param kwargs: all the keyword arguments to be passed to elm_detection class
        :return: OrderedDictionary containing the profiles the errors and the raw data
        """

        _idx = np.where((self.time >= trange[0]) & (self.time <= trange[1]))[0]
        if interelm:
            self._loadauxiliarydata()
            _augidx = np.logical_and(
                self._rawipoltime >= trange[0], self._rawipoltime <= trange[1]
            )
            _xElm, _yElm = self._rawipoltime[_augidx], self._rawipol[_augidx]
            ED = elm_detection.elm_detection(
                _xElm,
                _yElm,
                rho=kwargs.get("rho", 0.92),
                width=kwargs.get("width", 0.2),
                t_sep=kwargs.get("t_sep", 0.002),
                mode=kwargs.get("mode", "fractional"),
                mtime=kwargs.get("mtime", 500),
                hwidth=kwargs.get("hwidth", 8),
                dnpoint=kwargs.get("dnpoint", 5),
            )
            ED.run()
            elm_range = kwargs.get("elm_range", [0.7, 0.9])
            maskElm = ED.filter_signal(self.time[_idx], trange, elm_range)
            logging.warning("Computing inter-ELM profiles")
            if check:
                fig, ax = mpl.pylab.subplots(figsize=(7, 4), nrows=1, ncols=1)
                ax.plot(_xElm, _yElm, lw=1.5, color="k")
                for _t, _dt in zip(ED.elm_times, ED.elm_sep):
                    ax.axvline(_t, ls="--", color="k", lw=2)
                    ax.axvspan(
                        _t - (1 - elm_range[0]) * _dt,
                        _t - (1 - elm_range[1]) * _dt,
                        color="gray",
                        alpha=0.5,
                    )
                    ax.set_title(r"#{}".format(self._shot))
                    ax.set_xlabel(r"t [s]")
        else:
            maskElm = np.ones(_idx.size,dtype=bool)
        profiles = np.nanmean(self.neraw[_idx[maskElm], :], axis=0)
        rho = np.nanmean(self.rhoraw[_idx[maskElm], :], axis=0)
        error = np.nanstd(self.neraw[_idx[maskElm], :], axis=0)
        error_X = np.nanstd(self.rhoraw[_idx[maskElm], :], axis=0)
        # interpolation weighted on the error
        # Rmid = self.eq.rho2rho("sqrtpsinorm", "Rmid", rho, self.time[_idx].mean())
        # #
        # S = UnivariateSpline(Rmid, profiles, w=1.0 / error, s=0)
        # Efold = np.abs(S(Rmid) / S.derivative()(Rmid))
        Out = OrderedDict(
            [
                ("ne", profiles),
                ("err_Y", error),
                ("rho", rho),
                ("err_X", error_X),
                ("neraw", self.neraw[_idx[maskElm], :].ravel()),
                ("rhoraw", self.rhoraw[_idx[maskElm], :].ravel()),
                ("lightraw", self.lightraw[_idx[maskElm], :].ravel()),
            ]
        )
        return Out

    def _oldamplitudeShoulder(self, dt=0.1, reference=[1.5, 1.6], start=1.6, **kwargs):
        """
        Compute the amplitude and location of the shoulder defined as
        the difference between normalized profiles in the SOL w.r.t.
        a reference profile. It also compute the location in term of
        normalized poloidal flux and Rmid of the maximum of the shoulder

        Parameters
        ----------
        dt : floating
            resolution in [s]. If given compute the evolution with the
            given time resolution

        reference : 2D floating 
            It contains the time interval computed for the reference
            profile

        start : floating
            Starting point for the evaluation of the amplitude 

        kwargs : 
            These are the keyowords which can be passed to average profiles
            in order to eventually compute the interELM behavior

        Returns
        -------
        None

        Attributes
        ----------
        Define the following attributes to the class
        Amplitude :
            Amplitude as normalized difference with respect to the reference profile
            computed during the time interval defined in reference
        Location :
            The Location as a function of rho of the maximum of the normalized difference
        Efold :
            Efolding as a function of rho and time
        rhoAmplitude:
            The rho values of the normalized difference (defined for rho >= 1)
        
        """
        # first compute the normalized reference profile
        _, _, _, pN, eN = self.averageProfile(trange=reference, **kwargs)
        # limit to the region for rho > 1
        _idx = np.where((self.time > start))[0]
        _npoint = int(np.floor((self.time[_idx].max() - self.time[_idx].min()) / dt))
        # we use array split
        _dummy = self.neNorm[np.where(self.time > start)[0], :]
        Split = np.asarray(np.array_split(_dummy, _npoint, axis=0))
        Amplitude = np.asarray([np.nanmean(p, axis=0) - pN for p in Split])
        self.timeAmplitude = np.nanmean(
            np.asarray(np.array_split(self.time[_idx], _npoint))
        )
        self.Amplitude = Amplitude[:, np.where(self.rho > 1)]
        self.Location = np.max(self.Amplitude, axis=1)
        self.rhoAmplitude = self.rho[self.rho >= 1]

    def _oldmaskElm(self, usedda=False, threshold=3000, trange=[2, 3], check=False):
        """
        Provide an appropriate mask where we identify
        both the ELM and inter-ELM regime

        Parameters
        ----------
        usedda : :obj: `bool`
            Boolean, if True use the default ELM
            diagnostic ELM in the shotfile

        threshold : :obj: `float`
            If we choose to detect as threshold in the
            SOL current then this is the threshold chosen
        Returns
        -------
        None

        Attributes
        ----------
        Define the class hidden attributes
        self._elm
        self._interelm
        which are the indexes of the ELM and inter
        ELM intervals
        """

        if usedda:
            logging.warning("Using ELM dda")
            ELM = dd.shotfile("ELM", self.shot, experiment="AUGD")
            elmd = ELM("t_endELM", tBegin=trange[0], tEnd=trange[1])
            # limit to the ELM included in the trange
            _idx = np.where((elmd.time >= trange[0]) & (elmd.time <= trange[1]))[0]
            self.tBegElm = eldm.time[_idx]
            self.tEndElm = elmd.data[_idx]
            ELM.close()
        else:
            logging.warning("Using IpolSolI")
            Mac = dd.shotfile("MAC", self.shot, experiment="AUGD")
            Ipol = Mac("Ipolsoli")
            _idxT = np.where(((Ipol.time >= trange[0]) & (Ipol.time <= trange[1])))[0]
            # now create an appropriate savgolfile
            IpolS = savgol_filter(Ipol.data[_idxT], 301, 3)
            IpolT = Ipol.time[_idxT]
            IpolO = Ipol.data[_idxT]
            # we generate an UnivariateSpline object
            _dummyTime = self.time[
                np.where((self.time >= trange[0]) & (self.time <= trange[1]))[0]
            ]
            IpolSp = UnivariateSpline(IpolT, IpolS, s=0)(_dummyTime)
            # on these we choose a threshold
            # which can be set as also set as keyword
            self._Elm = np.where(IpolSp > threshold)
            # generate a fake interval
            ElmMask = np.zeros(IpolSp.size, dtype="bool")
            ElmMask[self._Elm] = True
            self._interElm = np.where(ElmMask == False)[0]
            if check:
                fig, ax = mpl.pylab.subplots(nrows=1, ncols=1, figsize=(6, 4))
                fig.subplots_adjust(bottom=0.15, left=0.15)
                ax.plot(IpolT, IpolO, color="gray", alpha=0.5)
                ax.plot(IpolT, IpolS, "k", lw=1.2, alpha=0.5)
                ax.plot(_dummyTime[self._interElm], IpolSp[self._interElm], "g", lw=1.5)
                ax.set_xlabel(r"t[s]")
                ax.set_ylabel(r"Ipol SOL I")
                ax.axhline(threshold, ls="--", color="#d62728")
