import numpy as np
from general import elm_detection, bin_by
import MDSplus as mds
import logging
import matplotlib as mpl
import eqtools
from collections import OrderedDict
import gpr1dfusion


class Thomson(object):
    def __init__(
        self, shot, server="localhost:8000", location="edge", equilibrium=None
    ):
        """
        Class to load Vertical Thomson data and remap properly on rho
        :param shot: Shot number
        :param server: MDSplus server. On AUG cluster just use mdsplus.aug.mpg.de otherwise
            implement an MDSplus tunnel
        :param location: String indicating if we use 'edge' or 'core' thomson data
        :param equilibrium: If None then load the appropriate equilibrium through eqtools otherwise
            an equilibrium object can be passed directly
        """

        self.shot = shot
        self.server = server
        self._connection = mds.Connection(server)
        self._location = location
        if not equilibrium:
            self._equilibrium = eqtools.AUGMDSTree(
                self.shot, server=self.server, tspline=True
            )
        else:
            self._equilibrium = equilibrium

        self._loadsignal()
        self._remap()
        self._loadauxiliarydata()

    def _loadsignal(self):
        """
        Hidden method to read the data of density and temperature from the corresponding
        thomson together with the appropriate time basis
        :return: none but define attribute to the class
        """

        if self._location == "edge":
            Ne = self._connection.get(
                'augdiag({},"VTA","Ne_e"'.format(self.shot) + ")"
            ).data()
            Te = self._connection.get(
                'augdiag({},"VTA","Te_e"'.format(self.shot) + ")"
            ).data()
            R = self._connection.get(
                'augdiag({},"VTA","R_edge"'.format(self.shot) + ")"
            ).data()
            self.Z = self._connection.get(
                'augdiag({},"VTA","Z_edge"'.format(self.shot) + ")"
            ).data()
            time = self._connection.get(
                'dim_of(augdiag({},"VTA","Ne_e"'.format(self.shot) + "),0)"
            )
            dNe = self._connection.get(
                'augdiag({},"VTA","SigNe_e"'.format(self.shot) + ")"
            ).data()
            dTe = self._connection.get(
                'augdiag({},"VTA","SigTe_e"'.format(self.shot) + ")"
            ).data()

        else:
            Ne = self._connection.get(
                'augdiag({},"VTA","Ne_c"'.format(self.shot) + ")"
            ).data()
            Te = self._connection.get(
                'augdiag({},"VTA","Te_c"'.format(self.shot) + ")"
            ).data()
            R = self._connection.get(
                'augdiag({},"VTA","R_core"'.format(self.shot) + ")"
            ).data()
            self.Z = self._connection.get(
                'augdiag({},"VTA","Z_core"'.format(self.shot) + ")"
            ).data()
            time = self._connection.get(
                'dim_of(augdiag({},"VTA","Ne_c"'.format(self.shot) + "),0)"
            )
            dNe = self._connection.get(
                'augdiag({},"VTA","SigNe_c"'.format(self.shot) + ")"
            ).data()
            dTe = self._connection.get(
                'augdiag({},"VTA","SigTe_c"'.format(self.shot) + ")"
            ).data()

        # limit to the available time in equilibrium
        _idxtime = np.logical_and(
            time >= self._equilibrium.getTimeBase().min(),
            time <= self._equilibrium.getTimeBase().max(),
        )
        self.Ne = Ne[:, _idxtime]
        self.Te = Te[:, _idxtime]
        self.dNe = dNe[:, _idxtime]
        self.dTe = dTe[:, _idxtime]
        self.R = R[_idxtime]
        self.time = time[_idxtime]

    def _remap(self):
        """
        Map the R,Z into rho Poloidal
        :return:
        """
        self.rhoMesh = np.zeros((self.Z.size, self.R.size))
        # find the unique values of R which are just 6
        rUnique = np.unique(self.R)
        for r in rUnique:
            _dd = np.where(self.R == r)[0]
            self.rhoMesh[:, _dd] = self._equilibrium.rz2psinorm(
                np.repeat(r, self.Z.size),
                self.Z,
                self.time[_dd],
                each_t=True,
                sqrt=True,
            ).transpose()

    def _loadauxiliarydata(self):
        """
        Method to load auxiliary data to perform ELM filtering
        :return: outer target Ipolsola signal defined as attribute to the class
        """

        self._rawipol = -(
            self._connection.get(
                'augdiag({}, "Mac", "Ipolsola")'.format(self.shot)
            ).data()
            / 1e3
        )
        self._rawipoltime = self._connection.get(
            'dim_of(augdiag({}, "Mac", "Ipolsola"))'.format(self.shot)
        ).data()

    def getTSprofiles(self, trange=[2, 3], interelm=False, check=False, **kwargs):
        """

        :param trange: time range where profiles needs to be computed
        :param interelm: boolean, if True determine the inter-ELM
        :param kwargs: each of the keywords which can be passed to elm_detection method
        :return: rho, ne, te, dne, dte
        """

        _idxtime = np.where((self.time >= trange[0]) & (self.time <= trange[1]))[0]
        _neprof, _teprof, _abscissa, _dneprof, _dteprof = (
            self.Ne[:, _idxtime],
            self.Te[:, _idxtime],
            self.rhoMesh[:, _idxtime],
            self.dNe[:, _idxtime],
            self.dTe[:, _idxtime],
        )
        if interelm:
            try:
                _idxipol = np.logical_and(
                    self._rawipoltime >= trange[0], self._rawipoltime <= trange[1]
                )
                _xElm, _yElm = self._rawipoltime[_idxipol], self._rawipol[_idxipol]
                self.ED = elm_detection.elm_detection(
                    _xElm,
                    _yElm,
                    rho=kwargs.get("rho", 0.92),
                    width=kwargs.get("width", 0.2),
                    t_sep=kwargs.get("t_sep", 0.002),
                    mode=kwargs.get("mode", "fractional"),
                    mtime=kwargs.get("mtime", 500),
                    hwidth=kwargs.get("hwidth", 8),
                    dnpoint=kwargs.get("dnpoint", 5),
                )
                self.ED.run()
                elm_range = kwargs.get("elm_range", [0.7, 0.9])
                maskElm = self.ED.filter_signal(self.time[_idxtime], trange, elm_range)
                logging.warning("Computing inter-ELM profiles")
            except:
                # for some reason does not work for a time window below 0.2 seconds
                # so in case of failure I increas the time window and then drop the elm_times and
                # elm_dsep outside of he time window
                logging.warning("Need to increase the time window")
                _tCenter = np.asarray(trange).mean()
                _idxipol = np.logical_and(
                    self._rawipoltime >= _tCenter - 0.1,
                    self._rawipoltime <= _tCenter + 0.1,
                )
                _xElm, _yElm = self._rawipoltime[_idxipol], self._rawipol[_idxipol]
                self.ED = elm_detection.elm_detection(
                    _xElm,
                    _yElm,
                    rho=kwargs.get("rho", 0.92),
                    width=kwargs.get("width", 0.2),
                    t_sep=kwargs.get("t_sep", 0.002),
                    mode=kwargs.get("mode", "fractional"),
                    mtime=kwargs.get("mtime", 500),
                    hwidth=kwargs.get("hwidth", 8),
                    dnpoint=kwargs.get("dnpoint", 5),
                )
                self.ED.run()
                elm_range = kwargs.get("elm_range", [0.7, 0.9])
                maskElm = self.ED.filter_signal(self.time[_idxtime], trange, elm_range)
            if check:
                fig, ax = mpl.pylab.subplots(figsize=(7, 4), nrows=1, ncols=1)
                ax.plot(_xElm, _yElm, lw=1.5, color="k")
                for _t, _dt in zip(self.ED.elm_times, self.ED.elm_sep):
                    ax.axvline(_t, ls="--", color="k", lw=2)
                    ax.axvspan(
                        _t - (1 - elm_range[0]) * _dt,
                        _t - (1 - elm_range[1]) * _dt,
                        color="gray",
                        alpha=0.5,
                    )
                    ax.set_title(r"#{}".format(self.shot))
                    ax.set_xlabel(r"t [s]")
        else:
            maskElm = np.ones(_idxtime.size, dtype=bool)

        rho = _abscissa[:, maskElm].ravel()
        nE = _neprof[:, maskElm].ravel()
        te = _teprof[:, maskElm].ravel()
        dne = _dneprof[:, maskElm].ravel()
        dte = _dteprof[:, maskElm].ravel()
        mask = np.logical_and(dte/te < 0.5, np.logical_and(dne/nE < 0.5,np.logical_and(nE != 0, te != 0)))
        Out = OrderedDict(
            [
                ("rho", rho[mask][np.argsort(rho[mask])]),
                ("ne", nE[mask][np.argsort(rho[mask])]),
                ("te", te[mask][np.argsort(rho[mask])]),
                ("dne", dne[mask][np.argsort(rho[mask])]),
                ("dte", dte[mask][np.argsort(rho[mask])]),
            ]
        )
        return Out

    def computeshift(
        self,
        inDictionary,
        te_sep=100,
        kernel="GibbsInverseGaussianFGPR1D",
        npoint=100,
        check=False,
        nbins=None,
        clip=False,
        **kwargs,
    ):
        """
        Method to compute the shift in psinorm in order to get the te_sep shift on temperature
        provide as well the values of density at separatrix for 100 eV. This is done fitting the Te and ne
        with a gaussian process regression fit using the gpr1dfusion
        :param inDictionary: this is the output of the getTS profile
        :param te: values for te_sep. Default is 100 eV
        :param kernel: string indicating the appropriate kernel for gpr1dfusion. Available values are
            'GibbsInverseGaussianFGPR1D' [Default], 'RationalQuadraticFGPR1D', 'Matern52FGPR1D'
        :param npoint: number of point of the grid where fit is performed
        :param clip: Boolean to be passed eventually to binned_statistics
        :param **kwargs: Argument to be passed to gpr1dfusion
        :return: shift in psinorm, density at the separatrix
        """
        if not nbins:
            x = inDictionary["rho"]
            # fit of te
            y = inDictionary["te"]
            y_err = inDictionary["dte"]
            if y_err is None:
                y_err = 0.05 * y
            mask = np.logical_and(
                np.logical_and(np.isfinite(x), np.isfinite(y)), np.isfinite(y_err)
            )
            x, y, y_err = x[mask], y[mask], y_err[mask]
            if not "x_err" in inDictionary.keys():
                x_err = None
        else:
            outBinned = bin_by.binned_weighted_statistics(
                inDictionary["rho"], inDictionary["te"], inDictionary["dte"], nbins=nbins)
            x = outBinned['x']
            x_err = outBinned['x_err']
            y = outBinned['y']
            y_err = outBinned['y_err']
        xN = np.linspace(x.min(), x.max(), npoint)

        if kernel == "GibbsInverseGaussianFGPR1D":
            if x_err is None:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    x, y, y_err, **kwargs
                )
            else:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    x, y, y_err, xe=x_err, **kwargs
                )
        elif kernel == "RationalQuadraticFGPR1D":
            if x_err is None:
                gp = gpr1dfusion.RationalQuadraticFGPR1D(
                    x, y, y_err, **kwargs
                )
            else:
                gp = gpr1dfusion.RationalQuadraticFGPR1D(
                    x, y, y_err, xe=x_err, **kwargs
                )
        else:
            if x_err is None:
                gp = gpr1dfusion.Matern52FGPR1D(
                    x, y, y_err, **kwargs
                )

            else:
                gp = gpr1dfusion.Matern52FGPR1D(
                    x, y, y_err, xe=x_err, **kwargs
                )
        gp.GPRFit(xN)
        yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
        shift = xN[np.argmin(np.abs(yFit - te_sep))]

        if check:
            fig, ax = mpl.pylab.subplots(figsize=(6,8), nrows=2, ncols=1, sharex='col')
            ax[0].errorbar(x, y, yerr=y_err, fmt='o', color='orange', alpha=0.5)
            ax[0].plot(xN, yFit,'k--', lw=2)
            ax[0].fill_between(xN, yFit-yFitE,yFit+yFitE, color='gray', alpha=0.5)
            ax[0].set_ylabel(r'T$_e$ [eV]')
            ax[0].axhline(te_sep,ls='-.', color='red', lw=2)

        # now repeat the fir for ne and determine ne_sep
        if not nbins:
            y = inDictionary["ne"] / 1e19
            y_err = inDictionary["dne"] / 1e19
            if y_err is None:
                y_err = 0.05 * y
            mask = np.logical_and(
                np.logical_and(np.isfinite(x), np.isfinite(y)), np.isfinite(y_err)
            )
            x, y, y_err = x[mask], y[mask], y_err[mask]
        else:
            outBinned = bin_by.binned_weighted_statistics(
                inDictionary["rho"], inDictionary["ne"]/1e19, inDictionary["dne"]/1e19, nbins=nbins)
            x = outBinned['x']
            x_err = outBinned['x_err']
            y = outBinned['y']
            y_err = outBinned['y_err']
        if kernel == "GibbsInverseGaussianFGPR1D":
            if x_err is None:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    x, y, y_err, **kwargs
                )
            else:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    x, y, y_err, xe=x_err, **kwargs
                )
        elif kernel == "RationalQuadraticFGPR1D":
            if x_err is None:
                gp = gpr1dfusion.RationalQuadraticFGPR1D(
                    x, y, y_err, **kwargs
                )
            else:
                gp = gpr1dfusion.RationalQuadraticFGPR1D(
                    x, y, y_err, xe=x_err, **kwargs
                )
        else:
            if x_err is None:
                gp = gpr1dfusion.Matern52FGPR1D(
                    x, y, y_err,  **kwargs
                )

            else:
                gp = gpr1dfusion.Matern52FGPR1D(
                    x, y, y_err, xe=x_err, **kwargs
                )
        gp.GPRFit(xN)
        yFit, yFitE, dYrho, dYrhoE = gp.get_gp_results()
        ne_sep = yFit[np.argmin(np.abs(xN - shift))]
        shift = 1 - shift
        if check:
            ax[1].errorbar(x, y, yerr=y_err, fmt='o', color='orange', alpha=0.5)
            ax[1].plot(xN, yFit,'k--', lw=2)
            ax[1].fill_between(xN, yFit-yFitE,yFit+yFitE, color='gray', alpha=0.5)
            ax[1].set_ylabel(r'n$_e$ [10$^{19}$m$^{-3}$]')

        return shift, ne_sep

    def combineprofiles(self, edgeObject, coreObject):
        """

        :param edgeObject: output of getTSprofile from edge VTA
        :param coreObject: output of getTSprofile from core VTA
        :return: OrederedDict with combined edge and core profiles
        """

        Out = OrderedDict()
        for key in edgeObject.keys():
            Out[key] = np.append(edgeObject[key],coreObject[key])
        # sort them
        _isort = np.argsort(Out['rho'])
        for key in Out.keys():
            Out[key] = Out[key][_isort]
        return Out