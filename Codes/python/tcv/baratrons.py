from __future__ import print_function
import MDSplus as mds
import numpy as np
import xarray
import matplotlib as mpl

class pressure(object):

    def __init__(self, shot, remote=False):
        """
        Class to read the baratron and get the calibrated pressure
        For details see the baratron wiki-page in the SPC intranet
        Parameters
        ----------
        shot: shot number
        remote: boolean, default is False. If true it is assumed that you have
            opened and mdstunnel connection on localhost:1600 port
        """

        self.shot = shot
        if not remote:
            self._server = 'tcvdata.epfl.ch'
        else:
            self._server = 'localhost:1600'

        # open the connection and open the shot
        self._connection = mds.Connection(self._server)
        self._connection.openTree('tcv_shot', self.shot)
        self._getmidplane()
        self._getdivertor()
        self._gettmp()
        self._get45()
        self._connection.closeTree

    def _getmidplane(self):
        """
        Get the midplane pressure
        Returns
        -------
        an attribute (midplane) as an xarray dataset with time and values of the pressure
        """
        try:
            if self.shot > 55348:
                _node = r'\base::trch_baratron:channel_001'
            else:
                _node = r'\base::trch_ece_pols:channel_0012'


            self.midplane = xarray.DataArray(-self._connection.get(_node).data() * 0.267,
                                             coords=[self._connection.get('dim_of(' + _node + ')').data()],
                                             dims=['t'])
            self.midplane -= self.midplane.where((self.midplane.t > -6) & (self.midplane.t < -4)).mean(dim='t')
        except BaseException:
            print('midplane baratron channel not found')
            self.midplane=None

    def _gettmp(self):
        """
        Get the turbo-pump pressure
        Returns
        -------
        an attribute (turbopump) as an xarray dataset with time and values of the pressure for both
        the measurements (turbo pump and turbo pump x10)
        """
        try:
            if self.shot >= 63053:
                _node = r'\base::trch_baratron:channel_003'
                _nodex10 = r'\base::trch_baratron:channel_004'
            else:
                _node = None

            self.turbopump = xarray.DataArray(
                np.vstack( (-self._connection.get( _node ).data() * 0.267,
                            -self._connection.get( _nodex10 ).data() * 0.0267) ).transpose(),
                coords=[ self._connection.get( 'dim_of(' + _node + ')' ).data(),
                         np.asarray( [ 'Turbo Pump', 'Turbo Pump x 10' ] ) ],
                dims=[ 't', 'Baratron' ] )
            self.turbopump -= self.turbopump.where( (self.turbopump.t > -6) & (self.turbopump.t < -4) ).mean( dim='t' )
        except BaseException:
            print('turbo-pump baratron channels not found')
            self.turbopump=None

    def _get45(self):
        """
        Get the baratron pressure measured at the 45 angle
        Returns
        -------
        an attribute (p45) as an xarray dataset with time and values of the pressure
        """
        try:
            if self.shot >= 63532:
                _node = r'\base::trch_baratron:channel_005'
            else:
                _node = None


            self.p45 = xarray.DataArray(-self._connection.get(_node).data() * 0.267,
                                             coords=[self._connection.get('dim_of(' + _node + ')').data()],
                                             dims=['t'])
            self.p45 -= self.p45.where((self.p45.t > -6) & (self.p45.t < -4)).mean(dim='t')
        except BaseException:
            print('45 Degree baratron channel not found')
            self.p45=None

    def _getdivertor(self):
        """
        Get the baratron pressure measured at the divertor
        Returns
        -------
        an attribute (divertor) as an xarray dataset with time and values of the pressure
        """
        try:
            if self.shot >= 66788:
                _node = r'\atlas::dt4g_mix_001:channel_049'
            elif 65817 <= self.shot < 66788:
                _node = r'\atlas::dt4g_mix_001:channel_060'
            else:
                _node = r'\diagz::trcf_gaz:channel_012'

            self.divertor = xarray.DataArray(self._connection.get(_node).data() * 0.267,
                                             coords=[self._connection.get('dim_of(' + _node + ')').data()],
                                             dims=['t'])
            self.divertor -= self.divertor.where((self.divertor.t > -6) & (self.divertor.t < -4)).mean(dim='t')
        except BaseException:
            print('Divertor baratron channel not found')
            self.divertor=None

    def getcompression(self):
        """
        Provide estimate of the compression midplane/divertor and return as
        xarray
        Parameters
        ----------
        self

        Returns
        -------

        """

        self.compression = self.divertor/self.midplane.interp(t=self.divertor.t)

    def plotall(self):

        fig,ax = mpl.pylab.subplots(figsize=(7,5), nrows=1,ncols=1)
        fig.subplots_adjust(bottom=0.17,left=0.17,top=0.9)
        self.divertor.plot(color='k', ax=ax)
        self.midplane.plot(color='r', ax=ax)
        self.p45.plot(color='g', ax=ax)
        ax.text(0.05,0.9,'Divertor',color='k',transform=ax.transAxes)
        ax.text(0.05,0.83,'Midplane',color='r',transform=ax.transAxes)
        ax.text(0.05,0.74,'P45',color='g',transform=ax.transAxes)
        ax.set_ylabel(r' [Pa]')
        ax.set_xlabel(r't [s]')
        ax.set_xlim([0, 2.5])
        ax.set_title(r'#'.format(self.shot))