from __future__ import print_function
import warnings
import gpr1dfusion
import profiletools
import numpy as np
import elm_detection
import MDSplus as mds


class tcvProfiles(object):

    def __init__(self, shot, interelm=False):
        """

        Parameters
        ----------
        shot
            Shot number
        """
        self.shot = shot
        self._interelm = interelm
        if self._interelm:
            Tree = mds.Tree('tcv_shot', self.shot)
            _Node = Tree.getNode(r'\base::pd:pd_001')
            self._dalpha = _Node.data()
            self._dalphaT = _Node.getDimensionAt().data()

    def profileNe(self, **kwargs):
        """

        Parameters
        ----------
        **kwargs
            All the keywords inherited from profiletools.TCV.ne or elm_detection

        Returns
        -------
            Return a profile istances from profiletools class
        """

        self.ne = profiletools.TCV.ne(self.shot, **kwargs)
        self.ne.time_average()
        return self.ne

    def profileTe(self, **kwargs):
        """

        Parameters
        ----------
        kwargs
            All the keywords inherited from profiletools.TCV.te
        Returns
        -------
        Return a profile istances from profiletools class
        """
        self.te = profiletools.TCV.Te(self.shot, **kwargs)
        self.te.time_average()
        return self.te

    def gpr_robustfit(self, xnew, kernel = 'GibbsInverseGaussianFGPR1D',
                      density=True, temperature=False, **kwargs):
        """
        Perform a gaussian process regression fit using a combination
        of two kernels (GSE_GL and MATERN_HI)
        Parameters
        ----------
        xnew : new abscissa for the evaluation of the regression
        density : boolean
            If set [default] performs the fit on the density profile
        temperature : boolean
            If set performs the fit on the temperature profile
        kernel : String
            These are the possible kernel used in GPR1DFusion class.
            Possible values are 'GibbsInverseGaussianFGPR1D' (Default)
            'RationalQuadraticFGPR1D' and 'Matern52FGPR1D'
        Returns
        -------
        ynew :
            Interpolated profiles
        err :
            standard deviation
        gpr :
            Instance to gaussian process regression from OMFIT_gptools

        """
        if density:
            x = self.ne.X[:, 0]
            y = self.ne.y
            e = self.ne.err_y
            eX = self.ne.err_X[:, 0]
        elif temperature:
            x = self.te.X[:,0]
            y = self.te.y
            e = self.te.err_y
            eX = self.te.err_X[:, 0]
        else:
            warnings.warn('You must specify density or temperature. Assume temeperature')
            x = self.ne.X[:,0]
            y = self.ne.y
            e = self.ne.err_y

        nrestarts = kwargs.get('nrestarts', 100)
        if kernel == 'GibbsInverseGaussianFGPR1D':
            if np.nanmean(eX) != 0:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    x, y, e,
                    xe=eX, nrestarts=nrestarts, **kwargs)
            else:
                gp = gpr1dfusion.GibbsInverseGaussianFGPR1D(
                    x, y, e, nrestarts=nrestarts, **kwargs)
        elif kernel == 'RationalQuadraticFGPR1D':
            if np.nanmean(eX) != 0:
                gp = gpr1dfusion.RationalQuadraticFGPR1D(
                    x, y, e,
                    xe=eX, nrestarts=nrestarts, **kwargs)
            else:
                gp = gpr1dfusion.RationalQuadraticFGPR1D(
                    x, y, e, nrestarts=nrestarts, **kwargs)
        else:
            if np.nanmean(eX) != 0:
                gp = gpr1dfusion.Matern52FGPR1D(
                    x, y, e,
                    xe=eX, nrestarts=nrestarts, **kwargs)
            else:
                gp = gpr1dfusion.Matern52FGPR1D(
                    x, y, e, nrestarts=nrestarts, **kwargs)
        gp.GPRFit(xnew)
        ynew, yerr, dydr,dydrE = gp.get_gp_results()


        return ynew, yerr, dydr, dydrE
