# pylint: disable=invalid-name, no-member
"""
TCV geometry and vessel properties
"""

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from matplotlib.ticker import MaxNLocator
import MDSplus
import equilibrium
import sys, os
from scipy.io import loadmat


class StaticParameters(object):
    """ TCV static parameters """

    def __init__(self, coordinates):
        self._coordinates = coordinates

    def get_coordinates(self, part, coord_names=None):
        """
        Get the coordinates of a certain vessel part.
        """
        if coord_names is None:
            return self._coordinates[part]

        names = coord_names.split(',')
        names = [str.strip(x) for x in names]

        ret = [self._coordinates[part][key] for key in names]

        if len(ret) == 1:
            ret = ret[0]
        else:
            ret = tuple(ret)
        return ret

    @classmethod
    def from_tree(cls, shotnum):
        """ Read the vessel's parameters from the static MDS nodes """
        Tree = MDSplus.Tree('tcv_shot',shotnum)
        vessel = {}
        tiles = {}
#        with tcv.shot(shotnum) as conn:
        for key, query in cls._vessel_nodes().items():
            vessel[key] = MDSplus.Data.execute(query).getValue().data()
        for key, query in cls._tiles_nodes().items():
            tiles[key] = np.flip(MDSplus.Data.execute(query).getValue().data())
        Tree.quit

        return cls({'vessel': vessel, 'tiles': tiles})

    @staticmethod
    def _tiles_nodes():
        """ Return the static MDS nodes containing the vessel parameters """

        nodes = {
            'R': 'static("r_t")',
            'Z': 'static("z_t")'}
        return nodes


    @staticmethod
    def _vessel_nodes():
        """ Return the static MDS nodes containing the vessel parameters """

        nodes = {
            'R_in': 'static("r_v:in")',
            'R_out': 'static("r_v:out")',
            'Z_in': 'static("z_v:in")',
            'Z_out': 'static("z_v:out")'}
        return nodes


class VesselDrawer(object):
    def __init__(self, coordinates):
        """
        Input
        -----
        coordinates : dict
            Dictionary with the following structure
                'vessel': dict with keys 'R_in', 'Z_in', 'R_out', 'Z_out'
                'tiles': dict with keys 'R', 'Z'
        """
        self.coordinates = coordinates
        self._tiles_shown = False

    @classmethod
    def from_static(cls, shotnum=0):
        static = StaticParameters.from_tree(shotnum)
        coords = {'vessel': static.get_coordinates('vessel'),
                  'tiles': static.get_coordinates('tiles')}
        return cls(coords)

    def render(self, ax=None, tiles=True, droplabel=False):
        ax = ax or plt.gca()
        ax.set_xlim([0.55,1.2])
        ax.set_ylim([-0.9,0.9])
        ax.set_aspect('equal')
        ax.add_patch(self._get_vessel_patch())
        if tiles and self.has_tiles_coordinates():
            ax.add_patch(self._get_tiles_patch())
            self._tiles_shown = True
        if droplabel:
            ax.ticks_params(labelbottom=False, labelleft=False)
    def get_coordinates(self, key):
        return self.coordinates.get(key)

    def has_tiles_coordinates(self):
        return 'tiles' in self.coordinates

    def get_patches(self):
        patches = [self._get_vessel_patch()]
        if self.get_coordinates('tiles') is not None:
            patches.append(self._get_tiles_patch())
        return patches

    def _inside(self):
        coords = self.get_coordinates('vessel')
        vertices = [r for r in zip(coords['R_in'], coords['Z_in'])]
        vertices.append(vertices[0])
        codes = [Path.MOVETO] + (len(vertices)-1) * [Path.LINETO]
        return vertices, codes

    def _outside(self):
        coords = self.get_coordinates('vessel')
        vertices = [r for r in zip(coords['R_out'], coords['Z_out'])][::-1]
        vertices.append(vertices[0])
        codes = [Path.MOVETO] + (len(vertices)-1) * [Path.LINETO]
        return vertices, codes

    def _tiles(self):
        coords = self.get_coordinates('tiles')
        vertices = [r for r in zip(coords['R'], coords['Z'])][::-1]
        vertices.append(vertices[0])
        codes = [Path.MOVETO] + (len(vertices)-1) * [Path.LINETO]
        return vertices, codes

    def _get_vessel_patch(self):
        vertices_in, codes_in = self._inside()
        vertices_out, codes_out = self._outside()
        vessel_path = Path(vertices_in + vertices_out, codes_in + codes_out)
        vessel_patch = PathPatch(vessel_path, facecolor=(0.6, 0.6, 0.6),
                                 edgecolor='black')
        return vessel_patch

    def _get_tiles_patch(self):
        vertices_in, codes_in = self._tiles()
        vertices_out, codes_out = self._inside()
        tiles_path = Path(vertices_in + vertices_out, codes_in + codes_out)
        tiles_patch = PathPatch(tiles_path, facecolor=(0.75, 0.75, 0.75),
                                edgecolor='black')

        return tiles_patch

    def _transparent_inside_patch(self):
        if self._tiles_shown:
            vertices, codes = self._tiles()
        else:
            vertices, codes = self._inside()
        path = Path(vertices, codes)
        return PathPatch(path, alpha=0)

    def clip_collections_inside(self, ax):
        vessel_in = self._transparent_inside_patch()
        ax.add_patch(vessel_in)
        for c in ax.collections:
            c.set_clip_path(vessel_in)


def tcvview(shotnum, time, vessel=True, ports=False,
            col='red', tiles=True, ax=None, savefig=False,
            core_levels=21, sol_levels=7):
    """
    Popular way to display TCV.

    Parameters
    ----------
    shotnum : int, or MDSConnection
        Shot number or an open MDS connection instance.
    time : float
        Time of the equilibrium.
    vessel : bool, optional
        Draw vessel.
    ports : bool, optional
        Draw ports
    tiles : bool, optional
        Draw tiles.
    core_levels : int
        number of contour lines in the core. Default 21
    sol_levels : int
        number of contour lines in the SOL. Default 7


    Example
    -------
    >>> import tcvgeom
    >>> tcvgeom.tcvview(42660, 1)
    """
    if ax is None:
        fig, ax = mpl.pylab.subplots(figsize=(5,7),nrows=1,ncols=1)
        fig.subplots_adjust(bottom=0.12)
    eq = equilibrium.equilibrium(device='TCV', shot=shotnum,time=time, remote=False)
    if ports:
        PortsDrawer().render()

    if vessel:
        vd = VesselDrawer.from_static(shotnum)
        vd.render(ax, tiles)

    r, z = eq.R, eq.Z
    r0, z0 = eq.axis.r, eq.axis.z
    levels_core = np.linspace(0, 1, core_levels)
    levels_sol = np.linspace(1.001, 1.3, sol_levels)

    ax.contour(r, z, eq.psiN(r,z), levels=levels_core, colors=col, linestyles='-')
    ax.contour(r, z, eq.psiN(r,z), levels=levels_sol, colors=col, linestyles='--')
    ax.plot([r0], [z0], 'r+')
    ax.set_aspect('equal')
    ax.set_xlabel(r'R[m]')
    ax.set_ylabel(r'Z[m]')
    ax.autoscale(enable=True,axis='both', tight=True)
    if vessel:
        vd.clip_collections_inside(ax)
    _add_shot_time(shotnum, time, ax)

    ax.xaxis.set_major_locator(MaxNLocator(3))

    if plt.isinteractive():
        ax.figure.canvas.draw()
    if savefig:
        fig.savefig('EquilibriumShot{0}_{1:.2f}.pdf'.format(shotnum,time),bbox_to_inches='tight')

def _add_shot_time(shot, time, ax):
    text = r'$\#%d\ %1.2f\ \mathrm{s}$' % (shot, time)
    ax.text(1.0, 0, text, rotation=270, transform=ax.transAxes,
            va='bottom', ha='left', size='small')


def _add_plasma_parameters(connection, time, ax):
    text = r'$I_\mathrm{p}=999\ \mathrm{kA}$'
    ax.text(0.5, 1.0, text, transform=ax.transAxes,
            va='bottom', ha='center', size='small')


class PortsDrawer(object):
    def get_ports_coordinates(self):
        Hportx = np.array([
            0.04, 0, 0, 0.022, 0.046, 0.245, 0.245, 0.27, 0.27, 0.04
        ])
        Hporty = np.array([
            0.06, 0.06, 0.115, 0.115, 0.08, 0.08, 0.126, 0.126, 0.064, 0.064
        ])

        pm1y = 0.455
        pt1y = pm1y + Hporty
        pb1y = pm1y - Hporty
        p1x = 1.16 + Hportx

        pm2y = -0.0025
        pt2y = pm2y + Hporty
        pb2y = pm2y - Hporty
        p2x = 1.16 + Hportx

        pm3y = -0.46
        pt3y = pm3y + Hporty
        pb3y = pm3y - Hporty
        p3x = 1.16 + Hportx

        Vportx = np.array([
            0.035, 0.035, 0.068, 0.068, 0.054, 0.054, 0.075, 0.075, 0.04, 0.04
        ])
        Vporty = np.array([
            0.045, 0, 0, 0.045, 0.059, 0.069, 0.069, 0.09, 0.09, 0.045
        ])
        Vporty1 = np.array([
            0.045, -0.059157, -0.086867, 0.045, 0.059, 0.069, 0.069, 0.09,
            0.09, 0.045
        ])

        pm4x = 0.715
        pl4x = pm4x - Vportx
        pr4x = pm4x + Vportx
        p4y = 0.77 + Vporty

        pm5x = 0.715
        pl5x = pm5x - Vportx
        pr5x = pm5x + Vportx
        p5y = -0.77 - Vporty

        pm6x = 0.88
        pl6x = pm6x - Vportx
        pr6x = pm6x + Vportx
        pl6y = -0.77 - Vporty
        # pm6y = -0.77 - Vporty
        pr6y = -0.77 - Vporty

        # pm7x = 0.88
        pl7x = pm6x - Vportx
        pr7x = pm6x + Vportx
        pl7y = 0.77 + Vporty
        # pm7y = 0.77 + Vporty
        pr7y = 0.77 + Vporty

        pm8x = 1.045
        pl8x = pm8x - Vportx
        pr8x = pm8x + Vportx
        pl8y = -0.77 - Vporty
        # pm8y = -0.77 - Vporty
        pr8y = -0.77 - Vporty1

        pm9x = 1.045
        pl9x = pm9x - Vportx
        pr9x = pm9x + Vportx
        pl9y = 0.77 + Vporty
        # pm9y = 0.77 + Vporty
        pr9y = 0.77 + Vporty1

        p1 = self._vertices_codes(p1x, pt1y, p1x, pb1y)
        p2 = self._vertices_codes(p2x, pt2y, p2x, pb2y)
        p3 = self._vertices_codes(p3x, pt3y, p3x, pb3y)

        p4 = self._vertices_codes(pl4x, p4y, pr4x, p4y)
        p5 = self._vertices_codes(pl5x, p5y, pr5x, p5y)
        p6 = self._vertices_codes(pl6x, pl6y, pr6x, pr6y)
        p7 = self._vertices_codes(pl7x, pl7y, pr7x, pr7y)
        p8 = self._vertices_codes(pl8x, pl8y, pr8x, pr8y)
        p9 = self._vertices_codes(pl9x, pl9y, pr9x, pr9y)

        return [p1, p2, p3, p4, p5, p6, p7, p8, p9]

    def _vertices_codes(self, x1, y1, x2, y2):
        vertices1 = [r for r in zip(x1, y1)]
        vertices1.append(vertices1[0])
        codes1 = [Path.MOVETO] + (len(vertices1)-1) * [Path.LINETO]
        vertices2 = [r for r in zip(x2, y2)]
        vertices2.append(vertices2[0])
        codes2 = [Path.MOVETO] + (len(vertices2)-1) * [Path.LINETO]

        vertices = vertices1 + vertices2
        codes = codes1 + codes2

        return vertices, codes

    def get_patch(self):
        codes, vertices = [], []
        for p in self.get_ports_coordinates():
            v, c = p
            codes += c
            vertices += v

        path = Path(vertices, codes)
        return PathPatch(path, facecolor=(0.6, 0.6, 0.6), edgecolor='black')

    def render(self, ax=None):
        ax = ax or plt.gca()
        ax.add_patch(self.get_patch())


class BaffleDrawer(object):
    def get_baffle_coordinates(self,kind):
        base = os.path.dirname(os.path.abspath(sys.modules[BaffleDrawer.__module__].__file__))
        _cad = loadmat(os.path.join(base,'baffles_I.3_B.3.mat'))
        if kind == 'lfs':
            # LFS baffles coordinate
            _bx = np.array(_cad['rzo'][0,:])
            _by = np.array(_cad['rzo'][1,:])
        elif kind == 'hfs':
            # HFS baffles coordinate
            _bx = np.array(_cad['rzi'][0,:])
            _by = np.array(_cad['rzi'][1,:])
        elif kind == 'protection':
            # LFS port protection
            _bx = np.array([ 1.1360, 1.1360, 1.08927, 1.060, 1.060, 1.075, 1.136])
            _by = np.array([-0.5427,-0.5499,-0.60517,-0.461,-0.459,-0.384,-0.3489])

        return _bx, _by
    def _vertices_codes(self, x1, y1):
        vertices = [r for r in zip(x1, y1)]
        vertices.append(vertices[0])
        codes = [Path.MOVETO] + (len(vertices)-1) * [Path.LINETO]
        return vertices, codes

    def get_patch(self, kind):
        _bx, _by = self.get_baffle_coordinates(kind)
        vertices, codes = self._vertices_codes(_bx, _by)
        path = Path(vertices, codes)
        return PathPatch(path, facecolor=(0.75, 0.75, 0.75),
                                edgecolor='black')

    def render(self, ax=None, kind='lfs'):
        ax = ax or plt.gca()
        if kind != 'all':
            ax.add_patch(self.get_patch(kind))
        else:
            ax.add_patch(self.get_patch('lfs'))
            ax.add_patch(self.get_patch('hfs'))
            ax.add_patch(self.get_patch('protection'))

