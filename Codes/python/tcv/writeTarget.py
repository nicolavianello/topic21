import MDSplus as mds
import os
from scipy.io import loadmat
import warnings


class Target(object):
    def __init__(self, shot, interelm=True,write=False):
        """

        Parameters
        ----------
        shot : shot number
        interelm : boolean, default is True to write in the interELM
            Tree. Writing in not interELM part of the shot is not
            implemented since written in default MDSplus tree
        write : Boolean, default is False. We load the data and check
            the validity and then write through method. If set to True
            it already write into the Tree
        """
        self.shot = shot
        self.interelm = interelm
        self.write = write
        self._loadmat()
        if self.write and self._found:
            self.toMds()

    def _loadmat(self):
        """
        Read the data saved in mat files and store in appropriate a
        attributes the variabes
        Returns
        -------

        """

        # check the existence of the file
        fname = '/home/vianello/NoTivoli/work/topic21/Experiments/TCV/analysis/data/lp_interelm_{}.mat'.format(self.shot)
        if os.path.isfile(fname):
            Data = loadmat(fname)
            self.te = Data['te'].transpose()
            self.Jsat2 = Data['Jsat2'].transpose()
            self.probes = Data['probes'].ravel()
            self.area = Data['area'].transpose()
            self.ne = Data['dens'].transpose()
            self.rho = Data['rho_psi'].transpose()
            self.LpTime = Data['time'].ravel()
            self.LpTime2 = Data['time2'].ravel()
            self.r,self.z = Data['pos'].transpose()
            self.rho2 = Data['rho_psi2'].transpose()
            self.Lp_drUs = Data['dsep_mid'].transpose()
            self.Lp_drUs2 = Data['dsep_mid2'].transpose()
            self.Pperp  = Data['P_perp'].transpose()
            self.ang = Data['ang'].transpose()
            self.ang2 = Data['ang2'].transpose()
            self._found = True
            self.pos = Data['pos']
        else:
            warnings.warn('Matlab file not found')
            self._found = False

    def toMds(self):
        """
        Write the variables loaded from the matlab file into the tree initally checking
        if tree exists
        Returns
        -------
        None
        """

        # check for the existence of the tree file otherwise create the pulse
        base = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            '../../../Experiments/TCV/data/tree')

        fname = os.path.join(base,'tcv_topic21_{}.data'.format(self.shot))
        if not os.path.isfile(fname):
            Model = mds.Tree('tcv_topic21')
            Model.createPulse(self.shot)
            del Model
        self.saveTree = mds.Tree('tcv_topic21',self.shot)
        # area
        dummy = self.saveTree.getNode(r'\LANGAREA')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.area, self.LpTime,
                             self.probes))
        # densita
        dummy = self.saveTree.getNode(r'\LANGNE')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.ne, self.LpTime,
                             self.probes))
        dummy.setUnits('m^-3')
        # dsep
        dummy = self.saveTree.getNode(r'\LANGDSEP')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.Lp_drUs, self.LpTime,
                             self.probes))
        dummy.setUnits('m')
        # dsep2
        dummy = self.saveTree.getNode(r'\LANGDSEP2')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.Lp_drUs2, self.LpTime2,
                             self.probes))
        dummy.setUnits('m')
        # Jsat2
        dummy = self.saveTree.getNode(r'\LANGJSAT2')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.Jsat2, self.LpTime2,
                             self.probes))
        dummy.setUnits('A/m^2')
        # position
        dummy = self.saveTree.getNode(r'\LANGPOS')
        dummy.putData(self.pos)
        dummy.setUnits('m')
        # probes
        dummy = self.saveTree.getNode(r'\LPROBES')
        dummy.putData(self.probes)
        # P-Perp
        dummy = self.saveTree.getNode(r'\LANGPPERP')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.Pperp, self.LpTime,
                             self.probes))
        # rho
        dummy = self.saveTree.getNode(r'\LANGRHO')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.rho, self.LpTime,
                             self.probes))
        # rho2
        dummy = self.saveTree.getNode(r'\LANGRHO2')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.rho2, self.LpTime2,
                             self.probes))
        # time & time2
        dummy = self.saveTree.getNode(r'\LANGTIME')
        dummy.putData(self.LpTime)
        dummy.setUnits('s')
        dummy = self.saveTree.getNode(r'\LANGTIME2')
        dummy.putData(self.LpTime2)
        dummy.setUnits('s')
        # temperatura
        dummy = self.saveTree.getNode(r'\LANGTE')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.te, self.LpTime,
                             self.probes))
        dummy.setUnits('m^-3')
        # ang
        dummy = self.saveTree.getNode(r'\LANGANG')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.ang, self.LpTime,
                             self.probes))
        # ang
        dummy = self.saveTree.getNode(r'\LANGANG2')
        dummy.putData(
            mds.Data.compile("BUILD_SIGNAL(($VALUE), $1, $2, $3)",
                             self.ang2, self.LpTime2,
                             self.probes))
        # quit
        self.saveTree.quit